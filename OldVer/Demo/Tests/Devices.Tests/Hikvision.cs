﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Squarebit.APS.Devices;
using Squarebit.APS.Devices.HikVision;

namespace Squarebit.APS.Devices.Tests
{
	[TestFixture]
	public class Hikvision
	{
		[Test, ExpectedException(typeof(CameraException))]
		public void Camera_should_intialize_with_incorrect_params()
		{
            var camera = NetworkCamera.OpenCamera("", "", "", "");
            Assert.AreEqual(null, camera);
		}

		[Test]
		public void Camera_should_intialize_with_correct_params()
		{
            var camera = NetworkCamera.OpenCamera("192.168.1.64", "8080", "admin", "12345");
            Assert.AreNotEqual(null, camera);
		}

		[Test]
		public void Camera_should_capture()
		{
            var camera = NetworkCamera.OpenCamera("", "", "", "");
			int frameCount = 0;
			AutoResetEvent frameReceived = new AutoResetEvent(false);

			camera.OnFrameReceived += (o, e) =>
			{
				Interlocked.Add(ref frameCount, 1);
				frameReceived.Set();
			};

			camera.Start();
			frameReceived.WaitOne(1000);
			camera.Stop();

			Assert.AreEqual(1, frameCount);
		}
	}
}
