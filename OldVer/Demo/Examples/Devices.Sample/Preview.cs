﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Squarebit.APS.Devices;
using Squarebit.APS.Devices.HikVision;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Squarebit.APS.Sample
{
    // HOW TO USE DEVICES ASSEMBLY
    // Step 1: Open a CaptureDevice with host, username and password
    // Step 2: Indicate a picturebox (its handle) for that device to display
    // Step 3: Use OnFrameReceived event of camera to get per-frame bitmap
    // Step 4: Start camera
    // Step 5: We can pause / continue / turn off that device

    public partial class Preview : Form
    {
        Dictionary<int, CaptureDevice> _devices;
        List<PictureBox> _pictureBoxes;        

        string[] device1 = { "192.168.1.64", "8080", "admin", "12345" };
        string[] device2 = { "192.168.1.65", "8000", "admin", "12345" };

        object obj = new object();
        PictureBox picBox;
        Bitmap bmp = null;
        Bitmap srcBmp;

        [DllImport("msvcrt.dll", SetLastError = false)]
        unsafe static extern IntPtr memcpy(byte* dest, byte* src, int count);

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
        static extern void MoveMemory(IntPtr dest, IntPtr src, int size);

        public void DrawBitMap()
        {
            lock (obj)
            {
                if (bmp == null)
                    bmp = new Bitmap(srcBmp.Width, srcBmp.Height, srcBmp.PixelFormat);

                BitmapData srcBmpData = srcBmp.LockBits(new Rectangle(0, 0, srcBmp.Width, srcBmp.Height), ImageLockMode.WriteOnly, srcBmp.PixelFormat);
                BitmapData desBmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                IntPtr ptrSrc = srcBmpData.Scan0;
                IntPtr ptrDes = desBmpData.Scan0;

                int bytes = srcBmpData.Stride * bmp.Height;

                byte[] data = new byte[bytes];
                Marshal.Copy(ptrSrc, data, 0, bytes);
                Marshal.Copy(data, 0, ptrDes, bytes);

                bmp.UnlockBits(desBmpData);
                srcBmp.UnlockBits(srcBmpData);

                picBox.Image = bmp;
                Debug.WriteLine("run");
            }
        }

        public Preview()
        {
            InitializeComponent();

            _devices = new Dictionary<int, CaptureDevice>();
            _pictureBoxes = new List<PictureBox>();

            //StartDevice();

            srcBmp = new Bitmap("D:/Cappp.bmp");
            int width = this.Size.Width / 2;
            picBox = CreatePictureBox(width * _pictureBoxes.Count + 10 * (_pictureBoxes.Count + 1), 50);
            _pictureBoxes.Add(picBox);
            this.Controls.Add(picBox);
        }

        /// <summary>
        /// Add and start device
        /// </summary>
        private void StartDevice()
        {
            AddDevice(device1[0], device1[1], device1[2], device1[3]);
            AddDevice(device2[0], device2[1], device2[2], device2[3]);
            foreach (var item in _devices)
            {
                item.Value.Start();
            }                
        }

        /// <summary>
        /// Open a device and register a handle to display it
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        private void AddDevice(string ip, string port, string username, string password)
        {
            CaptureDevice camera = NetworkCamera.OpenCamera(ip, port, username, password);

            if (camera == null)
            {
                Debug.WriteLine(string.Format("Cannot open camera {0}:{1}", ip, port));
                return;
            }

            _devices.Add(camera.DeviceId, camera);

            int width = this.Size.Width / 2;
            PictureBox picBox = CreatePictureBox(width * _pictureBoxes.Count + 10 * (_pictureBoxes.Count + 1), 50);
            _pictureBoxes.Add(picBox);
            this.Controls.Add(picBox);

            // There are 2 ways to display camera content
            // The first one is register a picturebox's handle for that device
            // The second one is use OnFrameReceived event to draw bitmap on the picturebox

            // The first way
            //camera.AddWndHandle(picBox.Handle);

            // The second one
            Bitmap bmp = null;
            camera.OnFrameReceived += (obj, arg) => {
                try
                {
                    // TODO: Get per-frame bitmap here
                    if (bmp == null)
                        bmp = new Bitmap(arg.Frame.Width, arg.Frame.Height, arg.Frame.PixelFormat);
                    
                    BitmapData srcBmpData = arg.Frame.LockBits(new Rectangle(0, 0, arg.Frame.Width, arg.Frame.Height), ImageLockMode.WriteOnly, arg.Frame.PixelFormat);
                    BitmapData desBmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                    IntPtr ptrSrc = srcBmpData.Scan0;
                    IntPtr ptrDes = desBmpData.Scan0;

                    int bytes = srcBmpData.Stride * bmp.Height;

                    byte[] data = new byte[bytes];
                    Marshal.Copy(ptrSrc, data, 0, bytes);
                    Marshal.Copy(data, 0, ptrDes, bytes);

                    bmp.UnlockBits(desBmpData);
                    arg.Frame.UnlockBits(srcBmpData);

                    picBox.Image = bmp;

                }
                catch (Exception ex)
                {
                }
            };

            comboBoxDeviceId.Items.Add(camera.DeviceId);
        }

        private PictureBox CreatePictureBox(int x, int y)
        {
            PictureBox picBox = new PictureBox();
            picBox.Location = new Point(x, y);
            int width = this.Size.Width / 2 - 40;
            picBox.Size = new Size(width, width * 3 / 4);
            return picBox;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            NetworkCamera.Dispose();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Pause specified camera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPause_Click(object sender, EventArgs e)
        {
            int id = (int)comboBoxDeviceId.SelectedItem;

            if (_devices.ContainsKey(id))
                _devices[id].Pause();
        }

        /// <summary>
        /// Continue paused camera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonContinue_Click(object sender, EventArgs e)
        {
            int id = (int)comboBoxDeviceId.SelectedItem;

            if (_devices.ContainsKey(id))
                _devices[id].Continue();
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            new Task(() => {
                while (true)
                {
                    DrawBitMap();
                }
            }).Start(); ;
        }
    }
}
