﻿using System;
using System.Drawing;

/// <summary>
/// 
/// </summary>
namespace Squarebit.APS.Devices
{
	/// <summary>
	/// Describes an event relating to new image frames. 
	/// </summary>
	public class FrameEventArgs
	{
		/// <summary>
		/// Gets or set the image frame.
		/// </summary>
		public Bitmap Frame { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="sender">The sender.</param>
	/// <param name="e">The <see cref="FrameEventArgs"/> instance containing the event data.</param>
	public delegate void FrameEventHandler(object sender, FrameEventArgs e);

	/// <summary>
	/// Describes a capture device.
	/// </summary>
	public abstract class CaptureDevice
	{
        public int DeviceId { get; set; }

		/// <summary>
		/// Occurs when a new frame is received.
		/// </summary>
		public event FrameEventHandler OnFrameReceived;

        protected virtual void FrameReceived(FrameEventArgs arg)
        {
            FrameEventHandler handler = OnFrameReceived;

            if (handler != null)
                handler(this, arg);
        }

		/// <summary>
		/// Starts capturing and sending image data.
		/// </summary>
		public abstract void Start();

		/// <summary>
		/// Pauses the device temporarily.
		/// </summary>
		public abstract void Pause();

        /// <summary>
        /// Continue capturing device
        /// </summary>
        public abstract void Continue();

        /// <summary>
        /// Add window handle to start device
        /// </summary>
        /// <param name="wndHandle"></param>
        public abstract void AddWndHandle(IntPtr wndHandle);

		/// <summary>
		/// Stops the device and closes it. After calling this function, the capture devices
		/// cannot be started again.
		/// </summary>
		public abstract void Stop();
	}
}
