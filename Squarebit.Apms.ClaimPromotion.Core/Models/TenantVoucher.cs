﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    public class TenantVoucher
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("short_value")]
        public string ShortValue { get; set; }
       
        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("updated")]
        public string Updated { get; set; }
    }
}
