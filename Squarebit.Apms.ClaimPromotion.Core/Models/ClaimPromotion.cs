﻿using System.Collections.ObjectModel;
using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    public class SearchClaimPromotionResult
    {
        [JsonProperty("count")]
        public int Total { get; set; }

        [JsonProperty("next")]
        public string Next { get; set; }

        [JsonProperty("previous")]
        public string Previous { get; set; }

        [JsonProperty("results")]
        public List<ClaimPromotion> ClaimPromotions { get; set; }
    }

    [Serializable]
    public class ClaimPromotion : MvxNotifyPropertyChanged
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("card_id")]
        public string CardId
        {
            get { return _cardId; }
            set
            {
                _cardId = value;
                RaisePropertyChanged(() => CardId);
            }
        }

        private int _vehicleTypeId;

        [JsonProperty("vehicle_type")]
        public int VehicleTypeId
        {
            get { return _vehicleTypeId; }
            set
            {
                _vehicleTypeId = value;
                TypeHelper.GetVehicleType(_vehicleTypeId, type => VehicleType = type);
            }
        }

        private VehicleType _vehicleType;
        private string _cardId;
        private string _vehicleNumber;
        private long _serverTime;
        private float _amountA;
        private float _amountB;
        private float _amountC;
        private float _amountD;
        private float _amountE;
        private int _parkingSessionId;
        private int _user_id;

        [JsonProperty("parking_session_id")]
        public int ParkingSessionId
        {
            get { return _parkingSessionId; }
            set
            {
                _parkingSessionId = value;
                RaisePropertyChanged(() => ParkingSessionId);
            }
        }

        [JsonProperty("user_id")]
        public int UserId
        {
            get { return _user_id; }
            set
            {
                _user_id = value;
                RaisePropertyChanged(() => UserId);
            }
        }


        public int BillNumber { get; set; }
        public int NTimes { get; set; }
        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            set
            {
                _vehicleType = value;
                _vehicleTypeId = _vehicleType.Id;
                RaisePropertyChanged(() => VehicleType);
            }
        }

        [JsonProperty("vehicle_number")]
        public string VehicleNumber
        {
            get { return _vehicleNumber; }
            set
            {
                _vehicleNumber = value;
                RaisePropertyChanged(() => VehicleNumber);
            }
        }

        public ParkingSessionEnum ParkingSessionType { get; set; }

        [JsonProperty("server_time")]
        public long ServerTime
        {
            get { return _serverTime; }
            set
            {
                _serverTime = value;
                RaisePropertyChanged(() => ServerTime);
            }
        }

        public string StrServerTime
        {
            get { return TimestampConverter.Timestamp2String(ServerTime); }
        }

        [JsonProperty("amount_a")]
        public float Amount_A
        {
            get { return _amountA; }
            set
            {
                _amountA = value;
                RaisePropertyChanged(() => Amount_A);
            }
        }

        [JsonProperty("amount_b")]
        public float Amount_B
        {
            get { return _amountB; }
            set
            {
                _amountB = value;
                RaisePropertyChanged(() => Amount_B);
            }
        }

        [JsonProperty("amount_c")]
        public float Amount_C
        {
            get { return _amountC; }
            set
            {
                _amountC = value;
                RaisePropertyChanged(() => Amount_C);
            }
        }

        [JsonProperty("amount_d")]
        public float Amount_D
        {
            get { return _amountD; }
            set
            {
                _amountD = value;
                RaisePropertyChanged(() => Amount_D);
            }
        }

        [JsonProperty("amount_e")]
        public float Amount_E
        {
            get { return _amountE; }
            set
            {
                _amountE = value;
                RaisePropertyChanged(() => Amount_E);
            }
        }

        [JsonProperty("promotion_bills")]
        public ObservableCollection<Bill> Bills { get; set; }


        [JsonProperty("promotion_coupons")]
        public ObservableCollection<Coupon> Coupons { get; set; }


        public ClaimPromotion()
        {
            TypeHelper.GetVehicleType((int) VehicleTypeEnum.All, result => VehicleType = result);
        }
    }
}