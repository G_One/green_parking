﻿using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    public class AddTenantMessage : MvxMessage
    {
        public object Sender { get; set; }
        public string TenantName { get; set; }

        public AddTenantMessage(object sender, string name)
            : base(sender)
        {
            Sender = sender;
            TenantName = name;
        }
    }

    public class Bill : MvxNotifyPropertyChanged
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        string _companyName;
        [JsonProperty("company_info")]
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                _companyName = value;
                RaisePropertyChanged(() => CompanyName);
            }
        }

        string _billCode;
        [JsonProperty("bill_number")]
        public string BillCode
        {
            get { return _billCode; }
            set
            {
                _billCode = value;
                RaisePropertyChanged(() => BillCode);
            }
        }

        long _billAmount;
        [JsonProperty("bill_amount")]
        public long BillAmount
        {
            get { return _billAmount; }
            set
            {
                _billAmount = value;
                RaisePropertyChanged(() => BillAmount);
            }
        }

        DateTime _date;
        [JsonProperty("date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }

        public string DateLocal
        {
            get
            {
                return Date == default(DateTime) ? "": Date.ToLocalTime().ToString("dd/MM/yyyy");
            }
        }

        [JsonProperty("dateUtc")]
        public DateTime DateUtc
        {
            get { return TimeZoneInfo.ConvertTimeToUtc(Date, TimeZoneInfo.Local); }
        }

        public Bill()
        {
            Type = "BILL";
        }
    }
}
