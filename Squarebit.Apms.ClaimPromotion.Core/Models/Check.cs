﻿using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    /// <summary>
    /// Check abstract model
    /// </summary>
    public abstract class Check : MvxNotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the identifier code of card
        /// </summary>
        [JsonProperty("card_id")]
        public string CardId { get; set; }

        /// <summary>
        /// Gets or sets the label printed on the card
        /// </summary>
        [JsonIgnore]
        string _cardLabel;
        [JsonProperty("card_label")]
        public string CardLabel
        {
            get { return _cardLabel; }
            set
            {
                _cardLabel = value;
                RaisePropertyChanged(() => CardLabel);
            }
        }

        /// <summary>
        /// Gets or sets the path of front image
        /// </summary>
        [JsonProperty("front_image_path")]
        public string FrontImagePath { get; set; }

        /// <summary>
        /// Gets or sets the path of back image
        /// </summary>
        [JsonProperty("back_image_path")]
        public string BackImagePath { get; set; }

        /// <summary>
        /// Gets or sets the data in bytes of front image
        /// </summary>
        [JsonIgnore]
        private byte[] _frontImage;
        [JsonIgnore]
        public byte[] FrontImage
        {
            get { return _frontImage; }
            set
            {
                _frontImage = value;
                RaisePropertyChanged(() => FrontImage);
            }
        }

        /// <summary>
        /// Gets or sets the data in bytes of back image
        /// </summary>
        [JsonIgnore]
        private byte[] _backImage;
        [JsonIgnore]
        public byte[] BackImage
        {
            get { return _backImage; }
            set
            {
                _backImage = value;
                RaisePropertyChanged(() => BackImage);
            }
        }

        /// <summary>
        /// Gets or sets the license plate number of vehicle provided by ALPR service
        /// </summary>
        private string _AlprVehicleNumber;
        [JsonProperty("alpr_vehicle_number")]
        public string AlprVehicleNumber
        {
            get { return _AlprVehicleNumber; }
            set
            {
                _AlprVehicleNumber = value;
                RaisePropertyChanged(() => AlprVehicleNumber);
            }
        }

        /// <summary>
        /// Gets or sets the verified license plate number of the vehicle
        /// </summary>
        [JsonIgnore]
        private string _vehicleNumber;
        [JsonProperty("vehicle_number")]
        public string VehicleNumber
        {
            get { return _vehicleNumber; }
            set
            {
                _vehicleNumber = value;
                RaisePropertyChanged(() => VehicleNumber);
            }
        }

        [JsonIgnore]
        string _prefixNumberVehicle;
        [JsonIgnore]
        public string PrefixNumberVehicle
        {
            get { return _prefixNumberVehicle; }
            set
            {
                _prefixNumberVehicle = value;
                RaisePropertyChanged(() => PrefixNumberVehicle);
            }
        }

        /// <summary>
        /// Gets or sets the lane id processes this check
        /// </summary>
        [JsonProperty("lane_id")]
        public int LaneId { get; set; }

        /// <summary>
        /// Gets or sets the terminal id processes this check
        /// </summary>
        [JsonProperty("terminal_id")]
        public int TerminalId { get; set; }

        /// <summary>
        /// Gets or sets the operator id processes this check
        /// </summary>
        [JsonProperty("operator_id")]
        public int OperatorId { get; set; }

        [JsonProperty("card_type")]
        public int CardTypeId { get; set; }
    }
}