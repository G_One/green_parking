﻿using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    [Serializable()]
    public class Coupon : MvxNotifyPropertyChanged
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        string _companyName;
        [JsonProperty("company_info")]
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                _companyName = value;
                RaisePropertyChanged(() => CompanyName);
            }
        }

        string _couponCode;
        [JsonProperty("coupon_code")]
        public string CouponCode
        {
            get { return _couponCode; }
            set
            {
                _couponCode = value;
                RaisePropertyChanged(() => CouponCode);
            }
        }

        long _couponDiscount;
        [JsonProperty("coupon_amount")]
        public long CouponDiscount
        {
            get { return _couponDiscount; }
            set
            {
                _couponDiscount = value;
                RaisePropertyChanged(() => CouponDiscount);
            }
        }

        string _voucherName;
        [JsonIgnore]
        public string VoucherName
        {
            get { return _voucherName; }
            set
            {
                _voucherName = value;
                RaisePropertyChanged(() => VoucherName);
            }
        }

        public Coupon()
        {
            Type = "COUPON";
        }
    }
}
