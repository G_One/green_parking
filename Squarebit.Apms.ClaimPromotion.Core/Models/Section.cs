﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    public enum SectionPosition
    {
        Lane1 = 0,
        Lane2,
        Lane3,
        Admin = 999,
    }

    public interface ISection
    {
        IUserService UserService { get; set; }
        SectionPosition Id { get; set; }
        string LaneName { get; set; }
        List<Camera> Cameras { get; set; }
        bool IsConfigured { get; set; }
        CardReaderWrapper CardReader { get; set; }
        //VehicleType VehicleType { get; set; }
        int VehicleTypeId { get; set; }
        IBarrierDevice Barrier { get; set; }
        LaneDirection Direction { get; set; }
        LaneDirection TemporaryDirection { get; set; }
        Camera FrontInCamera { get; }
        Camera FrontOutCamera { get; }
        Camera BackInCamera { get; }
        Camera BackOutCamera { get; }
        //Camera FrontCamera { get; }
        //Camera BackCamera { get; }
        Lane Lane { get; set; }
        void AttachCamera(string ip, LaneDirection direction, CameraPosition cameraPosition);
        void AttachCardReader(string serialNo);
        void SetupDevice(LaneDirection direction);
        void SetupCameras(LaneDirection direction);
        void SetupCardReader();
        bool StopCardReader(CardReaderEventHandler read, CardReaderEventHandler takeoff);
        bool StartCardReader(CardReaderEventHandler read, CardReaderEventHandler takeoff);
        void StartCameras(LaneDirection direction);
        void StopDevices();
        void StopDevices(LaneDirection direction);
        KeyMap KeyMap { get; set; }
    }

    public class Section : MvxNotifyPropertyChanged, ISection
    {
        IUserPreferenceService _userPreferenceService;

        IResourceLocatorService _resourceLocatorService;
        IResourceLocatorService ResourceLocatorService
        {
            get 
            {
                return _resourceLocatorService = _resourceLocatorService ?? Mvx.Resolve<IResourceLocatorService>();
            }
        }

        [JsonIgnore]
        public IUserService UserService { get; set; }

        public string LaneName
        {
            get { return Lane != null ? Lane.Name : null; }
            set
            {
                if (Lane != null)
                    Lane.Name = value;
            }
        }
        public SectionPosition Id { get; set; }
        public bool IsConfigured { get; set; }
        public Lane Lane { get; set; }

        [JsonIgnore]
        private List<Camera> _cameras;
        public List<Camera> Cameras
        {
            get
            {
                if(_cameras == null)
                    _cameras = new List<Camera>();
                return _cameras;
            }
            set
            {
                if (_cameras == value) return;
                _cameras = value;
            }
        }

        public CardReaderWrapper CardReader { get; set; }

        //public VehicleType VehicleType
        //{
        //    get { return Lane != null ? Lane.VehicleType : VehicleType.Bike; }
        //    set
        //    {
        //        if (Lane != null)
        //            Lane.VehicleType = value;
        //    }
        //}

        [JsonIgnore]
        public int VehicleTypeId
        {
            get { return Lane != null ? Lane.VehicleTypeId : 2; }
            set
            {
                if (Lane != null)
                    Lane.VehicleTypeId = value;
            }
        }

		public string BarrierName { get; set; }
        public string BarrierPort { get; set; }

		[JsonIgnore]
		public IBarrierDevice Barrier { get; set; }
        
        [JsonIgnore]
        public LaneDirection Direction
        {
            get { return Lane != null ? Lane.Direction : LaneDirection.In; }
            set
            {
                if (Lane != null)
                {
                    Lane.Direction = value;
                    TemporaryDirection = value;
                }
            }
        }

        private LaneDirection _temporaryDirection;
        [JsonIgnore]
        public LaneDirection TemporaryDirection
        {
            get
            {
                if (_temporaryDirection == LaneDirection.Unknown) return this.Direction;
                else return _temporaryDirection;
            }
            set
            {
                _temporaryDirection = value;
            }
        }

        [JsonIgnore]
        public Camera FrontInCamera
        {
            get { return Cameras.Where(camera => camera.Direction == LaneDirection.In && camera.Position == CameraPosition.Front).FirstOrDefault(); }
        }

        [JsonIgnore]
        public Camera FrontOutCamera
        {
            get { return Cameras.Where(camera => camera.Direction == LaneDirection.Out && camera.Position == CameraPosition.Front).FirstOrDefault(); }
        }

        [JsonIgnore]
        public Camera BackInCamera
        {
            get { return Cameras.Where(camera => camera.Direction == LaneDirection.In && camera.Position == CameraPosition.Back).FirstOrDefault(); }
        }

        [JsonIgnore]
        public Camera BackOutCamera
        {
            get { return Cameras.Where(camera => camera.Direction == LaneDirection.Out && camera.Position == CameraPosition.Back).FirstOrDefault(); }
        }

        public Camera GetFrontCamera(LaneDirection direction)
        {
            return Cameras.Where(camera => camera.Direction == direction && camera.Position == CameraPosition.Front).FirstOrDefault();
        }

        public Camera GetBackCamera(LaneDirection direction)
        {
            return Cameras.Where(camera => camera.Direction == direction && camera.Position == CameraPosition.Back).FirstOrDefault();
        }
        
        [JsonIgnore]
        KeyMap _keyMap;
        public KeyMap KeyMap
        {
            get { return _keyMap=  _keyMap ?? new KeyMap(this.Id); }
            set { _keyMap = value; }
        }

        public Section()
        {
            this.TemporaryDirection = LaneDirection.Unknown;
        }

        public void AttachCamera(string ip, LaneDirection direction, CameraPosition cameraPosition)
        {
            Camera camera = Cameras.Where(c => c.Direction == direction && c.Position == cameraPosition).FirstOrDefault();

            if (camera == null)
            {
                camera = new Camera();
                this.Cameras.Add(camera);
            }

            camera.IP = ip;
            camera.LaneId = this.Lane.Id;
            camera.Position = cameraPosition;
            camera.Direction = direction;
        }

        void camera_OnZoomReceived(object sender, ZoomEventArgs e)
        {
            if (_userPreferenceService == null)
                _userPreferenceService = Mvx.Resolve<IUserPreferenceService>();

            _userPreferenceService.SystemSettings.Save();
            
        }

        public void AttachCardReader(string serialNo)
        {
            if (this.CardReader == null)
                CardReader = new CardReaderWrapper();

            ResourceLocatorService.SetupCardReader(this, serialNo);
            //CardReader.SerialNumber = serialNo;
        }

        public void StartCameras(LaneDirection direction)
        {
            try
            {
                GetFrontCamera(direction).Start();
                GetBackCamera(direction).Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool StopCardReader(CardReaderEventHandler read, CardReaderEventHandler takeoff)
        {
            if (this.CardReader == null)
            {
                Console.WriteLine("dont have card reader");
                return false;
            }

            this.CardReader.ReadingCompleted -= read;
            this.CardReader.TakingOffCompleted -= takeoff;

            return true;
        }

        public bool StartCardReader(CardReaderEventHandler read, CardReaderEventHandler takeoff)
        {
            if (this.CardReader == null)
            {
                Console.WriteLine("dont have card reader");
                return false;
            }

            this.CardReader.ReadingCompleted += read;
            this.CardReader.TakingOffCompleted += takeoff;

            return true;
        }

        public void StopDevices()
        {
            try
            {
                StopDevices(this.Direction);
                //FrontCamera.Stop();
                //BackCamera.Stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void StopDevices(LaneDirection direction)
        {
            try
            {
                GetFrontCamera(direction).Stop();
                GetBackCamera(direction).Stop();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        CameraType _preCameraType;
        public void SetupCameras(LaneDirection direction)
        {
            IOptionsSettings _optionSettings = Mvx.Resolve<IOptionsSettings>();
            Camera frontCamera = GetFrontCamera(direction);
            if (frontCamera != null)
            {
                frontCamera.Setup(_optionSettings.CameraType, _optionSettings.CameraType != _preCameraType);
                frontCamera.OnZoomReceived += camera_OnZoomReceived;
            }

            Camera backCamera = GetBackCamera(direction);
            if (backCamera != null)
            {
                backCamera.Setup(_optionSettings.CameraType, _optionSettings.CameraType != _preCameraType);
                backCamera.OnZoomReceived += camera_OnZoomReceived;
            }
            _preCameraType = _optionSettings.CameraType;
        }

        public void SetupCardReader()
        {
            if (this.CardReader != null)
                ResourceLocatorService.SetupCardReader(this, this.CardReader.SerialNumber);
        }

		public void SetupBarrier()
		{
            //if (!string.IsNullOrEmpty(BarrierName))
            ResourceLocatorService.SetupBarrier(this, BarrierName, BarrierPort);
		}

        public void SetupDevice(LaneDirection direction)
        {
            this.SetupCameras(direction);
            this.SetupCardReader();
			this.SetupBarrier();
        }

        public void SaveStates()
        {
            foreach (var cam in this.Cameras)
                cam.SaveStates();
        }
    }
}
