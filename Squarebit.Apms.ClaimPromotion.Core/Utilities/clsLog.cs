﻿using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.ClaimPromotion.Core.Utilities
{
    public static class clsLog
    {
        //public static int SetNtimes()
        //{
        //    System.Configuration.AppSettingsReader _appReader =
        //        new System.Configuration.AppSettingsReader();
        //    int _NTimes = (int)_appReader.GetValue("NTimes", typeof(int));
        //    string _ToDay = _appReader.GetValue("ToDay", typeof(string)).ToString();

        //    System.Configuration.Configuration config =
        //        System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

        //    DateTime _Date;
        //    DateTime.TryParse(_ToDay.ToString(), out _Date);
        //    if (DateTime.Compare(DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")), DateTime.Parse(_Date.ToString("yyyy/MM/dd"))) != 0)
        //    {
        //        _NTimes = 0;
        //        config.AppSettings.Settings["ToDay"].Value = DateTime.Now.ToString("yyyy/MM/dd 00:00:00");
        //    }

        //    _NTimes++;
        //    config.AppSettings.Settings["NTimes"].Value = _NTimes.ToString();
        //    config.Save(System.Configuration.ConfigurationSaveMode.Minimal, false);
        //    System.Configuration.ConfigurationManager.RefreshSection("appSettings");

        //    return _NTimes;
        //}

        //public static int GetNtimes()
        //{
        //    System.Configuration.AppSettingsReader _appReader =
        //       new System.Configuration.AppSettingsReader();
        //    int _NTimes = (int)_appReader.GetValue("NTimes", typeof(int));

        //    return _NTimes;
        //}

        //public static bool SetBillNumber(DateTime Date, int BillNumber, int ParkingSessionId)
        //{
        //    string _Folder = System.Windows.Forms.Application.StartupPath + "\\BillNumbers\\";
        //    if (!Directory.Exists(_Folder))
        //        Directory.CreateDirectory(_Folder);
        //    string _FileName = _Folder + string.Format("{0}.txt", Date.ToString("yyyy-MM-dd"));
        //    using (System.IO.StreamWriter sw = File.AppendText(_FileName))
        //    {
        //        string _frame = string.Format("{0}|{1}", ParkingSessionId, BillNumber);
        //        sw.WriteLine(_frame);
        //        return true;
        //    }
        //}

        //public static int GetBillNumber(DateTime Date, int ParkingSessionId)
        //{

        //    string _fileName = System.Windows.Forms.Application.StartupPath + string.Format("\\BillNumbers\\{0}.txt", Date.ToString("yyyy-MM-dd"));
        //    if (!System.IO.File.Exists(_fileName))
        //        return 0;

        //    string _cons = string.Format("{0}|", ParkingSessionId);
        //    var _Values = File.ReadAllLines(_fileName)
        //      .Where(line => line.Contains(_cons)).Take(1).ToList();

        //    if (_Values.Count == 0 ||
        //        string.IsNullOrEmpty(_Values[0]))
        //        return 0;
        //    try
        //    {
        //        return int.Parse(_Values[0].Substring(_Values[0].LastIndexOf('|') + 1));
        //    }
        //    catch { }

        //    return 0;

        //}

        public static bool writeObject(DateTime Date, object obj)
        {
            string _Folder = System.Windows.Forms.Application.StartupPath + "\\BillNumbers";
            if (!Directory.Exists(_Folder))
                Directory.CreateDirectory(_Folder);

            string _fullName = string.Format("{0}\\{1}.dat", _Folder, Date.ToString("yyyy-MM-dd"));
            if (File.Exists(_fullName))
                File.Delete(_fullName);

            FileStream _stream = new FileStream(_fullName, FileMode.Create);
            try
            {
                BinaryFormatter _formmater = new BinaryFormatter();

                _formmater.Serialize(_stream, obj);
                _stream.Flush();
                _stream.Close();
                _stream.Dispose();
                return true;
            }
            catch
            {
                _stream.Close();
                return false;
            }
        }
        public static object readObject(DateTime Date)
        {
            string _fileName = System.Windows.Forms.Application.StartupPath +
                string.Format("\\BillNumbers\\{0}.dat", Date.ToString("yyyy-MM-dd"));
            if (!System.IO.File.Exists(_fileName))
                return null;

            FileStream _stream = null;
            try
            {
                _stream = new FileStream(_fileName, FileMode.Open);
                BinaryFormatter _formatter = new BinaryFormatter();
                object obj = _formatter.Deserialize(_stream);
                _stream.Close();
                GC.Collect();
                return obj;
            }
            catch
            {
                if (_stream != null)
                    _stream.Close();
                return null;
            }
            finally
            {
                if (_stream != null)
                    _stream.Close();
                _stream = null;
            }
        }

        public static List<clsLogInfo> readObject(DateTime fromDate, DateTime toDate)
        {
            DateTime currentDay = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day);
            DateTime EndDate = new DateTime(toDate.Year, toDate.Month, toDate.Day);
            FileStream _stream = null;
            string _fileName;
            List<clsLogInfo> _temps = new List<clsLogInfo>();
            while (currentDay <= EndDate)
            {
                _fileName = System.Windows.Forms.Application.StartupPath +
                            string.Format("\\BillNumbers\\{0}.dat", currentDay.ToString("yyyy-MM-dd"));
                if (System.IO.File.Exists(_fileName))
                {
                    object _obj = null;
                    try
                    {
                        _stream = new FileStream(_fileName, FileMode.Open);
                        BinaryFormatter _formatter = new BinaryFormatter();
                        _obj = _formatter.Deserialize(_stream);
                        _stream.Close();
                    }
                    catch
                    {
                        if (_stream != null)
                            _stream.Close();
                    }
                    finally
                    {
                        if (_stream != null)
                            _stream.Close();
                        _stream = null;
                        GC.Collect();
                    }
                    _temps.AddRange(JsonConvert.DeserializeObject<List<clsLogInfo>>(_obj.ToString()));
                }

                currentDay = currentDay.AddDays(1);
            }
            return _temps;
        }

    }

    [Serializable()]
    public class clsLogInfo 
    {
        public clsLogInfo() {
            if (this.ClaimPromotionInfo == null)
                this.ClaimPromotionInfo = new Terminal.Core.Models.ClaimPromotion();
            if (this.CheckInInfo == null)
                this.CheckInInfo = new CheckIn();
        }
        public CheckIn CheckInInfo { get; set; }

        public Squarebit.Apms.Terminal.Core.Models.ClaimPromotion ClaimPromotionInfo { get; set; }

        //public void GetObjectData(SerializationInfo info, StreamingContext context)
        //{
        //    info.AddValue("CheckInInfo", CheckInInfo);
        //   // info.AddValue("ClaimPromotionInfo", ClaimPromotionInfo);
        //}
    }
}
