﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Squarebit.Apms.Terminal.Core.Models;

//@@@ tinh phi service
namespace Squarebit.Apms.Terminal.Core.Services
{
    public class ParkingTimeInfo
    {
        public int TotalNumberOfSegment { get; set; }
        public int TotalNumberOfParkingHour { get; set; }
        public int NumberOfFixedPricingHour { get; set; }

        public ParkingTimeInfo(int parkingHours, int segments, int fixedPricingHours)
        {
            TotalNumberOfParkingHour = parkingHours;
            TotalNumberOfSegment = segments;
            NumberOfFixedPricingHour = fixedPricingHours;
        }
    }

    public class ParkingTimeBlockCalculator
    {
        public int Start { get; set; }
        public int End { get; set; }
        public int FixedPricingHours { get; set; }

        public ParkingTimeBlockCalculator(int start, int end, int fixedPricingHours = 0)
        {
            Start = start;
            End = end;
            FixedPricingHours = fixedPricingHours;
        }

        /// <summary>
        /// Couting parking hour to see how many hour belong to this block time
        /// </summary>
        /// <param name="checkInTime"></param>
        /// <param name="claimTime"></param>
        /// <returns></returns>
        public ParkingTimeInfo Execute(DateTime checkInTime, DateTime claimTime)
        {
            int totalNumberOfParkingHour = 0;
            int totalNumberOfSegment = 0;
            int fixedPricingHours = 0;

            TimeSpan ts = claimTime - new DateTime(checkInTime.Year, checkInTime.Month, checkInTime.Day, checkInTime.Hour, 0, 0);
            int counter = 0;
            int begin = checkInTime.Hour + 1;
            bool flag = false;
                        
            while(counter < ts.TotalHours)
            {
                var tmp = begin;
                if (begin > 24)
                    tmp = begin % 24;

                if (tmp > Start && tmp <= End)
                {
                    if(counter < 3)
                    {
                        fixedPricingHours++;
                    }

                    if (!flag)
                    {
                        flag = true;
                        totalNumberOfSegment++;
                    }
                    totalNumberOfParkingHour++;
                }
                else
                    flag = false;

                begin++;
                counter++;
            }
            
            return new ParkingTimeInfo(totalNumberOfParkingHour, totalNumberOfSegment, fixedPricingHours);
        }
    }
    
    public class SaigonCentreFeeService : IParkingFeeService
    {
        ParkingTimeBlockCalculator _earlyBlockA;
        ParkingTimeBlockCalculator _remainBlockA;
        ParkingTimeBlockCalculator _blockB;

        const int BLOCK_B_BIKE_FEE = 5 * 1000;
        const int BLOCK_B_CAR_FEE = 50 * 1000;
        const int BLOCK_B_CAR_FEE_CLAIM_PROMOTION = 20 * 1000;
        const int BLOCK_B_BILL_THRESHOLD = 700 * 1000;

        const int BLOCK_A_BIKE_EARLY_FEE = 100 * 1000;
        const int BLOCK_A_CAR_EARLY_FEE = 500 * 1000;

        const int BLOCK_A_BIKE_BILL_THRESHOLD = 700 * 1000;
        const int BLOCK_A_CAR_BILL_THRESHOLD = 1500 * 1000;
        const int BLOCK_A_BIKE_FIXED_FEE = 5 * 1000;
        const int BLOCK_A_CAR_FIXED_FEE = 50 * 1000;
        const int BLOCK_A_CAR_FIXED_FEE_CLAIM_PROMOTION = 20 * 1000;

        
        const int BLOCK_A_BIKE_FEE_PER_HOUR = 2 * 1000;
        const int BLOCK_A_BIKE_FEE_PER_HOUR_CLAIM_PROMOTION = 1 * 1000;

        const int BLOCK_A_CAR_FEE_PER_HOUR = 20 * 1000;
        const int BLOCK_A_CAR_FEE_PER_HOUR_INCREASE = 40 * 1000;
        const int BLOCK_A_CAR_THRESHOLD_INCREASE_FEE_PER_HOUR = 3;


        public SaigonCentreFeeService()
        {
            _earlyBlockA = new ParkingTimeBlockCalculator(0, 5);
            _remainBlockA = new ParkingTimeBlockCalculator(5, 24, 3);
            _blockB = new ParkingTimeBlockCalculator(18, 24);
        }

        public float CalculateFee(VehicleTypeEnum type, float billAmount, DateTime checkInTime, DateTime claimTime)
        {
            if (claimTime < checkInTime)
                return 0;

            var t1 = _earlyBlockA.Execute(checkInTime, claimTime);
            var t2 = _remainBlockA.Execute(checkInTime, claimTime);
            var t3 = _blockB.Execute(checkInTime, claimTime);

            if ((claimTime - checkInTime).TotalMinutes <= 15 || ((claimTime - checkInTime).TotalMinutes <= 60 && type == VehicleTypeEnum.Truck))
            {
                return 0;
            }

            if (t2.TotalNumberOfParkingHour == t3.TotalNumberOfParkingHour && t1.TotalNumberOfParkingHour == 0)
            {
                return BlockBParkingFee(billAmount, type);
            }
            else
            {
                return BlockAParkingFee(billAmount, t1, t2, type);
            }
        }

        /// <summary>
        /// Tính phí cho block thời gian A 
        /// </summary>
        /// <param name="billAmount"></param>
        /// <param name="earlyParkingInfo"></param>
        /// <param name="remainInfo"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        float BlockAParkingFee(float billAmount, ParkingTimeInfo earlyParkingInfo, ParkingTimeInfo remainInfo, VehicleTypeEnum type)
        {
            int parkingFine = 0;
            int parkingFee = 0;
            switch (type)
            {
                case VehicleTypeEnum.Bike:
                    parkingFine = earlyParkingInfo.TotalNumberOfSegment * BLOCK_A_BIKE_EARLY_FEE;
                    if (billAmount >= BLOCK_A_BIKE_BILL_THRESHOLD)
                        parkingFee = 0;
                    else
                    {
                        parkingFee = BLOCK_A_BIKE_FIXED_FEE;
                        var pricingIncreaseHours = remainInfo.TotalNumberOfParkingHour - remainInfo.NumberOfFixedPricingHour;
                        pricingIncreaseHours = pricingIncreaseHours > 0 ? pricingIncreaseHours : 0;
                        parkingFee += (billAmount == 0 ? BLOCK_A_BIKE_FEE_PER_HOUR : BLOCK_A_BIKE_FEE_PER_HOUR_CLAIM_PROMOTION) * pricingIncreaseHours;
                    }
                    break;
                case VehicleTypeEnum.Truck:
                case VehicleTypeEnum.Car:
                    parkingFine = earlyParkingInfo.TotalNumberOfSegment * BLOCK_A_CAR_EARLY_FEE;
                    if (billAmount >= BLOCK_A_CAR_BILL_THRESHOLD)
                        parkingFee = 0;
                    else
                    {
                        parkingFee = billAmount == 0 ? BLOCK_A_CAR_FIXED_FEE : BLOCK_A_CAR_FIXED_FEE_CLAIM_PROMOTION;
                        var pricingIncreaseHours = remainInfo.TotalNumberOfParkingHour - remainInfo.NumberOfFixedPricingHour;
                        pricingIncreaseHours = pricingIncreaseHours > 0 ? pricingIncreaseHours : 0;
                        if (pricingIncreaseHours <= BLOCK_A_CAR_THRESHOLD_INCREASE_FEE_PER_HOUR)
                        {
                            parkingFee += pricingIncreaseHours * BLOCK_A_CAR_FEE_PER_HOUR;
                        }
                        else
                        {
                            parkingFee += BLOCK_A_CAR_THRESHOLD_INCREASE_FEE_PER_HOUR * BLOCK_A_CAR_FEE_PER_HOUR 
                                + (pricingIncreaseHours - BLOCK_A_CAR_THRESHOLD_INCREASE_FEE_PER_HOUR) * BLOCK_A_CAR_FEE_PER_HOUR_INCREASE;
                        }
                    }
                    break;
                default:
                    break;
            }
            return parkingFine + parkingFee;
        }

        /// <summary>
        /// Tính phí gửi xe cho block thời gian B
        /// </summary>
        /// <param name="billAmount"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        float BlockBParkingFee(float billAmount, VehicleTypeEnum type)
        {
            switch (type)
            {
                case VehicleTypeEnum.Bike:
                    if (billAmount < BLOCK_B_BILL_THRESHOLD)
                        return BLOCK_B_BIKE_FEE;
                    else
                        return 0;
                case VehicleTypeEnum.Car:
                    if (billAmount == 0)
                        return BLOCK_B_CAR_FEE;
                    else if (billAmount > 0 && billAmount < BLOCK_B_BILL_THRESHOLD)
                        return BLOCK_B_CAR_FEE_CLAIM_PROMOTION;
                    else
                        return 0;
                default:
                    return billAmount;
            }
        }
    }
}