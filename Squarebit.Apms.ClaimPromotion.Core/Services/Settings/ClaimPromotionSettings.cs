﻿using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public class ClaimPromotionSettingsData : BaseSettingsData
    {
    }

    public interface IClaimPromotionSettings : IBaseSettings
    {
    }

    public class ClaimPromotionSettings : BaseSettings<ClaimPromotionSettingsData>, IClaimPromotionSettings
    {
      

        public ClaimPromotionSettings(ArgumentParameter argParams = null)
            : base(argParams)
        {
            if (!HasLocal)
            {
            }
        }

        public override void ForceSave()
        {
            MarkChanged();
            base.ForceSave();
        }
    }
}
