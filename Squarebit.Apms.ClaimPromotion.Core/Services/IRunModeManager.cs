﻿using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public interface IRunModeManager
    {
        ArgumentParameter ArgumentParams { get; set; }
    }

    public class RunModeManager : IRunModeManager
    {
        public ArgumentParameter ArgumentParams { get; set; }
    }
}
