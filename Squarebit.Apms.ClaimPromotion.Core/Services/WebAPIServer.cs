﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Squarebit.Apms.Terminal.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Core;
using System.Net;

namespace Squarebit.Apms.Terminal.Core.Services
{
    /// <summary>
    /// Server using Web API
    /// </summary>
    public class WebAPIServer : IServer
    {
        readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore
        };

        public string DEFAULT_VEHICLE_NUMBER = "????";
        private IWebClient _restClient;

        public WebAPIServer(IWebClient restClient)
        {
            _restClient = restClient;
        }

        private void ExecuteAsync(RestRequest request, Action<IRestResponse> callback)
        {
            _restClient.ExecuteAsync(request, callback);
        }

        public void Login(string username, string password, int laneID, Action<Models.ApmsUser, Exception> complete)
        {
            var request = new RestRequest("/api/users/login/", Method.POST);
            request.AddParameter("username", username);
            request.AddParameter("password", password);
            request.AddParameter("lane_id", laneID);
            ExecuteAsync(request, (response) => OnLoginResult(response, complete));
        }


        //@@@ doi API login
        public void LoginClaimPromotion(string username, string password, int laneID, Action<Models.ApmsUser, Exception> complete)
        {
            var request = new RestRequest("/api/users/login/", Method.POST);
            request.AddParameter("username", username);
            request.AddParameter("password", password);
            request.AddParameter("lane_id", laneID);
            ExecuteAsync(request, (response) => OnLoginResult(response, complete));
        }

        public void Login(string cardID, int laneID, Action<Models.ApmsUser, Exception> complete)
        {
            var request = new RestRequest("/api/users/login-by-card/", Method.POST);
            request.AddParameter("card_id", cardID);
            request.AddParameter("lane_id", laneID);
            ExecuteAsync(request, (response) => OnLoginResult(response, complete));
        }

        public void Logout(int shiftID, int userID, int laneID, Action<UserShift, Exception> complete, int revenue = 0)
        {
            var request = new RestRequest("/api/users/logout/", Method.POST);
            request.AddParameter("shift_id", shiftID);
            request.AddParameter("user_id", userID);
            request.AddParameter("lane_id", laneID);
            request.AddParameter("revenue", revenue);
            ExecuteAsync(request, (response) => OnLogoutResult(response, complete));
        }

        private void OnLoginResult(IRestResponse response, Action<Models.ApmsUser, Exception> complete)
        {
            if (response != null)
            {
                ApmsUser user = null;
                Exception exception = GetLoginException(response);
                if (exception == null)
                {
                    user = JsonConvert.DeserializeObject<ApmsUser>(response.Content, _jsonSerializerSettings);
                }
                if (complete != null)
                    complete(user, exception);
            }
        }

        private void OnLogoutResult(IRestResponse response, Action<Models.UserShift, Exception> complete)
        {
            if (response != null)
            {
                UserShift shift = null;
                Exception exception = GetLoginException(response);
                if (exception == null)
                {
                    shift = JsonConvert.DeserializeObject<UserShift>(response.Content);
                }
                if (complete != null)
                    complete(shift, exception);
            }
        }

        private Exception GetLoginException(IRestResponse response)
        {
            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        return null;
                    case System.Net.HttpStatusCode.BadRequest:
                        if (response.Content.Contains("Login fail"))
                            return new LoginInvalidException(response.Content);
                        else
                            return new ServerErrorException(response.Content);
                    default:
                        if (response.ErrorException != null)
                            return new ServerErrorException(response.ErrorException.Message);
                        else
                            return new ServerErrorException(response.Content);
                }
            }
            else
            {
                return new ServerDisconnectException(response.ErrorMessage);
            }
        }

        private Exception GetException(IRestResponse response)
        {
            if (response != null)
            {
                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    switch (response.StatusCode)
                    {
                        case System.Net.HttpStatusCode.OK:
                        case System.Net.HttpStatusCode.Created:
                            return null;
                        case System.Net.HttpStatusCode.NotFound:
                            return new NotFoundException(response.Content);
                        case System.Net.HttpStatusCode.InternalServerError:
                            if (response.ErrorException != null)
                                return new InternalServerErrorException(response.ErrorException.Message);
                            else
                                return new InternalServerErrorException(response.ToString());
                        default:
                            if (response.ErrorException != null)
                                return new ServerErrorException(response.ErrorException.Message);
                            else
                                return new ServerErrorException(response.Content);
                    }
                }
                else
                {

                    return new ServerDisconnectException(response.ErrorMessage);
                }
            }
            else
            {
                return new ServerDisconnectException("Disconnect");
            }
        }

        public void CreateCheckIn(Models.CheckIn data, Action<Models.CheckIn, Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/cards/{0}/checkin/", data.CardId), Method.POST);
            request.AddParameter("terminal_id", data.TerminalId);
            request.AddParameter("lane_id", data.LaneId);
            request.AddParameter("operator_id", data.OperatorId);
            request.AddParameter("vehicle_type", data.VehicleTypeId);
            //request.AddParameter("vehicle_sub_type", (int)data.VehicleSubType);

            if (string.IsNullOrEmpty(data.VehicleNumber))
                request.AddParameter("vehicle_number", DEFAULT_VEHICLE_NUMBER);
            else
                request.AddParameter("vehicle_number", data.VehicleNumber);

            if (string.IsNullOrEmpty(data.AlprVehicleNumber))
                request.AddParameter("alpr_vehicle_number", DEFAULT_VEHICLE_NUMBER);
            else
                request.AddParameter("alpr_vehicle_number", data.AlprVehicleNumber);

            //request.AddParameter("vehicle_number", data.VehicleNumber);
            //request.AddParameter("alpr_vehicle_number", data.AlprVehicleNumber);
            request.AddFile("front_thumb", data.FrontImage, "front_thumb");
            request.AddFile("back_thumb", data.BackImage, "back_thumb");
            ExecuteAsync(request, (response) => {
                if (complete == null) return;
                CheckIn model = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    model = data;
                    CheckIn resModel = JsonConvert.DeserializeObject<CheckIn>(response.Content);

                    model.CardLabel = resModel.CardLabel;
                    model.FrontImagePath = resModel.FrontImagePath;
                    model.BackImagePath = resModel.BackImagePath;
                    model.VehicleNumberExists = resModel.VehicleNumberExists;
                    model.LimitNumSlots = resModel.LimitNumSlots;
                    model.CurrentNumSlots = resModel.CurrentNumSlots;
                    model.CheckInTimestamp = resModel.CheckInTimestamp;
                    model.CustomerInfo = resModel.CustomerInfo;
                }
                complete(model, exception);

            });
        }

        public void UpdateCheckIn(Models.CheckIn data, Action<Models.CheckIn, Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/cards/{0}/checkin/", data.CardId), Method.PUT);
            //if (data.VehicleType != VehicleType.None)
            request.AddParameter("vehicle_type", data.VehicleTypeId);
            request.AddParameter("terminal_id", data.TerminalId);
            request.AddParameter("lane_id", data.LaneId);
            request.AddParameter("operator_id", data.OperatorId);
            request.AddParameter("vehicle_type", data.VehicleTypeId);

            if (string.IsNullOrEmpty(data.VehicleNumber))
                request.AddParameter("vehicle_number", DEFAULT_VEHICLE_NUMBER);
            else
                request.AddParameter("vehicle_number", data.VehicleNumber);

            if (string.IsNullOrEmpty(data.AlprVehicleNumber))
                request.AddParameter("alpr_vehicle_number", DEFAULT_VEHICLE_NUMBER);
            else
                request.AddParameter("alpr_vehicle_number", data.AlprVehicleNumber);

            if (data.FrontImage != null)
                request.AddFile("front_thumb", data.FrontImage, "front_thumb");
            if (data.BackImage != null)
                request.AddFile("back_thumb", data.BackImage, "back_thumb");
            ExecuteAsync(request, (response) => {
                if (complete == null) return;
                CheckIn model = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    model = data;
                    CheckIn resModel = JsonConvert.DeserializeObject<CheckIn>(response.Content);
                    model.CardLabel = resModel.CardLabel;
                    model.FrontImagePath = resModel.FrontImagePath;
                    model.BackImagePath = resModel.BackImagePath;
                    model.VehicleNumberExists = resModel.VehicleNumberExists;
                    model.LimitNumSlots = resModel.LimitNumSlots;
                    model.CurrentNumSlots = resModel.CurrentNumSlots;
                    model.CheckInTimestamp = resModel.CheckInTimestamp;
                }
                complete(model, exception);
            });
        }

        public void GetCheckIn(string cardID, Action<Models.CheckIn, Exception> complete)
        {
            if (complete == null) return;
            var request = new RestRequest(string.Format("/api/cards/{0}/checkin/", cardID), Method.GET);
            ExecuteAsync(request, (response) => {
                CheckIn model = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    model = JsonConvert.DeserializeObject<CheckIn>(response.Content);
                }
                complete(model, exception);
            });
        }

        public void CreateCheckOut(Models.CheckOut data, Action<Exception> complete)
        {
            try
            {
                var request = new RestRequest(string.Format("/api/cards/{0}/checkout/", data.CardId), Method.POST);
                request.AddParameter("terminal_id", data.TerminalId);
                request.AddParameter("lane_id", data.LaneId);
                request.AddParameter("operator_id", data.OperatorId);
                request.AddParameter("alpr_vehicle_number", string.IsNullOrEmpty(data.AlprVehicleNumber) ? DEFAULT_VEHICLE_NUMBER : data.AlprVehicleNumber);
                request.AddParameter("parking_fee", data.ParkingFee);
                //request.AddParameter("front_image_path", data.FrontImagePath);
                //request.AddParameter("back_image_path", data.BackImagePath);
                request.AddFile("front_thumb", data.FrontImage, "front_thumb");
                request.AddFile("back_thumb", data.BackImage, "back_thumb");
                ExecuteAsync(request, (response) => {
                    if (complete == null) return;
                    complete(GetException(response));
                });
            }
            catch (Exception ex)
            {
                if (complete == null) return;
                complete(ex);
            }
        }

        public void GetTerminals(Action<Models.Terminal[], Exception> complete)
        {
            var request = new RestRequest("/api/terminals/", Method.GET);
            ExecuteAsync(request, (response) => OnTerminalResponse(response, complete));
        }

        public void CreateTerminal(Models.Terminal data, Action<Models.Terminal, Exception> complete)
        {
            var request = new RestRequest("/api/terminals/", Method.POST);
            request.AddParameter("terminal_id", data.TerminalId);
            request.AddParameter("name", data.Name);
            request.AddParameter("status", (int)data.Status);
            ExecuteAsync(request, (response) => OnTerminalResponse(response, complete));
        }

        public void UpdateTerminal(Models.Terminal data, Action<Models.Terminal, Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/terminals/{0}", data.Id), Method.PUT);
            request.AddParameter("name", data.Name);
            request.AddParameter("status", (int)data.Status);
            ExecuteAsync(request, (response) => OnTerminalResponse(response, complete));
        }

        private void OnTerminalResponse(IRestResponse response, Action<Models.Terminal[], Exception> complete)
        {
            if (complete == null) return;
            Models.Terminal[] model = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                model = JsonConvert.DeserializeObject<Models.Terminal[]>(response.Content);
            }
            complete(model, exception);
        }

        private void OnTerminalResponse(IRestResponse response, Action<Models.Terminal, Exception> complete)
        {
            if (complete == null) return;
            Models.Terminal model = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                model = JsonConvert.DeserializeObject<Models.Terminal>(response.Content);
            }
            complete(model, exception);
        }

        public void CreateLane(Models.Lane data, Action<Models.Lane, Exception> complete)
        {
            var request = new RestRequest("/api/lanes/", Method.POST);
            request.AddParameter("name", data.Name);
            request.AddParameter("vehicle_type", data.VehicleTypeId);
            request.AddParameter("direction", (int)data.Direction);
            request.AddParameter("enabled", data.Enabled);
            request.AddParameter("terminal_id", data.TerminalId);
            ExecuteAsync(request, (response) => OnLaneResponse(response, complete));
        }

        public void UpdateLane(Models.Lane data, Action<Models.Lane, Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/lanes/{0}", data.Id), Method.PUT);
            request.AddParameter("name", data.Name);
            request.AddParameter("vehicle_type", data.VehicleTypeId);
            request.AddParameter("direction", (int)data.Direction);
            request.AddParameter("enabled", data.Enabled);
            request.AddParameter("terminal_id", data.TerminalId);
            ExecuteAsync(request, (response) => OnLaneResponse(response, complete));
        }

        private void OnLaneResponse(IRestResponse response, Action<Lane, Exception> complete)
        {
            if (complete == null) return;
            Lane model = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                model = JsonConvert.DeserializeObject<Lane>(response.Content);
            }
            complete(model, exception);
        }

        public void CreateLane(Models.Lane[] data, Action<Models.Lane[], Exception> complete)
        {
            var request = new RestRequest("/api/lanes/", Method.POST);
            Dictionary<string, object>[] request_lst = new Dictionary<string, object>[data.Length];
            int idx = 0;
            foreach (Models.Lane lane in data)
            {
                if (lane == null) continue;
                Dictionary<string, object> request_obj = new Dictionary<string, object>();
                request_obj["name"] = lane.Name;
                request_obj["vehicle_type"] = lane.VehicleTypeId;
                request_obj["direction"] = (int)lane.Direction;
                request_obj["enabled"] = lane.Enabled;
                request_obj["terminal_id"] = lane.TerminalId;
                request_lst[idx] = request_obj;
                idx++;
            }
            request.AddParameter("bulk", JsonConvert.SerializeObject(request_lst));
            ExecuteAsync(request, (response) => OnLanesResponse(response, complete));
        }

        public void GetLanes(Action<List<Lane>, Exception> complete)
        {
            var request = new RestRequest("/api/lanes/", Method.GET);
            ExecuteAsync(request, response => {
                List<Lane> rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<List<Lane>>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        private void OnLanesResponse(IRestResponse response, Action<Lane[], Exception> complete)
        {
            if (complete == null) return;
            Lane[] rs = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                rs = JsonConvert.DeserializeObject<Lane[]>(response.Content);
            }
            complete(rs, exception);
        }

        public void CreateCamera(Models.Camera data, Action<Models.Camera, Exception> complete)
        {
            var request = new RestRequest("/api/cameras/", Method.POST);
            request.AddParameter("name", data.Name);
            request.AddParameter("ip", data.IP);
            request.AddParameter("direction", (int)data.Direction);
            request.AddParameter("position", (int)data.Position);
            request.AddParameter("serial_number", data.SerialNumber);
            request.AddParameter("lane_id", data.LaneId);
            ExecuteAsync(request, (response) => OnCameraResponse(response, complete));
        }

        public void UpdateCamera(Models.Camera data, Action<Models.Camera, Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/cameras/{0}", data.Id), Method.PUT);
            request.AddParameter("name", data.Name);
            request.AddParameter("ip", data.IP);
            request.AddParameter("direction", (int)data.Direction);
            request.AddParameter("position", (int)data.Position);
            request.AddParameter("serial_number", data.SerialNumber);
            request.AddParameter("lane_id", data.LaneId);
            ExecuteAsync(request, (response) => OnCameraResponse(response, complete));
        }

        private void OnCameraResponse(IRestResponse response, Action<Camera, Exception> complete)
        {
            if (complete == null) return;
            Camera model = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                model = JsonConvert.DeserializeObject<Camera>(response.Content);
            }
            complete(model, exception);
        }

        public void CheckHealthServer(string ip, Action<Exception> complete)
        {
            if (complete == null) return;
            var request = new RestRequest("/api/health/", Method.GET);
            _restClient.ExecuteAsync(ip, request, (response) => OnCheckHealthResponse(response, complete));
        }

        private void OnCheckHealthResponse(IRestResponse response, Action<Exception> complete)
        {
            complete(GetException(response));
        }

        public void ParkingSessionSearchBasic(string cardId, Action<CheckIn[], Exception> complete)
        {
            if (complete == null) return;
            var request = new RestRequest("/api/parking-sessions/", Method.GET);
            request.AddParameter("card_id", cardId);
            ExecuteAsync(request, (response) => OnSearchResponse(response, complete));
        }

        public void ParkingSessionSearchAdvance(ParkingSession data, ParkingSessionEnum mode, Action<ParkingSession[], Exception> complete, int limit = 100)
        {
            var request = new RestRequest("/api/parking-sessions/", Method.GET);
            request.AddParameter("from_time", TimestampConverter.DateTime2Timestamp(data.StartDate));
            request.AddParameter("to_time", TimestampConverter.DateTime2Timestamp(data.EndDate));
            //if (limit > 0)
            request.AddParameter("limit", limit);
            if (!string.IsNullOrEmpty(data.CardLabel))
                request.AddParameter("card_label", data.CardLabel.Trim());
            if (!string.IsNullOrEmpty(data.CardId))
                request.AddParameter("card_id", data.CardId);
            if (!string.IsNullOrEmpty(data.VehicleNumber))
                request.AddParameter("vehicle_number", data.VehicleNumber.Trim());

            request.AddParameter("mode", (int)mode);
            request.AddParameter("vehicle_type", data.VehicleType.Id);

            ExecuteAsync(request, (response) => OnSearchResponse(response, complete));
        }

        public void ParkingSessionSearch(ParkingSession data, ParkingSessionEnum mode, int page, int pageSize, Action<SearchResult, Exception> complete)
        {
            var request = new RestRequest("/api/parking-sessions/search/", Method.GET);
            request.AddParameter("from_time", TimestampConverter.DateTime2Timestamp(data.StartDate));
            request.AddParameter("to_time", TimestampConverter.DateTime2Timestamp(data.EndDate));
            if (!string.IsNullOrEmpty(data.CardLabel))
                request.AddParameter("card_label", data.CardLabel.Trim());
            if (!string.IsNullOrEmpty(data.CardId))
                request.AddParameter("card_id", data.CardId);
            if (!string.IsNullOrEmpty(data.VehicleNumber))
                request.AddParameter("vehicle_number", data.VehicleNumber.Trim());

            request.AddParameter("page", page);
            request.AddParameter("page_size", pageSize);
            request.AddParameter("mode", (int)mode);
            //if (data.TerminalGroup != null)
            //    request.AddParameter("terminal_group", data.TerminalGroup.Id);
            request.AddParameter("vehicle_type", data.VehicleType.Id);

            ExecuteAsync(request, (response) => OnSearchResponse(response, complete));
        }

        private void OnSearchResponse(IRestResponse response, Action<SearchResult, Exception> complete)
        {
            SearchResult rs = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                rs = JsonConvert.DeserializeObject<SearchResult>(response.Content);
            }
            complete(rs, exception);
        }

        private void OnSearchResponse(IRestResponse response, Action<ParkingSession[], Exception> complete)
        {
            ParkingSession[] rs = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                rs = JsonConvert.DeserializeObject<ParkingSession[]>(response.Content);
            }
            complete(rs, exception);
        }

        //public void ParkingSessionSearchAdvance(Action<CheckIn[], Exception> complete, DateTime fromTime, DateTime toTime, int limit = 0, string cardId = null, string cardLabel = null, string vehicleNumber = null, int vehicleTypeId = 0, VehicleSubType vehicleSubType = VehicleSubType.None)
        //{
        //    if (complete == null) return;
        //    var request = new RestRequest("/api/parking-sessions/", Method.GET);
        //    request.AddParameter("from_time", TimestampConverter.DateTime2Timestamp(fromTime));
        //    request.AddParameter("to_time", TimestampConverter.DateTime2Timestamp(toTime));
        //    if (limit > 0)
        //        request.AddParameter("limit", limit);
        //    if (cardLabel != null)
        //        request.AddParameter("card_label", cardLabel);
        //    if (cardId != null)
        //        request.AddParameter("card_id", cardId);
        //    if (vehicleNumber != null)
        //        request.AddParameter("vehicle_number", vehicleNumber);
        //    //if (vehicleType != VehicleType.None)
        //    request.AddParameter("vehicle_type", vehicleTypeId);
        //    if (vehicleSubType != VehicleSubType.None)
        //        request.AddParameter("vehicle_sub_type", (int)vehicleSubType);
        //    ExecuteAsync(request, (response) => OnSearchResponse(response, complete));
        //}

        private void OnSearchResponse(IRestResponse response, Action<CheckIn[], Exception> complete)
        {
            CheckIn[] rs = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                rs = JsonConvert.DeserializeObject<CheckIn[]>(response.Content);
            }
            complete(rs, exception);
        }

        public void GetGlobalConfig(int terminalId, string version, Action<GlobalConfig, Exception> complete)
        {
            if (complete == null) return;
            var request = new RestRequest("/api/global-config/", Method.GET);
            request.AddParameter("terminal_id", terminalId);
            request.AddParameter("version", version);
            ExecuteAsync(request, (response) => OnGetGlobalConfigReceived(response, complete));
        }

        private void OnGetGlobalConfigReceived(IRestResponse response, Action<GlobalConfig, Exception> complete)
        {
            GlobalConfig rs = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                rs = JsonConvert.DeserializeObject<GlobalConfig>(response.Content);
            }
            complete(rs, exception);
        }

        public void GetCardInfo(string cardId, Action<Card, Exception> complete)
        {
            if (complete == null) return;
            var request = new RestRequest(string.Format("/api/cards/{0}", cardId), Method.GET);
            ExecuteAsync(request, (response) => OnCardInfoReceived(response, complete));
        }

        private void OnCardInfoReceived(IRestResponse response, Action<Card, Exception> complete)
        {
            Card rs = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                rs = JsonConvert.DeserializeObject<Card>(response.Content);
            }
            complete(rs, exception);
        }

        public void ReplicateImages(Models.CheckIn checkInInfo, Action<Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/image-replication/"), Method.POST);
            request.AddParameter("card_id", checkInInfo.CardId);
            request.AddParameter("front_image", checkInInfo.FrontImagePath);
            request.AddParameter("back_image", checkInInfo.BackImagePath);
            ExecuteAsync(request, (response) => {
                if (complete != null) complete(GetException(response));
            });
        }

        public void CreateCards(Models.Card[] data, Action<Models.BulkCreateCardResult, Exception> complete)
        {
            List<Dictionary<string, object>> request_lst = new List<Dictionary<string, object>>();
            foreach (Models.Card item in data)
            {
                if (item == null) continue;
                Dictionary<string, object> request_obj = new Dictionary<string, object>();
                request_obj["card_id"] = item.Id;
                request_obj["card_label"] = item.Label.Trim();
                request_obj["status"] = item.Status;
                request_obj["vehicle_type"] = item.VehicleTypeId;
                request_obj["card_type"] = item.CardType.Id;
                request_lst.Add(request_obj);
            }
            var request = new RestRequest(string.Format("/api/cards/"), Method.POST);
            request.AddParameter("bulk", JsonConvert.SerializeObject(request_lst));
            ExecuteAsync(request, (response) => {
                Models.BulkCreateCardResult rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<BulkCreateCardResult>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void GetCards(Action<string, Exception> complete)
        {
            var request = new RestRequest("/api/cards/", Method.GET);
            ExecuteAsync(request, response => {
                if (complete != null) complete(response.Content, GetException(response));
            });
        }

        public void UpdateParkingSession(ParkingSession parkingSession, Action<Exception> complete)
        {
            var request = new RestRequest(string.Format("/api/parking-sessions/{0}/", parkingSession.Id), Method.PUT);
            request.AddParameter("vehicle_number", parkingSession.VehicleNumber);
            ExecuteAsync(request, (response) => {
                if (complete != null) complete(GetException(response));
            });
        }

        public void CreateExceptionalCheckOut(string cardId, int terminalId, int laneId, int operatorId, string notes, bool isLocked, Action<Exception> complete)
        {
            //POST 
            var request = new RestRequest(string.Format("/api/cards/{0}/exception-checkout/", cardId), Method.POST);
            request.AddParameter("terminal_id", terminalId);
            request.AddParameter("lane_id", laneId);
            request.AddParameter("operator_id", operatorId);
            request.AddParameter("notes", notes);
            request.AddParameter("lock_card", isLocked ? 1 : 0);

            ExecuteAsync(request, (response) => {

                if (complete != null) complete(GetException(response));
            });
        }

        public void GetCardTypes(Action<List<CardType>, Exception> complete)
        {
            var request = new RestRequest("/api/card-types/", Method.GET);
            _restClient.ExecuteAsync(request, (response) => {

                List<CardType> rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<List<CardType>>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void GetVehicleTypes(Action<List<VehicleType>, Exception> complete)
        {
            var request = new RestRequest("/api/vehicle-types/", Method.GET);
            _restClient.ExecuteAsync(request, (response) => {

                List<VehicleType> rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<List<VehicleType>>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void CrawlPage(string host, string endpoint, Action<IRestResponse, Exception> callback)
        {
            var request = new RestRequest(endpoint, Method.GET);
            _restClient.ExecuteAsync(host, request, response => {
                //var exception = GetException(response);
                if (callback != null)
                    callback(response, null);
            });
        }

        public void GetStatistics(DateTime from, DateTime to, int terminalId, Action<Statistics, Exception> complete)
        {
            var request = new RestRequest("/api/statistics/", Method.GET);
            request.AddParameter("time_from", TimestampConverter.DateTime2Timestamp(from));
            request.AddParameter("time_to", TimestampConverter.DateTime2Timestamp(to));
            request.AddParameter("terminal_id", terminalId);
            _restClient.ExecuteAsync(request, (response) => {

                Statistics rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<Statistics>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void GetTerminalGroups(Action<Models.TerminalGroup[], Exception> complete)
        {
            var request = new RestRequest("/api/terminal-groups/", Method.GET);
            ExecuteAsync(request, (response) => OnTerminalGroupResponse(response, complete));
        }

        private void OnTerminalGroupResponse(IRestResponse response, Action<Models.TerminalGroup[], Exception> complete)
        {
            if (complete == null) return;
            Models.TerminalGroup[] model = null;
            Exception exception = GetException(response);
            if (exception == null)
            {
                model = JsonConvert.DeserializeObject<Models.TerminalGroup[]>(response.Content);
            }
            complete(model, exception);
        }

        public void UpdateClaimPromotion(int userid, string billNo, float amount, int parkingSessionId,
                            string cardId, string vehicleNumber, int vehicleType, float parkingFee, float promotionDiscount, 
                        float vipDiscount, float tenantDiscount, float finalParkingFee, string data, Action<Exception> complete)
        {
            var request = new RestRequest("/api/claim-promotion/", Method.POST);
            request.AddParameter("user_id", userid);
            //request.AddParameter("card_id", cardId);
            request.AddParameter("parking_session_id", parkingSessionId);
            //request.AddParameter("vehicle_number", vehicleNumber);
            //request.AddParameter("vehicle_type", vehicleType);
            request.AddParameter("bill_number", billNo);
            request.AddParameter("bill_amount", amount);
            request.AddParameter("amount_a", parkingFee);
            request.AddParameter("amount_b", promotionDiscount);
            request.AddParameter("amount_c", vipDiscount);
            request.AddParameter("amount_d", tenantDiscount);
            request.AddParameter("amount_e", finalParkingFee);
            request.AddParameter("data", data);

            _restClient.ExecuteAsync(request, (response) => {
                Exception exception = GetException(response);
                if (complete != null) complete(exception);
            });
        }

        public void ClaimPromotionSearch(string vehicleNumber, int vehicleType, DateTime fromTime, DateTime toTime, int page, int pageSize, Action<SearchClaimPromotionResult, Exception> complete)
        {
            var request = new RestRequest("/api/claim-promotion/search/", Method.GET);
            request.AddParameter("from_time", TimestampConverter.DateTime2Timestamp(fromTime));
            request.AddParameter("to_time", TimestampConverter.DateTime2Timestamp(toTime));
            if (!string.IsNullOrEmpty(vehicleNumber))
                request.AddParameter("vehicle_number", vehicleNumber.Trim());

            request.AddParameter("page", page);
            request.AddParameter("page_size", pageSize);
            if (vehicleType != 0)
            {
                request.AddParameter("vehicle_type", vehicleType);    
            }
            
            _restClient.ExecuteAsync(request, (response) =>
            {
                SearchClaimPromotionResult rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<SearchClaimPromotionResult>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void GetServerTime(Action<ServerTimeInfo, Exception> complete)
        {
            var request = new RestRequest("/api/time-info/", Method.GET);
            _restClient.ExecuteAsync(request, (response) => {

                ServerTimeInfo rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<ServerTimeInfo>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void GetTenants(Action<List<Tenant>, Exception> complete)
        {
            var request = new RestRequest("/api/claim-promotion/tenants", Method.GET);
            
            _restClient.ExecuteAsync(request, (response) => {

                List<Tenant> rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<List<Tenant>>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }

        public void GetTenantVourchers(Action<List<TenantVoucher>, Exception> complete)
        {
            var request = new RestRequest("/api/claim-promotion/vouchers", Method.GET);

            _restClient.ExecuteAsync(request, (response) => {

                List<TenantVoucher> rs = null;
                Exception exception = GetException(response);
                if (exception == null)
                {
                    rs = JsonConvert.DeserializeObject<List<TenantVoucher>>(response.Content);
                }
                if (complete != null) complete(rs, exception);
            });
        }
    }
}