﻿using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public interface ICardReaderService
    {
        /// <summary>
        /// Get card reader by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CardReaderWrapper GetCardReader(string id);

        List<CardReaderWrapper> GetCardReaders();
    }

    public class CardReaderService : ICardReaderService
    {
        IRFIDCardReaderService _cardReaderService;

        Dictionary<string, CardReaderWrapper> _cardReaders;

        public CardReaderService(IRFIDCardReaderService rfidService)
        {
            _cardReaderService = rfidService;
            _cardReaderService.Run();

            InitCardReaders();
        }

        private void InitCardReaders()
        {
            _cardReaders = new Dictionary<string, CardReaderWrapper>();
            Dictionary<string, ICardReader> dic = _cardReaderService.Devices;
            foreach(var item in dic)
            {
                _cardReaders.Add(item.Key, new CardReaderWrapper { RawCardReader = item.Value, SerialNumber = item.Key });
            }
        }

        public CardReaderWrapper GetCardReader(string serialNumber)
        {
			if (string.IsNullOrEmpty(serialNumber))
				return null;
            if (_cardReaders.ContainsKey(serialNumber))
                return _cardReaders[serialNumber];
            else
                return null;
            //ICardReader cardReader = _cardReaderService.GetCardReader(serialNumber);
            //return new CardReader() { RawCardReader = cardReader, SerialNumber = serialNumber };
        }

        public List<CardReaderWrapper> GetCardReaders()
        {
            return _cardReaders.Values.ToList();
        }
    }
}
