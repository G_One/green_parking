﻿using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public interface IParkingFeeService
    {
        //float CalculatePromotion(VehicleTypeEnum type, float billAmount, DateTime checkInTime, DateTime claimTime);

        //float CalculateParkingFee(VehicleTypeEnum type, DateTime checkInTime, DateTime checkoutTime);

        float CalculateFee(VehicleTypeEnum type, float billAmount, DateTime checkInTime, DateTime claimTime);

        float CalculatePromotion(VehicleTypeEnum type, float billAmount, DateTime checkInTime, DateTime claimTime);
    }
}