﻿//using Cirrious.CrossCore;
//using LicensePlateRecognition;
//using System;
//using System.Collections.Concurrent;
//using System.Threading.Tasks;

//namespace Squarebit.Apms.Terminal.Core.Services
//{
//    public class ALPRService : IALPRService
//    {
//        class TaskItem
//        {
//            private byte[] _imageData = null;
//            private Action<string, Exception> _complete = null;


//            public byte[] ImageData
//            {
//                get
//                {
//                    return _imageData;
//                }
//                set
//                {
//                    _imageData = value;
//                }
//            }

//            public Action<string, Exception> Complete
//            {
//                get
//                {
//                    return _complete;
//                }
//                set
//                {
//                    _complete = value;
//                }
//            }
//        }

//        IUserPreferenceService _userpreferenceService;
//        private bool _stop = false;
//        public const int NUMWORKER = 2;
//        private BlockingCollection<TaskItem> _tasks = new BlockingCollection<TaskItem>();

//        public ALPRService()
//        {
//            _userpreferenceService = Mvx.Resolve<IUserPreferenceService>();

//            for (int i = 0; i < NUMWORKER; i++)
//            {
//                Action action = DoProcess;
//                Task.Run(action);
//            }
//        }

//        public void RecognizeLicensePlate(byte[] image, Action<string, Exception> complete)
//        {
//            //string rs = worker.LicensePlateRecognize(image);
//            //complete(rs, null);
//            //complete("54F1-3333", null);
//            if (_userpreferenceService.OptionsSettings.PlateRecognitionEnale)
//                _tasks.Add(new TaskItem { ImageData = image, Complete = complete });
//            else
//                complete("????", null);
//            //tasks.CompleteAdding();
//        }

//        private void DoProcess()
//        {
//            IALPRWorker worker = new ALPRWorker();
//            while (!_stop)
//            {
//                foreach (TaskItem task in _tasks.GetConsumingEnumerable())
//                {
//                    //task.complete("3333", null);
//                    //return;
//                    Exception exception = null;
//                    string rs = null;
//                    try
//                    {
//                        rs = worker.LicensePlateRecognize(task.ImageData);
//                    }
//                    catch (Exception ex)
//                    {
//                        rs = "????";
//                        exception = ex;
//                    }
//                    try
//                    {
//                        task.Complete(rs, exception);
//                    }
//                    catch (Exception ex)
//                    {
//                        Console.WriteLine(ex);
//                    }
//                }
//            }
//        }
//    }
//}
