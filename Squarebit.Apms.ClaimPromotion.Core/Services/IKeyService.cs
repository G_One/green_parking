﻿using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

//@@@ shortcut key
namespace Squarebit.Apms.Terminal.Core.Services
{
    public class KeyMap
    {
        [JsonProperty("key_map")]
        public Dictionary<KeyAction, string> KeysMap { get; private set; }

        //public KeyMapDictionary Dataaa { get; set; }

        [JsonProperty("lane_position")]
        public SectionPosition LanePosition { get; private set; }

        public KeyMap()
        {
            KeysMap = new Dictionary<KeyAction, string>();

            KeysMap.Add(KeyAction.GoBack, Key.Escape.ToString());
            KeysMap.Add(KeyAction.GoNext, Key.RightCtrl.ToString());
            KeysMap.Add(KeyAction.ResetForm, Key.F12.ToString());
            KeysMap.Add(KeyAction.AlternativeChoice, Key.LeftCtrl.ToString());
            /*** 07-08-2016 ***/
            KeysMap.Add(KeyAction.PrintBill, Key.F3.ToString());
        }

        public KeyMap(SectionPosition pos)
            : this()
        {
            LanePosition = pos;

            // init if there is no configuration file

        }

        public void AddItem(KeyAction keyAction, string keyboard, bool isUnique = false)
        {
            if (KeysMap.ContainsValue(keyboard))
            {
                var k = KeysMap.FirstOrDefault(x => x.Value == keyboard).Key;
                KeysMap[keyAction] = string.Empty;
            }

            if (KeysMap.ContainsKey(keyAction))
                KeysMap[keyAction] = keyboard;
            else
                KeysMap.Add(keyAction, keyboard);
        }

        public string GetKey(KeyAction action)
        {
            if (KeysMap.ContainsKey(action))
                return KeysMap[action];
            else
                return string.Empty;
        }

        public static string ConvertToNumericKey(Key key)
        {
            if (key >= Key.D0 && key <= Key.D9)
            {
                int number = (int)key - (int)Key.D0;
                return number.ToString();
            }
            else if (key >= Key.NumPad0 && key <= Key.NumPad9)
            {
                int number = (int)key - (int)Key.NumPad0;
                return number.ToString();
            }
            else return string.Empty;
        }

        public KeyAction GetAction(KeyEventArgs keyArgs, out string output, Type type = null)
        {
            Key key = keyArgs.Key;
            output = string.Empty;

            if (key >= Key.D0 && key <= Key.D9)
            {
                int number = (int)key - (int)Key.D0;
                output = number.ToString();
                return KeyAction.Number;
            }
            else if (key >= Key.NumPad0 && key <= Key.NumPad9)
            {
                int number = (int)key - (int)Key.NumPad0;
                output = number.ToString();
                return KeyAction.Number;
            }

            else
            {
                string strKey = KeyUtil.ConvertToString(keyArgs);
                return this.KeysMap.FirstOrDefault(x => x.Value == strKey).Key;
            }
        }
    }

    public enum KeyAction
    {
        DoNothing = 0,
        GoNext,
        GoBack,
        ResetForm,
        AlternativeChoice,



        Number,
        CheckOut,
        CancelCheckOut,
        UpdateCheckOut,
        Delete,
        Search,
        ChangeLane,
        Back,
        Logout,
        ExceptionalCheckout,
        Configuration,
        DoSearch,
        ShowVehicleType,

        PrintBill
    }

    public class KeyResponse
    {
        public KeyAction KeyAdvice { get; set; }
        public string Output { get; set; }
    }
}
