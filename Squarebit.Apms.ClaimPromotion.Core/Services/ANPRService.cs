﻿using Cirrious.CrossCore;
using SilverSea.Sockets;
using Squarebit.Apms.Terminal.Core.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public enum ConnectStatus
    {
        Init = 0,
        Connecting = 1,
        Connected = 2,
        Fail = 3,
    }

    public class ANPRService : IALPRService
    {
        private const string DATALENGTHFORMAT = "00000000";
        private const string DEFAULT_VEHICLE_NUMBER = "";

        IUserPreferenceService _userpreferenceService;
        private SocketClient _socketClient;

        private ConcurrentDictionary<string, Action<string, Exception>> _dict = new ConcurrentDictionary<string, Action<string, Exception>>();
        public string ServerIP { get; set; }

        public int ServerPort { get; set; }

        public static ConnectStatus Status { get; set; }

        private Task _worker = null;

        // start socket client
        private void StartSocketClient()
        {
            _socketClient = new SocketClient();
            if (_socketClient == null)
                return;

            _socketClient.ServerIP = ServerIP;
            _socketClient.ServerPort = ServerPort;
            _socketClient.DataReceivedInStringEvent += new DataReceivedInStringEventHandler(socketClient_DataReceivedEvent);
            _socketClient.SocketErrorEvent += new SocketErrorEventHandler(socketClient_SocketErrorEvent);
            _socketClient.ServerConnected += _socketClient_ServerConnected;
            Status = ConnectStatus.Connecting;
            _socketClient.Connect();
        }

        // send message to server
        private void SendMessageToServer(string message)
        {
            if (_socketClient == null || !_socketClient.IsRunning)
                return;
            _socketClient.SendMessage(message);
        }

        private void SendMessageToServer(byte[] buffer)
        {
            if (_socketClient == null || !_socketClient.IsRunning)
                return;
            _socketClient.SendMessage(buffer);
        }

        public ANPRService()
        {
            _userpreferenceService = Mvx.Resolve<IUserPreferenceService>();
            ServerIP = "127.0.0.1";
            ServerPort = 50000;
            if (_worker == null)
            {
                _worker = Task.Factory.StartNew(() => {
                    WorkerDoWork();
                });
            }
        }

        private void WorkerDoWork()
        {
            StartSocketClient();
        }

        private void _socketClient_ServerConnected(string serverIP, int serverPort)
        {
            Status = ConnectStatus.Connected;
        }

        // socket data event
        private void socketClient_DataReceivedEvent(string receivedString)
        {
            Exception exception = null;
            string vehicleNumber = DEFAULT_VEHICLE_NUMBER;
            string key = string.Empty;
            string receivedMessage = receivedString;

            try
            {
                receivedMessage = receivedMessage.Remove(receivedMessage.Length - 1)
                                                 .Remove(0, 1)
                                                 .Replace("RP20", "");
                int length = int.Parse(receivedMessage.Substring(0, DATALENGTHFORMAT.Length));
                string content = receivedMessage.Substring(DATALENGTHFORMAT.Length, length);
                vehicleNumber = content.Substring(0, content.IndexOf(";"));
                //vehicleNumber = "60-C1-3336";
                string path = receivedMessage.Substring(receivedMessage.IndexOf(';') + 1);
                string oldPath = path.Substring(0, path.LastIndexOf('.'));
                key = Path.GetFileNameWithoutExtension(oldPath);

                if (string.IsNullOrEmpty(vehicleNumber))
                    vehicleNumber = DEFAULT_VEHICLE_NUMBER;

                File.Delete(path);
                File.Delete(oldPath);
            }
            catch (Exception ex)
            {
                exception = (Exception)Activator.CreateInstance(ex.GetType(), ex.Message + "\n Data: " + receivedString, ex);
                Mvx.Resolve<ILogService>().Log(exception, _userpreferenceService.HostSettings.LogServerIP);
            }
            finally
            {
                if (_dict.ContainsKey(key))
                {
                    var tValue = _dict[key];
                    if (tValue != null)
                        tValue(vehicleNumber, exception);

                    Action<string, Exception> action = null;
                    _dict.TryRemove(key, out action);
                }
            }
        }

        // socket error event
        private void socketClient_SocketErrorEvent(string errorString)
        {
            Status = ConnectStatus.Fail;
        }

        // stop socket client
        private void StopSocketClient()
        {
            if (_socketClient == null)
                return;
            _socketClient.DataReceivedInStringEvent -= new DataReceivedInStringEventHandler(socketClient_DataReceivedEvent);
            _socketClient.SocketErrorEvent -= new SocketErrorEventHandler(socketClient_SocketErrorEvent);

            _socketClient.Disconnect();
        }

        public void RecognizeLicensePlate(byte[] image, Action<string, Exception> complete)
        {
            if (_userpreferenceService.OptionsSettings.PlateRecognitionEnale)
            {
                switch (Status)
                {
                    case ConnectStatus.Init:
                        complete(DEFAULT_VEHICLE_NUMBER, new Exception("anpr.init_fail"));
                        return;
                    case ConnectStatus.Connecting:
                        complete(DEFAULT_VEHICLE_NUMBER, new Exception("anpr.connecting"));
                        return;
                    case ConnectStatus.Connected:
                        break;
                    case ConnectStatus.Fail:
                        complete(DEFAULT_VEHICLE_NUMBER, new Exception("anpr.cannot_connect"));
                        return;
                    default:
                        break;
                }

                var key = Guid.NewGuid();
                string dir = System.AppDomain.CurrentDomain.BaseDirectory + "Temp";
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                string imagePath = string.Format(dir + @"\{0}.jpg", key);
                _dict.TryAdd(key.ToString(), complete);
                File.WriteAllBytes(imagePath, image);
                string sendMessage = string.Format("%CM20{0}{1}$", imagePath.Length.ToString(DATALENGTHFORMAT), imagePath);
                SendMessageToServer(sendMessage);
            }
            else
            {
                complete(DEFAULT_VEHICLE_NUMBER, null);
            }
        }

        public static string ExtractVehicleNumber(string rawNumber)
        {
            if (string.IsNullOrEmpty(rawNumber)) return DEFAULT_VEHICLE_NUMBER;
            string result = OtherUtilities.GetLastGroupNumber(rawNumber);
            if (string.IsNullOrEmpty(result))
                return DEFAULT_VEHICLE_NUMBER;
            return result;
        }

        public static string ExtractPrefixVehicleNumber(string rawNumber, string vehicleNumber)
        {
            string prefix = DEFAULT_VEHICLE_NUMBER;

            if (!string.IsNullOrEmpty(rawNumber) && !string.IsNullOrEmpty(vehicleNumber))
                prefix = rawNumber.Replace(vehicleNumber, "");

            prefix = OtherUtilities.RemoveLastNonDigitWordChar(prefix);
            //if (!string.IsNullOrEmpty(lastChar) && !string.IsNullOrEmpty(prefix))
            //    prefix = prefix.Replace(lastChar, "");

            return prefix;
        }
    }
}