﻿using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.MvvmCross.Localization;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Devices.Dal;
using System.Windows.Input;
using Squarebit.Devices.Vivotek;
using Cirrious.CrossCore;
using System.Windows.Media;
using Squarebit.Apms.Terminal.Core.Models;
using System.Threading;
using NLog;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class KeyPressedMessage : MvxMessage
    {
        public object Sender { get; set; }
        public KeyEventArgs KeyEventArgs { get; set; }
        public KeyPressedMessage(object sender, KeyEventArgs args)
            : base(sender)
        {
            Sender = sender;
            KeyEventArgs = args;
        }
    }

    public class CloseChildMessage : MvxMessage
    {
        public SectionPosition SectionId { get; set; }

        public CloseChildMessage(object sender, SectionPosition pos)
            : base(sender)
        {
            SectionId = pos;
        }
    }

    public class ClosePopupMessage : MvxMessage
    {
        public ClosePopupMessage(object sender)
            : base(sender)
        {

        }
    }

    public class ShowChildMessage : MvxMessage
    {
        public SectionPosition SectionId { get; set; }

        public Type ChildTypeViewModel { get; set; }

        public object Params { get; set; }

        //public ShowChildMessage(object sender, SectionPosition pos, Type type)
        //    : base(sender)
        //{
        //    SectionId = pos;
        //    ChildTypeViewModel = type;
        //}
        public ShowChildMessage(object sender, SectionPosition pos, Type type, object param = null)
            : base(sender)
        {
            SectionId = pos;
            ChildTypeViewModel = type;
            Params = param;
        }
    }

    public class ChangeLaneMessage : MvxMessage
    {
        public SectionPosition SectionId { get; set; }

        public ChangeLaneMessage(object sender, SectionPosition pos)
            : base(sender)
        {
            SectionId = pos;
        }
    }

    public class BaseLaneViewModel : BaseViewModel
    {
        protected ITestingService _testingService;

        IMvxMessenger _messenger;

        //protected IKeyService _keyService;

        protected IRunModeManager _modeManager;

        protected IStorageService _storageService;

        protected ILogService _logService;

        protected IResourceLocatorService _resourceService;

        protected IALPRService _alprService;

        protected IServer _server;

        protected IUserPreferenceService _userPreferenceService;

        protected IUserServiceLocator _userServiceLocator;

        protected IUserService _userService;

        protected MvxSubscriptionToken _keyPressedToken;

        protected MvxSubscriptionToken _closePopupToken;

        protected bool _notEditPlateNumberYet = true;

        protected LaneDirection _laneDirection;

        #region Properties
        private ApmsUser _user;
        public ApmsUser User
        {
            get { return _user; }
            set { _user = value; RaisePropertyChanged(() => UserNameAndId); }
        }

        public string UserNameAndId
        {
            get
            {
                if (_user != null)
                    return _user.DisplayName + " - " + _user.StaffID;
                else
                    return null;
            }
        }

        private Card _checkedCard;
        public Card CheckedCard
        {
            get { return _checkedCard; }
            set
            {
                if (_checkedCard == value)
                    return;

                _checkedCard = value;
                RaisePropertyChanged(() => CheckedCard);
            }
        }

        private DateTime _checkInTime;
        public DateTime CheckInTime
        {
            get { return _checkInTime; }
            set
            {
                if (_checkInTime == value) return;

                _checkInTime = value;
                RaisePropertyChanged(() => CheckInTime);
            }
        }

        public LaneDirection Direction
        {
            get { return this.Section.Lane.Direction; }
            set
            {
                if (Section.Lane.Direction == value)
                    return;

                Section.Lane.Direction = value;

                RaisePropertyChanged(() => Direction);
            }
        }

        bool _showChooseVehicleType;
        public bool ShowChooseVehicleType
        {
            get { return _showChooseVehicleType; }
            set
            {
                _showChooseVehicleType = value;
                RaisePropertyChanged(() => ShowChooseVehicleType);
            }
        }

        PreCheckoutInfo _customerInfo;
        public PreCheckoutInfo CustomerInfo
        {
            get { return _customerInfo; }
            set
            {
                if (_customerInfo == value) return;
                _customerInfo = value;
                RaisePropertyChanged(() => CustomerInfo);
            }
        }

        protected bool CheckCustomerInfoValid()
        {
            if (CustomerInfo == null || CustomerInfo.VehicleRegistrationInfo == null)
                return false;
            return true;
        }
        #endregion
        
        public void SetupDevices()
        {
            this.Section.SetupDevice(_laneDirection);
        }

        public BaseLaneViewModel(IViewModelServiceLocator service, IStorageService storageService, IMvxMessenger messenger)
            : base(service)
        {
            _modeManager = service.ModeManager;
            _storageService = storageService;
            _messenger = messenger;
            
            _testingService = Mvx.Resolve<ITestingService>();
            _resourceService = Mvx.Resolve<IResourceLocatorService>();
            _logService = Mvx.Resolve<ILogService>();
            _server = Mvx.Resolve<IServer>();
            _userPreferenceService = Mvx.Resolve<IUserPreferenceService>();
            _alprService = Mvx.Resolve<IALPRService>();
            _userServiceLocator = Mvx.Resolve<IUserServiceLocator>();
            //_keyService = Mvx.Resolve<IKeyService>();

            _keyPressedToken = service.Messenger.Subscribe<KeyPressedMessage>(OnKeyPressed);
            _closePopupToken = service.Messenger.Subscribe<ClosePopupMessage>(OnClosePopup);
        }

        public virtual void Init(ParameterKey key)
        {
            this.Section = (ISection)Services.Parameter.Retrieve(key);
            _userService = _userServiceLocator.GetUserService(this.Section.Id);

            this.User = _userService.CurrentUser;
            _laneDirection = Section.TemporaryDirection;
            //if (this is CheckInLaneViewModel)
            //    _laneDirection = LaneDirection.In;
            //else
            //    _laneDirection = LaneDirection.Out;

            SetupDevices();
        }

        public override void Start()
        {
            base.Start();
            
            this.Section.StartCardReader(ReadingCompleted, TakingOffCompleted);
            this.Section.StartCameras(_laneDirection);
        }

        public virtual void TakingOffCompleted(object sender, CardReaderEventArgs e)
        {
            if (e.ex != null) 
                return;
        }

        public virtual void ReadingCompleted(object sender, CardReaderEventArgs e)
        {
            //MessageToUser = null;

            if (_messenger.HasSubscriptionsFor<ClosePopupMessage>())
            {
                _messenger.Publish(new ClosePopupMessage(this));
            }

            if (e.ex != null) 
                return;

            this.CheckedCard = new Card(e.CardID);
        }

        public void PublishCloseChildEvent(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<CloseChildMessage>())
            {
                _messenger.Publish(new CloseChildMessage(this, position));
            }
        }

        public void PublishChangeLaneEvent(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
            {
                LaneDirection dir = _laneDirection == LaneDirection.In ? LaneDirection.Out : LaneDirection.In;
                Section.TemporaryDirection = dir;
                //_laneDirection = dir;
                //_userPreferenceService.SystemSettings.ChangeLaneDirection(Section.Id, dir);
                //_userPreferenceService.SystemSettings.Save();

                _messenger.Publish(new ShowChildMessage(this, position, typeof(BaseLaneViewModel)));
            }
        }

        public void PublishShowEndingShiftView(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
            {
                _messenger.Publish(new ShowChildMessage(this, position, typeof(EndingShiftInformationViewModel)));
            }
        }

        public void PublishShowSearchEvent(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
            {
                //_messenger.Publish(new ShowChildMessage(this, position, typeof(SearchViewModel)));
            }
        }

        protected string ExtractVehicleNumber(string rawNumber)
        {
            return ANPRService.ExtractVehicleNumber(rawNumber);
        }

        protected string ExtractPrefixVehicleNumber(string rawNumber, string vehicleNumber)
        {
            return ANPRService.ExtractPrefixVehicleNumber(rawNumber, vehicleNumber);
        }
        
        protected virtual void OnKeyPressed(KeyPressedMessage msg)
        {
            
        }

        public virtual void ChooseVehicleType(VehicleType type)
        {
            
        }

        protected virtual void OnClosePopup(ClosePopupMessage msg)
        {
            this.MessageToUser = null;
        }

        public void HandleError(string icon, string err, bool showUp, bool isAppend)
        {
            InvokeOnMainThread(() => {
                if (!isAppend)
                    this.Notices.Clear();

                this.Notices.Add(new NoticeToUser(icon, err));
                if (showUp)
                {
                    if (this.Notices.Where(n => n.Icon.Equals(IconEnums.Guide)).FirstOrDefault() != null)
                        this.Notices.TimeOut = INFINITIVE;
                    else if (this.Notices.TimeOut <= DEFAULT_NOTICE_TIMEOUT)
                        this.Notices.TimeOut = DEFAULT_NOTICE_TIMEOUT;
                    else
                        this.Notices.TimeOut = DEFAULT_NOTICE_TIMEOUT;
                }

                RaisePropertyChanged(() => Notices);
            });
        }

        public void HandleError(string icon, string err, bool isAppend)
        {
            InvokeOnMainThread(() => {
                if (isAppend)
                {
                    this.Notices.Add(new NoticeToUser(icon, err));
                }
                else
                {
                    this.Notices.Add(new NoticeToUser(icon, err));

                    if (this.Notices.Where(n => n.Icon.Equals(IconEnums.Guide)).FirstOrDefault() != null)
                    {
                        this.Notices.TimeOut = INFINITIVE;
                    }
                    else if (this.Notices.TimeOut <= DEFAULT_NOTICE_TIMEOUT)
                        this.Notices.TimeOut = DEFAULT_NOTICE_TIMEOUT;
                    else
                        this.Notices.TimeOut = DEFAULT_NOTICE_TIMEOUT;

                    RaisePropertyChanged(() => Notices);
                }
            });
        }

        public void ReleaseResource()
        {
            this.Section.StopCardReader(ReadingCompleted, TakingOffCompleted);

            //if (this is CheckInLaneViewModel)
            if (_laneDirection == LaneDirection.In)
                this.Section.StopDevices(LaneDirection.In);
            //else if(this is CheckOutLaneViewModel)
            else if (_laneDirection == LaneDirection.Out)
                this.Section.StopDevices(LaneDirection.Out);
        }

        public override void Unloaded()
        {
            base.Unloaded();
            ReleaseResource();

            Mvx.Resolve<IMvxMessenger>().Unsubscribe<KeyPressedMessage>(_keyPressedToken);
            _keyPressedToken = null;
        }

        public override void Close()
        {
            base.Close();
            _showChooseVehicleType = false;
        }

        private MvxCommand _changeLaneDirectionCommand;
        public ICommand ChangeLaneDirectionCommand
        {
            get
            {
                _changeLaneDirectionCommand = _changeLaneDirectionCommand ?? new MvxCommand(() => {
                    PublishCloseChildEvent(this.Section.Id);
                    PublishChangeLaneEvent(this.Section.Id);
                    Close();
                });

                return _changeLaneDirectionCommand;
            }
        }

        private MvxCommand _showShiftReportCommand;
        public ICommand ShowShiftReportCommand
        {
            get
            {
                _showShiftReportCommand = _showShiftReportCommand ?? new MvxCommand(() => {
                    //this.Section.ApmsUser = null;
                    PublishCloseChildEvent(this.Section.Id);
                    PublishShowEndingShiftView(this.Section.Id);
                });

                return _showShiftReportCommand;
            }
        }

        private MvxCommand _confirmLogoutCommand;
        public ICommand ConfirmLogoutCommand
        {
            get
            {
                _confirmLogoutCommand = _confirmLogoutCommand ?? new MvxCommand(() => {
                    ShowConfirmLogout();
                });
                return _confirmLogoutCommand;
            }
        }

        MvxCommand _showSearchCommand;
        public ICommand ShowSearchCommand
        {
            get
            {
                _showSearchCommand = _showSearchCommand ?? new MvxCommand(() => {
                    PublishCloseChildEvent(this.Section.Id);
                    PublishShowSearchEvent(this.Section.Id);
                });
                return _showSearchCommand;
            }
        }

        #region Test Properties and Method
        protected void GetMockCardIds()
        {

        }

        protected void GetARandomCardIds()
        {

        }
        #endregion

        protected void PrintLog<T>(Exception exception, string logServer = null, bool captureScreen = false) where T : class
        {
            _logService.Log(exception, logServer, null, 0, null, captureScreen);
        }

        public void ShowConfirmLogout()
        {
            this.MessageToUser = new MessageToUser(GetText("logout.title"), GetText("logout.message"), GetText("logout.ok"), () => { ShowShiftReportCommand.Execute(null); return true; }, GetText("logout.cancel"), () => false);
        }
    }
}