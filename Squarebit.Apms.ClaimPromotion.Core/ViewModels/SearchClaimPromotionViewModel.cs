﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Devices.Dal;
using Squarebit.Apms.ClaimPromotion.Core.Utilities;
using Squarebit.Apms.Terminal.Core;
using Newtonsoft.Json;

namespace Squarebit.Apms.ClaimPromotion.Core.ViewModels
{
    public class SearchClaimPromotionViewModel : BaseViewModel
    {
        IServer _server;
        IStorageService _storageService;
        MvxSubscriptionToken _keyPressedToken;
        IMvxMessenger _messenger;
        IUserService _userService;

        #region Properties
        private List<string> _numberOfResult;
        public List<string> NumberOfResult
        {
            get { return _numberOfResult; }
            set
            {
                _numberOfResult = value;
                RaisePropertyChanged(() => NumberOfResult);
            }
        }

        private string _selectedNumberOfResult;
        public string SelectedNumberOfResult
        {
            get { return _selectedNumberOfResult; }
            set
            {
                _selectedNumberOfResult = value;
                RaisePropertyChanged(() => SelectedNumberOfResult);
            }
        }

        private List<int> _pageNumbers;
        public List<int> PageNumbers
        {
            get { return _pageNumbers; }
            set
            {
                if (_pageNumbers == value) return;
                _pageNumbers = value;
                RaisePropertyChanged(() => PageNumbers);
            }
        }

        private int _selectedPage = 1;
        public int SelectedPage
        {
            get { return _selectedPage; }
            set
            {
                if (_selectedPage == value) return;
                _selectedPage = value;
                RaisePropertyChanged(() => SelectedPage);
            }
        }

        List<VehicleType> _vehicleTypes;
        public List<VehicleType> VehicleTypes
        {
            get { return _vehicleTypes; }
            set
            {
                _vehicleTypes = value;
                RaisePropertyChanged(() => VehicleTypes);
            }
        }

        private string _vehicleNumber;
        public string VehicleNumber
        {
            get { return _vehicleNumber; }
            set
            {
                _vehicleNumber = value;
                RaisePropertyChanged(() => VehicleNumber);
            }
        }

        private VehicleType _vehicleType;
        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            set
            {
                _vehicleType = value;
                RaisePropertyChanged(() => VehicleType);
            }
        }

        private DateTime _fromTime;
        public DateTime FromTime
        {
            get { return _fromTime; }
            set
            {
                _fromTime = value;
                RaisePropertyChanged(() => FromTime);
            }
        }

        private DateTime _toTime;
        public DateTime ToTime
        {
            get { return _toTime; }
            set
            {
                _toTime = value;
                RaisePropertyChanged(() => ToTime);
            }
        }

        ParkingSession _parkingSession;
        public ParkingSession ParkingSession
        {
            get { return _parkingSession; }
            set
            {
                if (_parkingSession == value)
                    return;
                _parkingSession = value;
                RaisePropertyChanged(() => ParkingSession);
            }
        }


        ObservableCollection<Terminal.Core.Models.ClaimPromotion> _claimPromotion;
        public ObservableCollection<Terminal.Core.Models.ClaimPromotion> ClaimPromotions
        {
            get { return _claimPromotion; }
            set
            {
                if (_claimPromotion == value) return;
                _claimPromotion = value;
                RaisePropertyChanged(() => ClaimPromotions);
            }
        }

        private byte[] _frontImage;
        public byte[] FrontImage
        {
            get { return _frontImage; }
            set
            {
                _frontImage = value;
                RaisePropertyChanged(() => FrontImage);
            }
        }

        private byte[] _backImage;
        public byte[] BackImage
        {
            get { return _backImage; }
            set
            {
                _backImage = value;
                RaisePropertyChanged(() => BackImage);
            }
        }

        private byte[] _miniFrontImage;
        public byte[] MiniFrontImage
        {
            get { return _miniFrontImage; }
            set
            {
                _miniFrontImage = value;
                RaisePropertyChanged(() => MiniFrontImage);
            }
        }

        private byte[] _miniBackImage;
        public byte[] MiniBackImage
        {
            get { return _miniBackImage; }
            set
            {
                _miniBackImage = value;
                RaisePropertyChanged(() => MiniBackImage);
            }
        }

        string _resultMessage;
        public string ResultMessage
        {
            get { return _resultMessage; }
            set
            {
                _resultMessage = value;
                RaisePropertyChanged(() => ResultMessage);
            }
        }

        private int _messageLevel;
        public int MessageLevel
        {
            get { return _messageLevel; }
            set
            {
                if (_messageLevel == value) return;
                _messageLevel = value;
                RaisePropertyChanged(() => MessageLevel);
            }
        }
        public int UserId { get; set; }

        private bool _isCheckedCurrentUser;
        public bool IsCheckedCurrentUser
        {
            get { return _isCheckedCurrentUser; }
            set
            {
                _isCheckedCurrentUser = value;
                RaisePropertyChanged(() => IsCheckedCurrentUser);
            }
        }

        private float _Amount_E;
        public float Amount_E
        {
            get { return _Amount_E; }
            set
            {
                this._Amount_E = value;
                RaisePropertyChanged(() => Amount_E);
            }
        }

        private bool _IsCounter;
        public bool IsCounter
        {
            get { return _IsCounter; }
            set{
                this._IsCounter=value;
                RaisePropertyChanged(() => IsCounter);
            }
        }
        #endregion

        public SearchClaimPromotionViewModel(IViewModelServiceLocator service, IMvxMessenger messenger)
            : base(service)
        {
            _server = Mvx.Resolve<IServer>();
            _storageService = Mvx.Resolve<IStorageService>();
            _messenger = messenger;
            _keyPressedToken = service.Messenger.Subscribe<KeyPressedMessage>(OnKeyPressed);
           
        }

        public void Init(ParameterKey key)
        {
            //this.Section = (Section)Services.Parameter.Retrieve(key);
            FromTime = DateTime.Now;
            ToTime = DateTime.Now;

            IUserServiceLocator userServiceLocator = Mvx.Resolve<IUserServiceLocator>();
            _userService = userServiceLocator.GetUserService(AppConfig.ClaimPromotionSection.Id);
           // Username = _userService.CurrentUser.DisplayName;
            UserId = _userService.CurrentUser.Id;
        }

        public override void Start()
        {
            base.Start();
            NumberOfResult = new List<string> { "50", "100"};
            SelectedNumberOfResult = NumberOfResult[1];
            
            TypeHelper.GetVehicleTypes(result => {
                if(result != null)
                {
                    result.Reverse();
                    VehicleTypes = result;
                    TypeHelper.GetVehicleType((int)VehicleTypeEnum.All, type => VehicleType = type);
                }
            });
        }

        SearchClaimPromotionResult _currentResult = null;
        //int _currentPage = 1;
        int _maxPage = 1;
        List<clsLogInfo> _LogBillClaim;
        public void DoSearch(Action complete)
        {
            int pageSize = 2;
            if (!int.TryParse(SelectedNumberOfResult, out pageSize)) pageSize = 2;
            //if (ParkingSession.IsCurrentUser)
            //{
            //    ParkingSession.CurrentUserId = Section.UserService.CurrentUser.Id;
            //}
            //else
            //{
            //    ParkingSession.CurrentUserId = null;
            //}
            _server.ClaimPromotionSearch(VehicleNumber, VehicleType.Id, FromTime, ToTime, SelectedPage, pageSize, (result, ex) => {
                if (ex != null)
                {
                    ResultMessage = ex.Message;
                    MessageLevel = 3;
                    return;
                }
                else
                {
                    _currentResult = result;
                    _maxPage = _currentResult.Total / pageSize;
                    if (_currentResult.Total % pageSize > 0) _maxPage += 1;

                    List<int> lst = new List<int>();
                    for (int i = 1; i <= _maxPage; i++)
                    {
                        lst.Add(i);
                    }
                    PageNumbers = lst;

                    InvokeOnMainThread(() => {
                        if (ex != null)
                        {
                            ResultMessage = ex.Message;
                            MessageLevel = 3;
                            return;
                        }

                        _LogBillClaim = clsLog.readObject(this.FromTime, this.ToTime);
                        List<Terminal.Core.Models.ClaimPromotion> _temps = _currentResult.ClaimPromotions;
                        if (this.IsCheckedCurrentUser)
                            _temps = _temps.FindAll(
                                delegate(Terminal.Core.Models.ClaimPromotion claim)
                                {
                                    return claim.UserId == this.UserId;
                                });

                        if (this.IsCounter && _LogBillClaim!=null)
                        {
                            _temps = _temps.Where(p => _LogBillClaim.Any(p2 => p2.CheckInInfo.ParkingSessionId == p.ParkingSessionId)).ToList();
                        }
                        
                        ClaimPromotions = new ObservableCollection<Terminal.Core.Models.ClaimPromotion>(_temps);
                        foreach (var item in ClaimPromotions)
                        {
                            //int id = 1;
                            //int.TryParse(item.CheckInLane, out id);
                            //item.TerminalName = _laneTerminal[id];
                            //item.BillNumber = clsLog.GetBillNumber(TimestampConverter.Timestamp2DateTime(item.ServerTime), item.ParkingSessionId);
                            if (_LogBillClaim != null)
                            {
                                var _find = _LogBillClaim.Find(
                                    delegate(clsLogInfo Info)
                                    {
                                        return Info.CheckInInfo.ParkingSessionId == item.ParkingSessionId;
                                    });
                                if (_find != null)
                                    item.BillNumber = _find.ClaimPromotionInfo.BillNumber;
                            }
                        }

                        if (ClaimPromotions != null && ClaimPromotions.Count > 0)
                            this.Amount_E = ClaimPromotions.Sum(s => s.Amount_E);
                        else
                            this.Amount_E = 0;
                        
                        //if (ClaimPromotions != null && ClaimPromotions.Count > 0)
                        //{
                        //    SelectedItem = ClaimPromotions[0];
                        //    ResultMessage = string.Format(GetText("search.number_of_results"), ClaimPromotions.Count);
                        //    MessageLevel = 0;
                        //}
                        //else
                        //{
                        //    ResultMessage = GetText("search.not_found");
                        //    MessageLevel = 3;
                        //}

                        //ParkingSession.CardId = string.Empty;

                        if (complete != null)
                            complete();
                    });
                }
            });
        }

        public void GoNext()
        {
            int tmp = SelectedPage;
            tmp++;
            if (tmp > _maxPage)
                tmp = _maxPage;
            if (tmp != SelectedPage && tmp != 0)
            {
                SelectedPage = tmp;
                DoSearch(() => {

                });
            }
        }

        public void GoBack()
        {
            int tmp = SelectedPage;
            tmp--;
            if (tmp <= 1)
                tmp = 1;
            if (tmp != SelectedPage && tmp != 0)
            {
                SelectedPage = tmp;
                DoSearch(() => {

                });
            }
        }

        public void Search(Action complete)
        {
            int limit = 100;
            if (!int.TryParse(SelectedNumberOfResult, out limit)) limit = 0;
            //_server.ParkingSessionSearchAdvance(ParkingSession, SelectedParkingSession, (result, ex) => {
            //    InvokeOnMainThread(() => {
            //        if (ex != null)
            //        {
            //            ResultMessage = ex.Message;
            //            MessageLevel = 3;
            //            return;
            //        }

            //        ClaimPromotions = new ObservableCollection<Terminal.Core.Models.ClaimPromotion>(result);
                    //foreach(var item in ClaimPromotions)
                    //{
                    //    int id = 1;
                    //    int.TryParse(item.CheckInLane, out id);
                    //    item.TerminalName = _laneTerminal[id];
                    //}

                    ////ParkingSessions = Filter(ParkingSessions, "Da");

                    //if (ClaimPromotions != null && ClaimPromotions.Count > 0)
                    //{
                    //    SelectedItem = ClaimPromotions[0];
                    //    ResultMessage = string.Format(GetText("search.number_of_results"), ClaimPromotions.Count);
                    //    MessageLevel = 0;
                    //}
                    //else
                    //{
                    //    ResultMessage = GetText("search.not_found");
                    //    MessageLevel = 3;
                    //}

            //        ParkingSession.CardId = string.Empty;

            //        if (complete != null)
            //            complete();
            //    });
            //}, limit);
        }


        MvxCommand _searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                _searchCommand = _searchCommand ?? new MvxCommand(() => {
                    DoSearch(null);
                });

                return _searchCommand;
            }
        }

        MvxCommand _backCommand;
        public ICommand BackCommand
        {
            get
            {
                _backCommand = _backCommand ?? new MvxCommand(() => {
                                                                        ShowViewModel<BillCheckoutViewModel>();
                });

                return _backCommand;
            }
        }

        MvxCommand _selectPageCommand;
        public ICommand SelectPageCommand
        {
            get
            {
                _selectPageCommand = _selectPageCommand ?? new MvxCommand(() => {
                    DoSearch(null);
                });
                return _selectPageCommand;
            }
        }

        MvxCommand _goNextCommand;
        public ICommand GoNextCommand
        {
            get
            {
                _goNextCommand = _goNextCommand ?? new MvxCommand(GoNext);
                return _goNextCommand;
            }
        }

        MvxCommand _goBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                _goBackCommand = _goBackCommand ?? new MvxCommand(GoBack);
                return _goBackCommand;
            }
        }

        public override void Unloaded()
        {
            base.Unloaded();

            var messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Unsubscribe<KeyPressedMessage>(_keyPressedToken);
            _keyPressedToken = null;
        }

        public void KeyPressed(object sender, KeyEventArgs e)
        {
            OnKeyPressed(new KeyPressedMessage(sender, e));
        }

        protected void OnKeyPressed(KeyPressedMessage msg)
        {
            string output;
            //KeyAction action = this.Section.KeyMap.GetAction(msg.KeyEventArgs, out output, typeof(SearchViewModel));

            //switch (action)
            //{
            //    case KeyAction.DoSearch:
            //        {
            //            DoSearch(null);
            //            break;
            //        }
            //    case KeyAction.Back:
            //        {
            //            BackCommand.Execute(null);
            //            break;
            //        }
            //}
        }


        Terminal.Core.Models.ClaimPromotion _selectedItem;
        public Terminal.Core.Models.ClaimPromotion SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem == value) return;
                _selectedItem = value;
                if (_selectedItem == null)
                    this.IsEnabledPrintBill = false;
                else
                    this.IsEnabledPrintBill = true;

                RaisePropertyChanged(() => SelectedItem);
            }
        }

        /***Chinh sua 07-08-2016 ***/
        private bool _IsEnabledPrintBill = false;
        public bool IsEnabledPrintBill
        {
            get { return this._IsEnabledPrintBill; }
            set
            {
                this._IsEnabledPrintBill = value;
                this.RaisePropertyChanged(() => this.IsEnabledPrintBill);
            }
        }

        /***Chinh sua 01-08-2016 ***/
        MvxCommand<Terminal.Core.Models.ClaimPromotion> _printBillCommand;
        public MvxCommand<Terminal.Core.Models.ClaimPromotion> PrintBillCommand
        {
            get
            {
                _printBillCommand = _printBillCommand ?? new MvxCommand<Terminal.Core.Models.ClaimPromotion>((PrintBillCommand) =>
                {
                    ShowConfirmMessage("Xác nhận In Phiếu", PublishPrintBill);
                });

                return _printBillCommand;
            }
        }

        public void PublishPrintBill()
        {//Tong tien: 100,000vnd

            try
            {
                if (_selectedItem == null) return;
                clsLogInfo _logInfo=null;
                if (_LogBillClaim != null)
                    _logInfo = _LogBillClaim.Find(
                        f => f.CheckInInfo.ParkingSessionId == _selectedItem.ParkingSessionId);
                if (_logInfo == null)
                    _logInfo = new clsLogInfo();
                if (_logInfo.CheckInInfo == null)
                    _logInfo.CheckInInfo = new CheckIn();
                com.clsCom _clsCom = new com.clsCom();
                _clsCom.UrlLogo = System.Windows.Forms.Application.StartupPath + "\\config\\logo.ini";

                string[] _Array = new string[10];
                
                _Array[0] = _clsCom.CombineString("THOI GIAN VAO: ", _logInfo.CheckInInfo.StrCheckInTime);
                _Array[1] = _clsCom.CombineString("LOAI XE: ", _logInfo.CheckInInfo.VehicleType.Name);
                _Array[2] = _clsCom.CombineString("BIEN SO: ", _logInfo.CheckInInfo.VehicleNumber);

                _Array[3] = _clsCom.CombineString("THOI GIAN CLAIM: ", "");

                _Array[4] = _clsCom.CombineString("THOI GIAN LUU BAI: ", "");
                _Array[5] = _clsCom.CombineString("TONG HOA DON: ", string.Format("{0:0,0}", _logInfo.CheckInInfo.BillAmount));
                _Array[6] = _clsCom.CombineString("PHI GIU XE: ", _logInfo.CheckInInfo.ParkingFee.ToString());
                
               //_Array[7] = _clsCom.CombineString("PHI CLAIM: ", _selectedItem..ToString());

                //_Array[8] = _clsCom.CombineString("GIA GIAM CUA TENANT: ", TenantDiscount.ToString());
                //_Array[9] = _clsCom.CombineString("PHI PHAI TRA: ", FinalParkingFee.ToString());

                _clsCom.XinChao = "HEN GAP LAI QUY KHACH";
                byte[] _buffers = _clsCom.CommandESC(_Array);

                if (BillCheckoutViewModel._SerialPort != null)
                {
                    if (!BillCheckoutViewModel._SerialPort.IsOpen)
                        BillCheckoutViewModel._SerialPort.Open();
                    BillCheckoutViewModel._SerialPort.Write(_buffers, 0, _buffers.Length);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

        }
        
    }
}