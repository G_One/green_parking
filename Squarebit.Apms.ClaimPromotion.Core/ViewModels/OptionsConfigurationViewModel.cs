﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Security.Cryptography;
using Squarebit.Apms.Terminal.Core.Utilities;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class OptionsConfigurationViewModel : BaseViewModel
    {
        bool _plateRecognition = true;
        public bool PlateRecognition
        {
            get { return _plateRecognition; }
            set
            {
                if (_plateRecognition == value) return;
                _plateRecognition = value;
                RaisePropertyChanged(() => PlateRecognition);
            }
        }

        bool _noMatching = true;
        public bool NoMatching
        {
            get { return _noMatching; }
            set
            {
                if (_noMatching == value) return;
                _noMatching = value;
                RaisePropertyChanged(() => NoMatching);
            }
        }

        bool _forceUpdatePlateNumber = false;
        public bool ForceUpdatePlateNumber
        {
            get { return _forceUpdatePlateNumber; }
            set
            {
                if (_forceUpdatePlateNumber == value) return;
                _forceUpdatePlateNumber = value;
                RaisePropertyChanged(() => ForceUpdatePlateNumber);
            }
        }

        bool _confirmCheckout = true;
        public bool ConfirmCheckout
        {
            get { return _confirmCheckout; }
            set
            {
                if (_confirmCheckout == value) return;
                _confirmCheckout = value;
                RaisePropertyChanged(() => ConfirmCheckout);
            }
        }

        IEnumerable<CameraType> _cameraTypes;
        public IEnumerable<CameraType> CameraTypes
        {
            get { return _cameraTypes; }
            set
            {
                _cameraTypes = value;
                RaisePropertyChanged(() => CameraType);
            }
        }

        CameraType _cameraType = CameraType.Vivotek;
        public CameraType CameraType
        {
            get { return _cameraType; }
            set
            {
                if (_cameraType == value) return;
                _cameraType = value;
                RaisePropertyChanged(() => CameraType);
            }
        }

        int _waitingCheckDuration;
        public int WaitingCheckDuration
        {
            get { return _waitingCheckDuration; }
            set
            {
                if (_waitingCheckDuration == value) return;
                _waitingCheckDuration = value;
                RaisePropertyChanged(() => WaitingCheckDuration);
            }
        }

        int _displayCheckOutDuration;
        public int DisplayCheckOutDuration
        {
            get { return _displayCheckOutDuration; }
            set
            {
                if (_displayCheckOutDuration == value) return;
                _displayCheckOutDuration = value;
                RaisePropertyChanged(() => DisplayCheckOutDuration);
            }
        }

		string _resultMessage;
		public string ResultMessage
		{
			get { return _resultMessage; }
			set
			{
				if (_resultMessage == value) return;
				_resultMessage = value;
				RaisePropertyChanged(() => ResultMessage);
			}
		}

        string _otherResultMessage;
        public string OtherResultMessage
        {
            get { return _otherResultMessage; }
            set
            {
                if (_otherResultMessage == value) return;
                _otherResultMessage = value;
                RaisePropertyChanged(() => OtherResultMessage);
            }
        }

		string _currentPassword;
		public string CurrentPassword
		{
			get { return _currentPassword; }
			set
			{
				if (_currentPassword == value) return;
				_currentPassword = value;
				RaisePropertyChanged(() => CurrentPassword);
			}
		}

		string _newPassword;
		public string NewPassword
		{
			get { return _newPassword; }
			set
			{
				if (_newPassword == value) return;
				_newPassword = value;
				RaisePropertyChanged(() => NewPassword);
			}
		}

		string _newPasswordAgain;
		public string NewPasswordAgain
		{
			get { return _newPasswordAgain; }
			set
			{
				if (_newPasswordAgain == value) return;
				_newPasswordAgain = value;
				RaisePropertyChanged(() => NewPasswordAgain);
			}
		}

        IOptionsSettings _optionSettings;

        public OptionsConfigurationViewModel(IViewModelServiceLocator service)
            : base(service)
        {
            _optionSettings = Mvx.Resolve<IOptionsSettings>();

            this.PlateRecognition = _optionSettings.PlateRecognitionEnale;
            this._noMatching = _optionSettings.NoMatchingPlateNoticeEnalbe;
            this._forceUpdatePlateNumber = _optionSettings.ForceUpdatePlateNumber;
            this._confirmCheckout = _optionSettings.ConfirmCheckout;
            this._waitingCheckDuration = _optionSettings.WaitingCheckDuration;

            this._cameraType = _optionSettings.CameraType;
            CameraTypes = Enum.GetValues(typeof(CameraType)).Cast<CameraType>();
            this._displayCheckOutDuration = _optionSettings.DisplayCheckOutDuration;
        }

        public override void Start()
        {
            base.Start();
        }

		protected bool UpdatePassword()
		{
			if (string.IsNullOrEmpty(CurrentPassword) ||
				string.IsNullOrEmpty(NewPassword) ||
				string.IsNullOrEmpty(NewPasswordAgain))
			{
				ResultMessage = "system.password_empty";
				return false;
			}

			var currPassMd5 = EncryptionUtility.GetMd5Hash(CurrentPassword);
			if (currPassMd5 != _optionSettings.MasterPasswordMd5)
			{
				ResultMessage = "system.invalid_master_password";
				return false;
			}

			if (NewPassword != NewPasswordAgain)
			{
				ResultMessage = "system.passwords_not_matched";
				return false;
			}

			_optionSettings.MasterPasswordMd5 = EncryptionUtility.GetMd5Hash( NewPassword);
			_optionSettings.Save();
			ResultMessage = "system.password_changed_success";
			return true;
		}

        protected void UpdateOtherOptions()
        {
            _optionSettings.PlateRecognitionEnale = this.PlateRecognition;
            _optionSettings.NoMatchingPlateNoticeEnalbe = this.NoMatching;
            _optionSettings.ForceUpdatePlateNumber = this.ForceUpdatePlateNumber;
            _optionSettings.ConfirmCheckout = this.ConfirmCheckout;
            _optionSettings.WaitingCheckDuration = this.WaitingCheckDuration;
            _optionSettings.CameraType = this.CameraType;
            _optionSettings.DisplayCheckOutDuration = this.DisplayCheckOutDuration;
            _optionSettings.Save();
            OtherResultMessage = "system.other_options_changed_success";
        }

        MvxCommand _passwordSaveCommand;
        public ICommand PasswordSaveCommand
        {
            get
            {
                _passwordSaveCommand = _passwordSaveCommand ?? new MvxCommand(() => {

                    if (UpdatePassword())
                    {
                        CurrentPassword = "";
                        NewPassword = "";
                        NewPasswordAgain = "";
                    }
                });

                return _passwordSaveCommand;
            }
        }

        MvxCommand _otherOptSaveCommand;
        public ICommand OtherOptSaveCommand
        {
            get
            {
                _otherOptSaveCommand = _otherOptSaveCommand ?? new MvxCommand(() => {
                    UpdateOtherOptions();
                });

                return _otherOptSaveCommand;
            }
        }
    }
}
