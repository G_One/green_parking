﻿using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class ClaimPromotionBootstrapViewModel : BaseViewModel
    {
        private IUserPreferenceService _userPreferenceService;
        IUserServiceLocator _userServiceLocator;

        public ClaimPromotionBootstrapViewModel(IViewModelServiceLocator services, IUserPreferenceService userPreferenceService)
            : base(services)
        {
            _userPreferenceService = userPreferenceService;
            _userServiceLocator = Mvx.Resolve<IUserServiceLocator>();
        }

        public void Init(ParameterKey key)
        {

        }

        public override void Start()
        {
            if (!_userPreferenceService.HostSettings.HasLocal)
            {
                ShowViewModelExt<ConfigViewModel>();
            }
            else
            {
                //ShowViewModel<BillCheckoutViewModel>();
                ShowLaneView();
            }
        }

        private void ShowLaneView()
        {
            IUserService laneUserService = _userServiceLocator.GetUserService(AppConfig.ClaimPromotionSection.Id);
            if (laneUserService.IsLogin)
            {
                ShowViewModel<BillCheckoutViewModel>();
            }
            else
            {
                ShowViewModelExt<LoginViewModel>(null, null, vm => {
                    ShowLaneView();
                });
            }
        }
    }
}
