﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class ExceptionalCheckOutMessage : MvxMessage
    {
        public ParkingSession CheckedOutItem { get; set; }
        public ExceptionalCheckOutMessage(object sender, ParkingSession item) : base(sender)
        {
            CheckedOutItem = item;
        }
    }

    public class ExceptionalCheckOutViewModel : BaseViewModel
    {
        IServer _server;
        IMvxMessenger _messenger;
        IUserService _userService;
        IUserServiceLocator _userServiceLocator;
        IUserPreferenceService _userPreferenceService;
        IMvxMessenger _exceptionCheckoutMessenger;

        string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                if (_errorMessage == value) return;
                _errorMessage = value;
                RaisePropertyChanged(() => ErrorMessage);
            }
        }

        private int _messageLevel;
        public int MessageLevel
        {
            get { return _messageLevel; }
            set
            {
                if (_messageLevel == value) return;
                _messageLevel = value;
                RaisePropertyChanged(() => MessageLevel);
            }
        }

        string _reason;
        public string Reason
        {
            get { return _reason; }
            set
            {
                if (_reason == value) return;
                _reason = value;
                RaisePropertyChanged(() => Reason);
            }
        }

        bool _isBlocked;
        public bool IsBlocked
        {
            get { return _isBlocked; }
            set
            {
                if (_isBlocked == value) return;
                _isBlocked = value;
                RaisePropertyChanged(() => IsBlocked);
            }
        }

        private ApmsUser _user;
        public ApmsUser User
        {
            get { return _user; }
            set { _user = value; }
        }

        ParkingSession _selectedItem;
        public ParkingSession SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem == value) return;
                _selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        public ExceptionalCheckOutViewModel(IViewModelServiceLocator service,
            IServer server)
            : base(service)
        {
            _server = server;
            _messenger = Mvx.Resolve<IMvxMessenger>();
            _exceptionCheckoutMessenger = Mvx.Resolve<IMvxMessenger>();
            _userServiceLocator = Mvx.Resolve<IUserServiceLocator>();
            _userPreferenceService = Mvx.Resolve<IUserPreferenceService>();
        }

        public virtual void Init(ParameterKey key)
        {
            object[] objs = (object[])Services.Parameter.Retrieve(key);

            this.Section = (Section)objs[0];
            this.SelectedItem = (ParkingSession)objs[1];
            _userService = _userServiceLocator.GetUserService(this.Section.Id);

            this.User = _userService.CurrentUser;
        }

        public override void Start()
        {
            base.Start();
        }

        void SendReason(Action<Exception> complete)
        {
            if (SelectedItem == null)
            {
                ErrorMessage = GetText("exceptional_checkout.information_missing");
                MessageLevel = 3;
                return;
            }
            
            if (string.IsNullOrEmpty(Reason))
            {
                ErrorMessage = GetText("exceptional_checkout.reason_missing");
                MessageLevel = 3;
                return;
            }
            else
            {
                Reason = Reason.TrimEnd(' ').TrimStart();
                if (string.IsNullOrEmpty(Reason))
                {
                    ErrorMessage = GetText("exceptional_checkout.reason_missing");
                    MessageLevel = 3;
                    return;
                }
            }

            string cardId = SelectedItem.CardId;
            int terminalId = _userPreferenceService.HostSettings.Terminal.Id;
            int laneId = Section.Lane.Id;
            int operatorId = User.Id;

            _server.CreateExceptionalCheckOut(cardId, terminalId, laneId, operatorId, Reason, IsBlocked, (exception) => {
                if (complete != null)
                    complete(exception);
            });
        }

        public void PublishCloseChildEvent(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<CloseChildMessage>())
            {
                _messenger.Publish(new CloseChildMessage(this, position));
            }
        }

        MvxCommand _exceptionalCheckoutCommand;
        public ICommand ExceptionalCheckoutCommand
        {
            get
            {
                _exceptionalCheckoutCommand = _exceptionalCheckoutCommand ?? new MvxCommand(() => {
                    SendReason(ex => {
                        if (ex == null)
                        {
                            SetMessage("exceptional_checkout.success");

                            if(_exceptionCheckoutMessenger.HasSubscriptionsFor<ExceptionalCheckOutMessage>())
                            {
                                _exceptionCheckoutMessenger.Publish(new ExceptionalCheckOutMessage(this, SelectedItem));
                                GoBackCommand.Execute(null);
                            }
                        }
                        else
                            SetMessage("exceptional_checkout.error");
                    });
                });
                return _exceptionalCheckoutCommand;
            }
        }

        private void SetMessage(string msg)
        {
            if (msg.Equals("exceptional_checkout.success"))
            {
                ErrorMessage = GetText("exceptional_checkout.success");
                MessageLevel = 1;
            }
            else if (msg.Equals("exceptional_checkout.error"))
            {
                ErrorMessage = GetText("exceptional_checkout.error");
                MessageLevel = 3;
            }
        }

        MvxCommand _goBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                _goBackCommand = _goBackCommand ?? new MvxCommand(() => {
                    PublishCloseChildEvent(this.Section.Id);
                });
                return _goBackCommand;
            }
        }
    }
}
