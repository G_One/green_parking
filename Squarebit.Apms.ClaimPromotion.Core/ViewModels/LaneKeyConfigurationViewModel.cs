﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class LaneKeyConfigurationViewModel : BaseViewModel
    {
        private string _laneId;
        public string LaneId
        {
            get { return _laneId; }
            set
            {
                if (_laneId == value) return;
                _laneId = value;
            }
        }

        private ISection _section;
        public override ISection Section
        {
            get { return _section; }
            set
            {
                if (_section == value) return;
                _section = value;
                LaneId = _section.Id.ToString();
            }
        }

        #region Properties
        string _logoutKey;
        public string LogoutKey
        {
            get { return _logoutKey; }
            set
            {
                _logoutKey = value;
                RaisePropertyChanged(() => LogoutKey);
            }
        }

        string _checkOutKey;
        public string CheckOutKey
        {
            get { return _checkOutKey; }
            set
            {
                _checkOutKey = value;
                RaisePropertyChanged(() => CheckOutKey);
            }
        }

        string _changeLaneKey;
        public string ChangeLaneKey
        {
            get { return _changeLaneKey; }
            set
            {
                _changeLaneKey = value;
                RaisePropertyChanged(() => ChangeLaneKey);
            }
        }

        string _searchKey;
        public string SearchKey
        {
            get { return _searchKey; }
            set
            {
                _searchKey = value;
                RaisePropertyChanged(() => SearchKey);
            }
        }

        string _showVehicleTypeKey;
        public string ShowVehicleTypeKey
        {
            get { return _showVehicleTypeKey; }
            set
            {
                _showVehicleTypeKey = value;
                RaisePropertyChanged(() => ShowVehicleTypeKey);
            }
        }

        string _cancelCheckOutKey;
        public string CancelCheckOutKey
        {
            get { return _cancelCheckOutKey; }
            set
            {
                _cancelCheckOutKey = value;
                RaisePropertyChanged(() => CancelCheckOutKey);
            }
        }

        string _backKey;
        public string BackKey
        {
            get { return _backKey; }
            set
            {
                _backKey = value;
                RaisePropertyChanged(() => BackKey);
            }
        }

        string _exceptionalCheckout;
        public string ExceptionalCheckout
        {
            get { return _exceptionalCheckout; }
            set
            {
                _exceptionalCheckout = value;
                RaisePropertyChanged(() => ExceptionalCheckout);
            }
        }

        string _resultMessage;
        public string ResultMessage
        {
            get { return _resultMessage; }
            set
            {
                _resultMessage = value;
                RaisePropertyChanged(() => ResultMessage);
            }
        }
        #endregion

        public LaneKeyConfigurationViewModel(IViewModelServiceLocator services)
            : base(services)
        {
        }

        public void Init(ParameterKey key)
        {
            this.Section = (Section)Services.Parameter.Retrieve(key);
        }

        public override void Start()
        {
            base.Start();
            LoadKey();
        }

        private void LoadKey()
        {
            LogoutKey = Section.KeyMap.GetKey(KeyAction.Logout);
            CheckOutKey = Section.KeyMap.GetKey(KeyAction.CheckOut);
            CancelCheckOutKey = Section.KeyMap.GetKey(KeyAction.CancelCheckOut);
            SearchKey = Section.KeyMap.GetKey(KeyAction.Search);
            ChangeLaneKey = Section.KeyMap.GetKey(KeyAction.ChangeLane);
            ShowVehicleTypeKey = Section.KeyMap.GetKey(KeyAction.ShowVehicleType);
        }

        public void SaveKey()
        {
            Section.KeyMap.AddItem(KeyAction.Logout, _logoutKey);
            Section.KeyMap.AddItem(KeyAction.ExceptionalCheckout, _exceptionalCheckout);
            //Section.KeyMap.AddItem(KeyAction.Back, _cancelCheckOutKey);
            Section.KeyMap.AddItem(KeyAction.CancelCheckOut, _cancelCheckOutKey);
            Section.KeyMap.AddItem(KeyAction.Search, _searchKey);
            Section.KeyMap.AddItem(KeyAction.ChangeLane, _changeLaneKey);
            Section.KeyMap.AddItem(KeyAction.CheckOut, _checkOutKey);
            Section.KeyMap.AddItem(KeyAction.ShowVehicleType, _showVehicleTypeKey);
            ResultMessage = GetText("keyconfigure.success");
        }

        MvxCommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                _saveCommand = _saveCommand ?? new MvxCommand(() => {
                    SaveKey();
                });

                return _saveCommand;
            }
        }
    }
}
