﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using Squarebit.Apms.ClaimPromotion.Core.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Devices.Dal;
using Squarebit.Apms.ClaimPromotion.Core.Utilities;
using System.Runtime.Serialization;
using System.Collections;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    [Serializable]
    public class BillCheckoutViewModel : BaseViewModel
    {
        /*** 07-08-2016 ***/
        public static System.IO.Ports.SerialPort _SerialPort;
        MvxCommand _showSearchCommand;
        public ICommand ShowSearchCommand
        {
            get
            {
                _showSearchCommand = _showSearchCommand ?? new MvxCommand(() => ShowViewModel<SearchClaimPromotionViewModel>());
                return _showSearchCommand;
            }
        }

        MvxCommand _showSearchInParkingCommand;
        public ICommand ShowSeachInParkingCommand
        {
            get
            {
                _showSearchInParkingCommand = _showSearchInParkingCommand ?? new MvxCommand(() => ShowViewModel<SearchInParkingViewModel>());
                return _showSearchInParkingCommand;
            }
        }

        public int UserId { get; set; }

        string _username;
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                RaisePropertyChanged(() => Username);
                //_claimed = false;
            }
        }

        /// <summary>
        /// Thông tin c?a vi?c g?i xe
        /// </summary>
        CheckIn _checkInInfo;
        public CheckIn CheckInInfo
        {
            get { return _checkInInfo; }
            set
            {
                _checkInInfo = value;
                RaisePropertyChanged(() => CheckInInfo);
            }
        }


        bool _showMessage;
        public bool ShowMessage
        {
            get { return _showMessage; }
            set
            {
                _showMessage = value;
                RaisePropertyChanged(() => ShowMessage);
                //_claimed = false;
            }
        }

        ObservableCollection<Bill> _bills;
        public ObservableCollection<Bill> Bills
        {
            get { return _bills; }
            set
            {
                _bills = value;
                RaisePropertyChanged(() => Bills);
            }
        }

        ObservableCollection<Coupon> _coupons;
        public ObservableCollection<Coupon> Coupons
        {
            get { return _coupons; }
            set
            {
                _coupons = value;
                RaisePropertyChanged(() => Coupons);
            }
        }

        Bill _currentBill;
        public Bill CurrentBill
        {
            get { return _currentBill; }
            set
            {
                _currentBill = value;
                RaisePropertyChanged(() => CurrentBill);
            }
        }

        Coupon _currentCoupon;
        public Coupon CurrentCoupon
        {
            get { return _currentCoupon; }
            set
            {
                _currentCoupon = value;
                RaisePropertyChanged(() => CurrentCoupon);
            }
        }

        private string _parkingDuration;
        public string ParkingDuration
        {
            get { return _parkingDuration; }
            set
            {
                _parkingDuration = value;
                RaisePropertyChanged(() => ParkingDuration);
            }
        }

        DateTime _claimTime;
        public DateTime ClaimTime
        {
            get { return _claimTime; }
            set
            {
                _claimTime = value;
                RaisePropertyChanged(() => ClaimTime);
                RaisePropertyChanged(() => StrClaimTime);
                //_claimed = false;
            }
        }

        public string StrClaimTime
        {
            get { return ClaimTime == default(DateTime) ? "" : ClaimTime.ToString("dd/MM/yyyy  HH:mm:ss"); }
        }

        /// <summary>
        /// Step này dùng để thay đổi nội dung hiển thị trên màn hình
        /// Bởi vì mình có nhiều bước như hiển thị form nhập mã hóa đơn, nhập doanh thu, nhập các loại giảm giá
        /// </summary>
        int _step;
        public int Step
        {
            get { return _step; }
            set
            {
                if (value < 0) value = 0;

                _step = value;
                switch (_step)
                {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        FinalParkingFee = ParkingFee + PromotionDiscount + TenantDiscount;
                        break;
                    default:
                        Reset();
                        break;
                }

                RaisePropertyChanged(() => Step);
            }
        }

        /// <summary>
        /// Mã hóa đơn
        /// </summary>
        string _billNumber;
        public string BillNumber
        {
            get { return _billNumber; }
            set
            {
                _billNumber = value;
                RaisePropertyChanged(() => BillNumber);
            }
        }

        /// <summary>
        /// Số tiền của người dùng ghi trong hóa đơn
        /// </summary>
        long _amount;
        public long Amount
        {
            get { return _amount; }
            set
            {
                _amount = value;
                RaisePropertyChanged(() => Amount);
            }
        }

        /// <summary>
        /// Phí gửi xe ban đầu (chưa trừ các loại giảm giá)
        /// </summary>
        long _parkingFee;
        public long ParkingFee
        {
            get { return _parkingFee; }
            set
            {
                _parkingFee = value;
                RaisePropertyChanged(() => ParkingFee);
            }
        }

        /// <summary>
        /// Giảm giá của promotion
        /// </summary>
        long _promotionDiscount;
        public long PromotionDiscount
        {
            get { return _promotionDiscount; }
            set
            {
                if (value > 0) value *= -1;
                _promotionDiscount = value;
                RaisePropertyChanged(() => PromotionDiscount);

                FinalParkingFee = ParkingFee + PromotionDiscount + TenantDiscount;
            }
        }

        long _feeAfterClaimPromotion;
        public long FeeAfterClaimPromotion
        {
            get { return _feeAfterClaimPromotion; }
            set
            {
                _feeAfterClaimPromotion = value;
                RaisePropertyChanged(() => FeeAfterClaimPromotion);
            }
        }

        /// <summary>
        /// Giảm giá của tenant
        /// </summary>
        long _tenantDiscount;
        public long TenantDiscount
        {
            get { return _tenantDiscount; }
            set
            {
                if (value > 0) value *= -1;
                _tenantDiscount = value;
                RaisePropertyChanged(() => TenantDiscount);
                //FinalParkingFee = ParkingFee * 0.8f + TenantDiscount;
                FinalParkingFee = ParkingFee + PromotionDiscount + TenantDiscount;
            }
        }

        /// <summary>
        /// Giảm giá của thẻ vip
        /// </summary>
        long _vipDiscount;
        public long VipDiscount
        {
            get { return _vipDiscount; }
            set
            {
                _vipDiscount = value;
                RaisePropertyChanged(() => VipDiscount);
            }
        }

        /// <summary>
        ///  Phí cuối cùng, sau khi trừ ra các loại giảm giá
        /// </summary>
        long _finalParkingFee;
        public long FinalParkingFee
        {
            get { return _finalParkingFee; }
            set
            {
                if (value < 0) value = 0;
                _finalParkingFee = value;
                RaisePropertyChanged(() => FinalParkingFee);
            }
        }

        string _warningMessage;
        public string WarningMessage
        {
            get { return _warningMessage; }
            set
            {
                _warningMessage = value;
                RaisePropertyChanged(() => WarningMessage);


                ShowMessage = _warningMessage != null;
            }
        }

        string _currentBillTenantName;
        public string CurrentBillTenantName
        {
            get { return _currentBillTenantName; }
            set
            {
                _currentBillTenantName = value;
                RaisePropertyChanged(() => CurrentBillTenantName);
            }
        }

        string _currentCouponTenantName;
        public string CurrentCouponTenantName
        {
            get { return _currentCouponTenantName; }
            set
            {
                _currentCouponTenantName = value;
                RaisePropertyChanged(() => CurrentCouponTenantName);
            }
        }

        string _currentTenantVoucherName;
        public string CurrentTenantVoucherName
        {
            get { return _currentTenantVoucherName; }
            set
            {
                _currentTenantVoucherName = value;
                RaisePropertyChanged(() => CurrentTenantVoucherName);
            }
        }

        private ObservableCollection<Tenant> _tenants;
        public ObservableCollection<Tenant> Tenants
        {
            get
            {
                return _tenants;
            }
            set
            {
                if (_tenants == value)
                    return;

                _tenants = value;
                RaisePropertyChanged(() => Tenants);

                TenantNames = new ObservableCollection<string>(_tenants.Select(t => t.Name));
            }
        }

        private ObservableCollection<string> _tenantNames;
        public ObservableCollection<string> TenantNames
        {
            get
            {
                return _tenantNames;
            }
            set
            {
                if (_tenantNames == value)
                    return;

                _tenantNames = value;
                RaisePropertyChanged(() => TenantNames);
            }
        }


        private ObservableCollection<TenantVoucher> _tenantVouchers;
        public ObservableCollection<TenantVoucher> TenantVouchers
        {
            get
            {
                return _tenantVouchers;
            }
            set
            {
                if (_tenantVouchers == value)
                    return;

                _tenantVouchers = value;
                RaisePropertyChanged(() => TenantVouchers);

                TenantVoucherNames = new ObservableCollection<string>(_tenantVouchers.Select(t => t.Name));
            }
        }


        private ObservableCollection<string> _tenantVoucherNames;
        public ObservableCollection<string> TenantVoucherNames
        {
            get
            {
                return _tenantVoucherNames;
            }
            set
            {
                if (_tenantVoucherNames == value)
                    return;

                _tenantVoucherNames = value;
                RaisePropertyChanged(() => TenantVoucherNames);
            }
        }


        string _cardId;
        //string _vehicleNumber;
        //int _vehicleType;
        //int _parkingSessionId;

        protected MvxSubscriptionToken _keyPressedToken;
        ICardReaderService _cardReaderService;
        IParkingFeeService _parkingFeeService;
        List<CardReaderWrapper> _cardReaders;
        IClaimPromotionSettings _claimSettings;

        IUserService _userService;
        IServer _server;
        bool _choosePay = false;
        KeyMap _keymap;

        public BillCheckoutViewModel(IViewModelServiceLocator services, ICardReaderService cardReaderService, IServer server, IParkingFeeService feeService)
            : base(services)
        {
            _keyPressedToken = services.Messenger.Subscribe<KeyPressedMessage>(OnKeyPressed);

            _cardReaderService = cardReaderService;
            _server = server;
            _claimSettings = Mvx.Resolve<IClaimPromotionSettings>();
            _parkingFeeService = feeService;
            _keymap = new KeyMap();

        }

        public void Init(ParameterKey key)
        {
            // Attach card reader callback
            IUserServiceLocator userServiceLocator = Mvx.Resolve<IUserServiceLocator>();
            //Section.TemporaryDirection = Section.Direction;
            //Section.SetupCardReader();
            _userService = userServiceLocator.GetUserService(AppConfig.ClaimPromotionSection.Id);
            Username = _userService.CurrentUser.DisplayName;
            UserId = _userService.CurrentUser.Id;

            //NTimes = Squarebit.Apms.ClaimPromotion.Core.Utilities.clsLog.GetNtimes();
            //int BillNo = clsLog.GetBillNumber(DateTime.Now, 55719);
            //int k = BillNo;
            Bills = new ObservableCollection<Bill>();
            
            this.ReadLogClaimPromotion();

            Bills.CollectionChanged += (s, e) =>
            {
                Amount = Bills.Sum(b => b.BillAmount);
            };
            Coupons = new ObservableCollection<Coupon>();
            Coupons.CollectionChanged += (s, e) =>
            {
                TenantDiscount = Coupons.Sum(c => c.CouponDiscount);
            };

            CurrentBill = new Bill() { Date = DateTime.Now };
            CurrentCoupon = new Coupon();

            /*** 07-08-2016 ***/
            if (_SerialPort == null)
            {
                _SerialPort = new System.IO.Ports.SerialPort();
                _SerialPort.PortName = "COM1";
                _SerialPort.BaudRate = 9600;
            }
			var cardId = Services.Parameter.Retrieve<string>(key);
            if (!string.IsNullOrWhiteSpace(cardId))
            {
                GetCheckIn(cardId);
            }

        }

        /*** 07-08-2016 ***/
        public void PrinToPrinter()
        {

            try
            {
                //if (_SerialPort == null || CheckInInfo == null) return;

                com.clsCom _clsCom = new com.clsCom();
                _clsCom.UrlLogo = System.Windows.Forms.Application.StartupPath + "\\config\\logo.ini";

                string[] _Array = new string[10];

                _Array[0] = _clsCom.CombineString("THOI GIAN VAO: ", _checkInInfo.StrCheckInTime);
                _Array[1] = _clsCom.CombineString("LOAI XE: ", _checkInInfo.VehicleType.Name);
                _Array[2] = _clsCom.CombineString("BIEN SO: ", _checkInInfo.AlprVehicleNumber);

                _Array[3] = _clsCom.CombineString("THOI GIAN CLAIM: ", StrClaimTime);

                _Array[4] = _clsCom.CombineString("THOI GIAN LUU BAI: ", ParkingDuration);
                _Array[5] = _clsCom.CombineString("TONG HOA DON: ", Amount.ToString());
                _Array[6] = _clsCom.CombineString("PHI GIU XE: ", ParkingFee.ToString());

                _Array[7] = _clsCom.CombineString("PHI CLAIM: ", PromotionDiscount.ToString());

                _Array[8] = _clsCom.CombineString("GIA GIAM CUA TENANT: ", TenantDiscount.ToString());
                _Array[9] = _clsCom.CombineString("PHI PHAI TRA: ", FinalParkingFee.ToString());

                _clsCom.XinChao = "HEN GAP LAI QUY KHACH";
                byte[] _buffers = _clsCom.CommandESC(_Array);

                if (!_SerialPort.IsOpen)
                    _SerialPort.Open();
                _SerialPort.Write(_buffers, 0, _buffers.Length);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

        }
        private MvxCommand _commandPrintToPrinter;
        public ICommand CommandPrintToPrinter
        {
            get
            {
                _commandPrintToPrinter = _commandPrintToPrinter ?? new MvxCommand(() =>
                {
                    /*** 07-08-2016 ***/
                    if (CheckInInfo != null)
                        ShowConfirmMessage("Xách nhận in phiếu", PrinToPrinter);

                });

                return _commandPrintToPrinter;
            }
        }

        public override void Start()
        {
            Step = 0;
            WarningMessage = GetText("BillCheckout.please_tap_card");

            FetchConfigs();
        }

        private void FetchConfigs()
        {
            StatusChanged(ProgressStatus.Started);

            bool tenantsFetched = false;
            bool vouchersFetched = false;
            _server.GetTenants((tenants, ex) =>
            {
                Tenants = new ObservableCollection<Tenant>(tenants);
                tenantsFetched = true;

                if (tenantsFetched && vouchersFetched)
                    StatusChanged(ProgressStatus.Ended);
            });

            _server.GetTenantVourchers((vouchers, ex) =>
            {
                TenantVouchers = new ObservableCollection<TenantVoucher>(vouchers);
                vouchersFetched = true;

                if (tenantsFetched && vouchersFetched)
                    StatusChanged(ProgressStatus.Ended);
            });
        }

        /// <summary>
        /// Đăng ký event đọc thẻ
        /// </summary>
        public void StartCardReader()
        {
            _cardReaders = _cardReaderService.GetCardReaders();

            foreach (var item in _cardReaders)
                item.RawCardReader.ReadingCompleted += RawCardReader_ReadingCompleted;
        }

        public override void Close()
        {
            base.Close();

            foreach (var item in _cardReaders)
                item.RawCardReader.ReadingCompleted -= RawCardReader_ReadingCompleted;
        }

        int _flag = 0;
        int _lastReadTime = 0;

        /// <summary>
        /// Đọc thông tin thẻ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void RawCardReader_ReadingCompleted(object sender, CardReaderEventArgs e)
        {
            // thread-safe when doing checkout
            lock (this)
            {
                Interlocked.Increment(ref _flag);
                var curTime = Environment.TickCount;
                if (_flag > 1 && (curTime - _lastReadTime) < 3000)
                    return;
                Interlocked.Exchange(ref _flag, 1);
                _lastReadTime = curTime;
            }

            _cardId = e.CardID;
            Reset();
            GetCheckIn(_cardId);
        }

        /// <summary>
        /// Reset lại phiên làm việc mới
        /// </summary>
        public void Reset()
        {
            Step = 0;
            TenantDiscount = 0;
            PromotionDiscount = 0;
            FeeAfterClaimPromotion = 0;
            ParkingFee = 0;
            Amount = 0;
            FinalParkingFee = 0;
            BillNumber = string.Empty;
            CheckInInfo = null;
            CurrentBill = new Bill() { Date = DateTime.Now };
            CurrentCoupon = new Coupon();
            if (_bills.Count > 0)
                _bills.Clear();
            if (_coupons.Count > 0)
                _coupons.Clear();
            CurrentBillTenantName = "";
            CurrentCouponTenantName = "";
            CurrentTenantVoucherName = "";
            ClaimTime = new DateTime();
            ParkingDuration = "";
        }

        /// <summary>
        /// Tính loại promotion để có tiền giảm giá tương ứng
        /// </summary>
        public void CalculatePromotionFee()
        {
            _server.GetServerTime((serverTime, ex1) =>
            {
                PromotionDiscount = (long)_parkingFeeService.CalculatePromotion((VehicleTypeEnum)CheckInInfo.VehicleType.Id, Amount, CheckInInfo.CheckInTime, serverTime.LocalTime);
                //FeeAfterClaimPromotion =(int) (ParkingFee * 0.80f);
                FeeAfterClaimPromotion = ParkingFee + PromotionDiscount;
            });
        }

        /// <summary>
        /// Lay thong tin checkin cua the vua tap'
        /// </summary>
        /// <param name="cardId"></param>
        public void GetCheckIn(string cardId)
        {
            _cardId = cardId;
            _server.GetCheckIn(cardId, (result, ex) =>
            {
                WarningMessage = null;

                if (ex != null)
                {
                    if (ex is NotFoundException)
                    {
                        WarningMessage = GetText("checkin.card_not_found");

                    }
                    else
                    {
                        if (ex is InternalServerErrorException || ex is ServerDisconnectException)
                        {
                            WarningMessage = GetText("checkout.something_wrong");
                        }
                        if (RequestExceptionManager.GetExceptionMessage<CheckIn>(ex.Message).Key == RequestExceptionEnum.CardIsNotInUse)
                        {
                            WarningMessage = GetText("checkin.not_check_in_yet");
                        }
                    }
                }
                else
                {
                    if (result != null)
                    {
                        if (string.IsNullOrEmpty(result.ClaimPromotionId))
                        {
                            CheckInInfo = result;
                            ParkingFee = (long)CheckInInfo.CustomerInfo.ParkingFee;
                            ClaimTime = DateTime.Now;
                            ParkingDuration = ToReadableString(DateTime.Now - CheckInInfo.CheckInTime);
                            //_server.GetServerTime((serverTime, ex1) => {
                            //    ParkingFee = _parkingFeeService.CalculateFee((VehicleTypeEnum)CheckInInfo.VehicleType.Id, 0, CheckInInfo.CheckInTime, serverTime.LocalTime);
                            //});
                            WarningMessage = GetText("BillCheckout.checkin_valid");
                        }
                        else
                        {
                            WarningMessage = GetText("claimed_promotion");
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Xử lý phím tắt
        /// Enter: next step
        /// </summary>
        /// <param name="msg"></param>
        private void OnKeyPressed(KeyPressedMessage msg)
        {
            string output;
            KeyAction action = this._keymap.GetAction(msg.KeyEventArgs, out output);

            // Quay lại form trước đó
            if (action == KeyAction.GoBack)
                Step--;
            // Reset lại phiên làm việc mới
            if (action == KeyAction.ResetForm)
                Reset();

            // Nếu ở các form nhập thông tin bình thường thì Enter sẽ xác nhận thông tin vừa nhập 
            //và di chuyển tới màn hình tiếp theo
            if (Step != 1 && Step != 3)
            {
                if (Step == 0)
                    if (action == KeyAction.AlternativeChoice)
                        AddMoreBill.Execute(null);
                    else if (action == KeyAction.GoNext)
                    {
                        AddBill();
                        //Amount = _bills.Sum(b => b.BillAmount);
                    }

                if (Step == 2)
                    if (action == KeyAction.AlternativeChoice)
                        AddMoreCoupon.Execute(null);
                    else if (action == KeyAction.GoNext)
                    {
                        AddCoupon();
                        //TenantDiscount = _coupons.Sum(c => c.CouponDiscount);
                    }

                if (action == KeyAction.GoNext)
                    Step++;
            }
            else
            {
                // trong trường hợp đang ở bước chọn tích lũy hoặc chọn loại promotion (step == 2) thì
                // Enter: chọn loại promotion
                // Left Control: tích lũy
                if (Step == 1)
                {
                    if (action == KeyAction.GoNext)
                        PayCommand.Execute(null);
                    else if (action == KeyAction.AlternativeChoice)
                        StoreCommand.Execute(null);
                }
                // Hoàn tất việc tính giảm giá và gửi số tiền về cho server
                else if (Step == 3)
                {
                    if (action == KeyAction.GoNext)
                        CompleteCommand.Execute(null);
                }


            }
            /*** 07-08-2016 ***/
            if (action == KeyAction.PrintBill)
            {
                ShowConfirmMessage("Xác nhận In phiếu", PrinToPrinter);
            }
        }

        public override void Unloaded()
        {
            base.Unloaded();

            _claimSettings.Save();

            foreach (var item in _cardReaders)
                item.RawCardReader.ReadingCompleted -= RawCardReader_ReadingCompleted;

            Mvx.Resolve<IMvxMessenger>().Unsubscribe<KeyPressedMessage>(_keyPressedToken);

            _keyPressedToken = null;
        }

        /// <summary>
        /// Tích lũy
        /// </summary>
        MvxCommand _storeCommand;
        public ICommand StoreCommand
        {
            get
            {
                _storeCommand = _storeCommand ?? new MvxCommand(() =>
                {
                    _choosePay = false;
                    //PromotionDiscount = 0;
                    CalculatePromotionFee();
                    Step++;
                });
                return _storeCommand;
            }
        }

        /// <summary>
        /// Chọn loại promotion 
        /// </summary>
        MvxCommand _payCommand;
        public ICommand PayCommand
        {
            get
            {
                _payCommand = _payCommand ?? new MvxCommand(() =>
                {
                    _choosePay = true;
                    CalculatePromotionFee();
                    Step++;
                });
                return _payCommand;
            }
        }

        /// <summary>
        /// Hoàn tất việc tính giảm giá
        /// </summary>
        MvxCommand _completeCommand;
        public ICommand CompleteCommand
        {
            get
            {
                _completeCommand = _completeCommand ?? new MvxCommand(() =>
                {
                    var billData = JsonConvert.SerializeObject(_bills);
                    billData = billData.TrimStart('[');
                    billData = billData.TrimEnd(']');

                    var couponData = JsonConvert.SerializeObject(_coupons);
                    couponData = couponData.TrimStart('[');
                    couponData = couponData.TrimEnd(']');
                    //NTimes = clsLog.SetNtimes();

                    NTimes++;
                    clsLogInfo _info = new clsLogInfo();
                    _info.CheckInInfo = CheckInInfo;

                    _info.ClaimPromotionInfo = new Models.ClaimPromotion();
                    _info.ClaimPromotionInfo.BillNumber = NTimes;
                    _info.ClaimPromotionInfo.CardId = this._cardId;
                    _info.ClaimPromotionInfo.NTimes = this.NTimes;
                    this._LogBillClaimPromotion.Add(_info);

                    this.LogClaimPromotion();
                    //clsLog.SetBillNumber(DateTime.Now, NTimes, CheckInInfo.ParkingSessionId);

                    var result = "[" + billData + (string.IsNullOrEmpty(couponData) ? "" : ("," + couponData)) + "]";
                    //if (CheckInInfo != null)
                    ShowConfirmMessage("Xách nhận in phiếu", PrinToPrinter);

                    _server.UpdateClaimPromotion(UserId, BillNumber, Amount, CheckInInfo.ParkingSessionId, _cardId, CheckInInfo.VehicleNumber, CheckInInfo.VehicleTypeId, ParkingFee, PromotionDiscount, VipDiscount, TenantDiscount, FinalParkingFee, result, (ex) =>
                    {
                        InvokeOnMainThread(() =>
                        {
                            Step++;
                            
                        });

                    });

                });
                return _completeCommand;

            }
        }

        //public ICommand CompleteCommand
        //{
        //    get
        //    {
        //        _completeCommand = _completeCommand ?? new MvxCommand(() =>
        //        {

        //            var billData = JsonConvert.SerializeObject(_bills);
        //            billData = billData.TrimStart('[');
        //            billData = billData.TrimEnd(']');

        //            var couponData = JsonConvert.SerializeObject(_coupons);
        //            couponData = couponData.TrimStart('[');
        //            couponData = couponData.TrimEnd(']');

        //            var result = "[" + billData + (string.IsNullOrEmpty(couponData) ? "" : ("," + couponData)) + "]";

        //            _server.UpdateClaimPromotion(UserId, BillNumber, Amount, CheckInInfo.ParkingSessionId, _cardId, CheckInInfo.VehicleNumber, CheckInInfo.VehicleTypeId, ParkingFee, PromotionDiscount, VipDiscount, TenantDiscount, FinalParkingFee, result, (ex) =>
        //            {
        //                InvokeOnMainThread(() =>
        //                {
        //                    Step++;
        //                });
        //            });
        //        });
        //        return _completeCommand;
        //    }
        //}

        private int _NTimes;
        public int NTimes
        {
            get { return _NTimes; }
            set
            {
                if (_NTimes == value) return;
                _NTimes = value;
                RaisePropertyChanged(() => NTimes);
            }
        }

        MvxCommand _addMoreBill;
        public ICommand AddMoreBill
        {
            get
            {
                _addMoreBill = _addMoreBill ?? new MvxCommand(() =>
                {
                    AddBill();
                    Step = 0;
                });
                return _addMoreBill;
            }
        }

        private void AddBill()
        {
            var isInvalid = CurrentBill.Date.Date != DateTime.Now.Date;

            if (isInvalid)
            {
                WarningMessage = GetText("BillCheckout.billdate_invalid");
                return;
            }

            if (CurrentBill.BillAmount == 0)
                return;

            var tmp = new Bill
            {
                CompanyName = CurrentBill.CompanyName,
                BillCode = CurrentBill.BillCode,
                Date = CurrentBill.Date,
                BillAmount = CurrentBill.BillAmount
            };
            _bills.Add(tmp);
            tmp.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName.Equals("BillAmount"))
                {
                    Amount = _bills.Sum(b => b.BillAmount);
                    if (_choosePay)
                        CalculatePromotionFee();
                }
            };
            CurrentBill = new Bill() { Date = DateTime.Now };
            CurrentBillTenantName = "";
        }

        MvxCommand _addMoreCoupon;
        public ICommand AddMoreCoupon
        {
            get
            {
                _addMoreCoupon = _addMoreCoupon ?? new MvxCommand(() =>
                {
                    AddCoupon();
                    Step = 2;
                });
                return _addMoreCoupon;
            }
        }
        private void AddCoupon()
        {
            if (string.IsNullOrEmpty(CurrentCoupon.VoucherName))
                return;

            var voucher = TenantVouchers.Where(v => v.Name == CurrentCoupon.VoucherName).FirstOrDefault();
            if (voucher == null)
                return;

            var tmp = new Coupon
            {
                CompanyName = CurrentCoupon.CompanyName,
                CouponCode = CurrentCoupon.CouponCode,
                CouponDiscount = voucher.Value
            };
            tmp.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName.Equals("CouponDiscount"))
                {
                    TenantDiscount = _coupons.Sum(c => c.CouponDiscount);
                }
            };
            _coupons.Add(tmp);

            CurrentCoupon = new Coupon();
            CurrentCouponTenantName = "";
            CurrentTenantVoucherName = "";
        }

        public void RemoveBill(int idx)
        {
            this.Bills.Remove(Bills[idx]);
            //Amount = _bills.Sum(b => b.BillAmount);
        }

        public void RemoveCoupon(int idx)
        {
            this.Coupons.Remove(Coupons[idx]);
            //TenantDiscount = _coupons.Sum(c => c.CouponDiscount);
        }

        private string ToReadableString(TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}",
                span.Duration().Days > 0 ? string.Format("{0:0}n ", span.Days) : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0}g", span.Hours) : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0}p", span.Minutes) : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "< 1p";

            return formatted;
        }

        private MvxCommand _logoutCommand;
        public ICommand LogoutCommand
        {
            get
            {
                _logoutCommand = _logoutCommand ?? new MvxCommand(() =>
                {
                    StatusChanged(ProgressStatus.Started);
                    _userService.Logout(AppConfig.ClaimPromotionLaneId, (s, ex) =>
                    {
                        ShowViewModel<LoginViewModel>();
                    });
                });

                return _logoutCommand;
            }
        }
        private MvxCommand _resetCommand;
        public ICommand resetCommand
        {
            get
            {
                _resetCommand = _resetCommand ?? new MvxCommand(() =>
                {
                    Reset();
                });
                return _resetCommand;
            }
        }

        List<clsLogInfo> _LogBillClaimPromotion;
        private void LogClaimPromotion()
        {
            if (_LogBillClaimPromotion == null) return;

            string _StrJson = JsonConvert.SerializeObject(_LogBillClaimPromotion);
            clsLog.writeObject(DateTime.Now, _StrJson);
        }

        private void ReadLogClaimPromotion()
        {
            object _obj = clsLog.readObject(DateTime.Now);
            if (_obj != null)
            {
                this._LogBillClaimPromotion = JsonConvert.DeserializeObject<List<clsLogInfo>>(_obj.ToString());
            }
            if (this._LogBillClaimPromotion == null)
                this._LogBillClaimPromotion = new List<clsLogInfo>();
            if (_LogBillClaimPromotion.Count > 0)
                NTimes = this._LogBillClaimPromotion.Max(m => m.ClaimPromotionInfo.NTimes);

        }

    }
}