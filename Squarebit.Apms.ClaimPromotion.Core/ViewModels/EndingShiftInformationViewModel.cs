﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class EndingShiftInformationViewModel : BaseViewModel
    {
        IMvxMessenger _messenger;

        int _numberOfVehicleOut;
        public int NumberOfVehicleOut
        {
            get { return _numberOfVehicleOut; }
            set
            {
                if (_numberOfVehicleOut == value) return;
                _numberOfVehicleOut = value;
                RaisePropertyChanged(() => NumberOfVehicleOut);
            }
        }

        int _numberOfVehicleIn;
        public int NumberOfVehicleIn
        {
            get { return _numberOfVehicleIn; }
            set
            {
                if (_numberOfVehicleIn == value) return;
                _numberOfVehicleIn = value;
                RaisePropertyChanged(() => NumberOfVehicleIn);
            }
        }

        int _usedCards;
        public int UsedCards
        {
            get { return _usedCards; }
            set
            {
                if (_usedCards == value) return;
                _usedCards = value;
                RaisePropertyChanged(() => UsedCards);
            }
        }

        int _revenue;
        public int Revenue
        {
            get { return _revenue; }
            set
            {
                if (_revenue == value) return;
                _revenue = value;
                RaisePropertyChanged(() => Revenue);
            }
        }

        string _beginTime;
        public string BeginTime
        {
            get { return _beginTime; }
            set
            {
                if (_beginTime == value) return;
                _beginTime = value;
                RaisePropertyChanged(() => BeginTime);
            }
        }

        string _endTime;
        public string EndTime
        {
            get { return _endTime; }
            set
            {
                if (_endTime == value) return;
                _endTime = value;
                RaisePropertyChanged(() => EndTime);
            }
        }

        string _staffID;
        public string StaffID
        {
            get { return _staffID; }
            set
            {
                if (_staffID == value) return;
                _staffID = value;
                RaisePropertyChanged(() => StaffID);
            }
        }

        string _staffName;
        public string StaffName
        {
            get { return _staffName; }
            set
            {
                if (_staffName == value) return;
                _staffName = value;
                RaisePropertyChanged(() => StaffName);
            }
        }

        IUserPreferenceService _userPreferenceService;
        IUserServiceLocator _userServiceLocator;

        public EndingShiftInformationViewModel(IViewModelServiceLocator service, IMvxMessenger messenger)
            : base(service)
        {
            _userPreferenceService = Mvx.Resolve<IUserPreferenceService>();
            _userServiceLocator = Mvx.Resolve<IUserServiceLocator>();
            _messenger = messenger;
        }

        public void PublishCloseChildEvent(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<CloseChildMessage>())
            {
                _messenger.Publish(new CloseChildMessage(this, position));
            }
        }

        public void PublishShowLoginView(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
            {
                _messenger.Publish(new ShowChildMessage(this, position, typeof(LoginViewModel)));
            }
        }

        public void PublishShowCheckingLaneEvent()
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
            {
                _messenger.Publish(new ShowChildMessage(this, Section.Id, typeof(BaseLaneViewModel)));
            }
        }

        private void LogoutSuccess()
        {
            PublishCloseChildEvent(Section.Id);
            PublishShowLoginView(Section.Id);
        }

        UserShift _shift;

        public virtual void Init(ParameterKey key)
        {
            this.Section = (Section)Services.Parameter.Retrieve(key);
            this.Section.UserService.Logout(this.Section.Lane.Id, (shift, exception) =>
            {
                if (exception == null)
                {
                    StaffID = shift.User.StaffID;
                    StaffName = shift.User.DisplayName;
                    BeginTime = shift.BeginTime.ToString("dd/MM/yyyy - HH:mm");
                    EndTime = shift.EndTime.ToString("dd/MM/yyyy - HH:mm");
                    NumberOfVehicleIn = shift.NumberOfCheckIn;
                    NumberOfVehicleOut = shift.NumberOfCheckOut;
                    UsedCards = NumberOfVehicleIn - NumberOfVehicleOut;
                    Revenue = shift.Revenue;
                    _shift = shift;
                }
            });
        }

        private MvxCommand _logoutCommand;
        public ICommand LogoutCommand
        {
            get
            {
                _logoutCommand = _logoutCommand ?? new MvxCommand(() => {
                    if (_shift != null && _shift.Revenue != Revenue)
                    {
                        _shift.Revenue = Revenue;
                        this.Section.UserService.UpdateLogout(_shift, (shift, exception) => {
                            if (exception != null)
                                Mvx.Resolve<ILogService>().Log(exception, _userPreferenceService.HostSettings.LogServerIP);

                            LogoutSuccess();
                        });
                    }
                    else
                    {
                        if (_shift == null)
                            Mvx.Resolve<ILogService>().Log(new Exception("_shift is null"), _userPreferenceService.HostSettings.LogServerIP);

                        LogoutSuccess();
                    }
                });
                return _logoutCommand;
            }
        }

        private MvxCommand _backCommand;
        public ICommand BackCommand
        {
            get
            {
                _backCommand = _backCommand ?? new MvxCommand(() => {
                    Section.UserService.CurrentUser = _shift.User;
                    PublishCloseChildEvent(this.Section.Id);
                    PublishShowCheckingLaneEvent();
                });
                return _backCommand;
            }
        }    
    }
}
