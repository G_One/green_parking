﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class SearchInParkingViewModel : BaseViewModel
    {
        IServer _server;
        IStorageService _storageService;
        //IKeyService _keyService;
        MvxSubscriptionToken _keyPressedToken;
        MvxSubscriptionToken _exceptionalCheckOutToken;

        IMvxMessenger _messenger;

        #region Properties
        private List<string> _numberOfResult;
        public List<string> NumberOfResult
        {
            get { return _numberOfResult; }
            set
            {
                _numberOfResult = value;
                RaisePropertyChanged(() => NumberOfResult);
            }
        }

        private string _selectedNumberOfResult;
        public string SelectedNumberOfResult
        {
            get { return _selectedNumberOfResult; }
            set
            {
                _selectedNumberOfResult = value;
                RaisePropertyChanged(() => SelectedNumberOfResult);
            }
        }

        private List<int> _pageNumbers;
        public List<int> PageNumbers
        {
            get { return _pageNumbers; }
            set
            {
                if (_pageNumbers == value) return;
                _pageNumbers = value;
                RaisePropertyChanged(() => PageNumbers);
            }
        }

        private int _selectedPage = 1;
        public int SelectedPage
        {
            get { return _selectedPage; }
            set
            {
                if (_selectedPage == value) return;
                _selectedPage = value;
                RaisePropertyChanged(() => SelectedPage);
            }
        }

        private VehicleType _selectedVehicleType;
        public VehicleType SelectedVehicleType
        {
            get { return _selectedVehicleType; }
            set
            {
                _selectedVehicleType = value;
                RaisePropertyChanged(() => SelectedVehicleType);
            }
        }

        private bool _selectedItemIsAvailableToClaim;
        public bool SelectedItemIsAvailableToClaim
        {
            get { return _selectedItemIsAvailableToClaim; }
            set
            {
                _selectedItemIsAvailableToClaim = value;
                RaisePropertyChanged(() => SelectedItemIsAvailableToClaim);
            }
        }

        List<VehicleType> _vehicleTypes;
        public List<VehicleType> VehicleTypes
        {
            get { return _vehicleTypes; }
            set
            {
                _vehicleTypes = value;
                RaisePropertyChanged(() => VehicleTypes);
            }
        }

        List<TerminalGroup> _terminalGroups;
        public List<TerminalGroup> TerminalGroups
        {
            get { return _terminalGroups; }
            set
            {
                _terminalGroups = value;
                RaisePropertyChanged(() => TerminalGroups);
            }
        }

        IEnumerable<ParkingSessionEnum> _parkingSessionEnum;
        public IEnumerable<ParkingSessionEnum> ParkingSessionEnum
        {
            get { return _parkingSessionEnum; }
            set
            {
                _parkingSessionEnum = value;
                RaisePropertyChanged(() => ParkingSessionEnum);
            }
        }

        public ParkingSessionEnum SelectedParkingSession { get; set; }

        ParkingSession _parkingSession;
        public ParkingSession ParkingSession
        {
            get { return _parkingSession; }
            set
            {
                if (_parkingSession == value)
                    return;
                _parkingSession = value;
                RaisePropertyChanged(() => ParkingSession);
            }
        }

        ParkingSession _selectedItem;
        public ParkingSession SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem == value) return;
                _selectedItem = value;
                SelectItem(_selectedItem);
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        ObservableCollection<ParkingSession> _parkingSessions;
        public ObservableCollection<ParkingSession> ParkingSessions
        {
            get { return _parkingSessions; }
            set
            {
                if (_parkingSessions == value) return;
                _parkingSessions = value;
                RaisePropertyChanged(() => ParkingSessions);
            }
        }

        private byte[] _frontImage;
        public byte[] FrontImage
        {
            get { return _frontImage; }
            set
            {
                _frontImage = value;
                RaisePropertyChanged(() => FrontImage);
            }
        }

        private byte[] _backImage;
        public byte[] BackImage
        {
            get { return _backImage; }
            set
            {
                _backImage = value;
                RaisePropertyChanged(() => BackImage);
            }
        }

        private byte[] _miniFrontImage;
        public byte[] MiniFrontImage
        {
            get { return _miniFrontImage; }
            set
            {
                _miniFrontImage = value;
                RaisePropertyChanged(() => MiniFrontImage);
            }
        }

        private byte[] _miniBackImage;
        public byte[] MiniBackImage
        {
            get { return _miniBackImage; }
            set
            {
                _miniBackImage = value;
                RaisePropertyChanged(() => MiniBackImage);
            }
        }

        string _resultMessage;
        public string ResultMessage
        {
            get { return _resultMessage; }
            set
            {
                _resultMessage = value;
                RaisePropertyChanged(() => ResultMessage);
            }
        }

        private int _messageLevel;
        public int MessageLevel
        {
            get { return _messageLevel; }
            set
            {
                if (_messageLevel == value) return;
                _messageLevel = value;
                RaisePropertyChanged(() => MessageLevel);
            }
        }
        #endregion

        public SearchInParkingViewModel(IViewModelServiceLocator service, IMvxMessenger messenger)
            : base(service)
        {
            _server = Mvx.Resolve<IServer>();
            _storageService = Mvx.Resolve<IStorageService>();
            //_keyService = Mvx.Resolve<IKeyService>();
            ParkingSession = new ParkingSession();
            _messenger = messenger;
            _keyPressedToken = service.Messenger.Subscribe<KeyPressedMessage>(OnKeyPressed);
            _exceptionalCheckOutToken = service.Messenger.Subscribe<ExceptionalCheckOutMessage>(OnExceptionalCheckOut);
        }

        public void Init(ParameterKey key)
        {
            //this.Section = (Section)Services.Parameter.Retrieve(key);
        }

        public override void Start()
        {
            base.Start();
            NumberOfResult = new List<string> { "50", "100", "500" };
            SelectedNumberOfResult = NumberOfResult[1];

            ParkingSessionEnum = Enum.GetValues(typeof(ParkingSessionEnum)).Cast<ParkingSessionEnum>();
            SelectedParkingSession = Models.ParkingSessionEnum.InParking;

            TypeHelper.GetVehicleTypes(result =>
            {
                if (result != null)
                {
                    result.Reverse();
                    VehicleTypes = result;
                    TypeHelper.GetVehicleType((int)VehicleTypeEnum.All, type => ParkingSession.VehicleType = type);
                }
            });

            TypeHelper.GetTerminalGroups(result =>
            {
                if (result != null)
                {
                    TerminalGroups = result;
                    TypeHelper.GetTerminalGroup(1, type => ParkingSession.TerminalGroup = type);
                }
            });

            //this.Section.StartCardReader(ReadingCompleted, null);

            BuildLaneTerminalDictionary();
        }

        public override void Loaded()
        {
            base.Loaded();
        }

        public void ReadingCompleted(object sender, CardReaderEventArgs e)
        {
            this.ParkingSession.CardId = e.CardID;
            _server.GetCardInfo(ParkingSession.CardId, (card, ex) =>
            {
                if (ex == null)
                {
                    ParkingSession.CardLabel = card.Label;
                    ParkingSession.CardId = string.Empty;
                }
            });
        }

        Dictionary<int, string> _laneTerminal = null;
        public void BuildLaneTerminalDictionary()
        {
            _laneTerminal = new Dictionary<int, string>();
            TypeHelper.GetLanes(lanes =>
            {
                TypeHelper.GetTerminals(terminals =>
                {
                    if (lanes == null || terminals == null) return;
                    foreach (var item in lanes)
                    {
                        int tId = item.TerminalId;
                        string tName = terminals.Where(t => t.Id == tId).Select(t => t.Name).FirstOrDefault();
                        if (!_laneTerminal.ContainsKey(item.Id))
                            _laneTerminal.Add(item.Id, tName);
                    }
                });
            });
        }

        SearchResult _currentResult = null;
        //int _currentPage = 1;
        int _maxPage = 1;

        public void DoSearch(Action complete)
        {
            int pageSize = 2;
            if (!int.TryParse(SelectedNumberOfResult, out pageSize)) pageSize = 2;

            _server.ParkingSessionSearch(ParkingSession, SelectedParkingSession, SelectedPage, pageSize, (result, ex) =>
            {
                if (ex != null)
                {
                    ResultMessage = ex.Message;
                    MessageLevel = 3;
                    return;
                }
                else
                {
                    _currentResult = result;
                    _maxPage = _currentResult.Total / pageSize;
                    if (_currentResult.Total % pageSize > 0) _maxPage += 1;

                    List<int> lst = new List<int>();
                    for (int i = 1; i <= _maxPage; i++)
                    {
                        lst.Add(i);
                    }
                    PageNumbers = lst;

                    InvokeOnMainThread(() =>
                    {
                        if (ex != null)
                        {
                            ResultMessage = ex.Message;
                            MessageLevel = 3;
                            return;
                        }

                        ParkingSessions = new ObservableCollection<Models.ParkingSession>(_currentResult.ParkingSessions);
                        foreach (var item in ParkingSessions)
                        {
                            int id = 1;
                            int.TryParse(item.CheckInLane, out id);
                            item.TerminalName = _laneTerminal[id];
                        }

                        if (ParkingSessions != null && ParkingSessions.Count > 0)
                        {
                            SelectedItem = ParkingSessions[0];
                            ResultMessage = string.Format(GetText("search.number_of_results"), ParkingSessions.Count);
                            MessageLevel = 0;
                        }
                        else
                        {
                            ResultMessage = GetText("search.not_found");
                            MessageLevel = 3;
                        }

                        ParkingSession.CardId = string.Empty;

                        if (complete != null)
                            complete();
                    });
                }
            });
        }

        public void GoNext()
        {
            int tmp = SelectedPage;
            tmp++;
            if (tmp > _maxPage)
                tmp = _maxPage;
            if (tmp != SelectedPage && tmp != 0)
            {
                SelectedPage = tmp;
                DoSearch(() =>
                {

                });
            }
        }

        public void GoBack()
        {
            int tmp = SelectedPage;
            tmp--;
            if (tmp <= 1)
                tmp = 1;
            if (tmp != SelectedPage && tmp != 0)
            {
                SelectedPage = tmp;
                DoSearch(() =>
                {

                });
            }
        }

        public void Search(Action complete)
        {
            int limit = 100;
            if (!int.TryParse(SelectedNumberOfResult, out limit)) limit = 0;
            _server.ParkingSessionSearchAdvance(ParkingSession, SelectedParkingSession, (result, ex) =>
            {
                InvokeOnMainThread(() =>
                {
                    if (ex != null)
                    {
                        ResultMessage = ex.Message;
                        MessageLevel = 3;
                        return;
                    }

                    ParkingSessions = new ObservableCollection<Models.ParkingSession>(result);
                    foreach (var item in ParkingSessions)
                    {
                        int id = 1;
                        int.TryParse(item.CheckInLane, out id);
                        item.TerminalName = _laneTerminal[id];
                    }

                    //ParkingSessions = Filter(ParkingSessions, "Da");

                    if (ParkingSessions != null && ParkingSessions.Count > 0)
                    {
                        SelectedItem = ParkingSessions[0];
                        ResultMessage = string.Format(GetText("search.number_of_results"), ParkingSessions.Count);
                        MessageLevel = 0;
                    }
                    else
                    {
                        ResultMessage = GetText("search.not_found");
                        MessageLevel = 3;
                    }

                    ParkingSession.CardId = string.Empty;

                    if (complete != null)
                        complete();
                });
            }, limit);
        }

        private ObservableCollection<ParkingSession> Filter(ObservableCollection<ParkingSession> origin, string pattern)
        {
            if (string.IsNullOrEmpty(pattern)) return origin;

            var rs = origin.Where(i => i.TerminalName.Contains(pattern));
            return new ObservableCollection<ParkingSession>(rs);
        }

        private void SelectItem(ParkingSession parkingSession)
        {
            if (parkingSession == null)
            {
                MiniFrontImage = null;
                MiniBackImage = null;
                FrontImage = null;
                BackImage = null;
                return;
            }

            if (parkingSession.CheckOutTime > 0)
            {
                _storageService.LoadImage(parkingSession.CheckOutFrontImage, string.Empty, (fiBytes, ex) =>
                {
                    MiniFrontImage = fiBytes;
                });
                _storageService.LoadImage(parkingSession.CheckOutBackImage, string.Empty, (biBytes, ex) =>
                {
                    MiniBackImage = biBytes;
                });

                _storageService.LoadImage(parkingSession.CheckInFrontImage, string.Empty, (biBytes, ex) =>
                {
                    FrontImage = biBytes;
                });
                _storageService.LoadImage(parkingSession.CheckInBackImage, string.Empty, (biBytes, ex) =>
                {
                    BackImage = biBytes;
                });
            }
            else
            {
                MiniFrontImage = null;
                MiniBackImage = null;
                _storageService.LoadImage(parkingSession.CheckInFrontImage, string.Empty, (fiBytes, ex) =>
                {
                    FrontImage = fiBytes;
                });
                _storageService.LoadImage(parkingSession.CheckInBackImage, string.Empty, (biBytes, ex) =>
                {
                    BackImage = biBytes;
                });
            }

            SelectedItemIsAvailableToClaim = CanCheckOut(parkingSession);
        }

        private bool CanCheckOut(ParkingSession parkingSession)
        {
            if (parkingSession == null) return false;

            if (parkingSession.CheckOutTime == -1)
                return true;

            return false;
        }

        MvxCommand _searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                _searchCommand = _searchCommand ?? new MvxCommand(() =>
                {
                    //Search(null);
                    DoSearch(null);
                });

                return _searchCommand;
            }
        }

        MvxCommand _backCommand;
        public ICommand BackCommand
        {
            get
            {
                _backCommand = _backCommand ?? new MvxCommand(() => { ShowViewModel<BillCheckoutViewModel>(); });

                return _backCommand;
            }
        }

        MvxCommand _selectPageCommand;
        public ICommand SelectPageCommand
        {
            get
            {
                _selectPageCommand = _selectPageCommand ?? new MvxCommand(() =>
                {
                    DoSearch(null);
                });
                return _selectPageCommand;
            }
        }

        MvxCommand _goNextCommand;
        public ICommand GoNextCommand
        {
            get
            {
                _goNextCommand = _goNextCommand ?? new MvxCommand(() =>
                {
                    GoNext();
                });
                return _goNextCommand;
            }
        }

        MvxCommand _goBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                _goBackCommand = _goBackCommand ?? new MvxCommand(() =>
                {
                    GoBack();
                });
                return _goBackCommand;
            }
        }

        MvxCommand<ParkingSession> _claimPromotionCommand;
        public MvxCommand<ParkingSession> ClaimPromotionCommand
        {
            get
            {
                _claimPromotionCommand = _claimPromotionCommand ?? new MvxCommand<ParkingSession>((parkingSession) =>
                {
                    ShowViewModel<BillCheckoutViewModel>(Services.Parameter.Store(parkingSession.CardId));
                });

                return _claimPromotionCommand;
            }
        }

        MvxCommand<string> _editEndingCommand;
        public MvxCommand<string> EditEndingCommand
        {
            get
            {
                _editEndingCommand = _editEndingCommand ?? new MvxCommand<string>(value =>
                {
                    //_server.UpdateCheckIn(new CheckIn { CardId = SelectedItem.CardId, VehicleNumber = value }, (result, ex) => {
                    //    SelectedItem.VehicleNumber = value;
                    //});
                    SelectedItem.VehicleNumber = value;
                    _server.UpdateParkingSession(SelectedItem, ex => { });
                });
                return _editEndingCommand;
            }
        }

        public void PublishCloseChildEvent(SectionPosition position)
        {
            if (_messenger.HasSubscriptionsFor<CloseChildMessage>())
            {
                _messenger.Publish(new CloseChildMessage(this, position));
            }
        }

        public void PublishShowCheckingLaneEvent()
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
            {
                _messenger.Publish(new ShowChildMessage(this, Section.Id, typeof(BaseLaneViewModel)));
            }
        } 

        public void PublishShowExceptionalCheckout(ParkingSession data)
        {
            if (_messenger.HasSubscriptionsFor<ShowChildMessage>())
                _messenger.Publish(new ShowChildMessage(this, Section.Id, typeof(ExceptionalCheckOutViewModel), data));
        }

        public override void Close()
        {
            base.Close();
        }

        public override void Unloaded()
        {
            base.Unloaded();
            //this.Section.StopCardReader(ReadingCompleted, null);

            IMvxMessenger messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Unsubscribe<KeyPressedMessage>(_keyPressedToken);
            _keyPressedToken = null;
        }

        protected void OnExceptionalCheckOut(ExceptionalCheckOutMessage msg)
        {
            if (ParkingSessions.Contains(msg.CheckedOutItem))
            {
                InvokeOnMainThread(() =>
                {
                    ParkingSessions.Remove(msg.CheckedOutItem);
                    if (ParkingSessions != null && ParkingSessions.Count > 0)
                    {
                        SelectedItem = ParkingSessions.Count > 0 ? ParkingSessions[0] : null;
                        SelectItem(SelectedItem);
                    }
                });
            }
        }

        public void KeyPressed(object sender, KeyEventArgs e)
        {
            OnKeyPressed(new KeyPressedMessage(sender, e));
        }

        protected void OnKeyPressed(KeyPressedMessage msg)
        {
            //    string output;
            //    KeyAction action = this.Section.KeyMap.GetAction(msg.KeyEventArgs, out output, typeof(SearchInParkingViewModel));

            //    switch (action)
            //    {
            //        case KeyAction.DoSearch:
            //            {
            //                Search(null);
            //                break;
            //            }
            //        case KeyAction.Back:
            //            {
            //                BackCommand.Execute(null);
            //                break;
            //            }
            //        case KeyAction.ExceptionalCheckout:
            //            {
            //                ExceptionalCheckoutCommand.Execute(SelectedItem);
            //                break;
            //            }
            //    }
        }
    }
}