﻿using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class TenantDiscountViewModel : BaseViewModel
    {
        public TenantDiscountViewModel(IViewModelServiceLocator services, IUserPreferenceService userPreferenceService, IServer server)
            : base(services)
        {

        }

        public void Init(ParameterKey key)
        {
        }

        public override void Start()
        {
        }

        MvxCommand _startCommand;
        public ICommand StartCommand
        {
            get
            {
                _startCommand = _startCommand ?? new MvxCommand(() => {
                    ShowViewModel<BillCheckoutViewModel>();
                });
                return _startCommand;
            }
        }
    }
}
