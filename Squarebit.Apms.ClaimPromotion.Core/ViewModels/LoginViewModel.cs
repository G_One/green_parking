﻿using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Squarebit.Devices.Dal;
using Cirrious.MvvmCross.Plugins.Messenger;
using Squarebit.Apms.Terminal.Core.Models;
using Cirrious.CrossCore;
using System.Threading;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class LoginSuccessMessage : MvxMessage
    {
        public LoginSuccessMessage(object sender) : base(sender) { }
    }

    //public class LogoutSuccessMessage : MvxMessage
    //{
    //    public LogoutSuccessMessage(object sender) : base(sender) { }
    //}

    public class LoginViewModel : BaseViewModel
    {
        IRunModeManager _modeManager;
        private IMvxMessenger _messenger;
        private IUserServiceLocator _userServiceLocator;
        private IResourceLocatorService _resourceLocator;
        private IUserService _userService;
        List<CardReaderWrapper> _cardReaders;
        ICardReaderService _cardReaderService;

        private string _username;
        private string _password;
		private string _resultMessage;

        public const string msgCardReaderDisconnect = "error.cardreader_disconnect";
        public const string msgInvalid = "login.invalid";
        public const string msgServerError = "error.server_error";
        public const string msgServerDisconnect = "error.server_disconnect";

		public string ResultMessage
		{
			get { return _resultMessage; }
			set
			{
				if (_resultMessage == value)
					return;

				_resultMessage = value;
				RaisePropertyChanged(() => ResultMessage);
                RaisePropertyChanged(() => HasError);
			}
		}

        public bool HasError
        {
            get { return _resultMessage != null; }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                if (_username == value) return;
                _username = value;
                RaisePropertyChanged(() => Username);
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (_password == value) return;
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public LoginViewModel(IViewModelServiceLocator services
            , IUserServiceLocator userServiceLocator, IResourceLocatorService resourceLocator
            , ICardReaderService cardReaderService)
            : base(services)
        {
            this._userServiceLocator = userServiceLocator;
            this._resourceLocator = resourceLocator;
            this._messenger = services.Messenger;
            _modeManager = services.ModeManager;
            _cardReaderService = cardReaderService;

            this.Username = "support";
            this.Password = "gp142536";
        }

        public void Init(ParameterKey key)
        {
            // Attach card reader callback
            //Section.TemporaryDirection = Section.Direction;
            //Section.SetupCardReader();
            _userService = _userServiceLocator.GetUserService(AppConfig.ClaimPromotionSection.Id);
            //if (!this.Section.StartCardReader(OnRFIDCardReceived, null))
            //{
            //    ResultMessage = msgCardReaderDisconnect;
            //}
            //else
            //{
            //    //_cardReader.ReadingCompleted += OnRFIDCardReceived;
            //    ResultMessage = null;
            //}
        }

        public void StartCardReader()
        {
            _cardReaders = _cardReaderService.GetCardReaders();

            foreach (var item in _cardReaders)
                item.RawCardReader.ReadingCompleted += OnRFIDCardReceived;
        }

        public override void Start()
        {
            base.Start();

            DoLoginForTesting();
        }

        private void DoLoginForTesting()
        {
            if (_modeManager.ArgumentParams.Mode == RunMode.Testing)
            {
                Mvx.Resolve<IWebApiTestingServer>().RegisterLane(Section, lane => {
                    if (lane != null && lane.Enabled)
                    {
                        this.Section.Direction = lane.Direction;
                        Username = "admin";
                        Password = "nopass";
                        LoginCommand.Execute(null);
                    }
                });
            }
        }

		private MvxCommand _loginCommand;
		public ICommand LoginCommand {
			get {
				_loginCommand = _loginCommand ?? new MvxCommand(() =>
				{
                    if (Username == null || Password == null || Username.Length == 0 || Password.Length == 0)
                    {
                        ResultMessage = msgInvalid;
                    }
                    else
                    {
                        ResultMessage = null;
                        StatusChanged(ProgressStatus.Started);
                        _userService.Login(Username, Password, AppConfig.ClaimPromotionLaneId, OnLoginResultReceived);
                    }
				});

				return _loginCommand;
			}
		}

        private void OnRFIDCardReceived(object sender, CardReaderEventArgs e)
        {
            if (e == null || e.CardID == null)
            {
                ResultMessage = msgInvalid;
            }
            else
            {
                ResultMessage = null;
                StatusChanged(ProgressStatus.Started);
                _userService.Login(e.CardID, AppConfig.ClaimPromotionLaneId, OnLoginResultReceived);
            }
        }

        private void OnLoginResultReceived(Exception exception)
        {
            StatusChanged(ProgressStatus.Ended);
            if (exception == null)
            {
                ShowViewModel<BillCheckoutViewModel>();
            }
            else if(exception is LoginInvalidException)
            {
                ResultMessage = msgInvalid;
            }
            else if (exception is ServerErrorException)
            {
                ResultMessage = msgServerError;
            }
            else if(exception is ServerDisconnectException)
            {
                ResultMessage = msgServerDisconnect;
            }
        }
        
        public override void Close()
        {
            base.Close();

            foreach (var item in _cardReaders)
                item.RawCardReader.ReadingCompleted -= OnRFIDCardReceived;
        }

        public override void Unloaded()
        {
            base.Unloaded();
            
            foreach (var item in _cardReaders)
                item.RawCardReader.ReadingCompleted -= OnRFIDCardReceived;
        }
    }
}
