﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class SubLaneConfigurationViewModel : BaseViewModel
    {
        IResourceLocatorService _resourceLocatorService;

        IUserPreferenceService _userPreferenceService;

        ICardReaderService _cardReaderService;

		IBarrierDeviceManager _barrierDeviceManager;

        IServer _server;

        private const string DEFAULT_BARRIER_PORT = "1111";

        #region Properties
        bool _isDetecting;
        public bool IsDetecting
        {
            get { return _isDetecting; }
            set
            {
                _isDetecting = value;
                RaisePropertyChanged(() => IsDetecting);
            }
        }

        List<CardReaderWrapper> _cardReaders;
        public List<CardReaderWrapper> CardReaders
        {
            get { return _cardReaders; }
            set
            {
                if (_cardReaders == value) return;
                _cardReaders = value;
                RaisePropertyChanged(() => CardReaders);
            }
        }

        List<Video_Device> _webcams;
        public List<Video_Device> Webcams
        {
            get { return _webcams; }
            set
            {
                if (_webcams == value) return;
                _webcams = value;
                RaisePropertyChanged(() => Webcams);
            }
        }

        private string _laneId;
        public string LaneId
        {
            get { return _laneId; }
            set
            {
                if (_laneId == value) return;
                _laneId = value;
            }
        }

        private Section _section;
        public new Section Section
        {
            get { return _section; }
            set
            {
                if (_section == value) return;
                _section = value;
                LaneId = _section.Id.ToString();
                SetupView();
            }
        }

        private LaneDirection _direction;
        public LaneDirection Direction
        {
            get { return _direction; }
            set
            {
                if (_direction == value) return;
                _direction = value;
            }
        }
                
        string _laneName;
        public string LaneName
        {
            get { return _laneName; }
            set
            {
                if (_laneName == value) return;

                _laneName = value;
                this.Section.LaneName = _laneName;
                RaisePropertyChanged(() => LaneName);
            }
        }

        string _frontInCamera;
        public string FrontInCamera
        {
            get { return _frontInCamera; }
            set
            {
                if (_frontInCamera == value)
                    return;

                _frontInCamera = value;
                RaisePropertyChanged(() => FrontInCamera);
            }
        }

        private VehicleTypeEnum _selectedVehicleType;
        public VehicleTypeEnum SelectedVehicleType
        {
            get { return _selectedVehicleType; }
            set
            {
                _selectedVehicleType = value;
                RaisePropertyChanged(() => SelectedVehicleType);
            }
        }

        IEnumerable<VehicleTypeEnum> _vehicleTypes;
        public IEnumerable<VehicleTypeEnum> VehicleTypes
        {
            get { return _vehicleTypes; }
            set
            {
                _vehicleTypes = value;
                RaisePropertyChanged(() => VehicleTypes);
            }
        }

        private IEnumerable<string> _barriers;
		public IEnumerable<string> Barriers
		{
            get
            {
                if (_barriers != null)
                    return _barriers;

                List<string> barrierNames = new List<string>();
                barrierNames.Add("");
                barrierNames.AddRange(_barrierDeviceManager.GetAllDeviceNames());
                _barriers = barrierNames;
                return _barriers;
            }
		}


		private string _selectedBarrier;
		public string SelectedBarrier
		{
			get { return _selectedBarrier; }
			set
			{
				_selectedBarrier = value;
				RaisePropertyChanged(() => SelectedBarrier);
			}
		}

        private string _barrierPort;
        public string BarrierPort
        {
            get { return _barrierPort; }
            set
            {
                _barrierPort = value;
                RaisePropertyChanged(() => BarrierPort);
            }
        }

        string _backInCamera;
        public string BackInCamera
        {
            get { return _backInCamera; }
            set
            {
                if (_backInCamera == value)
                    return;

                _backInCamera = value;
                RaisePropertyChanged(() => BackInCamera);
            }
        }

        string _frontOutCamera;
        public string FrontOutCamera
        {
            get { return _frontOutCamera; }
            set
            {
                if (_frontOutCamera == value)
                    return;

                _frontOutCamera = value;
                RaisePropertyChanged(() => FrontOutCamera);
            }
        }

        string _backOutCamera;
        public string BackOutCamera
        {
            get { return _backOutCamera; }
            set
            {
                if (_backOutCamera == value)
                    return;

                _backOutCamera = value;
                RaisePropertyChanged(() => BackOutCamera);
            }
        }

        string _selectedCardReader;
        public string SelectedCardReader
        {
            get { return _selectedCardReader; }
            set
            {
                if (_selectedCardReader == value)
                    return;

                _selectedCardReader = value;
                RaisePropertyChanged(() => SelectedCardReader);
            }
        }
        #endregion

        public SubLaneConfigurationViewModel(IViewModelServiceLocator service
			, IResourceLocatorService resourceLocatorService
			, ICardReaderService cardReaderService
			, IUserPreferenceService userPreferenceService
			, IServer server
			, IBarrierDeviceManager barrierDeviceManager)
            : base(service)
        {
			_resourceLocatorService = resourceLocatorService;
			_cardReaderService = cardReaderService;
			_userPreferenceService = userPreferenceService;
			_server = server;
			_barrierDeviceManager = barrierDeviceManager;
        }

        private void SetupView()
        {
            this.LaneName = this.Section.LaneName;
            this.SelectedCardReader = Section.CardReader != null ? Section.CardReader.SerialNumber : null;
            VehicleTypes = Enum.GetValues(typeof(VehicleTypeEnum)).Cast<VehicleTypeEnum>();
            //this.SelectedVehicleType = VehicleTypeEnum.All;
            TypeHelper.GetVehicleType(Section.VehicleTypeId, result => {
                if (result != null)
                    SelectedVehicleType = (VehicleTypeEnum)result.Id;
            });
            
            //TypeHelper.GetVehicleTypes(result => {
            //    VehicleTypes = result;
            //    SelectedVehicleType = VehicleTypes.Where(t => t.Id == Section.VehicleTypeId).FirstOrDefault();
            //});
            if (Section.FrontInCamera != null && Section.FrontInCamera != null &&
                Section.BackInCamera != null && Section.BackOutCamera != null)
            {
                this.FrontInCamera = this.Section.FrontInCamera.IP;
                this.FrontOutCamera = this.Section.FrontOutCamera.IP;
                this.BackInCamera = this.Section.BackInCamera.IP;
                this.BackOutCamera = this.Section.BackOutCamera.IP;
            }
            this.Direction = this.Section.Direction;

			if (this.Section.BarrierName != null)
				SelectedBarrier = this.Section.BarrierName;
            
            this.BarrierPort = this.Section.BarrierPort;
            if (string.IsNullOrEmpty(BarrierPort))
                BarrierPort = DEFAULT_BARRIER_PORT;
        }

        public void Init(ParameterKey key)
        {
            _server = Mvx.Resolve<IServer>();
        }

        public override void Start()
        {
            base.Start();
            IsDetecting = false;
            //_resourceLocatorService.RegisterSection(section, lane);
        }

        public void LaneNameChanged(string name)
        {
            LaneName = name;
        }

        public void GetCardReaderInfo()
        {
            this.CardReaders = _cardReaderService.GetCardReaders();
        }

        public void GetWebcamInfo()
        {
            this.Webcams = WebcamFactoryService.WebCams.ToList();
        }

        private string _saveResultMessage;
        public string SaveResultMessage
        {
            get { return _saveResultMessage; }
            set { _saveResultMessage = value; RaisePropertyChanged(() => SaveResultMessage); }
        }

        MvxCommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                _saveCommand = _saveCommand ?? new MvxCommand(() =>
                {
                    Save(null);
                    SaveResultMessage = GeneralConfigViewModel.msgSaved;
                });

                return _saveCommand;
            }
        }

        public void Save(Action complete)
        {
            Dictionary<SectionPosition, Section> aaa = _userPreferenceService.SystemSettings.Sections;
            Section a1 = aaa[SectionPosition.Lane1];
            Section a2 = aaa[SectionPosition.Lane2];
            if (this.Section.Lane == null)
                this.Section.Lane = new Models.Lane();
            this.Section.Lane.Name = this.LaneName;
			this.Section.Lane.VehicleTypeId = (int)SelectedVehicleType; 
            this.Section.Lane.Direction = this.Direction;
            this.Section.Lane.Enabled = true;
            this.Section.Direction = this.Direction;
            this.Section.BarrierName = this.SelectedBarrier;
            this.Section.BarrierPort = this.BarrierPort;
            AttachDevices();
            _userPreferenceService.SystemSettings.UpdateSection(this.Section as Section);
            _userPreferenceService.SystemSettings.Save();
            if (complete != null) complete();
        }

        public void AttachDevices()
        {
            this.Section.VehicleTypeId = (int)SelectedVehicleType;
            this.Section.AttachCamera(FrontInCamera, LaneDirection.In, CameraPosition.Front);
            this.Section.AttachCamera(BackInCamera, LaneDirection.In, CameraPosition.Back);
            this.Section.AttachCamera(FrontOutCamera, LaneDirection.Out, CameraPosition.Front);
            this.Section.AttachCamera(BackOutCamera, LaneDirection.Out, CameraPosition.Back);

            this.Section.AttachCardReader(this.SelectedCardReader);
        }

        private MvxCommand _detectCardCommand;
        public ICommand DetectCardCommand
        {
            get
            {
				_detectCardCommand = _detectCardCommand ?? new MvxCommand(() =>
                {
                    if (!IsDetecting)
                    {
                        IsDetecting = true;
                        foreach (var item in this.CardReaders)
                            item.RawCardReader.ReadingCompleted += RawCardReader_ReadingCompleted;
                    }
                    else
                    {
                        IsDetecting = false;
                        foreach (var item in this.CardReaders)
                            item.RawCardReader.ReadingCompleted -= RawCardReader_ReadingCompleted;
                    }
                });

				return _detectCardCommand;
            }
        }

		private MvxCommand _detectBarrierCommand;
		public ICommand DetectBarrierCommand
		{
			get
			{
				_detectBarrierCommand = _detectBarrierCommand ?? new MvxCommand(() =>
				{
                    var barrier = _barrierDeviceManager.GetDevice(SelectedBarrier, BarrierPort);
					if (barrier != null)
						barrier.Open();
				});

				return _detectBarrierCommand;
			}
		}

        void RawCardReader_ReadingCompleted(object sender, CardReaderEventArgs e)
        {
            string serialNo = e.CardReader.CardReaderInfo.SerialNumber;
            this.SelectedCardReader = this.CardReaders.Where(c => c.SerialNumber.Equals(serialNo)).FirstOrDefault().SerialNumber;
        }
    }
}
