﻿using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core
{
    public class AppConfig
    {
        // Web API http://118.69.32.139:9191/api/docs/
        public static class WebApi
        {
            // Base url
            public static string RootUrl { get; set; }
            public static string BaseApiUrl { get; set; }           

            public static string LogUrl { get; private set; }

            public static string ClientId { get; private set; }            

            static WebApi()
            {
                //#if TESTING
                //RootUrl = "https://115.79.51.140:8001";
                //BaseApiUrl = "https://115.79.51.140:8001/v1";
                //#else
                RootUrl = "https://api.manga360.net";
                BaseApiUrl = "https://api.manga360.net/v1";
                //#endif
            }
        }

        public static int ClaimPromotionLaneId = 999999;
        public static ISection ClaimPromotionSection = new Section { Id = 0, Lane = new Lane { Id = AppConfig.ClaimPromotionLaneId } };
    }
}
