﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Squarebit.Devices.Dal;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Forms.Integration;
using System.Windows.Forms;
using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Services;
using System.Drawing.Drawing2D;
using Squarebit.Apms.Terminal.Core.Utility;
using Cirrious.CrossCore.Core;
using Squarebit.APS.Devices.HikVision;
using Squarebit.Apms.Terminal.Wpf.UI;
using Squarebit.Apms.Terminal.Core.Utilities;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;

namespace Squarebit.Apms.Terminal.Wpf.Devices
{
    public class MyImageBox: ImageBox
    {
        public MyImageBox()
        {
            PanableAndZoomable = false;
        }
    }

    public class Webcam : ICamera
    {
        public int DeviceId { get; set; }

        public ZoomFactor ZoomFactor { get; set; }

        public event FrameEventHandler OnFrameReceived;

        public event ZoomEventHandler OnZoomReceived;

        private Capture _capture = null;

        public DeviceInfo DeviceInfo { get; set; }

        MyImageBox PicBox { get; set; }

        public CameraOverlay Overlay { get; private set; }

        string _ipAddress;
        public string IPAddress
        {
            get { return _ipAddress; }
            set
            {
                _ipAddress = value;

                //if (this.Overlay != null)
                //    this.Overlay.TextContent = _ipAddress;

                SetOverlay(_ipAddress);

                if (DeviceInfo != null)
                    DeviceInfo.IP = _ipAddress;
            }
        }

        void SetOverlay(string content)
        {
            if (this.Overlay != null)
            {
                Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() => {
                    this.Overlay.TextContent = content;
                });
            }
        }

        public Webcam()
        {
            DeviceInfo = new DeviceInfo();
            _capture = new Capture();
            _capture.ImageGrabbed += ProcessFrame;
        }

        public void FrameReceived(FrameEventArgs arg)
        {
        }

        public void Start()
        {
            _capture.Start();
        }

        public void Pause()
        {
        }

        public void Continue()
        {
        }

        public void Stop()
        {
        }

        public void Dispose()
        {
            if (_capture != null)
                _capture.Dispose();
        }

        public byte[] CaptureImage(string waterMark = "")
        {
            try
            {
                byte[] bytes = CaptureZoom(waterMark);
                SaveZoomState();
                return bytes;
            }
            catch (Exception ex)
            {
                Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                return null;
            }
        }

        public byte[] CaptureZoom(string waterMark = "")
        {
            Bitmap bmp = null;
            lock (this)
            {
                try
                {
                    bmp = GetCameraBitMap();
                    return bmp.ToByteArray(ImageFormat.Jpeg);
                }
                catch (Exception ex)
                {
                    Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                    return null;
                }
                finally
                {
                    if (bmp != null)
                    {
                        bmp.Dispose();
                        bmp = null;
                    }
                }
            }
        }

        public Image CaptureImage()
        {
            var img = GetCameraBitMap();
            return (System.Drawing.Image)img;
        }

        public Bitmap GetCameraBitMap()
        {
            Bitmap bmp = null;
            lock (this)
            {
                try
                {
                    IntPtr pt = IntPtr.Zero;

                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() => {
                        pt = PicBox.Handle;
                    });

                    IntPtr bmpDC;
                    IntPtr cameraDC;
                    RECT windowRect = new RECT(0, 0, 0, 0);
                    User32.GetWindowRect(pt, ref windowRect);

                    cameraDC = User32.GetWindowDC(pt);

                    if (cameraDC != IntPtr.Zero)
                    {
                        bmp = new Bitmap(windowRect.Width, windowRect.Height);

                        using (Graphics g = System.Drawing.Graphics.FromImage(bmp))
                        {
                            bmpDC = g.GetHdc();
                            GDI32.BitBlt(bmpDC, 0, 0, windowRect.Width, windowRect.Height, cameraDC, 0, 0, TernaryRasterOperations.SRCCOPY);
                            g.ReleaseHdc(bmpDC);
                        }
                        User32.ReleaseDC(pt, cameraDC);
                        return bmp;
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                    return null;
                }
                finally
                {
                    if (bmp != null)
                    {
                        //bmp.Dispose();
                        //bmp = null;
                    }
                }
            }
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            Image<Bgr, Byte> frame = _capture.RetrieveBgrFrame();
            PicBox.Image = frame;
        }

        public void Setup(string ip)
        {
            this.Container = new System.Windows.Controls.UserControl();
            System.Windows.Forms.Panel panel = new System.Windows.Forms.Panel();
            panel.Location = new System.Drawing.Point(0, 0);
            panel.Dock = System.Windows.Forms.DockStyle.Fill;

            WindowsFormsHost wfhost = new WindowsFormsHost();
            PicBox = new MyImageBox();
            PicBox.SizeMode = PictureBoxSizeMode.Zoom;
            PicBox.HorizontalScrollBar.Enabled = false;
            PicBox.VerticalScrollBar.Enabled = false;
            PicBox.Location = new System.Drawing.Point(0, 0);
            PicBox.Dock = System.Windows.Forms.DockStyle.Fill;
            

            panel.Controls.Add(PicBox);
            wfhost.Child = panel;
            Container.Content = wfhost;

            Start();

            Overlay = new CameraOverlay()
            {
                Container = this.Container,
                TextContent = DeviceInfo.IP,
            };

            IPAddress = ip;
        }

        public void Load(string ip, ZoomFactor zoomFactor)
        {
            if (PicBox == null)
            {
                Setup(ip);
            }
        }

        public void SaveZoomState()
        {
        }

        public void ChangeIPAddress(string ip)
        {
        }

        public System.Windows.Controls.UserControl Container { get; set; }
    }
}
