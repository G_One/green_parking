﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Squarebit.Devices.Dal;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Forms.Integration;
using System.Windows.Forms;
using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Services;
using System.Drawing.Drawing2D;
using Squarebit.Apms.Terminal.Core.Utility;
using Cirrious.CrossCore.Core;
using Squarebit.APS.Devices.HikVision;
using Squarebit.Apms.Terminal.Wpf.UI;
using Squarebit.Apms.Terminal.Core.Utilities;

namespace Squarebit.Apms.Terminal.Wpf.Devices
{
    public enum DISPLAY_MODE
    {
        Callback = 0,
        Direct = 1
    }

    public class DeviceInfo
    {
        public string IP { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class HIKCamera : ICamera
    {
        public DeviceInfo DeviceInfo { get; set; }

        private int _handle;
        private IntPtr _playWndHandle = IntPtr.Zero;
        private int _port;

        private bool _isPaused;
        public bool IsPaused { get { return _isPaused; } }

        private bool _isStarted;

        private static bool _sdkInit = false;

        private byte[] rgbData = null;

        private PlayCtrl.DISPLAYCBFUN _func;
        private CHCNetSDK.REALDATACALLBACK _realDataCallbackFunc;

        public CameraOverlay Overlay { get; private set; }

        string _ipAddress;
        public string IPAddress
        {
            get { return _ipAddress; }
            set
            {
                _ipAddress = value;

                //if (this.Overlay != null)
                //    this.Overlay.TextContent = _ipAddress;

                SetOverlay(_ipAddress);

                if (DeviceInfo != null)
                    DeviceInfo.IP = _ipAddress;
            }
        }

        void SetOverlay(string content)
        {
            if (this.Overlay != null)
            {
                Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() => {
                    this.Overlay.TextContent = content;
                });
            }
        }

        /// <summary>
        /// Starts capturing and sending image data.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Start()
        {
            if (!_isStarted)
                StartDeviceWithMode(DISPLAY_MODE.Direct);
        }

        /// <summary>
        /// Add window handle to display device's image
        /// </summary>
        /// <param name="wndHandle"></param>
        public void AddWndHandle(IntPtr wndHandle)
        {
            _playWndHandle = wndHandle;
        }

        /// <summary>
        /// Play device
        /// </summary>
        private bool StartDeviceWithMode(DISPLAY_MODE mode)
        {
            CHCNetSDK.NET_DVR_CLIENTINFO lpClientInfo = new CHCNetSDK.NET_DVR_CLIENTINFO();
            lpClientInfo.lChannel = 1;
            lpClientInfo.lLinkMode = 1;
            lpClientInfo.sMultiCastIP = null;

            IntPtr pUser = new IntPtr();

            int handle = -1;

            if (mode == DISPLAY_MODE.Callback)
            {
                lpClientInfo.hPlayWnd = IntPtr.Zero;

                _func = new PlayCtrl.DISPLAYCBFUN(RemoteDisplayCBFun);

                _realDataCallbackFunc = new CHCNetSDK.REALDATACALLBACK(RealDataCallBack);

                handle = CHCNetSDK.NET_DVR_RealPlay_V30(DeviceId, ref lpClientInfo, _realDataCallbackFunc, pUser, 1);
            }
            else if (mode == DISPLAY_MODE.Direct)
            {
                lpClientInfo.hPlayWnd = _playWndHandle;

                handle = CHCNetSDK.NET_DVR_RealPlay_V30(DeviceId, ref lpClientInfo, null, pUser, 1);
            }

            if (handle > -1)
            {
                _handle = handle;
                _isStarted = true;
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// Convert YV12 byte array to RGB byte array
        /// </summary>
        public unsafe static void ConvertYUVtoRGB(byte* data, int width, int height, ref byte[] newData)
        {
            int size = width * height;
            int offset = size;

            if (newData == null)
                newData = new byte[size * 3];

            int u, v, y1, y2, y3, y4;

            for (int i = 0, k = 0; i < size; i += 2, k += 1)
            {
                y1 = data[i];
                y2 = data[i + 1];
                y3 = data[width + i];
                y4 = data[width + i + 1];

                u = data[offset + k];
                v = data[offset + (size / 4) + k];

                int[] pixel0 = ConvertYUVtoRGB(y1, u, v);
                newData[i * 3] = (byte)pixel0[0];
                newData[i * 3 + 1] = (byte)pixel0[1];
                newData[i * 3 + 2] = (byte)pixel0[2];


                int[] pixel1 = ConvertYUVtoRGB(y2, u, v);
                newData[(i + 1) * 3] = (byte)pixel1[0];
                newData[(i + 1) * 3 + 1] = (byte)pixel1[1];
                newData[(i + 1) * 3 + 2] = (byte)pixel1[2];

                int[] pixel2 = ConvertYUVtoRGB(y3, u, v);
                newData[(width + i) * 3] = (byte)pixel2[0];
                newData[(width + i) * 3 + 1] = (byte)pixel2[1];
                newData[(width + i) * 3 + 2] = (byte)pixel2[2];

                int[] pixel3 = ConvertYUVtoRGB(y4, u, v);
                newData[(width + i + 1) * 3] = (byte)pixel3[0];
                newData[(width + i + 1) * 3 + 1] = (byte)pixel3[1];
                newData[(width + i + 1) * 3 + 2] = (byte)pixel3[2];

                if (i != 0 && (i + 2) % width == 0)
                    i += width;
            }

            //return newData;
        }

        /// <summary>
        /// Calculate RGB value from YUV value
        /// </summary>
        private static int[] ConvertYUVtoRGB(int y, int u, int v)
        {
            int c, d, e;
            c = y - 16;
            d = u - 128;
            e = v - 128;

            int r = (298 * c + 409 * e + 128) >> 8;
            int g = (298 * c - 100 * d - 208 * e + 128) >> 8;
            int b = (298 * c + 516 * d + 128) >> 8;

            r = (r > 255 ? 255 : r < 0 ? 0 : r);
            g = (g > 255 ? 255 : g < 0 ? 0 : g);
            b = (b > 255 ? 255 : b < 0 ? 0 : b);

            int[] result = new int[3];
            result[0] = r;
            result[1] = g;
            result[2] = b;
            return result;
        }

        /// <summary>
        /// Device's per-frame callback
        /// </summary>
        public unsafe void RemoteDisplayCBFun(int port, IntPtr buff, int size, int width, int height, int stamp, int type, int reserved)
        {
            //byte[] managedArray = new byte[size];
            //Marshal.Copy(buff, managedArray, 0, size);
            byte* managedArray = (byte*)buff;

            ConvertYUVtoRGB(managedArray, width, height, ref rgbData);

            using (Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb))
            {
                BitmapData bmpD = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                IntPtr ptr = bmpD.Scan0;
                int bytes = bmpD.Stride * bmp.Height;
                Marshal.Copy(rgbData, 0, ptr, bytes);
                bmp.UnlockBits(bmpD);
                //bmp.Save(@"D:\adafs.bmp");
                FrameReceived(new FrameEventArgs() { Frame = bmp });
            }
        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public BitmapSource LoadBitmap(Bitmap bmp)
        {
            var pBmp = bmp.GetHbitmap();

            BitmapSource bmpSrc = Imaging.CreateBitmapSourceFromHBitmap(pBmp, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            DeleteObject(pBmp);

            return bmpSrc;
        }

        Bitmap _bmp = null;
        protected void FrameReceived(FrameEventArgs arg)
        {
            try
            {


                // TODO: Get per-frame bitmap here
                if (_bmp == null)
                    _bmp = new Bitmap(arg.Frame.Width, arg.Frame.Height, arg.Frame.PixelFormat);

                BitmapData srcBmpData = arg.Frame.LockBits(new Rectangle(0, 0, arg.Frame.Width, arg.Frame.Height), ImageLockMode.WriteOnly, arg.Frame.PixelFormat);
                BitmapData desBmpData = _bmp.LockBits(new Rectangle(0, 0, _bmp.Width, _bmp.Height), ImageLockMode.WriteOnly, _bmp.PixelFormat);

                IntPtr ptrSrc = srcBmpData.Scan0;
                IntPtr ptrDes = desBmpData.Scan0;

                int bytes = srcBmpData.Stride * _bmp.Height;

                byte[] data = new byte[bytes];
                Marshal.Copy(ptrSrc, data, 0, bytes);
                Marshal.Copy(data, 0, ptrDes, bytes);

                _bmp.UnlockBits(desBmpData);
                arg.Frame.UnlockBits(srcBmpData);

                //Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                //    new Action(() => imgView.Source = LoadBitmap(_bmp)));

                //picBox.Image = _bmp;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            FrameEventHandler handler = OnFrameReceived;

            if (handler != null)
                handler(this, arg);
        }

        /// <summary>
        /// A callback when playing device real-time
        /// </summary>
        /// <param name="realHandle"></param>
        /// <param name="dataType"></param>
        /// <param name="buffer"></param>
        /// <param name="bufSize"></param>
        /// <param name="user"></param>
        private void RealDataCallBack(int realHandle, uint dataType, ref byte buffer, uint bufSize, IntPtr user)
        {
            if (_isPaused)
                return;

            switch (dataType)
            {
                case CHCNetSDK.NET_DVR_SYSHEAD:     // sys head
                    int tPort = -1;
                    if (!PlayCtrl.PlayM4_GetPort(ref tPort))
                    {
                        Debug.WriteLine("Get port fail");
                    }
                    _port = tPort;

                    if (bufSize > 0)
                    {
                        //set as stream mode, real-time stream under preview
                        if (!PlayCtrl.PlayM4_SetStreamOpenMode(_port, PlayCtrl.STREAME_REALTIME))
                        {
                            Debug.WriteLine("PlayM4_SetStreamOpenMode fail");
                        }
                        //start player
                        if (!PlayCtrl.PlayM4_OpenStream(_port, ref buffer, bufSize, 1024 * 1024))
                        {
                            _port = -1;
                            Debug.WriteLine("PlayM4_OpenStream fail");
                            break;
                        }
                        //set soft decode display callback function to capture

                        if (!PlayCtrl.PlayM4_SetDisplayCallBack(_port, _func))
                        {
                            Debug.WriteLine("PlayM4_SetDisplayCallBack fail");
                        }

                        //start play, set play window
                        Debug.WriteLine("About to call PlayM4_Play");

                        //if (_playWndHandle != IntPtr.Zero)
                        //{

                        if (!PlayCtrl.PlayM4_Play(_port, _playWndHandle))
                        {
                            _port = -1;
                            Debug.WriteLine("PlayM4_Play fail");
                            break;
                        }
                        //}
                        //else
                        //{
                        //    Debug.WriteLine("PlayM4_Play fail - Missing window handle to display");
                        //}

                        //set frame buffer number

                        if (!PlayCtrl.PlayM4_SetDisplayBuf(_port, 15))
                        {
                            Debug.WriteLine("PlayM4_SetDisplayBuf fail");
                        }

                        //set display mode
                        if (!PlayCtrl.PlayM4_SetOverlayMode(_port, 0, 0/* COLORREF(0)*/))//play off screen // todo!!!
                        {
                            Debug.WriteLine("PlayM4_SetOverlayMode fail ");
                        }
                    }

                    break;
                case CHCNetSDK.NET_DVR_STREAMDATA:     // video stream data
                    if (bufSize > 0 && _port != -1)
                    {
                        if (!PlayCtrl.PlayM4_InputData(_port, ref buffer, bufSize))
                        {
                            Debug.WriteLine("PlayM4_InputData fail ");
                        }
                    }
                    break;

                case CHCNetSDK.NET_DVR_AUDIOSTREAMDATA:     //  Audio Stream Data
                    if (bufSize > 0 && _port != -1)
                    {
                        if (!PlayCtrl.PlayM4_InputVideoData(_port, ref buffer, bufSize))
                        {
                            Debug.WriteLine("PlayM4_InputVideoData Fail ");
                        }
                    }

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Pauses the device temporarily.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Pause()
        {
            _isPaused = true;
        }

        /// <summary>
        /// Continue playing device
        /// </summary>
        public void Continue()
        {
            _isPaused = false;
        }

        /// <summary>
        /// Stops the device and closes it. After calling this function, the capture devices
        /// cannot be started again.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Stop()
        {
            Debug.WriteLine("Stop device - " + DeviceId);
            if (!CHCNetSDK.NET_DVR_StopRealPlay(_handle))
                Debug.WriteLine("Stop device: " + DeviceId + " fail");

            //if (!CHCNetSDK.NET_DVR_Logout(DeviceId))
            //{
            //    Debug.WriteLine("Logout device: " + DeviceId + " fail");
            //}

            _isStarted = false;
        }

        /// <summary>
        /// Release SDK resource
        /// </summary>
        public void Dispose()
        {
            if (_sdkInit)
            {
                CHCNetSDK.NET_DVR_Cleanup();
                _sdkInit = false;
            }
        }

        public int DeviceId { get; set; }

        public ZoomFactor ZoomFactor { get; set; }

        public event FrameEventHandler OnFrameReceived;

        public event ZoomEventHandler OnZoomReceived;

        void ICamera.FrameReceived(FrameEventArgs arg)
        {

        }

        public byte[] CaptureImage(string waterMark = "")
        {
            try
            {
                byte[] bytes = CaptureZoom(waterMark);
                SaveZoomState();
                return bytes;
            }
            catch (Exception ex)
            {
                Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                return null;
            }
        }

        public System.Drawing.Image CaptureImage()
        {
            var img = GetCameraBitMap();
            return (System.Drawing.Image)img;
        }

        public Bitmap GetCameraBitMap()
        {
            Bitmap bmp = null;
            lock (this)
            {
                try
                {
                    IntPtr pt = IntPtr.Zero;

                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() => {
                        pt = PicBox.Handle;
                    });

                    //Bitmap bmp = null;
                    IntPtr bmpDC;
                    //Graphics g;
                    IntPtr cameraDC;
                    RECT windowRect = new RECT(0, 0, 0, 0);
                    User32.GetWindowRect(pt, ref windowRect);

                    cameraDC = User32.GetWindowDC(pt);

                    if (cameraDC != IntPtr.Zero)
                    {
                        bmp = new Bitmap(windowRect.Width, windowRect.Height);

                        using (Graphics g = System.Drawing.Graphics.FromImage(bmp))
                        {
                            bmpDC = g.GetHdc();
                            GDI32.BitBlt(bmpDC, 0, 0, windowRect.Width, windowRect.Height, cameraDC, 0, 0, TernaryRasterOperations.SRCCOPY);
                            g.ReleaseHdc(bmpDC);
                        }
                        User32.ReleaseDC(pt, cameraDC);
                        return bmp;
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                    return null;
                }
                finally
                {
                    if (bmp != null)
                    {
                        //bmp.Dispose();
                        //bmp = null;
                    }
                }
            }
        }

        public byte[] CaptureZoom(string waterMark = "")
        {
            Bitmap bmp = null;
            lock (this)
            {
                try
                {
                    bmp = GetCameraBitMap();
                    return bmp.ToByteArray(ImageFormat.Jpeg);
                }
                catch (Exception ex)
                {
                    Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                    return null;
                }
                finally
                {
                    if (bmp != null)
                    {
                        bmp.Dispose();
                        bmp = null;
                    }
                }
            }
        }

        public PictureBox PicBox { get; set; }

        public void Setup(string ip)
        {
            if (!_sdkInit)
                _sdkInit = CHCNetSDK.NET_DVR_Init();

            this.Container = new System.Windows.Controls.UserControl();
            System.Windows.Forms.Panel panel = new System.Windows.Forms.Panel();
            panel.Location = new System.Drawing.Point(0, 0);
            panel.Dock = System.Windows.Forms.DockStyle.Fill;

            WindowsFormsHost wfhost = new WindowsFormsHost();
            PicBox = new System.Windows.Forms.PictureBox();
            PicBox.SizeMode = PictureBoxSizeMode.Zoom;
            PicBox.Location = new System.Drawing.Point(0, 0);
            PicBox.Dock = System.Windows.Forms.DockStyle.Fill;

            panel.Controls.Add(PicBox);
            wfhost.Child = panel;
            Container.Content = wfhost;
            AddWndHandle(PicBox.Handle);

            Login(ip);
            Start();

            Overlay = new CameraOverlay()
            {
                Container = this.Container,
                TextContent = DeviceInfo.IP,
            };

            IPAddress = ip;
        }

        private void Login(string ip)
        {
            string username = "admin";
            string password = "12345";
            string port = OtherUtilities.GetPort(ip);
            string host = ip;

            if (!string.IsNullOrEmpty(port))
                host = OtherUtilities.RemoveLastNonDigitWordChar(ip.Replace(port, ""));
            else
                port = "8000";

            CHCNetSDK.NET_DVR_DEVICEINFO_V30 deviceV30 = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();
            DeviceId = CHCNetSDK.NET_DVR_Login_V30(host, int.Parse(port), username, password, ref deviceV30);

            if (DeviceId > -1)
            {
                DeviceInfo = new DeviceInfo() { IP = host, Port = int.Parse(port), UserName = username, Password = password };
            }
        }

        private void Logout()
        {
            if (!CHCNetSDK.NET_DVR_Logout(DeviceId))
            {
                Debug.WriteLine("Logout device: " + DeviceId + " fail");
            }
        }

        public void Load(string ip, ZoomFactor zoomFactor)
        {
            if(PicBox == null)
            {
                Setup(ip);
            }
            else if (!DeviceInfo.IP.Equals(ip))
            {
                IPAddress = ip;
                Logout();
                Login(ip);
            }
        }

        public void SaveZoomState()
        {

        }

        public void ChangeIPAddress(string ip)
        {

        }

        public System.Windows.Controls.UserControl Container { get; set; }
    }
}
