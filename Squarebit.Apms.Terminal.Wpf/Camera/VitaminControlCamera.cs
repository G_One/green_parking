﻿using AxVITAMINDECODERLib;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Core;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Core.Utility;
using Squarebit.Apms.Terminal.Wpf.UI;
//using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Wpf.Views;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Interop;
using System.Windows.Media;
using VITAMINDECODERLib;

namespace Squarebit.Apms.Terminal.Wpf.Devices
{

    /// <summary>
    /// 
    /// </summary>
    public class VitaminCamera : ICamera
    {
        private bool _isZoomFactorChanged = false;
        public bool IsZoomFactorChanged
        {
            get { return _isZoomFactorChanged; }
            set
            {
                _isZoomFactorChanged = value;
                ZoomReceived(new ZoomEventArgs { ZoomFactor = this.ZoomFactor });
            }
        }

        private ZoomFactor _zoomFactor;
        public ZoomFactor ZoomFactor
        {
            get
            {
                if (_zoomFactor == null)
                    _zoomFactor = new ZoomFactor();

                return _zoomFactor;
            }
            set
            {
                _zoomFactor = value;
            }
        }

        public System.Windows.Controls.UserControl Container { get; set; }
        //public UserControl Container { get { return VitaminControl.Container; } }

        public AxVitaminCtrl VitaminControl { get; set; }
         
		public CameraOverlay Overlay { get; private set; }

        bool _canUse = false;
        bool _connectionOK = false;
        int _timeSinceConnectionOK = 0;

        string _ipAddress;
        public string IPAddress
        {
            get { return _ipAddress; }
            set
            {
                _ipAddress = value;

                //if (this.Overlay != null)
                //    this.Overlay.TextContent = _ipAddress;

                SetOverlay(_ipAddress + " - " + this.ZoomFactor.Factor + "%");

                if (VitaminControl != null)
                    VitaminControl.RemoteIPAddr = _ipAddress;
            }
        }

        //byte[] _data = null;

        public VitaminCamera()
        {

        }

        public VitaminCamera(string ip)
        {

        }

        public void Setup(string ip)
        {
            Container = new System.Windows.Controls.UserControl();

			System.Windows.Forms.Panel panel = new System.Windows.Forms.Panel();
            panel.Location = new System.Drawing.Point(0, 0);
            panel.Dock = System.Windows.Forms.DockStyle.Fill;
            
            WindowsFormsHost host = new WindowsFormsHost();
            
            VitaminControl = new AxVitaminCtrl();
            VitaminControl.Dock = System.Windows.Forms.DockStyle.Fill;
            VitaminControl.Enabled = true;
            VitaminControl.Location = new System.Drawing.Point(0, 0);
            VitaminControl.BeginInit();
            VitaminControl.OnConnectionOK += VitaminControl_OnConnectionOK;
            VitaminControl.OnConnectionBroken += VitaminControl_OnConnectionBroken;
            //VitaminControl.OnVideoCodec += VitaminControl_OnVideoCodec;
            //VitaminControl.OnNewVideo += VitaminControl_OnNewVideo;
            VitaminControl.OnNewPacket += VitaminControl_OnNewPacket;

            panel.Controls.Add(VitaminControl);
            VitaminControl.EndInit();

            host.Child = panel;
            
            Container.Content = host;
            VitaminControl.ControlType = EControlType.eCtrlNormal;
            VitaminControl.IgnoreCaption = true;
            VitaminControl.IgnoreBorder = true;
            VitaminControl.AutoReconnect = true;
            VitaminControl.MediaType = EMediaType.eMediaVideo;
            //VitaminControl.RemoteIPAddr = IPAddress;
            VitaminControl.ServerModelType = EServerModelType.esrv7KServer;
            VitaminControl.ConnectionProtocol = EConnProtocol.eProtUDP;
            VitaminControl.ViewStream = EDualStreamOption.eStream1;
            VitaminControl.NotifyImageFormat = EPictureFormat.ePicFmtYV12;
            VitaminControl.VideoQuality2K = EVideoQuality2K.evqua2KGood;
            VitaminControl.VideoSize2K = EVideoSize2K.evsz2KNormal;
            VitaminControl.DisplayErrorMsg = false;
            //VitaminControl.NotifyVideoData = true;
            VitaminControl.NotifyVideoPacket = true;
			// Create overlay
            Overlay = new CameraOverlay()
            {
                Container = this.Container,
                TextContent = VitaminControl.RemoteIPAddr,
            };

            IPAddress = ip;
        }

        void SetOverlay(string content)
        {
            if (this.Overlay != null)
            {
                Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() => {
                    this.Overlay.TextContent = content;
                });
            }
        }

        void VitaminControl_OnNewPacket(object sender, _IVitaminCtrlEvents_OnNewPacketEvent e)
        {
            if (!_connectionOK)
                return;
            
            LoadZoomState(this.ZoomFactor);
        }

        public void Load(string ip, ZoomFactor zoomFactor)
        {
            if (VitaminControl == null)
            {
                Setup(ip);
                this.ZoomFactor = zoomFactor;
            }
            else if (!VitaminControl.RemoteIPAddr.Equals(ip))
            {
                VitaminControl.Disconnect();
                IPAddress = ip;
                this.ZoomFactor = zoomFactor;
                VitaminControl.Connect();
            }
        }

        private void ReloadCamera(string ip)
        {
            IPAddress = ip;
            VitaminControl.NotifyVideoPacket = true;
        }

        public void ChangeIPAddress(string ip)
        {
            if (VitaminControl == null)
                Setup(ip);
            else if(!VitaminControl.RemoteIPAddr.Equals(ip))
            {
                VitaminControl.Disconnect();
                ReloadCamera(ip);
                VitaminControl.Connect();
            }
        }

        void VitaminControl_OnConnectionBroken(object sender, _IVitaminCtrlEvents_OnConnectionBrokenEvent e)
        {
            //Console.WriteLine("Connection broken");
            _connectionOK = false;
            _timeSinceConnectionOK = 0;
        }

        void VitaminControl_OnConnectionOK(object sender, _IVitaminCtrlEvents_OnConnectionOKEvent e)
        {
            //Console.WriteLine("Connection OK");
            _connectionOK = true;
            _timeSinceConnectionOK = System.Environment.TickCount;
        }

        public int DeviceId { get; set; }

        public event FrameEventHandler OnFrameReceived;

        public event ZoomEventHandler OnZoomReceived;

        public void FrameReceived(FrameEventArgs arg)
        {
            FrameEventHandler handler = OnFrameReceived;

            if (handler != null)
                handler(this, arg);
        }

        public void ZoomReceived(ZoomEventArgs arg)
        {
            ZoomEventHandler handler = OnZoomReceived;

            if (handler != null && _isZoomFactorChanged)
            {
                _isZoomFactorChanged = false;
                handler(this, arg);
            }
        }

        /// <summary>
        /// Starts capturing and sending image data.
        /// </summary>
        public void Start()
        {
            LoadZoomState(this.ZoomFactor);
            VitaminControl.Connect();
        }

        /// <summary>
        /// Capture current frame image
        /// </summary>
        /// <returns></returns>
        public byte[] CaptureImage(string watermark = "")
        {
            try
            {
                byte[] bytes = CaptureZoom(watermark);
                SaveZoomState();
                return bytes;

                //if (_canUse && _timeSinceConnectionOK > 0 && System.Environment.TickCount - _timeSinceConnectionOK > 2000)
                //{
                //    object data, info;
                //    byte[] bData;
                //    Array arrInfo;

                //    int a = VitaminControl.GetSnapshot(EPictureFormat.ePicFmtJpeg, out data, out info);
                //    bData = (byte[])data;
                //    arrInfo = (Array)info;
                //    SaveZoomState();
                //    return bData;
                //}
                //else
                //{
                //    Mvx.Resolve<ILogService>().Log(new Exception("Cannot capture image"));
                //    return null;
                //}
            }
            catch (Exception ex)
            {
                Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                return null;
            }
        }

        public System.Drawing.Image CaptureImage()
        {
            var img = GetCameraBitMap();
            return (System.Drawing.Image)img;
        }

        public Bitmap GetCameraBitMap()
        {
            Bitmap bmp = null;
            lock (this)
            {
                try
                {
                    IntPtr pt = new IntPtr(VitaminControl.DrawHwnd);

                    //Bitmap bmp = null;
                    IntPtr bmpDC;
                    //Graphics g;
                    IntPtr cameraDC;
                    RECT windowRect = new RECT(0, 0, 0, 0);
                    GetWindowRect(pt, out windowRect);

                    cameraDC = GetWindowDC(pt);

                    if (cameraDC != IntPtr.Zero)
                    {
                        bmp = new Bitmap(windowRect.Width, windowRect.Height);

                        using (Graphics g = System.Drawing.Graphics.FromImage(bmp))
                        {
                            bmpDC = g.GetHdc();
                            BitBlt(bmpDC, 0, 0, windowRect.Width, windowRect.Height, cameraDC, 0, 0, TernaryRasterOperations.SRCCOPY);
                            g.ReleaseHdc(bmpDC);
                        }
                        ReleaseDC(pt, cameraDC);
                        return bmp;
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                    return null;
                }
                finally
                {
                    if (bmp != null)
                    {
                        //bmp.Dispose();
                        //bmp = null;
                    }
                }
            }
        }

        public byte[] CaptureZoom(string waterMark = "")
        {
            Bitmap bitMap = null;
            lock(this)
            {
                try
                {
                    bitMap = GetCameraBitMap();
                    if (bitMap == null) return null;

                    ImageUtility.Watermark(bitMap, waterMark);
                    return bitMap.ToByteArray(ImageFormat.Jpeg);
                }
                catch (Exception ex)
                {
                    Mvx.Resolve<ILogService>().Log(new Exception("Capture image exception: " + ex.ToString()));
                    return null;
                }
                finally
                {
                    if (bitMap != null)
                    {
                        bitMap.Dispose();
                        bitMap = null;
                    }
                }
            }
        }

        public void SaveZoomState()
        {
            try
            {
                if (this.ZoomFactor == null)
                    this.ZoomFactor = new ZoomFactor();
                if (ZoomFactor.Factor != VitaminControl.DigitalZoomFactor ||
                    ZoomFactor.ZoomX != VitaminControl.DigitalZoomX ||
                    ZoomFactor.ZoomY != VitaminControl.DigitalZoomY ||
                    ZoomFactor.ZoomEnabled != VitaminControl.DigitalZoomEnabled)
                {
                    this.ZoomFactor.ZoomEnabled = VitaminControl.DigitalZoomEnabled;
                    this.ZoomFactor.Factor = VitaminControl.DigitalZoomFactor;
                    this.ZoomFactor.ZoomX = VitaminControl.DigitalZoomX;
                    this.ZoomFactor.ZoomY = VitaminControl.DigitalZoomY;
                    SetOverlay(_ipAddress + " - " + this.ZoomFactor.Factor + "%");
                    IsZoomFactorChanged = true;
                }
            }
            catch (Exception ex)
            {
                Mvx.Resolve<ILogService>().Log(new Exception("Save zoom exception: " + ex.ToString()));
            }
        }

        private bool CheckZoomStateCorrect(ZoomFactor factor)
        {
            float a = Math.Abs((float)VitaminControl.DigitalZoomFactor - factor.Factor) / (float)factor.Factor;
            float b = Math.Abs((float)VitaminControl.DigitalZoomX - factor.ZoomX);
            float c = Math.Abs((float)VitaminControl.DigitalZoomY - factor.ZoomY);

            SetOverlay(_ipAddress + " - " + this.ZoomFactor.Factor + "%");

            if ((a <= 0.1 && b <= 80 && c <= 80 && factor.ZoomEnabled == VitaminControl.DigitalZoomEnabled) ||
                (factor.ZoomEnabled == false && VitaminControl.DigitalZoomEnabled == false))
            {
                return true;
            }
            else
                return false;
        }

        public void LoadZoomState(ZoomFactor factor)
        {
            try
            {
                if (factor != null)
                {
                    this.ZoomFactor = factor;

                    VitaminControl.DigitalZoomEnabled = this.ZoomFactor.ZoomEnabled;
                    VitaminControl.DigitalZoomFactor = (int)this.ZoomFactor.Factor;
                    VitaminControl.DigitalZoomX = (int)this.ZoomFactor.ZoomX;
                    VitaminControl.DigitalZoomY = (int)this.ZoomFactor.ZoomY;

                    if (CheckZoomStateCorrect(factor))
                    {
                        VitaminControl.NotifyVideoPacket = false;
                        _canUse = true;
                    }
                }
                else
                    _canUse = true;
            }
            catch(Exception ex)
            {
                Mvx.Resolve<ILogService>().Log(new Exception("Load zoom exception: " + ex.ToString()));
            }
        }

        public void Pause()
        {
            VitaminControl.RtspPause();
        }

        public void Continue()
        {
            VitaminControl.RtspPlay();
        }

        public void Stop()
        {
            SaveZoomState();
            if (VitaminControl != null &&
                (VitaminControl.ControlStatus == EControlStatus.ctrlRunning ||
                VitaminControl.ControlStatus == EControlStatus.ctrlConnecting ||
                VitaminControl.ControlStatus == EControlStatus.ctrlReConnecting))
            {
                VitaminControl.CloseConnect();
            }

            _timeSinceConnectionOK = 0;
        }

        public void Dispose()
        {
            
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);
        [DllImport("user32.dll")]
        static extern IntPtr GetWindowDC(IntPtr hWnd);
        [DllImport("gdi32.dll", EntryPoint = "BitBlt", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool BitBlt([In] IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, [In] IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);
        [DllImport("user32.dll")]
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);
    }
}