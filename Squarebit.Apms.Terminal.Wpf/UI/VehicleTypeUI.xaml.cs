﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Squarebit.Apms.Terminal.Wpf.UI
{
    /// <summary>
    /// Interaction logic for VehicleTypeUI.xaml
    /// </summary>
    public partial class VehicleTypeUI : UserControl
    {
        int _number;
        public int Number
        {
            get { return _number; }
            set
            {
                _number = value;
                NumberTB.Text = _number.ToString();
            }
        }

        string _typeName;
        public string TypeName
        {
            get { return _typeName; }
            set
            {
                _typeName = value;
                NameTB.Text = _typeName;
            }
        }

        public VehicleTypeUI()
        {
            InitializeComponent();
        }
    }
}
