﻿using Squarebit.Apms.Terminal.Wpf.Views;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Squarebit.Apms.Terminal.Wpf.UI;
using System.ComponentModel;
using System.Windows.Threading;
using Cirrious.MvvmCross.Plugins.Messenger;
using Squarebit.Apms.Terminal.Core.Services;
using System.Reflection;
using Squarebit.Apms.Terminal.Wpf.Views.AppViews;
using Cirrious.MvvmCross.Views;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System.Diagnostics;
using Squarebit.Apms.Terminal.Core.Utilities;

namespace Squarebit.Apms.Terminal.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        //public new static readonly DependencyProperty FlyoutsProperty = DependencyProperty.Register("Flyouts", typeof(FlyoutsControl), typeof(MainWindow), new PropertyMetadata(null));
        //public new FlyoutsControl Flyouts
        //{
        //    get { return (FlyoutsControl)GetValue(FlyoutsProperty); }
        //    set { SetValue(FlyoutsProperty, value); }
        //}

        private IMvxViewDispatcher _viewDispatcher;

        IMvxMessenger _keyPressedMessenger;

		DispatcherTimer _timer;

		SystemNotificationOverlay _systemNotification;

		ILocalizeService _localeService;

        IUserPreferenceService _userPreferenceService;

        MvxSubscriptionToken _changeServerToken;

        public MainWindow()
        {
            InitializeComponent();
            
            this.StateChanged += (sender, e) => {
                this.MainView.Focus();
            };

            _viewDispatcher = Mvx.Resolve<IMvxViewDispatcher>();

            _keyPressedMessenger = Mvx.Resolve<IMvxMessenger>();

            this.Background = Theme.Theme.Background;
            this.Topmost = true;
            this.IgnoreTaskbarOnMaximize = true;

			_systemNotification = new SystemNotificationOverlay()
			{
				Container = this
			};

            this.Loaded += MainWindow_Loaded;
            this.Closed += MainWindow_Closed;

			_timer = new DispatcherTimer();
			_timer.Interval = TimeSpan.FromSeconds(1);
			_timer.Tick += timer_Tick;
			_timer.Start();

            //lblVersion.Content = "Ver: " + OtherUtilities.GetVersion();

            this.KeyUp += MainWindow_KeyUp;
        }

		void timer_Tick(object sender, EventArgs e)
		{
			lblTime.Content = DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss tt");
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			MessageBoxResult result = MessageBox.Show("Bạn có muốn thoát chương trình?", "Xác nhận", MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (result == MessageBoxResult.Yes)
			{
                _userPreferenceService.SystemSettings.ForceSave();
				e.Cancel = false;
			}
			else
			{
				e.Cancel = true;
			}
		}

        void MainWindow_Closed(object sender, EventArgs e)
        {
			Application.Current.Shutdown();
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
			_localeService = Mvx.Resolve<ILocalizeService>();
            _userPreferenceService = Mvx.Resolve<IUserPreferenceService>();
            this.Title = string.IsNullOrEmpty(_userPreferenceService.HostSettings.ParkingName) ? Title : _userPreferenceService.HostSettings.ParkingName;
            this.Title += " - " + OtherUtilities.GetVersion();
			var messenger = Mvx.Resolve<IMvxMessenger>();
            _changeServerToken = messenger.Subscribe<ChangeServerMessage>(msg => {
                this.Dispatcher.Invoke(() => {
                    if (msg.IsUseSecondary)
                    {
                        _systemNotification.Text = _localeService.GetText("system.primary_server_unreachable");
                        _systemNotification.Show();
                    }
                    else
                    {
                        _systemNotification.Hide();
                    }
                });
            });
        }

        void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F12:
                    {
                        ButtonConfigClick(null, null);
                        break;
                    }
                case Key.F11:
                    {
                        ButtonFindClick(null, null);
                        break;
                    }
                case Key.F9:
                    ButtonStatisticsClick(null, null);
                    break;
            }

            if (_keyPressedMessenger.HasSubscriptionsFor<KeyPressedMessage>())
            {
                _keyPressedMessenger.Publish(new KeyPressedMessage(sender, e));
            }
        }

        public void PresentInRegion(BaseView frameworkElement, string regionName)
        {            
            this.MainView.Children.Add(frameworkElement);
        }

        public void ShowInMainView(BaseView frameworkElement)
        {
            this.MainView.Children.Clear();
            frameworkElement.MainWindow = this;
            this.MainView.Children.Add(frameworkElement);
        }

        public void CloseCurrentView()
        {
            int count = this.MainView.Children.Count;
            if (count > 0)
                this.MainView.Children.RemoveAt(count - 1);
        }

		private void ButtonConfigClick(object sender, RoutedEventArgs e)
		{
			var passwordWindow = new PasswordWindow()
			{
				Owner = this,
				Title = "Nhập mật mã để cấu hình",
			};
            passwordWindow.AuthenticateSuccess += (arg1, arg2) => {
                _viewDispatcher.ShowViewModel(new MvxViewModelRequest(typeof(ConfigViewModel), null, null, null));
            };
			passwordWindow.ShowDialog();
		}

        private void ButtonFindClick(object sender, RoutedEventArgs e)
        {

            //var passwordWindow = new PasswordWindow()
            //{
            //    Owner = this,
            //    Title = "Nhập mật mã để cấu hình",
            //};

            //passwordWindow.AuthenticateSuccess += (arg1, arg2) => {
                var findImagesWindow = new FindImagesWindow()
                {
                    Owner = this,
                    Title = "",
                };
                findImagesWindow.ShowDialog();
            //};

            //passwordWindow.ShowDialog();
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutView aboutView = new AboutView() { Container = this };
            aboutView.ShowDialog();
        }

        private void ButtonStatisticsClick(object sender, RoutedEventArgs e)
        {
            //var passwordWindow = new PasswordWindow()
            //{
            //    Owner = this,
            //    Title = "Nhập mật mã để xem thống kê",
            //};

            //passwordWindow.AuthenticateSuccess += (arg1, arg2) => {
                var statisticsWindow = new StatisticsWindow()
                {
                    Owner = this,
                    Title = "",
                };
                statisticsWindow.ShowDialog();
            //};

            //passwordWindow.ShowDialog();
        }

        //StatisticsWindow
    }
}