﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Squarebit.Apms.Terminal.Wpf.Theme
{
    public class Theme
    {
        public static readonly Brush Background = (Brush)new BrushConverter().ConvertFrom("#FFE5E5E5");
    }
}
