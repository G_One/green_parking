﻿using AxVITAMINDECODERLib;
using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Apms.Terminal.Wpf.UI;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VITAMINDECODERLib;

namespace Squarebit.Apms.Terminal.Wpf.Views
{
    /// <summary>
    /// Interaction logic for CameraView.xaml
    /// </summary>
    public partial class CameraView : BaseView
    {
        Camera _camera;
        public Camera Camera
        {
            get { return _camera; }
            set
            {
                if (_camera == value) return;

                _camera = value;
                if (Camera.Container.Parent == null)
                    this.MainGrid.Children.Add(Camera.Container);
                else
                {
                    (Camera.Container.Parent as Panel).Children.Remove(Camera.Container);
                    this.MainGrid.Children.Add(Camera.Container);
                }
            }
        }

        Image _imageView;
        public Image ImageView
        {
            get { return _imageView; }
            set
            {
                if (_imageView == value) return;
                _imageView = value;
            }
        }

        private bool _overlay;
        public bool Overlay
        {
            get { return _overlay; }
            set
            {
                _overlay = value;
            }
        }

        BitmapImage _bmpImg = null;

        public CameraView()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            InitializeComponent();
            this.Focusable = true;
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            //_imageView = new Image();
            //this.MainGrid.Children.Add(_imageView);
            //_bmpImg = new BitmapImage();

            //Camera.OnFrameReceived += Camera_OnFrameReceived;
        }

        //void Camera_OnFrameReceived(object sender, FrameEventArgs e)
        //{
        //    MemoryStream ms = new MemoryStream(e.Frame);

        //    _bmpImg.BeginInit();
        //    _bmpImg.StreamSource = ms;
        //    _bmpImg.EndInit();

        //    ImageSource imgSrc = _bmpImg as ImageSource;

        //    this.ImageView.Source = imgSrc;
        //}

        public override void Close()
        {
            this.MainGrid.Children.Clear();
        }
    }
}
