﻿using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.BindingContext;
using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.CrossCore;
using System.Windows.Forms.Integration;
using Squarebit.Apms.Terminal.Wpf.UI;
using Cirrious.MvvmCross.ViewModels;
using MahApps.Metro.Controls;

namespace Squarebit.Apms.Terminal.Wpf.Views.AppViews
{
    /// <summary>
    /// Interaction logic for CheckInLaneView.xaml
    /// </summary>
    public partial class CheckInLaneView : BaseLaneView
    {
        IResourceLocatorService _resourceLocator;

        public NotificationOverlay Notification { get; private set; }

        public new CheckInLaneViewModel ViewModel
        {
            get { return (CheckInLaneViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        byte[] _fontImage;
        public byte[] FrontImage
        {
            get { return _fontImage; }
            set
            {
                if (_fontImage == value) return;
                if (this.CIFrontImage.Source != null)
                {
                    (CIFrontImage.Source as BitmapImage).StreamSource.Dispose();
                    (CIFrontImage.Source as BitmapImage).StreamSource = null;
                    this.CIFrontImage.Source = null;
                }

                _fontImage = value;
                ConvertNow(_fontImage, this.CIFrontImage);
            }
        }

        byte[] _backImage;
        public byte[] BackImage
        {
            get { return _backImage; }
            set
            {
                if (_backImage == value) return;
                if (this.CIBackImage.Source != null)
                {
                    (CIBackImage.Source as BitmapImage).StreamSource.Dispose();
                    (CIBackImage.Source as BitmapImage).StreamSource = null;
                    this.CIBackImage.Source = null;
                }

                _backImage = value;
                ConvertNow(_backImage, this.CIBackImage);
            }
        }

        //BitmapImage _biImg = new BitmapImage();

        //private CheckIn _checkInData;
        //public CheckIn CheckInData
        //{
        //    get { return _checkInData; }
        //    set
        //    {
        //        if (_checkInData == value) 
        //            return;

        //        _checkInData = value;


        //        //this.VehicleTextBox.ShouldFocuse = true;
        //    }
        //}

        private bool _showCountdown;
        public bool ShowCountdown
        {
            get { return _showCountdown; }
            set
            {
                _showCountdown = value;
                CountdownPanel.Visibility = _showCountdown ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
            }
        }

        public override void DisplayNotices()
        {
            Notification.Notices = this.NoticesToUser;
            Notification.ShowNotification(NoticesToUser.TimeOut);
        }

        Section _section;
        public Section Section
        {
            get { return _section; }
            set
            {
                if (_section == value) return;

                _section = value;
                Setup();
            }
        }

        public CheckInLaneView()
        {
            InitializeComponent();

            this.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            this.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;

            _resourceLocator = Mvx.Resolve<IResourceLocatorService>();

            Notification = new NotificationOverlay()
            {
                Container = CameraGrid,
            };
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            BindData();

            this.FrontCamera.Focusable = true;
            this.FrontCamera.Focus();
        }

        private void Setup()
        {
            this.FrontCamera.Camera = this.Section.FrontInCamera;
            this.BackCamera.Camera = this.Section.BackInCamera;
        }

        public override void BindData()
        {
            base.BindData();

            var set = this.CreateBindingSet<CheckInLaneView, CheckInLaneViewModel>();
            set.Bind(this).For(v => v.Section).To(vm => vm.Section);
            //set.Bind(this).For(v => v.CheckInData).To(vm => vm.CheckInData);
            set.Bind(this).For(v => v.FrontImage).To(vm => vm.CheckInData.FrontImage);
            set.Bind(this).For(v => v.BackImage).To(vm => vm.CheckInData.BackImage);
            set.Bind(this).For(v => v.ShowCountdown).To(vm => vm.ShowCountdown);
            set.Apply();
        }

        public override void Close()
        {
            if (FrontCamera != null)
                FrontCamera.Close();
            if (BackCamera != null)
                BackCamera.Close();

            if (CIFrontImage != null)
            {
                this.CIFrontImage.Source = null;
                this.CIFrontImage = null;
            }

            if (CIBackImage != null)
            {
                this.CIBackImage.Source = null;
                this.CIBackImage = null;
            }
        }

        public override bool InterceptCloseViewRequest(BaseViewModel childVM)
        {


            return base.InterceptCloseViewRequest(childVM);
        }

        private void ToggleFlyout(int index)
        {
            var flyout = this.MainWindow.Flyouts.Items[index] as Flyout;
            if (flyout == null)
            {
                return;
            }

            flyout.IsOpen = !flyout.IsOpen;
        }
    }
}