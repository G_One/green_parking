﻿using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace Squarebit.Apms.Terminal.Wpf.Views.AppViews
{
    public class SearchTextBox : TextBox
    {
        public event KeyEventHandler SearchTextBoxKeyUp;
        public SearchTextBox()
            : base()
        {
            this.AddHandler(HookTextBox.KeyUpEvent, new RoutedEventHandler(SearchTextBox_KeyUp), true);
        }

        private void SearchTextBox_KeyUp(object sender, RoutedEventArgs e)
        {
            KeyEventArgs args = e as KeyEventArgs;
            switch (args.Key)
            {
                case Key.PageDown:
                case Key.PageUp:
                case Key.End:
                    e.Handled = true;
                    if (SearchTextBoxKeyUp != null)
                        SearchTextBoxKeyUp(sender, e as KeyEventArgs);
                    break;
            }
        }
    }

    /// <summary>
    /// Interaction logic for SearchView.xaml
    /// </summary>
    public partial class SearchView : BaseView
    {
        public new SearchViewModel ViewModel
        {
            get { return (SearchViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public SearchView()
        {
            InitializeComponent();

            ParkingSessionGrid.AddHandler(DataGrid.KeyUpEvent, new RoutedEventHandler(DataGrid_KeyUp), true);

            tbCardLabel.SearchTextBoxKeyUp += SearchTextBoxKeyUp;
            tbPlateNumber.SearchTextBoxKeyUp += SearchTextBoxKeyUp;
        }

        void SearchTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            ViewModel.KeyPressed(sender, e);
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            tbCardLabel.Focusable = true;
            tbCardLabel.Focus();
        }

        private void DataGrid_KeyUp(object sender, RoutedEventArgs e)
        {
            //Key key = (e as KeyEventArgs).Key;
            //if (key == Key.Delete)
            //    ViewModel.KeyPressed(sender, e as KeyEventArgs);
        }

        private void ParkingSessionGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var text = (e.EditingElement as TextBox).Text;
            ViewModel.EditEndingCommand.Execute(text);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.SelectPageCommand.Execute(null);
        }
    }
}
