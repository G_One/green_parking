﻿using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Squarebit.Apms.Terminal.Wpf.Views
{
    /// <summary>
    /// Interaction logic for TestView.xaml
    /// </summary>
    public partial class TestView : BaseView
    {
        public new TestViewModel ViewModel
        {
            get { return (TestViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public TestView()
        {
            InitializeComponent();
        }
    }
}
