﻿using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.BindingContext;
using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Wpf.UI;
using Squarebit.Apms.Terminal.Wpf.Utility;
using System.Media;

namespace Squarebit.Apms.Terminal.Wpf.Views.AppViews
{
    /// <summary>
    /// Interaction logic for CheckOutLaneView.xaml
    /// </summary>
    public partial class CheckOutLaneView : BaseLaneView
    {
        IResourceLocatorService _resourceLocator;

        MediaPlayer _player;
        Uri _uri;

        public NotificationOverlay Notification { get; private set; }

        public new CheckOutLaneViewModel ViewModel
        {
            get { return (CheckOutLaneViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        byte[] _refFontImage;
        public byte[] RefFrontImage
        {
            get { return _refFontImage; }
            set
            {
                if (_refFontImage == value) return;
                if (this.COReferenceFrontImage.Source != null)
                {
                    (COReferenceFrontImage.Source as BitmapImage).StreamSource.Dispose();
                    (COReferenceFrontImage.Source as BitmapImage).StreamSource = null;
                    this.COReferenceFrontImage.Source = null;
                }

                _refFontImage = value;
                ConvertNow(_refFontImage, this.COReferenceFrontImage);
            }
        }

        byte[] _refBackImage;
        public byte[] RefBackImage
        {
            get { return _refBackImage; }
            set
            {
                if (_refBackImage == value) return;
                if (this.COReferenceBackImage.Source != null)
                {
                    (COReferenceBackImage.Source as BitmapImage).StreamSource.Dispose();
                    (COReferenceBackImage.Source as BitmapImage).StreamSource = null;
                    this.COReferenceBackImage.Source = null;
                }

                _refBackImage = value;
                ConvertNow(_refBackImage, this.COReferenceBackImage);
            }
        }

        Section _section;
        public Section Section
        {
            get { return _section; }
            set
            {
                if (_section == value) return;

                _section = value;
                Setup();
            }
        }

        //CheckOut _checkOutData;
        //public CheckOut CheckOutData
        //{
        //    get { return _checkOutData; }
        //    set
        //    {
        //        if (_checkOutData == value) return;

        //        _checkOutData = value;
        //    }
        //}

        public override void DisplayNotices()
        {
            Notification.Notices = this.NoticesToUser;
            Notification.ShowNotification(NoticesToUser.TimeOut);
        }

        public CheckOutLaneView()
        {
            InitializeComponent();

            this.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            this.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;

            _resourceLocator = Mvx.Resolve<IResourceLocatorService>();

            Notification = new NotificationOverlay()
            {
                Container = CameraGrid,
            };

            _player = new MediaPlayer();
            _uri = new Uri(@"Sounds/checkout.wav", UriKind.Relative);

            //_soundPlayer = new SoundPlayer(@"Sounds/checkout.wav");
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            BindData();

            ViewModel.CheckOutCompleted += OnCheckoutCompleted;

            this.FrontCamera.Focusable = true;
            this.FrontCamera.Focus();
        }

        public override void ViewUnloaded(object sender, RoutedEventArgs e)
        {
            base.ViewUnloaded(sender, e);

            ViewModel.CheckOutCompleted -= OnCheckoutCompleted;
        }

        private void OnCheckoutCompleted(object sender, EventArgs e)
        {
            CheckoutEventArgs checkoutArgs = (CheckoutEventArgs)e;
            if (checkoutArgs.Key == KeyAction.CheckOut)
            {
                if (_player != null)
                {
                    _player.Open(_uri);
                    _player.Play();
                    //_soundPlayer.Play();
                }
            }
        }

        private void Setup()
        {
            this.FrontCamera.Camera = this.Section.FrontOutCamera;
            this.BackCamera.Camera = this.Section.BackOutCamera;
        }

        public override void BindData()
        {
            base.BindData();

            var set = this.CreateBindingSet<CheckOutLaneView, CheckOutLaneViewModel>();
            set.Bind(this).For(v => v.Section).To(vm => vm.Section);
            set.Bind(this).For(v => v.RefFrontImage).To(vm => vm.CheckOutData.ReferenceFrontImage);
            set.Bind(this).For(v => v.RefBackImage).To(vm => vm.CheckOutData.ReferenceBackImage);
            //set.Bind(this).For(v => v.CheckOutData).To(vm => vm.CheckOutData);
            //set.Bind(this).For(v => ShowChooseVehicleType).To(vm => vm.ShowChooseVehicleType);
            set.Apply();
        }

        public override void Close()
        {
            this.FrontCamera.Close();
            this.BackCamera.Close();
            COReferenceFrontImage.Source = null;
            COReferenceFrontImage = null;
            COReferenceBackImage.Source = null;
            COReferenceBackImage = null;
        }
    }
}
