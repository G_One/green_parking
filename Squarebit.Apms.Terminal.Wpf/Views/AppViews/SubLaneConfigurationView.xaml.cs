﻿using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.CrossCore;
using Squarebit.Devices.Dal;
using Squarebit.Apms.Terminal.Wpf.Devices;

namespace Squarebit.Apms.Terminal.Wpf.Views.AppViews
{
    /// <summary>
    /// Interaction logic for SubLaneConfigurationView.xaml
    /// </summary>
    public partial class SubLaneConfigurationView : BaseView
    {
        public new SubLaneConfigurationViewModel ViewModel
        {
            get { return (SubLaneConfigurationViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        bool _isDetecting;
        public bool IsDetecting
        {
            get { return _isDetecting; }
            set
            {
                _isDetecting = value;
                if(_isDetecting)
                    this.DetectButton.Background = Brushes.Red;
                else
                    this.DetectButton.SetResourceReference(Control.BackgroundProperty, "AccentColorBrush");
            }
        }
        
        public SubLaneConfigurationView()
        {
            InitializeComponent();
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            ViewModel.GetCardReaderInfo();
            if (Mvx.Resolve<ICamera>() is Webcam)
                ViewModel.GetWebcamInfo();
            BindData();
        }

        public override void BindData()
        {
            base.BindData();

            var set = this.CreateBindingSet<SubLaneConfigurationView, SubLaneConfigurationViewModel>();
            set.Bind(this).For(v => v.IsDetecting).To(vm => vm.IsDetecting);
            set.Apply();
        }

        private void DetectButton_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.DetectCardCommand.Execute(null);
            
        }

        
    }
}
