﻿using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Apms.Terminal.Wpf.Devices;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Squarebit.Apms.Terminal.Wpf.Views.AppViews
{
    /// <summary>
    /// Interaction logic for BootstrapView.xaml
    /// </summary>
    public partial class BootstrapView : BaseView
    {
        public new BootstrapViewModel ViewModel
        {
            get { return (BootstrapViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public BootstrapView()
        {
            InitializeComponent();

            IOptionsSettings _optionSettings = Mvx.Resolve<IOptionsSettings>();
            switch (_optionSettings.CameraType)
            {
                case CameraType.Hik:
                    Mvx.RegisterType<ICamera, HIKCamera>();
                    break;
                case CameraType.Vivotek:
                    Mvx.RegisterType<ICamera, VitaminCamera>();
                    break;
                case CameraType.Webcam:
                    Mvx.RegisterType<ICamera, Webcam>();
                    break;
            }
        }
    }
}
