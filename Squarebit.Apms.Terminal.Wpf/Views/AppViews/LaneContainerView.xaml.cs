﻿using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cirrious.MvvmCross.Binding;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace Squarebit.Apms.Terminal.Wpf.Views.AppViews
{
    /// <summary>
    /// Interaction logic for LaneContainerView.xaml
    /// </summary>
    public partial class LaneContainerView : BaseView
    {
        Dictionary<SectionPosition, Grid> _maps;

        List<Section> _sections;
        public List<Section> Sections
        {
            get { return _sections; }
            set
            {
                if (_sections == value) 
                    return;

                _sections = value;

                if (_sections != null)
                    LayoutView();
            }
        }

        private void LayoutView()
        {
            foreach (var item in Sections)
            {
                var colDefinition = new ColumnDefinition();
                colDefinition.Width = new GridLength(1, GridUnitType.Star);
                this.ContentGrid.ColumnDefinitions.Add(colDefinition);

                Grid grid = new Grid();
                _maps.Add(item.Id, grid);
                this.ContentGrid.Children.Add(grid);
                grid.SetValue(Grid.ColumnProperty, (int)item.Id);
            }
        }

        public new LaneContainerViewModel ViewModel
        {
            get { return (LaneContainerViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }
        public LaneContainerView()
        {
            InitializeComponent();

            _maps = new Dictionary<SectionPosition, Grid>();

            //BindData();
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);
            //ViewModel.GetUser();
            //this.KeyUp += LaneContainerView_KeyUp;
        }

        public override void BindData()
        {
            base.BindData();

            var set = this.CreateBindingSet<LaneContainerView, LaneContainerViewModel>();
            set.Bind(this).For(v => v.Sections).To(vm => vm.Sections);
            set.Apply();
        }

        public override bool InterceptViewRequest(BaseView requestedView)
        {
            if (requestedView.ViewModel is LoginViewModel)
            {
                LoginViewModel vm = requestedView.ViewModel as LoginViewModel;
                _maps[vm.Section.Id].Children.Add(requestedView);
                return true;
            }

            BaseViewModel requestedViewModel = requestedView.ViewModel as BaseViewModel;
            requestedView.MainWindow = this.MainWindow;
            _maps[requestedViewModel.Section.Id].Children.Add(requestedView);
            return true;
        }

        public override bool InterceptCloseViewRequest(BaseViewModel childVM)
        {
            if (childVM is BaseLaneViewModel || 
                childVM is SearchViewModel || 
                childVM is LoginViewModel ||
                childVM is ExceptionalCheckOutViewModel ||
                childVM is EndingShiftInformationViewModel)
            {
                BaseViewModel vm = childVM as BaseViewModel;
                Grid grid = _maps[vm.Section.Id];
                grid.Children.RemoveAt(grid.Children.Count - 1);
                return true;
            }
            
            return false;
        }

        //void LaneContainerView_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //this.ViewModel.KeyPressed(sender, e);
        //    Console.WriteLine(e.Key);
        //}
    }
}