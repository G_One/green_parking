using Cirrious.CrossCore.Plugins;

namespace Squarebit.Apms.Terminal.Wpf.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}