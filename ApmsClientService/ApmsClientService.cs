﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Squarebit.Apms.Terminal.Core.Services;
using ApmsTestService;
using Squarebit.Apms.Terminal.Core.Models;

namespace ApmsClientService
{
    public partial class ApmsClientService : ServiceBase
    {
        private HealthReporter _healthReporter;
        private WebServer _webServer;
        private ImageReplicator _imageReplicator;
        //private BugReporter _bugReporter;
        private IHostSettings _hostSettings;

        private bool ConfigReady { get { return _hostSettings.HasLocal && _hostSettings.Terminal.Id > 0 && _hostSettings.PrimaryServerIP != null && _hostSettings.SecondaryServerIP != null; } }

        private void ReloadConfig()
        {
            if (TestDetector.IsTesting())
                _hostSettings = new HostSettings(new ArgumentParameter() { Mode = RunMode.Testing });
            else
                _hostSettings = new HostSettings(new ArgumentParameter() { Mode = RunMode.Production });
        }

        public ApmsClientService()
        {
            InitializeComponent();
            _healthReporter = new HealthReporter();
            _webServer = new WebServer();
            _imageReplicator = new ImageReplicator();
            //_bugReporter = new BugReporter();
        }

        protected override void OnStart(string[] args)
        {
            ReloadConfig();
            if (ConfigReady)
            {
                _healthReporter.Start(_hostSettings);
                _webServer.Start(_hostSettings);
                _imageReplicator.Start(_hostSettings);
                //_bugReporter.Start(_hostSettings);
            }
        }

        protected override void OnStop()
        {
            if (ConfigReady)
            {
                _healthReporter.Stop();
                _webServer.Stop();
                _imageReplicator.Stop();
                //_bugReporter.Stop();
            }
        }
    }
}
