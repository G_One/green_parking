﻿using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApmsClientService
{
    public class ServiceTester
    {
        private HealthReporter _healthReporter;
        private WebServer _webServer;
        private IHostSettings _hostSettings;
        private ImageReplicator _imageReplicator;

        private bool ConfigReady { get { return _hostSettings.HasLocal && _hostSettings.Terminal.Id > 0 && _hostSettings.PrimaryServerIP != null && _hostSettings.SecondaryServerIP != null; } }

        private void ReloadConfig()
        {
            _hostSettings = new HostSettings();
        }

        public ServiceTester()
        {
            _healthReporter = new HealthReporter();
            _webServer = new WebServer();
            _imageReplicator = new ImageReplicator();
        }

        public void Start()
        {
            ReloadConfig();
            if (ConfigReady)
            {
                _healthReporter.Start(_hostSettings);
                _webServer.Start(_hostSettings);
                _imageReplicator.Start(_hostSettings);
            }
        }

        public void Stop()
        {
            if (ConfigReady)
            {
                _healthReporter.Stop();
                _webServer.Stop();
                _imageReplicator.Stop();
            }
        }
    }
}
