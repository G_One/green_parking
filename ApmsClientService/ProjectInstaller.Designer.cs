﻿namespace ApmsClientService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.apmsServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.apmsServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // apmsServiceProcessInstaller
            // 
            this.apmsServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.apmsServiceProcessInstaller.Password = null;
            this.apmsServiceProcessInstaller.Username = null;
            // 
            // apmsServiceInstaller
            // 
            this.apmsServiceInstaller.Description = "APMS Client Service used to report host health to APMS server automatically and r" +
    "eplicate images";
            this.apmsServiceInstaller.DisplayName = "APMS Client Service";
            this.apmsServiceInstaller.ServiceName = "ApmsClientService";
            this.apmsServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.apmsServiceProcessInstaller,
            this.apmsServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller apmsServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller apmsServiceInstaller;
    }
}