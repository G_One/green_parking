﻿using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ApmsClientService
{
    public class BugListener
    {
        SocketPermission permission;
        Socket _listener;
        IPEndPoint ipEndPoint;
        Socket _handler;

        public static string data = null;

        System.Timers.Timer _timer = null;

        string _path;

        public BugListener(string path)
        {
            _path = Path.Combine(path, "logs", "unhandled_crash");
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
        }

        public void StartListening()
        {
            _timer = new System.Timers.Timer();
            _timer.Interval = 1500;
            _timer.Elapsed += _timer_Elapsed;

            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];
            IPHostEntry ipHostInfo = Dns.GetHostEntry("");
            IPAddress ipAddress = ipHostInfo.AddressList.Where(add => add.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
            if (ipAddress == null)
            {
                return;
            }

            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 4510);

            // Create a TCP/IP socket.
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                _listener.Bind(localEndPoint);
                _listener.Listen(10);

                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");
                    _handler = _listener.Accept();

                    while (true)
                    {
                        bytes = new byte[1024];
                        _timer.Stop();
                        _timer.Start();
                        try
                        {
                            int bytesRec = _handler.Receive(bytes);
                            //Encoding.ASCII.GetString(bytes, 0, bytesRec);
                            //Console.WriteLine(_path);
                        }
                        catch (Exception ex)
                        {
                            if (ex is SocketException)
                            {
                                _handler.Shutdown(SocketShutdown.Both);
                                _handler.Close();
                            }
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Stop()
        {
            _timer.Stop();
            _timer.Dispose();

            _listener.Shutdown(SocketShutdown.Both);
            _listener.Close();
            _listener.Dispose();
        }

        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Stop();
            var path = CrashDump.MiniDumpToFile(_path);
            if (!string.IsNullOrEmpty(path))
                MailSender.Send(path);
        }
    }

    public class CrashDump
    {
        private static object obj = new object();
        internal enum MINIDUMP_TYPE
        {
            MiniDumpNormal = 0x00000000,
            MiniDumpWithDataSegs = 0x00000001,
            MiniDumpWithFullMemory = 0x00000002,
            MiniDumpWithHandleData = 0x00000004,
            MiniDumpFilterMemory = 0x00000008,
            MiniDumpScanMemory = 0x00000010,
            MiniDumpWithUnloadedModules = 0x00000020,
            MiniDumpWithIndirectlyReferencedMemory = 0x00000040,
            MiniDumpFilterModulePaths = 0x00000080,
            MiniDumpWithProcessThreadData = 0x00000100,
            MiniDumpWithPrivateReadWriteMemory = 0x00000200,
            MiniDumpWithoutOptionalData = 0x00000400,
            MiniDumpWithFullMemoryInfo = 0x00000800,
            MiniDumpWithThreadInfo = 0x00001000,
            MiniDumpWithCodeSegs = 0x00002000
        }
        [DllImport("dbghelp.dll")]
        static extern bool MiniDumpWriteDump(
            IntPtr hProcess,
            Int32 ProcessId,
            IntPtr hFile,
            MINIDUMP_TYPE DumpType,
            IntPtr ExceptionParam,
            IntPtr UserStreamParam,
            IntPtr CallackParam);

        public CrashDump() { }

        public static string MiniDumpToFile(string folderPath)
        {
            lock (obj)
            {
                try
                {
                    FileStream fsToDump = null;
                    string path = Path.Combine(folderPath, DateTime.Now.ToString("yyyyMMdd HHmmss") + ".dmp");
                    if (File.Exists(path))
                        fsToDump = File.Open(path, FileMode.Append);
                    else
                        fsToDump = File.Create(path);

                    Process[] processes = Process.GetProcessesByName("Squarebit.Apms.Terminal.Wpf");
                    Process thisProcess = null;
                    if (processes.Length > 0)
                    {
                        thisProcess = processes[0];
                        MiniDumpWriteDump(thisProcess.Handle, thisProcess.Id, fsToDump.SafeFileHandle.DangerousGetHandle(), MINIDUMP_TYPE.MiniDumpNormal, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
                    }
                    fsToDump.Close();
                    return path;
                }
                catch { }

                return string.Empty;
            }
        }

        private static string GetPath()
        {
            string path = Directory.GetCurrentDirectory();
            path = Path.Combine(path, "logs", "crash", DateTime.Now.ToString("yyyy-MM-dd"));

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }
    }

    public class MailSender
    {
        public static void Send(string filePath)
        {
            var client = new SmtpClient("smtp.gmail.com", 25)
            {
                Credentials = new NetworkCredential("squarebit.gpms@gmail.com", "Khongcop@55"),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
            };

            MailMessage msg = new MailMessage();

            msg.From = new MailAddress("squarebit.gpms@gmail.com");
            msg.To.Add("squarebit.gpms@gmail.com");
            msg.Subject = "[Squarebit.Apms.Terminal.Wpf] Crash Report";
            msg.Body = "Here you are !";

            Attachment attachment;
            attachment = new System.Net.Mail.Attachment(filePath);
            msg.Attachments.Add(attachment);

            client.SendCompleted += client_SendCompleted;
            client.SendAsync(msg, new object[] { msg, attachment });
            Console.WriteLine("Sent");
        }

        static void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            object[] objs = (object[])e.UserState;
            MailMessage msg = (MailMessage)objs[0];
            msg.Dispose();
            Attachment att = (Attachment)objs[1];
            att.Dispose();
        }
    }

    public class BugReporter
    {
        static BugListener server;

        public void Start(IHostSettings hostSettings)
        {
            server = new BugListener(hostSettings.StoragePath);
            Task.Factory.StartNew(() => {
                server.StartListening();
            }, TaskCreationOptions.LongRunning);
        }

        public void Stop()
        {

        }
    }
}
