﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using RestSharp;
using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ApmsClientService
{
    public class ImageReplicator
    {
        private IWebClient _webClient;
        private int _terminalId;
        private bool _isRunning = false;
        private IStorageService _storageService;
        private ConcurrentDictionary<string, IConnection> _connections = new ConcurrentDictionary<string,IConnection>();

        public ImageReplicator()
        {
            
        }

        public void Start(IHostSettings hostSettings)
        {
            if (_isRunning) return;
            if (hostSettings.Terminal.Ip == null || hostSettings.Terminal.Ip.Length == 0) return;
            _isRunning = true;
            _localIp = null;
            _terminalId = hostSettings.Terminal.Id;
            _webClient = new WebClientBasic(hostSettings);
            _storageService = new StorageService(hostSettings, _webClient);
            string primaryIP = hostSettings.PrimaryServerIP.Split(':')[0];
            string secondaryIP = hostSettings.SecondaryServerIP.Split(':')[0];
            Task.Run(() => { DoProcess(primaryIP); });
            if(primaryIP != secondaryIP)
                Task.Run(() => { DoProcess(secondaryIP); });
        }

        private void DoProcess(string serverIP)
        {
            while (_isRunning)
            {
                ConnectionFactory connectionFactory = new ConnectionFactory();
                connectionFactory.HostName = serverIP;
                connectionFactory.Port = AmqpTcpEndpoint.UseDefaultPort;
                connectionFactory.UserName = "apms";
                connectionFactory.Password = "nopass";
                connectionFactory.Protocol = Protocols.FromEnvironment();
                connectionFactory.VirtualHost = "/";
                connectionFactory.RequestedConnectionTimeout = 1000;
                try
                {
                    IConnection connection = connectionFactory.CreateConnection();
                    _connections[serverIP] = connection;
                    DoProcess(connection, _webClient);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                if (!_isRunning)
                    break;
                Thread.Sleep(1000);
            }
            Console.WriteLine("Stop");
        }

        private void DoProcess(IConnection connection, IWebClient webClient)
        {
            IModel channel = connection.CreateModel();
            string queueName = "Imagerepl." + LocalIPAddress();
            channel.QueueDeclare(queueName, false, false, false, null);
            var consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(queueName, true, consumer);
            while (_isRunning)
            {
                try
                {
                    var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    var message = Encoding.UTF8.GetString(ea.Body);
                    Dictionary<string, string> messageObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(message);
                    if (messageObj["type"] == "add")
                    {
                        AddImages(webClient, messageObj["host"], messageObj["front_image"], messageObj["back_image"], messageObj["card_id"]);
                    }
                    else if (messageObj["type"] == "del")
                    {
                        DeleteImages(messageObj["front_image"], messageObj["back_image"]);
                    }
                }
                catch(Exception ex) 
                {
                    Console.WriteLine(ex.Message);
                    if (ex is EndOfStreamException && ex.Message == "SharedQueue closed")
                    {
                        break;
                    }
                }
            }
        }

        private void AddImages(IWebClient webClient, string host, string frontImage, string backImage, string cardId)
        {
            if (host.StartsWith(LocalIPAddress()))
            {
                CloneCurrent(frontImage);
                CloneCurrent(backImage);
            }
            else
            {
                byte[] frontImageData = webClient.DownloadData(host, "images/" + frontImage);
                byte[] backImageData = webClient.DownloadData(host, "images/" + backImage);
                if (frontImageData == null || backImageData == null) return;
                if (!_storageService.SaveImageSync(frontImage, frontImageData)) return;
                //File.WriteAllBytes(@"D:\Upload\front.jpg", frontImageData);
                if (!_storageService.SaveImageSync(backImage, backImageData)) return;
                //File.WriteAllBytes(@"D:\Upload\back.jpg", backImageData);
                var request = new RestRequest(string.Format("/api/cards/{0}/imagehosts/", cardId), Method.POST);
                request.AddParameter("id", _terminalId);
                _webClient.ExecuteSync(request);

                _storageService.SaveImageSync(GetCurrentImagePath(frontImage), frontImageData);
                _storageService.SaveImageSync(GetCurrentImagePath(backImage), backImageData);
            }
        }

        private void CloneCurrent(string imagePath)
        {
            string imageSavePath = _storageService.CreateImageSavePath(imagePath);
            if (File.Exists(imageSavePath))
            {
                string cloneImagePath = _storageService.CreateImageSavePath(GetCurrentImagePath(imagePath));
                string directoryPath = Path.GetDirectoryName(cloneImagePath);
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                File.Copy(imageSavePath, cloneImagePath);
            }
        }

        private string GetCurrentImagePath(string originalPath)
        {
            string[] parts = originalPath.Split('/');
            string[] dashParts = originalPath.Split('_');
            return "current\\" + parts[2] + "\\" + parts[3] + "_" + dashParts[dashParts.Length - 1];
        }

        private void DeleteImages(string frontImage, string backImage)
        {
            string frontImageSavePath = _storageService.CreateImageSavePath(frontImage);
            DeleteFile(frontImageSavePath);
            string backImageSavePath = _storageService.CreateImageSavePath(backImage);
            DeleteFile(backImageSavePath);
            string currentFrontImageSavePath = _storageService.CreateImageSavePath(GetCurrentImagePath(frontImage));
            DeleteFile(currentFrontImageSavePath);
            string currentBackImageSavePath = _storageService.CreateImageSavePath(GetCurrentImagePath(backImage));
            DeleteFile(currentBackImageSavePath);
        }
        
        private void DeleteFile(string imagePath)
        {
            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);
            }
        }

        private string _localIp = null;

        private string LocalIPAddress()
        {
            if (_localIp == null)
            {
                IPHostEntry host;
                string localIP = "";
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                        break;
                    }
                }
                _localIp = localIP;
            }
            return _localIp;
        }

        public void Stop()
        {
            if (!_isRunning) return;
            _isRunning = false;
            foreach(IConnection conn in _connections.Values)
            {
                if(conn != null && conn.IsOpen)
                {
                    conn.Close();
                }
            }
        }
    }
}
