﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Squarebit.Apms.Terminal.Core.Services;

namespace ApmsClientService
{
    public class WebServer
    {
        private string _exeDirectory;
        private string _nginxDirectory;
        private string _nginxConfigFilePath;
        private string _nginxSampleFilePath;
        private bool _isRunning = false;
        private IHostSettings _hostSettings;
        public bool IsRunning { get { return _isRunning; } }

        public WebServer()
        {
            _exeDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            _nginxDirectory = Path.Combine(_exeDirectory, "nginx-1.7.4");
            _nginxConfigFilePath = Path.Combine(_nginxDirectory, "conf", "nginx.conf");
            _nginxSampleFilePath = Path.Combine(_exeDirectory, "Resources", "sample_nginx.conf");
        }

        private void ReloadNginxConfigWithImagePathSetting()
        {
            string rawData = File.ReadAllText(_nginxSampleFilePath);
            string storagePath = _hostSettings.StoragePath.Replace('\\', '/');
            if (storagePath[storagePath.Length - 1] != '/') storagePath += '/';
            File.WriteAllText(_nginxConfigFilePath, rawData.Replace(@"[$$$]", storagePath));
        }

        public void Start(IHostSettings hostSettings)
        {
            if (_isRunning) return;
            _isRunning = true;
            _hostSettings = hostSettings;
            ReloadNginxConfigWithImagePathSetting();
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.WorkingDirectory = _nginxDirectory;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = "nginx.exe";
                myProcess.Start();
            }
            catch { }
        }

        public void Stop()
        {
            if (!_isRunning) return;
            _isRunning = false;
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.WorkingDirectory = _nginxDirectory;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = "nginx.exe";
                myProcess.StartInfo.Arguments = "-s stop";
                myProcess.Start();
            }
            catch { }
        }
    }
}
