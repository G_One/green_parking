﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using RestSharp;
using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Services;

namespace ApmsClientService
{
    public class HealthReporter
    {
        private Timer _reportHealthTimer;
        private IWebClient _webClient;
        private int _terminalId;

        public HealthReporter()
        {
            _reportHealthTimer = new Timer();
        }

        public void Start(IHostSettings hostSettings)
        {
            _terminalId = hostSettings.Terminal.Id;
            _webClient = new WebClientBasic(hostSettings);
            _reportHealthTimer.Interval = 60000;
            _reportHealthTimer.Elapsed += _reportHealthTimer_Elapsed;
            _reportHealthTimer.Start();
        }

        public void Stop()
        {
            _reportHealthTimer.Stop();
            _reportHealthTimer.Elapsed -= _reportHealthTimer_Elapsed;
        }

        void _reportHealthTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var request = new RestRequest(string.Format("/api/terminals/{0}/health/", _terminalId), Method.PUT);
            _webClient.ExecuteSync(request);
        }
    }
}
