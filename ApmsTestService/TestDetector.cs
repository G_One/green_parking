﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ApmsTestService
{
    public class TestDetector
    {
        const string ConfigPath = @"C:\ProgramData\APMS\UserPrefs\";
        public const string TestConfigPath = ConfigPath + "Testing.lock";

        public static void StartTest()
        {
            if(!File.Exists(TestConfigPath))
            {
                FileStream file = File.Create(TestConfigPath);
                file.Close();
            }
            if(!File.Exists(ConfigPath + "HostSettingsTest.conf") && File.Exists(ConfigPath + "HostSettings.conf"))
            {
                File.Copy(ConfigPath + "HostSettings.conf", ConfigPath + "HostSettingsTest.conf");
            }
            if (!File.Exists(ConfigPath + "SystemSettingsTest.conf") && File.Exists(ConfigPath + "SystemSettings.conf"))
            {
                File.Copy(ConfigPath + "SystemSettings.conf", ConfigPath + "SystemSettingsTest.conf");
            }
        }

        public static void StopTest()
        {
            if(File.Exists(TestConfigPath))
            {
                File.Delete(TestConfigPath);
            }
        }

        public static bool IsTesting()
        {
            return File.Exists(TestConfigPath);
        }
    }
}
