﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.ServiceProcess;


namespace ApmsTestService
{
    public partial class ApmsTestService : ServiceBase
    {
        public GateLaunchService gateLaunchService = null;
        public ApmsTestService()
        {
            InitializeComponent();
            gateLaunchService = new GateLaunchService();
        }

        protected override void OnStart(string[] args)
        {
            gateLaunchService.Start();
        }

        protected override void OnStop()
        {
            gateLaunchService.Stop();
        }
    }
}
