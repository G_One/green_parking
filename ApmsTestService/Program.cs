﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace ApmsTestService
{
    static class Program
    {
        public static bool stop = false;
        public static string executePath = null;
        static GateLaunchService service = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            executePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            if (args.Length == 0)
            {
                AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
                service = new GateLaunchService();
                service.Start();
                while (!stop)
                {
                    Thread.Sleep(1000);
                }
                service.Stop();
            }
            else if (args.Length == 1)
            {
                switch (args[0])
                {
                    case "--install":
                    case "-i":
                        Console.WriteLine("Installing...");
                        InstallService();
                        Console.WriteLine("Installed!!!");
                        Console.WriteLine("Starting...");
                        StartService();
                        Console.WriteLine("Started!!!");
                        break;
                    case "--uninstall":
                    case "-u":
                        Console.WriteLine("Stopping...");
                        StopService();
                        Console.WriteLine("Stopped!!!");
                        Console.WriteLine("Uninstalling...");
                        UninstallService();
                        Console.WriteLine("Uninstalled...");
                        break;
                    default:
                        Console.WriteLine("Unknown argument");
                        throw new NotImplementedException();
                }
            }
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new ApmsTestService() 
            //};
            //ServiceBase.Run(ServicesToRun);
        }

        static void InstallService()
        {
            // Remove old service
            UninstallService();
            // Build file setting
            string data = File.ReadAllText(executePath + "\\ApmsTestServiceStartUpSample.xml");
            data = data.Replace("$$$ Username $$$", Environment.UserName);
            data = data.Replace("$$$ Path $$$", executePath + "\\ApmsTestService.exe");
            string configPath = executePath + "\\ApmsTestServiceStartUp.xml";
            File.WriteAllText(configPath, data);
            // Install new service
            Process.Start("schtasks", "/Create /XML \"" + configPath + "\" /TN \"ApmsTestServiceStartUp\"").WaitForExit();
        }

        static void StartService()
        {
            Process.Start("schtasks", "/Run /TN \"ApmsTestServiceStartUp\"").WaitForExit();
        }

        static void StopService()
        {
            Process.Start("schtasks", "/End /TN \"ApmsTestServiceStartUp\"").WaitForExit();
        }

        static void UninstallService()
        {
            // Remove old service
            Process.Start("schtasks", "/Delete /TN \"ApmsTestServiceStartUp\" /F").WaitForExit();
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            service.Stop();
        }

        static void DisableFirewall()
        {
            try
            {
                Process proc = new Process();
                string top = "netsh.exe";
                proc.StartInfo.Arguments = "Firewall set opmode disable";
                proc.StartInfo.FileName = top;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
