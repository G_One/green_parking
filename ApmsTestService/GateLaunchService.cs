﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Security.Cryptography;

namespace ApmsTestService
{
    [ServiceContract(Namespace = "http://ApmsTestService")]
    public interface IApmsLauncher
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "Health")]
        string Health();

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped, 
            UriTemplate="LaunchGate?primaryServer={primaryServer}&secondaryServer={secondaryServer}&testLocator={testLocator}")]
        string LaunchGate(string primaryServer, string secondaryServer, string testLocator);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "CloseGate")]
        string CloseGate();

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "Stop")]
        string Stop();

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "Shutdown")]
        string Shutdown();

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "UpdateGate?packageUrl={packageUrl}&checksum={checksum}")]
        string UpdateGate(string packageUrl, string checksum);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "RollbackGate")]
        string RollbackGate();
    }

    public class ApmsLauncher : IApmsLauncher
    {
        private static Process launchedApp = null;

        private string rootDir;
        private string installDir;
        private string backupDir;
        private string tempDir;
        private string installDirTemp;
        private string downloadFilePath;
        private string gateAppPath;
        private string servicePath;
        private string gateAppPathTemp;

        public ApmsLauncher()
        {
            rootDir = Path.GetDirectoryName(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
            installDir = rootDir + @"\GPMS";
            backupDir = rootDir + @"\GPMS_BACKUP";
            tempDir = rootDir + @"\GPMS_TEMP";
            installDirTemp = tempDir + "\\GPMS";
            downloadFilePath = tempDir + "\\GPMS.zip";
            gateAppPath = installDir + @"\Gate\Squarebit.Apms.Terminal.Wpf.exe";
            servicePath = installDir + @"\BackgroundService\ApmsClientService.exe";
            gateAppPathTemp = installDirTemp + @"\Gate\Squarebit.Apms.Terminal.Wpf.exe";
        }

        public string Health()
        {
            return "OK";
        }

        private void CloseApp()
        {
            TestDetector.StopTest();
            //if (launchedApp != null)
            //{
            //    if(!launchedApp.HasExited)
            //        launchedApp.Kill();
            //    launchedApp = null;
            //}
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("Squarebit.Apms.Terminal.Wpf"))
            {
                p.Kill();
            }
        }

        private void LaunchApp(string appPath, string arguments = null)
        {
            CloseApp();
            TestDetector.StartTest();
            if(arguments == null)
            {
                launchedApp = Process.Start(appPath);
            }
            else
            {
                launchedApp = Process.Start(appPath, arguments);
            }
        }

        public string LaunchGate(string primaryServer, string secondaryServer, string testLocator)
        {
            LaunchApp(gateAppPath, string.Format("-m test -f {0} {1} -s {2}", primaryServer, secondaryServer, testLocator));
            return "OK";
            //string configFile = Path.Combine(Program.executePath, "launch.config");
            //if(File.Exists(configFile))
            //{
            //    string[] lines = File.ReadAllLines(configFile);
            //    if(lines.Length == 1)
            //    {
            //        LaunchApp(lines[0]);
            //        return "OK";
            //    }
            //    else if(lines.Length == 2)
            //    {
            //        LaunchApp(lines[0], lines[1]);
            //        return "OK";
            //    }
            //    else
            //    {
            //        return "Invalid config file";
            //    }
            //}
            //return "Config file not found in " + Program.executePath;
        }

        public string CloseGate()
        {
            CloseApp();
            return "OK";
        }

        public string Stop()
        {
            Program.stop = true;
            return "OK";
        }

        private string GetChecksum(string filepath)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filepath))
                {
                    byte[] bytes = md5.ComputeHash(stream);
                    StringBuilder result = new StringBuilder(bytes.Length * 2);
                    for (int i = 0; i < bytes.Length; i++)
                        result.Append(bytes[i].ToString("X2"));
                    return result.ToString();
                }
            }
        }

        public string UpdateGate(string packageUrl, string checksum)
        {
            CloseApp();
            // Check temporatory directory
            if (!Directory.Exists(tempDir))
                Directory.CreateDirectory(tempDir);

            // Download package from server with checksum verify
            WebClient webClient = new WebClient();

            bool checksumOK = false;
            for (int i = 0; i < 3; i++)
            {
                if (File.Exists(downloadFilePath))
                    File.Delete(downloadFilePath);
                if (Directory.Exists(installDirTemp))
                    Directory.Delete(installDirTemp, true);
                webClient.DownloadFile(packageUrl, downloadFilePath);
                string calcChecksum = GetChecksum(downloadFilePath);
                if (calcChecksum == checksum)
                {
                    checksumOK = true;
                    break;
                }
            }
            if (!checksumOK)
                return "Checksum failed";

            // Extract package
            ZipFile.ExtractToDirectory(downloadFilePath, tempDir);
            Process p = null;
            if (Directory.Exists(installDir))
            {
                if (!Directory.Exists(backupDir))
                    Directory.CreateDirectory(backupDir);
                // ZipFile.CreateFromDirectory(installDir, backupDir + "\\" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".zip");
                // Uninstall background service
                p = Process.Start(servicePath, "-u");
                p.WaitForExit();
                p.Close();
                // Move old app directory to backup
                Directory.Move(installDir, backupDir + "\\" + DateTime.Now.ToString("yyyyMMdd_HHmmss"));
            }

            // Move app directory from temp to real
            Directory.Move(installDirTemp, installDir);
            // Install background service
            p = Process.Start(servicePath, "-i");
            p.WaitForExit();
            p.Close();

            // Delete temp directory
            Directory.Delete(tempDir, true);
            return "OK";
        }

        public string RollbackGate()
        {
            CloseApp();
            string selectDir = null;
            foreach (string path in Directory.GetDirectories(backupDir))
            {
                if(selectDir == null)
                {
                    selectDir = path;
                }
                else
                {
                    if(selectDir.CompareTo(path) < 0)
                    {
                        selectDir = path;
                    }
                }
            }

            Process p = null;
            if (Directory.Exists(installDir))
            {
                // Uninstall background service
                p = Process.Start(servicePath, "-u");
                p.WaitForExit();
                p.Close();
                // Delete old app directory
                Directory.Delete(installDir, true);
            }
            // Copy backup to real
            DirectoryCopy(selectDir, installDir);
            // Install background service
            p = Process.Start(servicePath, "-i");
            p.WaitForExit();
            p.Close();
            return "OK";
        }

        public string Shutdown()
        {
            Process.Start("shutdown", "/s /t 0");
            return "OK";
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }

    public class GateLaunchService
    {
        public ServiceHost serviceHost = null;

        public GateLaunchService()
        {

        }

        public void Start()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            // Create a ServiceHost for the CalculatorService type and 
            // provide the base address.
            serviceHost = new WebServiceHost(typeof(ApmsLauncher));

            // Open the ServiceHostBase to create listeners and start 
            // listening for messages.
            serviceHost.Open();
        }

        public void Stop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }
    }
}
