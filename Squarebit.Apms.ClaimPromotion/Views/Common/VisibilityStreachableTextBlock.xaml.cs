﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Squarebit.Apms.ClaimPromotion.Views.Common
{
    /// <summary>
    /// Interaction logic for VisibilityStreachableTextBlock.xaml
    /// </summary>
    public partial class VisibilityStreachableTextBlock : UserControl
    {
        public VisibilityStreachableTextBlock()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty TBContentProperty = DependencyProperty.Register("TBContent", typeof(string), typeof(VisibilityStreachableTextBlock),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(OnTBContentChanged))
            ); /*StreachableTextBox.TBContentProperty.AddOwner(typeof(VisibilityStreachableTextBlock));*/
        public string TBContent
        {
            get { return (string)GetValue(TBContentProperty); }
            set { SetValue(TBContentProperty, value); }
        }

        #region ShouldFocuse
        public static readonly DependencyProperty ShouldFocuseProperty = StreachableTextBox.ShouldFocuseProperty.AddOwner(typeof(VisibilityStreachableTextBlock));  // DependencyProperty.RegisterAttached("ShouldFocuse", typeof(bool), typeof(VisibilityStreachableTextBlock), new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        public bool ShouldFocuse
        {
            get { return (bool)GetValue(ShouldFocuseProperty); }
            set { SetValue(ShouldFocuseProperty, value); }
        }

        public static readonly DependencyProperty TextAlignmentProperty = TextBlock.TextAlignmentProperty.AddOwner(typeof(VisibilityStreachableTextBlock));

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        private static void OnIsFocusedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is StreachableTextBox)
            {
                if ((bool)e.NewValue)
                {
                    TextBox tb = (d as StreachableTextBox).TextBoxChild;
                    tb.Focus();
                    tb.SelectAll();
                }
            }
        }
        private static void OnTBContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
            // .......

        }
        #endregion
    }
}
