﻿using Squarebit.Apms.Terminal.Core.ViewModels;

namespace Squarebit.Apms.ClaimPromotion.Views.AppViews
{
    /// <summary>
    /// Interaction logic for BootstrapView.xaml
    /// </summary>
    public partial class ClaimPromotionBootstrapView : BaseView
    {
        public new ClaimPromotionBootstrapViewModel ViewModel
        {
            get { return (ClaimPromotionBootstrapViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public ClaimPromotionBootstrapView()
        {
            InitializeComponent();
        }
    }
}
