﻿using Cirrious.MvvmCross.Binding.BindingContext;
using Squarebit.Apms.ClaimPromotion.Utility;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Squarebit.Apms.ClaimPromotion.Views.AppViews
{
    /// <summary>
    /// Interaction logic for BillCheckoutView.xaml
    /// </summary>
    public partial class BillCheckoutView : BaseView
    {
        bool _showMessage;
        public bool ShowMessage
        {
            get { return _showMessage; }
            set
            {
                _showMessage = value;
                if (_showMessage)
                {
                    MessageWarning.BeginAnimation(TextBlock.OpacityProperty, null);
                    MessageWarning.Opacity = 1.0;
                    DoubleAnimation fadingAnimation = new DoubleAnimation();
                    fadingAnimation.BeginTime = TimeSpan.FromSeconds(3);
                    fadingAnimation.From = 1;
                    fadingAnimation.To = 0;
                    fadingAnimation.Duration = new Duration(TimeSpan.FromSeconds(3.0));
                    fadingAnimation.Completed += (sender, e) => MessageWarning.Opacity = 0.0;
                    MessageWarning.BeginAnimation(TextBlock.OpacityProperty, fadingAnimation);
                }
            }
        }

        ObservableCollection<Bill> _bills;
        public ObservableCollection<Bill> Bills
        {
            get { return _bills; }
            set
            {
                _bills = value;
            }
        }

        ObservableCollection<Coupon> _coupons;
        public ObservableCollection<Coupon> Coupons
        {
            get { return _coupons; }
            set
            {
                _coupons = value;
            }
        }

        CheckIn _checkInInfo;
        public CheckIn CheckInInfo
        {
            get { return _checkInInfo; }
            set
            {
                _checkInInfo = value;
                if (_checkInInfo != null)
                {
                    InputGrid.IsEnabled = true;
                    CompanyNameTextBox.Focus();
                }
                else
                {
                    InputGrid.IsEnabled = false;
                }
            }
        }

        public new BillCheckoutViewModel ViewModel
        {
            get { return (BillCheckoutViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        protected override void ShowBusyIndicator(bool show)
        {
            ProgressIndicator.IsBusy = show;
        }

        public BillCheckoutView()
        {
            InitializeComponent();
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            ViewModel.StartCardReader();

            BillNumberTextBox.Focus();

            BillGridItemsChanged();
            CouponGridItemsChanged();

            ViewModel.Bills.CollectionChanged += (s, args) => {
                Application.Current.Dispatcher.BeginInvoke(
                   DispatcherPriority.Normal,
                   new Action(BillGridItemsChanged));
            };

            ViewModel.Coupons.CollectionChanged += (s, args) => {
                Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Normal,
                  new Action(CouponGridItemsChanged));
            };
        }

        public override void BindData()
        {
            base.BindData();

            var set = this.CreateBindingSet<BillCheckoutView, BillCheckoutViewModel>();
            set.Bind(this).For(c => c.ShowMessage).To(vm => vm.ShowMessage);
            set.Bind(this).For(c => c.CheckInInfo).To(vm => vm.CheckInInfo);
            set.Bind(this).For(c => c.Bills).To(vm => vm.Bills);
            set.Bind(this).For(c => c.Coupons).To(vm => vm.Coupons);
            set.Apply();
        }

        private void AddMoreTentant(object sender, RoutedEventArgs e)
        {
            var w = new UI.AddTenantWindow();
            w.ShowDialog();
        }

        private void AddDeleteButton(Grid table, int column, int row, RoutedEventHandler handler)
        {
            Button btn = new Button();
            btn.SetValue(Grid.ColumnProperty, column);
            btn.SetValue(Grid.RowProperty, row);
            btn.Background = new SolidColorBrush(Colors.Red);
            btn.Foreground = Brushes.White;
            btn.Content = "Xóa";
            table.Children.Add(btn);
        }

        private void AddColumnBinding(Grid table, int column, int row, object source, string property, IValueConverter converter = null, TextAlignment textAlign = TextAlignment.Left, string format = null)
        {
            Binding binding = new Binding();
            binding.Source = source;
            binding.Path = new PropertyPath(property);
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Converter = converter;
            if (!string.IsNullOrEmpty(format))
                binding.StringFormat = format;

            TextBox text = new TextBox();
            text.SetValue(Grid.ColumnProperty, column);
            text.SetValue(Grid.RowProperty, row);
            text.FontSize = 20;
            text.FontWeight = FontWeights.Medium;
            text.TextAlignment = TextAlignment.Right;
            BindingOperations.SetBinding(text, TextBox.TextProperty, binding);
            table.Children.Add(text);
        }

        private void AddColumnHeader(Grid table, int columnId, string name)
        {
            TextBlock tb = new TextBlock();
            tb.Text = name;
            tb.FontSize = 20;
            tb.FontWeight = FontWeights.Bold;
            tb.Foreground = Brushes.White;
            tb.TextAlignment = TextAlignment.Center;
            Border bd = new Border();
            bd.SetValue(Grid.ColumnProperty, columnId);
            bd.Background = Brushes.Green;
            bd.BorderBrush = Brushes.Black;
            bd.BorderThickness = new Thickness(1);
            bd.Child = tb;
            table.Children.Add(bd);
        }

        private void BillGridItemsChanged()
        {
            var rows = this.Bills.Count + 1;
            BillGrid.Children.Clear();

            BillGrid.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#ecf0f1"));
            BillGrid.SetValue(GridHelpers.ColumnCountProperty, 4);
            BillGrid.SetValue(GridHelpers.StarColumnsProperty, "0,1,2");
            BillGrid.SetValue(GridHelpers.RowCountProperty, rows);

            AddColumnHeader(BillGrid, 0, "Tên Gian Hàng");
            AddColumnHeader(BillGrid, 1, "Số Hóa Đơn");
            AddColumnHeader(BillGrid, 2, "Thành Tiền");

            for (int i = 0; i < rows - 1; i++)
            {
                AddColumnBinding(BillGrid, 0, i + 1, Bills[i], "CompanyName");
                AddColumnBinding(BillGrid, 1, i + 1, Bills[i], "BillCode");
                AddColumnBinding(BillGrid, 2, i + 1, Bills[i], "BillAmount", new StringToNumberConverter(), TextAlignment.Right);

                var tmp = i;
                AddDeleteButton(BillGrid, 3, i + 1, (s, e) => {
                    ViewModel.RemoveBill(tmp);
                });
            }
        }


        private void CouponGridItemsChanged()
        {
            var rows = this.Coupons.Count + 1;
            CouponGrid.Children.Clear();

            CouponGrid.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#ecf0f1"));
            CouponGrid.SetValue(GridHelpers.ColumnCountProperty, 4);
            CouponGrid.SetValue(GridHelpers.StarColumnsProperty, "0,1,2");
            CouponGrid.SetValue(GridHelpers.RowCountProperty, rows);

            AddColumnHeader(CouponGrid, 0, "Tên Gian Hàng");
            AddColumnHeader(CouponGrid, 1, "Mã Voucher");
            AddColumnHeader(CouponGrid, 2, "Giảm Giá");  

            for (int i = 0; i < rows - 1; i++)
            {
                AddColumnBinding(CouponGrid, 0, i + 1, Coupons[i], "CompanyName");
                AddColumnBinding(CouponGrid, 1, i + 1, Coupons[i], "CouponCode");
                AddColumnBinding(CouponGrid, 2, i + 1, Coupons[i], "CouponDiscount", new StringToNumberConverter(), TextAlignment.Right);

                var tmp = i;
                AddDeleteButton(CouponGrid, 3, i + 1, (s, e) => {
                    ViewModel.RemoveCoupon(tmp);
                });
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.GetCheckIn("34D0D31A");
        }
    }
}