﻿using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace Squarebit.Apms.ClaimPromotion.Views.AppViews
{
    /// <summary>
    /// Interaction logic for SearchView.xaml
    /// </summary>
    public partial class SearchInParkingView : BaseView
    {
        public new SearchInParkingViewModel ViewModel 
        {
            get { return (SearchInParkingViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public SearchInParkingView()
        {
            InitializeComponent();

            ParkingSessionGrid.AddHandler(DataGrid.KeyUpEvent, new RoutedEventHandler(DataGrid_KeyUp), true);

            //tbCardLabel.SearchTextBoxKeyUp += SearchTextBoxKeyUp;
            //tbPlateNumber.SearchTextBoxKeyUp += SearchTextBoxKeyUp;
        }

        void SearchTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            ViewModel.KeyPressed(sender, e);
        }

        public override void ViewLoaded(object sender, RoutedEventArgs e)
        {
            base.ViewLoaded(sender, e);

            //tbCardLabel.Focusable = true;
            //tbCardLabel.Focus();
        }

        private void DataGrid_KeyUp(object sender, RoutedEventArgs e)
        {
            //Key key = (e as KeyEventArgs).Key;
            //if (key == Key.Delete)
            //    ViewModel.KeyPressed(sender, e as KeyEventArgs);
        }

        private void ParkingSessionGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var text = (e.EditingElement as TextBox).Text;
            ViewModel.EditEndingCommand.Execute(text);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.SelectPageCommand.Execute(null);
        }
    }
}
