﻿using Cirrious.MvvmCross.Wpf.Views;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.ClaimPromotion.Views
{
    public interface IApmsPresenter
    {
        void CloseCurrentView();

        BaseView CreateView<T>() where T : BaseViewModel;
    }
}
