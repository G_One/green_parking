﻿using Squarebit.Apms.Terminal.Core;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Wpf.Platform;
using Cirrious.MvvmCross.Wpf.Views;
using System;
using System.Windows.Controls;
using System.Windows.Threading;
using Cirrious.CrossCore;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.ClaimPromotion.Services;
using Cirrious.MvvmCross.BindingEx.WindowsShared;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Devices.Dal;
using Squarebit.Apms.ClaimPromotion.UI;
using Cirrious.CrossCore.Converters;
using System.Collections.Generic;
using System.Reflection;

namespace Squarebit.Apms.ClaimPromotion
{
    public class Setup : MvxWpfSetup
    {
        private ArgumentParameterManager _params;

        public Setup(ArgumentParameterManager param, Dispatcher dispatcher, IMvxWpfViewPresenter presenter)
            : base(dispatcher, presenter)
        {
            _params = param;
        }

        protected override IMvxApplication CreateApp()
        {
            return new Squarebit.Apms.Terminal.Core.ApmsApp();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

		protected override void InitializeFirstChance()
		{
			base.InitializeFirstChance();

            Mvx.RegisterSingleton<IRunModeManager>(new RunModeManager());

            Mvx.Resolve<IRunModeManager>().ArgumentParams = _params.Parameters;

            Mvx.LazyConstructAndRegisterSingleton<ILogService, LogService>();

            Mvx.LazyConstructAndRegisterSingleton<IUIService>(() => new UIService());

            Mvx.LazyConstructAndRegisterSingleton<ILocalizeService>(() => new LocalizeService());
        }

        protected override void InitializeLastChance()
        {
            base.InitializeLastChance();

            var builder = new MvxWindowsBindingBuilder();
            builder.DoRegistration();

            Mvx.CallbackWhenRegistered<IMvxValueConverterRegistry>(FillValueConverters);
        }

        private void FillValueConverters(IMvxValueConverterRegistry registry)
        {            
            registry.AddOrOverwrite("ByteImage", new ByteImageValueConverter());
        }

        //protected virtual List<Assembly> ValueConverterAssemblies
        //{
        //    get
        //    {
        //        var toReturn = new List<Assembly>();
        //        toReturn.AddRange(GetViewModelAssemblies());
        //        toReturn.AddRange(GetViewAssemblies());
        //        return toReturn;
        //    }
        //}
    }
}
