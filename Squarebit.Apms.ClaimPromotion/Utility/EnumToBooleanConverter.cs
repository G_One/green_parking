﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Squarebit.Apms.ClaimPromotion.Utility
{
    public class EnumToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }

    public class StringToNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                long tmp = (long)value;
                if (tmp != 0)
                    return tmp.ToString("N0");
                else
                    return "";
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string tmp = value as string;
            bool isNegative = false;

            if (string.IsNullOrEmpty(tmp))
                return 0;

            if(tmp.First().Equals('-'))
            {
                tmp = tmp.Substring(1);
                isNegative = true;
            }
            float result;
            float.TryParse(tmp, NumberStyles.AllowThousands, null, out result);

            return result * (isNegative ? -1 : 1);
        }
    }
}