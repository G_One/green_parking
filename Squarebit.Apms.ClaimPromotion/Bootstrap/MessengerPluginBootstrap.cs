using Cirrious.CrossCore.Plugins;

namespace Squarebit.Apms.ClaimPromotion.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}