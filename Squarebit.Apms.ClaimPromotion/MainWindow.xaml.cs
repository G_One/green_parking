﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Windows.Threading;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.Views;
using Cirrious.CrossCore;
using Squarebit.Apms.ClaimPromotion.UI;
using Squarebit.Apms.ClaimPromotion.Views;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Cirrious.MvvmCross.ViewModels;
using Squarebit.Apms.Terminal.Core.Services;

namespace Squarebit.Apms.ClaimPromotion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        IMvxViewDispatcher _viewDispatcher;
        IMvxMessenger _keyPressedMessenger;
        SystemNotificationOverlay _systemNotification;
        IUserPreferenceService _userPreferenceService;
        DispatcherTimer _timer;

        public MainWindow()
        {
            InitializeComponent();

            this.StateChanged += (sender, e) => {
                this.MainView.Focus();
            };

            _viewDispatcher = Mvx.Resolve<IMvxViewDispatcher>();
            _keyPressedMessenger = Mvx.Resolve<IMvxMessenger>();

            this.Background = Theme.Theme.Background;
            //this.Topmost = true;
            this.IgnoreTaskbarOnMaximize = true;

            _systemNotification = new SystemNotificationOverlay()
            {
                Container = this
            };

            this.Loaded += MainWindow_Loaded;
            this.Closed += MainWindow_Closed;
            this.KeyUp += MainWindow_KeyUp; 

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += timer_Tick;
            _timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            lblTime.Content = DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss tt");
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _userPreferenceService = Mvx.Resolve<IUserPreferenceService>();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Bạn có muốn thoát chương trình?", "Xác nhận", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                _userPreferenceService.ClaimPromotionSettings.ForceSave();
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.F12:
                    {
                        ButtonConfigClick(null, null);
                        break;
                    }
                default:
                    break;
            }

            if (_keyPressedMessenger.HasSubscriptionsFor<KeyPressedMessage>())
            {
                _keyPressedMessenger.Publish(new KeyPressedMessage(sender, e));
            }
        }

        public void PresentInRegion(BaseView frameworkElement, string regionName)
        {
            this.MainView.Children.Add(frameworkElement);
        }

        public void ShowInMainView(BaseView frameworkElement)
        {
            this.MainView.Children.Clear();
            frameworkElement.MainWindow = this;
            this.MainView.Children.Add(frameworkElement);
        }

        public void CloseCurrentView()
        {
            int count = this.MainView.Children.Count;
            if (count > 0)
                this.MainView.Children.RemoveAt(count - 1);
        }

        private void ButtonConfigClick(object sender, RoutedEventArgs e)
        {
            var passwordWindow = new PasswordWindow()
            {
                Owner = this,
                Title = "Nhập mật mã để cấu hình",
            };
            passwordWindow.AuthenticateSuccess += (arg1, arg2) => {
                _viewDispatcher.ShowViewModel(new MvxViewModelRequest(typeof(ConfigViewModel), null, null, null));
            };
            passwordWindow.ShowDialog();
        }
    }
}
