﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Views;
using MahApps.Metro.Controls;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Squarebit.Apms.ClaimPromotion.UI
{
	/// <summary>
	/// Interaction logic for PasswordWindow.xaml
	/// </summary>
	public partial class PasswordWindow : MetroWindow
	{
		private ILocalizeService _locale;
		private IUserPreferenceService _userPreferenceService;

        public event EventHandler AuthenticateSuccess;

		public PasswordWindow()
		{
			InitializeComponent();

			_userPreferenceService = Mvx.Resolve<IUserPreferenceService>();
			_locale = Mvx.Resolve<ILocalizeService>();

			this.Loaded += PasswordWindow_Loaded;

			this.PreviewKeyDown += new KeyEventHandler(HandleEsc);
		}

		void PasswordWindow_Loaded(object sender, RoutedEventArgs e)
		{
			textPassword.Focus();
		}

		private void HandleEsc(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
				Close();
		}

		private void ConfigButton_Click(object sender, RoutedEventArgs e)
		{
			Config();
		}

		private void textPassword_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				Config();
			}
		}

        private void Config()
        {
            if (string.IsNullOrEmpty(textPassword.Password))
            {
                textResult.Text = _locale.GetText("system.password_empty");
                return;
            }

            if (EncryptionUtility.GetMd5Hash(textPassword.Password) != _userPreferenceService.OptionsSettings.MasterPasswordMd5)
            {
                textResult.Text = _locale.GetText("system.invalid_master_password");
                return;
            }

            this.Hide();

            var handle = AuthenticateSuccess;
            if (handle != null)
                handle(null, null);
        }
	}
}