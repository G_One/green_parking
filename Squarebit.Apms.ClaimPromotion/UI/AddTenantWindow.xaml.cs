﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using MahApps.Metro.Controls;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Squarebit.Apms.ClaimPromotion.UI
{
    /// <summary>
    /// Interaction logic for AddTenantWindow.xaml
    /// </summary>
    public partial class AddTenantWindow : MetroWindow
    {
        IClaimPromotionSettings _claimPromoSettings;
        IMvxMessenger _tenantMessenger;

        public AddTenantWindow()
        {
            InitializeComponent();

            _claimPromoSettings = Mvx.Resolve<IClaimPromotionSettings>();
            _tenantMessenger = Mvx.Resolve<IMvxMessenger>();
        }

        private void ConfigButton_Click(object sender, RoutedEventArgs e)
        {
            AddTenant();
        }

        private void textTentant_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                AddTenant();
            }
        }

        private void AddTenant()
        {
           /* _claimPromoSettings.TenantNames.Add(textTentant.Text);
            Close();

            if (_tenantMessenger.HasSubscriptionsFor<AddTenantMessage>())
            {
                _tenantMessenger.Publish(new AddTenantMessage(this, textTentant.Text));
            }*/
        }
    }
}
