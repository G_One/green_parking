﻿#define CLAIM_PROMOTION

using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Shell;
using NLog;
using Squarebit.Apms.ClaimPromotion.Views;
using Squarebit.Apms.Terminal.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Squarebit.Apms.ClaimPromotion
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, ISingleInstanceApp
    {
        private bool _setupComplete = false;
        ArgumentParameterManager _params;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        // TODO: Make this unique!
        private const string Unique = "Change this to something that uniquely identifies your program.";

        /// <summary>
        /// Xử lý việc mở app như 1 singleton, có nghĩa là chỉ mở được 1 màn hình của ứng dụng này thôi
        /// không mở nhiều cái cùng lúc được
        /// </summary>
        [STAThread]
        public static void Main()
        {
            if (SingleInstance<App>.InitializeAsFirstInstance(Unique))
            {
                var application = new App();
                application.InitializeComponent();
                application.Run();

                // Allow single instance code to perform cleanup operations
                SingleInstance<App>.Cleanup();
            }
        }

        #region ISingleInstanceApp Members
        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            // Bring window to foreground
            if (this.MainWindow.WindowState == WindowState.Minimized)
            {
                this.MainWindow.WindowState = WindowState.Normal;
            }

            this.MainWindow.Activate();

            return true;
        }
        #endregion

        protected override void OnStartup(StartupEventArgs e)
        {
            //_version = OtherUtilities.GetVersion();
            _params = new ArgumentParameterManager();

            base.OnStartup(e);

            //Squarebit.Apms.Terminal.Core.Services.IParkingFeeService sv = new Squarebit.Apms.Terminal.Core.Services.SaigonCentreFeeService();
            //var fee = sv.CalculateFee(Terminal.Core.Models.VehicleTypeEnum.Bike, 0, new DateTime(2016, 6, 23, 15, 0, 0), new DateTime(2016, 6, 24, 14, 0, 0));
            //Console.WriteLine(fee);
        }

        private void DoSetup()
        {
            var presenter = new ApmsPresenter(MainWindow);

            var setup = new Setup(_params, Dispatcher, presenter);
            setup.Initialize();

            var start = Mvx.Resolve<IMvxAppStart>();
            start.Start();

            _setupComplete = true;
            _logger.Fatal("App start");
        }

        protected override void OnActivated(System.EventArgs e)
        {
            if (!_setupComplete)
                DoSetup();

            base.OnActivated(e);
        }
    }
}