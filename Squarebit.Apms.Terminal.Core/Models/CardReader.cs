﻿using Newtonsoft.Json;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Models
{
    public class CardReaderWrapper
    {
        public string SerialNumber { get; set; }

        [JsonIgnore]
        ICardReader _cardReader;
        [JsonIgnore]
        public ICardReader RawCardReader
        {
            get { return _cardReader; }
            set
            {
                if (_cardReader == value) return;
                _cardReader = value;
                _cardReader.ReadingCompleted += ReceivedCard;
                _cardReader.TakingOffCompleted += TakeoffCard;
            }
        }

        /// <summary>
        /// Card information
        /// </summary>
        CardReaderInfo CardReaderInfo { get { return RawCardReader.CardReaderInfo; } }

        /// <summary>
        /// Callback for receiving card id
        /// </summary>
        public virtual event CardReaderEventHandler ReadingCompleted;
        public void ReceivedCard(object sender, CardReaderEventArgs e)
        {
            CardReaderEventHandler handler = ReadingCompleted;

            if (handler != null)
                handler(this, e);
        }

        public virtual event CardReaderEventHandler TakingOffCompleted;
        public void TakeoffCard(object sender, CardReaderEventArgs e)
        {
            CardReaderEventHandler handler = TakingOffCompleted;

            if (handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Start card reader
        /// </summary>
        public void Run()
        {

        }

        /// <summary>
        /// Stop card reader
        /// </summary>
        public void Stop()
        {

        }

        public void Setup(string serialNumber)
        {

        }
    }
}