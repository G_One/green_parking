using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Squarebit.Devices.Dal;
using Cirrious.MvvmCross.Plugins.Messenger;
using Squarebit.Apms.Terminal.Core.Models;
using Cirrious.CrossCore;
using System.Threading;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class LoginSuccessMessage : MvxMessage
    {
        public LoginSuccessMessage(object sender) : base(sender) { }
    }

    //public class LogoutSuccessMessage : MvxMessage
    //{
    //    public LogoutSuccessMessage(object sender) : base(sender) { }
    //}

    public class LoginViewModel : BaseViewModel
    {
        IRunModeManager _modeManager;
        private IMvxMessenger _messenger;
        private IUserServiceLocator _userServiceLocator;
        private IResourceLocatorService _resourceLocator;
        private IUserService _userService;

        private string _username;
        private string _password;
		private string _resultMessage;

        public const string msgCardReaderDisconnect = "error.cardreader_disconnect";
        public const string msgInvalid = "login.invalid";
        public const string msgServerError = "error.server_error";
        public const string msgServerDisconnect = "error.server_disconnect";

		public string ResultMessage
		{
			get { return _resultMessage; }
			set
			{
				if (_resultMessage == value)
					return;

				_resultMessage = value;
				RaisePropertyChanged(() => ResultMessage);
                RaisePropertyChanged(() => HasError);
			}
		}

        public bool HasError
        {
            get { return _resultMessage != null; }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                if (_username == value) return;
                _username = value;
                RaisePropertyChanged(() => Username);
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (_password == value) return;
                _password = value;
                RaisePropertyChanged(() => Password);
            }
        }

        public LoginViewModel(IViewModelServiceLocator services
            , IUserServiceLocator userServiceLocator, IResourceLocatorService resourceLocator)
            : base(services)
        {
            this._userServiceLocator = userServiceLocator;
            this._resourceLocator = resourceLocator;
            this._messenger = services.Messenger;
            _modeManager = services.ModeManager;

           
        }

        public void Init(ParameterKey key)
        {
            // Attach card reader callback
            Section = Services.Parameter.Retrieve<ISection>(key);
            //Section.TemporaryDirection = Section.Direction;
            Section.SetupCardReader();
            _userService = _userServiceLocator.GetUserService(Section.Id);
            if (!this.Section.StartCardReader(OnRFIDCardReceived, null))
            {
                ResultMessage = msgCardReaderDisconnect;
            }
            else
            {
                //_cardReader.ReadingCompleted += OnRFIDCardReceived;
                ResultMessage = null;
            }
            
        }

        public override void Start()
        {
            base.Start();

            DoLoginForTesting();
        }

        private void DoLoginForTesting()
        {
            if (_modeManager.ArgumentParams.Mode == RunMode.Testing)
            {
                Mvx.Resolve<IWebApiTestingServer>().RegisterLane(Section, lane => {
                    if (lane != null && lane.Enabled)
                    {
                        this.Section.Direction = lane.Direction;
                        Username = "admin";
                        Password = "nopass";
                        LoginCommand.Execute(null);
                    }
                });
            }
        }

		private MvxCommand _loginCommand;
		public ICommand LoginCommand {
			get {
				_loginCommand = _loginCommand ?? new MvxCommand(() =>
				{
                    if (Username == null || Password == null || Username.Length == 0 || Password.Length == 0)
                    {
                        ResultMessage = msgInvalid;
                    }
                    else
                    {
                        ResultMessage = null;
                        StatusChanged(ProgressStatus.Started);
                        _userService.Login(Username, Password, Section.Lane.Id, OnLoginResultReceived);
                    }
				});

				return _loginCommand;
			}
		}

        private void OnRFIDCardReceived(object sender, CardReaderEventArgs e)
        {
            if (e == null || e.CardID == null)
            {
                ResultMessage = msgInvalid;
            }
            else
            {
                ResultMessage = null;
                StatusChanged(ProgressStatus.Started);
                _userService.Login(e.CardID, Section.Lane.Id, OnLoginResultReceived);
            }
        }

        private void OnLoginResultReceived(Exception exception)
        {
            StatusChanged(ProgressStatus.Ended);
            if (exception == null)
            {
                LoginSuccess();
            }
            else if(exception is LoginInvalidException)
            {
                ResultMessage = msgInvalid;
            }
            else if (exception is ServerErrorException)
            {
                ResultMessage = msgServerError;
            }
            else if(exception is ServerDisconnectException)
            {
                ResultMessage = msgServerDisconnect;
            }
        }

        int flag = 0;
        private void LoginSuccess()
        {
            lock(this)
            {
                Interlocked.Increment(ref flag);
                if (flag > 1)
                    return;
            }
            // Detach card reader callback
            this.Section.StopCardReader(OnRFIDCardReceived, null);
            this.Section.UserService = _userService;
            // Request LaneViewModelFactory to show corresponding LaneViewModel type
            ResultMessage = null;
            if (_messenger.HasSubscriptionsFor<LoginSuccessMessage>())
            {
                _messenger.Publish(new LoginSuccessMessage(this));
            }
            Close();
            BaseSettings<BaseSettingsData>.CreateBackup();
        }

        public override void Close()
        {
            base.Close();
            this.Section.StopCardReader(OnRFIDCardReceived, null);
        }

        public override void Unloaded()
        {
            base.Unloaded();

            this.Section.StopCardReader(OnRFIDCardReceived, null);
        }
    }
}
