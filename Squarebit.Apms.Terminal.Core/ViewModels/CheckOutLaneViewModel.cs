﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using NLog;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class CheckoutEventArgs: EventArgs
    {
        public KeyAction Key { get; set; }
    }

    public class CheckOutLaneViewModel : BaseLaneViewModel
    {
        IParkingFeeService _parkingFeeService;

        EditableTimer _editTimer = null;

        public event EventHandler CheckOutCompleted;

        private bool _allowCheckout = true;

        string _previousRawVehicleNumber = string.Empty;

        string _referencePrefixNumber;
        public string ReferencePrefixNumber
        {
            get { return _referencePrefixNumber; }
            set
            {
                _referencePrefixNumber = value;
                RaisePropertyChanged(() => ReferencePrefixNumber);
            }
        }

        CheckOut _checkOutData;
        public CheckOut CheckOutData
        {
            get { return _checkOutData; }
            set
            {
                if (_checkOutData == value) return;

                _checkOutData = value;

                if (_checkOutData == null)
                {
                    this.ParkingDuration = string.Empty;
                    CardType = null;
                    ReferencePrefixNumber = string.Empty;
                }
                RaisePropertyChanged(() => CheckOutData);
            }
        }

        CardType _cardType;
        public CardType CardType
        {
            get { return _cardType; }
            set
            {
                if (_cardType == value) return;
                _cardType = value;
                RaisePropertyChanged(() => CardType);
            }
        }

        bool _canCheckout = true;

        private string _parkingDuration;
        public string ParkingDuration
        {
            get { return _parkingDuration; }
            set
            {
                _parkingDuration = value;
                RaisePropertyChanged(() => ParkingDuration);
            }
        }

        public CheckOutLaneViewModel(IViewModelServiceLocator service, IStorageService storageService, IMvxMessenger messenger, IParkingFeeService feeService)
            : base(service, storageService, messenger)
        {
            //_keyService = Mvx.Resolve<IKeyService>();
            _parkingFeeService = feeService;

            _editTimer = new EditableTimer(_userPreferenceService.OptionsSettings.DisplayCheckOutDuration);
            _editTimer.TimeRunningOut += (sender, e) => {
                //this.CheckOutData = null;
                ResetUIInformation();
            };
        }

        public override void Init(ParameterKey key)
        {
            base.Init(key);
            //SetupDevices();
        }

        public override void Start()
        {
            base.Start();

            if (_modeManager.ArgumentParams.Mode == RunMode.Testing)
            {
                this._testingService.Start(() => {
                    _testingService.CreateSchedule("checkout " + DateTime.Now.ToString("yyyyMMdd HHmm"), callback => {
                        SimulateTapCheckout(callback);
                    });
                });
            }

            _allowCheckout = !_userPreferenceService.OptionsSettings.ForceUpdatePlateNumber;
        }

        private Image _backImage = null;
        private Image _frontImage = null;

        /// <summary>
        /// Packing the check out data.
        /// </summary>
        /// <param name="complete">The complete.</param>
        public CheckOut PackCheckOutData()
        {
            try
            {
                var card = this.CheckedCard;
                if (card == null)
                    return null;

                CheckOut sendData = new CheckOut();
                sendData.CheckOutTime = DateTime.Now;
                sendData.TerminalId = _userPreferenceService.HostSettings.Terminal.Id;
                sendData.CardId = card.Id;
                sendData.LaneId = Section.Lane.Id;
                sendData.OperatorId = User.Id;
                _frontImage = Section.FrontOutCamera.CaptureImage();
                _backImage = Section.BackOutCamera.CaptureImage();

                using (Image tmpFront = (Image)_frontImage.Clone())
                {
                    ImageUtility.Watermark(tmpFront as Bitmap, card.Id + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    sendData.FrontImage = tmpFront.ToByteArray(ImageFormat.Jpeg);
                }
                using (Image tmpBack = (Image)_backImage.Clone())
                {
                    ImageUtility.Watermark(tmpBack as Bitmap, card.Id + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    sendData.BackImage = tmpBack.ToByteArray(ImageFormat.Jpeg);
                }

                //sendData.FrontImage = Section.FrontOutCamera.CaptureImage(card.Id + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                //sendData.BackImage = Section.BackOutCamera.CaptureImage(card.Id + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                sendData.VehicleNumber = "...";
                sendData.AlprVehicleNumber = "...";
                if (sendData.FrontImage == null || sendData.BackImage == null)
                {
                    HandleError(IconEnums.Error, GetText("camera.capture_null_image"), false);
                    //complete(null);
                    return null;
                }
                return sendData;
            }
            catch (Exception ex)
            {
                PrintLog<CheckOutLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                return null;
            }
        }

        private CheckIn _checkinData;
        /// <summary>
        /// Gets the check in data before checking out
        /// </summary>
        public void GetCheckInData(Action<Exception> complete)
        {
            try
            {
                var card = this.CheckedCard;
                if (card == null || string.IsNullOrEmpty(card.Id))
                {
                    HandleError(IconEnums.Warning, GetText("cannot_read_card"), false, true);
                    if (complete != null)
                        complete(new Exception(GetText("cannot_read_card")));
                    return;
                }

                var data = PackCheckOutData();
                
                GetCheckIn(data, (result, ex) => {
                    if (ex != null)
                    {
                        if (complete != null)
                            complete(ex);
                    }
                    else
                    {
                        if (result != null)
                        {
                            _checkinData = result;
                            CustomerInfo = result.CustomerInfo;

                            if (CustomerInfo.VehicleRegistrationInfo == null)
                            {
                                _server.GetServerTime((svTime, ex1) => {
                                    DateTime tmp = result.ClaimPromotionId != null ? result.ClaimPromotionDeadline.ToLocalTime() : result.CheckInTime;
                                    // Cộng thêm khoản phí nếu ra chậm hơn 15' sau khi claim promotion
                                    var parkingFee = result.ClaimPromotionId != null ? 0 : result.ParkingFee;
                                    CustomerInfo.ParkingFee = parkingFee + _parkingFeeService.CalculateFee((VehicleTypeEnum)result.VehicleType.Id, 0, tmp, svTime.LocalTime);
                                    result.ParkingFee = CustomerInfo.ParkingFee;
                                });
                            }
                            else if (CheckCustomerInfoValid() && CustomerInfo.VehicleRegistrationInfo.Status == VehicleRegistrationStatus.InUse)
                                CustomerInfo.ParkingFee = 0;

                            if (_userPreferenceService.OptionsSettings.ConfirmCheckout)
                            {
                                var checkoutKey = Section.KeyMap.KeysMap[KeyAction.CheckOut];
                                var cancelCheckoutKey = Section.KeyMap.KeysMap[KeyAction.CancelCheckOut];
                                string msg = string.Format(GetText("checkout.guide"), checkoutKey, Environment.NewLine, cancelCheckoutKey);
                                HandleError(IconEnums.Guide, msg, true, true);
                            }
                            _editTimer.Stop();
                        }

                        ConvertToCheckOutData(data, result);
                        RecognizePlate(result);

                        LoadImages(result, (e) => {
                            if (complete != null)
                            {
                                _canCheckout = false;
                                complete(e);
                            }
                        });
                    }
                });
            }
            catch (Exception ex)
            {
                PrintLog<CheckOutLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                if (complete != null)
                    complete(ex);
            }
        }

        private void RecognizePlate(CheckIn receivedData)
        {
            var tempCheckOutData = CheckOutData;
            if (tempCheckOutData != null)
            {
                _alprService.RecognizeLicensePlate(_backImage.ToByteArray(ImageFormat.Jpeg), (recognizedPlateNumber, ex1) => {
                    if (ex1 != null)
                    {
                        HandleError(IconEnums.Warning, GetText("anpr.cannot_connect"), false);
                    }

                    if (!string.IsNullOrEmpty(recognizedPlateNumber) && recognizedPlateNumber.Equals(_previousRawVehicleNumber))
                    {
                        PrintLog<CheckInLaneViewModel>(new Exception("There is something fishy about ANPR"), null, true);
                    }

                    tempCheckOutData.AlprVehicleNumber = recognizedPlateNumber;
                    tempCheckOutData.VehicleNumber = ExtractVehicleNumber(recognizedPlateNumber);
                    tempCheckOutData.PrefixNumberVehicle = ExtractPrefixVehicleNumber(tempCheckOutData.AlprVehicleNumber, tempCheckOutData.VehicleNumber);

                    _previousRawVehicleNumber = recognizedPlateNumber;

                    if (!string.IsNullOrEmpty(tempCheckOutData.VehicleNumber) &&
                        !receivedData.VehicleNumber.Equals(tempCheckOutData.VehicleNumber) &&
                        CustomerInfo != null && CustomerInfo.VehicleRegistrationInfo.VehicleNumber.Equals(recognizedPlateNumber) &&
                        _userPreferenceService.OptionsSettings.NoMatchingPlateNoticeEnalbe)
                    {
                        HandleError(IconEnums.Close, GetText("checkin.not_match_vehicle_number"), true);
                    }
                });
            }
        }

        /// <summary>
        /// Gets the check in data of tapped card.
        /// </summary>
        /// <param name="checkoutData">The checkout data.</param>
        /// <param name="complete">The complete.</param>
        public void GetCheckIn(CheckOut checkoutData, Action<CheckIn, Exception> complete)
        {
            if (checkoutData == null)
            {
                complete(null, new Exception("Cannot get checkout"));
                return;
            }

            _server.GetCheckIn(checkoutData.CardId, (checkinData, ex) => {
                if (complete != null)
                    complete(checkinData, ex);
            });
        }

        private void ConvertToCheckOutData(CheckOut checkoutData, CheckIn checkinData)
        {
            if (checkinData != null)
            {
                ParkingDuration = ToReadableString(DateTime.Now - checkinData.CheckInTime);

                checkoutData.AlprVehicleNumber = checkinData.AlprVehicleNumber;
                string s = ExtractVehicleNumber(checkoutData.AlprVehicleNumber);
                ReferencePrefixNumber = ExtractPrefixVehicleNumber(checkoutData.AlprVehicleNumber, s);
                checkoutData.ReferenceCheckInTime = checkinData.CheckInTime;
                checkoutData.ReferenceVehicleNumber = checkinData.VehicleNumber.Equals("????") ? "" : checkinData.VehicleNumber;
                checkoutData.CardLabel = checkinData.CardLabel;
                checkoutData.ParkingFee = checkinData.ParkingFee;
                TypeHelper.GetCardType(checkinData.CardTypeId, result => CardType = result);
            }
            this.CheckOutData = checkoutData;
        }

        Logger _logger = LogManager.GetCurrentClassLogger();
        private void LoadImages(CheckIn checkinData, Action<Exception> complete)
        {
            Exception exception = null;

            // only call complete callback when
            if (checkinData != null)
            {
                ManualResetEvent resetEvent = new ManualResetEvent(false);
                int numberOfThreads = 2;
                var tempCheckOutData = CheckOutData;

                Stopwatch watch = new Stopwatch();
                watch.Start();
                _storageService.DoubleLoadImage(checkinData.FrontImagePath, checkinData.ImageHosts, (fiBytes, e) => {
                    if (e != null)
                    {
                        exception = e;
                        PrintLog<CheckOutLaneViewModel>(exception, _userPreferenceService.HostSettings.LogServerIP);
                    }
                    tempCheckOutData.ReferenceFrontImage = fiBytes;
                    Interlocked.Decrement(ref numberOfThreads);
                    if (numberOfThreads  == 0)
                        resetEvent.Set();
                });
                _storageService.DoubleLoadImage(checkinData.BackImagePath, checkinData.ImageHosts, (biBytes, e) => {
                    if (e != null)
                    {
                        exception = e;
                        PrintLog<CheckOutLaneViewModel>(exception, _userPreferenceService.HostSettings.LogServerIP);
                    }
                    tempCheckOutData.ReferenceBackImage = biBytes;
                    Interlocked.Decrement(ref numberOfThreads);
                    if (numberOfThreads == 0)
                        resetEvent.Set();
                });

                resetEvent.WaitOne(3000);
                watch.Stop();
                _logger.Info(string.Format("{0} - {1}-{2} ({3})", OtherUtilities.GetVersion(), checkinData.FrontImagePath, checkinData.BackImagePath, watch.ElapsedMilliseconds));
            }

            if (complete != null) complete(exception);
        }

        public void CheckOut(CheckOut checkout, Action<Exception> complete)
        {
            _server.CreateCheckOut(checkout, ex => {
                InvokeOnMainThread(() => {
                    string msg = string.Empty;
                    if (ex != null)
                    {
                        if (ex is InternalServerErrorException)
                        {
                            HandleError(IconEnums.Error, GetText("checkout.something_wrong"), false);
                            PrintLog<CheckOutLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                        }
                        else
                            PrintLog<CheckOutLaneViewModel>(ex);
                    }
                    else
                    {
                        HandleError(IconEnums.Check, GetText("checkout.success"), false);
                        //_canCheckout = true;
                        _notEditPlateNumberYet = true;
                    }
                    _canCheckout = true;
                    if (complete != null)
                        complete(ex);
                });

                //ParkingDuration = string.Empty;
            });
        }

        public string ToReadableString(TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}",
                span.Duration().Days > 0 ? string.Format("{0:0}n ", span.Days) : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0}g", span.Hours) : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0}p", span.Minutes) : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "< 1p";

            return formatted;
        }

        int _flag = 0;
        int _lastReadTime = 0;

        public override void ReadingCompleted(object sender, CardReaderEventArgs e)
        {
            // thread-safe when doing checkout
            lock (this)
            {
                Interlocked.Increment(ref _flag);
                var curTime = Environment.TickCount;
                if (_flag > 1 && (curTime - _lastReadTime) < 3000)
                    return;
                Interlocked.Exchange(ref _flag, 1);
                _lastReadTime = curTime;
            }

            base.ReadingCompleted(sender, e);
            HandleCheckout();
        }

        private void HandleCheckout()
        {
            if (_canCheckout)
            {
                InvokeOnMainThread(() => this.Notices = null);
                var card = this.CheckedCard;
                if (_editTimer.Enabled && card.Id.Equals(_editTimer.CurrentCardId))
                {
                    GetCheckInData(ex => { 
                        if (ex == null)
                        {
                            Interlocked.Exchange(ref _flag, 0);
                            if (!_userPreferenceService.OptionsSettings.ConfirmCheckout && ex == null)
                                CheckOut(this.CheckOutData, exception => {
                                    var tempCheckoutData = CheckOutData;
                                    _editTimer.Start(tempCheckoutData.CardId);
                                });
                        }
                    });
                }
                else
                {
                    //CheckOutData = null;
                    ResetUIInformation();
                    GetCheckInData(ex => {
                        if (ex != null)
                        {
                            if (ex is NotFoundException)
                                HandleError(IconEnums.Card, GetText("checkin.card_not_found"), false);
                            else
                            {
                                if (ex is InternalServerErrorException || ex is ServerDisconnectException)
                                {
                                    HandleError(IconEnums.Error, GetText("checkout.something_wrong"), false);
                                }
                                if (!(ex is ServerErrorException))
                                {
                                    PrintLog<CheckOutLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                                }
                                if (RequestExceptionManager.GetExceptionMessage<CheckIn>(ex.Message).Key == RequestExceptionEnum.CardIsNotInUse)
                                {
                                    HandleError(IconEnums.Card, GetText("checkin.not_check_in_yet"), false);
                                }
                            }
                        }
                        Interlocked.Exchange(ref _flag, 0);
                        if (!_userPreferenceService.OptionsSettings.ConfirmCheckout && ex == null)
                            CheckOut(this.CheckOutData, exception => {
                                var tempCheckoutData = CheckOutData;
                                _editTimer.Start(tempCheckoutData.CardId);
                            });
                    });
                }
            }
        }

        private void SimulateTapCheckout(Action<ISection, TestCard, Exception> complete)
        {
            Mvx.Resolve<IWebApiTestingServer>().GetCardCheckout(this.Section, (card, exception) => {
                if (card == null || card.Delay == -1)
                {
                    if (complete != null)
                        complete(Section, card, exception);
                    return;
                }

                string cardId = card.CardId;
                if (cardId != null)
                {
                    this.CheckedCard = new Card(cardId.ToString());
                    if (this.CheckOutData == null)
                        GetCheckInData(async exc => {
                            if (exc != null || this.CheckOutData == null)
                            {
                                if (complete != null)
                                    complete(Section, card, exc);
                            }
                            else
                            {
                                CheckOut(this.CheckOutData, (ex) => {
                                    if (complete != null)
                                        complete(Section, card, ex);
                                });
                            }
                            await Task.Delay(500);
                            InvokeOnMainThread(() => {
                                //this.CheckOutData = null;
                                ResetUIInformation();
                                this.Notices = null;
                            });
                        });
                }
                else
                {
                    if (complete != null)
                        complete(Section, card, exception);
                }
            });
        }

        public override void TakingOffCompleted(object sender, CardReaderEventArgs e)
        {
            base.TakingOffCompleted(sender, e);
        }

        protected override void OnKeyPressed(KeyPressedMessage msg)
        {
            //msg.KeyEventArgs.Handled = true;

            //if (this.CheckOutData == null)
            //    return;
            
            string output;
            KeyAction action = this.Section.KeyMap.GetAction(msg.KeyEventArgs, out output);

            switch (action)
            {
                case KeyAction.CheckOut:
                    {
                        if (this.Section.Barrier != null)
                            this.Section.Barrier.Open();

                        if (this.CheckOutData == null || !_userPreferenceService.OptionsSettings.ConfirmCheckout)
                            return;
                        if (_allowCheckout )
                        {
                            this.CheckOut(this.CheckOutData, (ex) => {
                                var tempCheckoutData = CheckOutData;
                                _editTimer.Start(tempCheckoutData.CardId);
                            });
                            this.Notices = null;
                            if (CheckOutCompleted != null)
                                CheckOutCompleted(null, new CheckoutEventArgs { Key = action });
                        }
                        else
                        {
                            HandleError(IconEnums.Error, GetText("checkout.force_update_plate_number"), true);
                        }
                        if (_userPreferenceService.OptionsSettings.ForceUpdatePlateNumber)
                            _allowCheckout = false;

                        break;
                    }
                case KeyAction.Number:
                    {
                        var tempCheckoutData = CheckOutData;
                        if (tempCheckoutData == null)
                            return;
                        if (_canCheckout)
                            return;

                        if (_notEditPlateNumberYet)
                        {
                            tempCheckoutData.VehicleNumber = string.Empty;
                            _notEditPlateNumberYet = false; 
                        }
                        tempCheckoutData.VehicleNumber += output; // response.Output
                        tempCheckoutData.AlprVehicleNumber = tempCheckoutData.VehicleNumber;
                        if (_userPreferenceService.OptionsSettings.ForceUpdatePlateNumber)
                            _allowCheckout = true;
                        break;
                    }
                case KeyAction.Delete:
                    {
                        var tempCheckoutData = CheckOutData;
                        if (tempCheckoutData == null)
                            return;
                        //if (CheckOutData == null)
                        //    break;
                        int leng = tempCheckoutData.VehicleNumber.Length;
                        if (leng > 0)
                            tempCheckoutData.VehicleNumber = tempCheckoutData.VehicleNumber.Remove(leng - 1);
                        break;
                    }
                case KeyAction.CancelCheckOut:
                    {
                        _canCheckout = true;
                        _notEditPlateNumberYet = true;
                        if (this.CheckOutData == null)
                            return;
                        this.Notices = null;
                        //this.CheckOutData = null;
                        ResetUIInformation();
                        if (CheckOutCompleted != null)
                            CheckOutCompleted(null, new CheckoutEventArgs { Key = action });
                        //this.ParkingDuration = string.Empty;
                        break;
                    }
                case KeyAction.Search:
                    {
                        ShowSearchCommand.Execute(null);
                        break;
                    }
                case KeyAction.Logout:
                    {
                        //ShowShiftReportCommand.Execute(null);
                        ConfirmLogoutCommand.Execute(null);
                        break;
                    }
                case KeyAction.ChangeLane:
                    {
                        ChangeLaneDirectionCommand.Execute(null);
                        break;
                    }
                case KeyAction.ShowVehicleType:
                    if (CustomerInfo != null && CustomerInfo.VehicleRegistrationInfo != null)
                        return;
                    ShowChooseVehicleType = true;
                    break;
            }
        }

        public override void ChooseVehicleType(VehicleType type)
        {
            var card = this.CheckedCard;
            if (_checkinData != null)
            {
                _checkinData.CardId = card.Id;
                _checkinData.VehicleType = type;
                _server.UpdateCheckIn(_checkinData, (result, ex) => {
                    if (ex == null)
                    {
                        _server.GetCheckIn(result.CardId, (result1, ex1) => {
                            if (ex1 == null && result1 != null)
                                CustomerInfo = result1.CustomerInfo;
                        });
                    }
                });
            }
        }

        private void ResetUIInformation()
        {
            CheckOutData = null;
            CustomerInfo = null;
        }

        public override void Close()
        {
            _editTimer.Stop();
            base.Close();
        }
    }
}