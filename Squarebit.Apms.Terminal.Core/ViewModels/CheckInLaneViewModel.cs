﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using Newtonsoft.Json;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.Utilities;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using System.Windows.Threading;

namespace Squarebit.Apms.Terminal.Core.ViewModels
{
    public class CountdownArgs : EventArgs
    {
        public int Value { get; set; }
    }

    public class EditableTimer
    {
        private int BOOMB_DURATION = 15;
        private int _boomb;

        public DispatcherTimer CountDownTimer { get; set; }

        public string RecentCheckedInCardId = string.Empty;
        public string CurrentCardId = string.Empty;

        public event EventHandler TimeRunningOut;
        public event EventHandler Countdown;

        public EditableTimer()
        {
            CountDownTimer = new DispatcherTimer();
            CountDownTimer.Interval = TimeSpan.FromSeconds(1);
            CountDownTimer.Tick += timer_Tick;
        }

        public EditableTimer(int duration) : this()
        {
            BOOMB_DURATION = duration;
            _boomb = BOOMB_DURATION;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            _boomb -= 1;
            var handle = Countdown;
            if (handle != null)
                handle(sender, new CountdownArgs { Value = _boomb });

            if (_boomb <= 0)
            {
                CountDownTimer.Stop();
                Stop();
                EditableTimer_Elapsed(null, null);
            }
        }

        void EditableTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var handle = TimeRunningOut;

            if (handle != null)
                handle(sender, e);
        }

        public bool Enabled
        {
            get { return CountDownTimer.IsEnabled; }
        }

        public void Start(string cardId)
        {
            CurrentCardId = cardId;

            if (!RecentCheckedInCardId.Equals(CurrentCardId))
            {
                Restart();
                RecentCheckedInCardId = CurrentCardId;
            }
        }

        public void Restart()
        {
            _boomb = BOOMB_DURATION;
            CountDownTimer.Stop();
            CountDownTimer.Start();
        }

        public void Stop()
        {
            CountDownTimer.Stop();

            RecentCheckedInCardId = string.Empty;
            CurrentCardId = string.Empty;
        }
    }

    public class CheckInLaneViewModel : BaseLaneViewModel
    {
        private const int AUTO_CHECKIN_DURATION = 5000;

        EditableTimer _editTimer = null;
        System.Timers.Timer _autoCheckInTimer = null;
        string _previousRawVehicleNumber = string.Empty;

        CheckIn _checkInData;
        public CheckIn CheckInData
        {
            get { return _checkInData; }
            set
            {
                if (_checkInData == value) return;

                _checkInData = value;
                if (_checkInData == null)
                {
                    CardType = null;
                }
                RaisePropertyChanged(() => CheckInData);
            }
        }

        CardType _cardType;
        public CardType CardType
        {
            get { return _cardType; }
            set
            {
                if (_cardType == value) return;
                _cardType = value;
                RaisePropertyChanged(() => CardType);
            }
        }

        private string _countdown;
        public string Countdown
        {
            get { return _countdown; }
            set
            {
                _countdown = value;
                RaisePropertyChanged(() => Countdown);
            }
        }

        private bool _showCountdown;
        public bool ShowCountdown
        {
            get { return _showCountdown; }
            set
            {
                _showCountdown = value;
                RaisePropertyChanged(() => ShowCountdown);
            }
        }

        private bool _checkCameraDoNotWork = true;
        public bool CheckCameraDoNotWork
        {
            get { return _checkCameraDoNotWork; }
            set
            {
                _checkCameraDoNotWork = value;
                RaisePropertyChanged(() => CheckCameraDoNotWork);
            }
        }

        public CheckInLaneViewModel(IViewModelServiceLocator service, IStorageService storageService, IMvxMessenger messenger)
            : base(service, storageService, messenger)
        {
            _editTimer = new EditableTimer(_userPreferenceService.OptionsSettings.WaitingCheckDuration);
            _editTimer.TimeRunningOut += (sender, e) => {
                //this.CheckInData = null;
                ResetUIInformation();
                this.Notices.Clear();
                Notices = null;
                _autoCheckInTimer.Stop();
            };
            _editTimer.Countdown += (sender, e) => {
                int cd = (e as CountdownArgs).Value;
                if (cd <= 0)
                {
                    Countdown = string.Empty;
                    ShowCountdown = false;
                }
                else
                {
                    Countdown = cd.ToString();
                    ShowCountdown = true;
                }
            };

            _autoCheckInTimer = new System.Timers.Timer();
            _autoCheckInTimer.Interval = AUTO_CHECKIN_DURATION;
            _autoCheckInTimer.Elapsed += (sender, e) => {
                UpdateCheckIn(this.CheckInData, (result, ex) => {
                    InvokeOnMainThread(() => {
                        if (ex != null)
                        {
                            //this.Notices.Clear();
                            if (ex is InternalServerErrorException)
                            {
                                HandleError(IconEnums.Error, GetText("checkin.something_wrong_update"), true, false);
                                PrintLog<CheckInLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                            }
                            else PrintLog<CheckInLaneViewModel>(ex);
                        }
                        else
                        {
                            if (result != null && result.VehicleNumberExists && !string.IsNullOrEmpty(result.AlprVehicleNumber))
                            {
                                HandleError(IconEnums.Warning, GetText("checkin.same_vehicle_number"), true, false);
                            }
                            else if (result != null && (!result.VehicleNumberExists || string.IsNullOrEmpty(result.VehicleNumber)))
                                HandleError(IconEnums.Check, GetText("checkin.update_success"), false, false);
                        }

                        _autoCheckInTimer.Stop();
                    });
                });
            };
        }

        public override void Init(ParameterKey key)
        {
            base.Init(key);
            //SetupDevices();
        }

        public override void Start()
        {
            base.Start();

            if (_modeManager.ArgumentParams.Mode == RunMode.Testing)
            {
                this._testingService.Start(() => {
                    _testingService.CreateSchedule("checkin " + DateTime.Now.ToString("yyyMMdd HHmm"), (callback) => {
                        SimulateTapCheckIn(callback);
                    });
                });
            }
        }

        private void HandleCheckIn(CheckIn sendData, Action<CheckIn, Exception> complete)
        {
            if(!sendData.CardId.Equals(_preCardId))
            {
                if (!HandleCaptureImage(sendData, sendData.CardId, CheckCameraDoNotWork))
                {
                    if (complete != null)
                    {
                        if (CheckCameraDoNotWork)
                        {
                            HandleError(IconEnums.Error, GetText("camera.capture_same_image"), false, false);
                            complete(null, new Exception("camera.capture_same_image"));
                        }
                        else
                        {
                            HandleError(IconEnums.Error, GetText("camera.capture_null_image"), false, false);
                            complete(null, new Exception("camera.capture_null_image"));
                        }
                    }
                    //CheckInData = null;
                    ResetUIInformation();
                    return;
                }
            }
            else
            {
                if (!HandleCaptureImage(sendData, sendData.CardId, false))
                {
                    if (complete != null)
                    {
                        HandleError(IconEnums.Error, GetText("camera.capture_null_image"), false, false);
                        complete(null, new Exception("camera.capture_null_image"));
                    }
                    //CheckInData = null;
                    ResetUIInformation();
                    return;
                }
            }

            // update if tapping same card within WaitingCheckDuration
            if (_editTimer.Enabled && sendData.CardId.Equals(_editTimer.CurrentCardId))
            {
                var tempCheckinData = CheckInData;

                if (tempCheckinData != null && !string.IsNullOrEmpty(tempCheckinData.VehicleNumber))
                    sendData.VehicleNumber = tempCheckinData.VehicleNumber;

                UpdateCheckIn(sendData, (data, exception) => {
                    if (exception == null)
                    {
                        SaveImage(data);
                        this.CheckInData = data;
                        HandleError(IconEnums.Check, GetText("checkin.update_success"), false, true);
                        if (complete != null)
                            complete(CheckInData, exception);
                    }
                    // handle: checkout and checkin again within WaitingCheckDuration 
                    else if (RequestExceptionManager.GetExceptionMessage<CheckIn>(exception.Message).Key == RequestExceptionEnum.CardIsNotInUse)
                    {
                        CheckIn(sendData, (checkInResult, ex) => {
                            CheckInData = checkInResult;
                            TypeHelper.GetCardType(tempCheckinData.CardTypeId, result => CardType = result);
                            _notEditPlateNumberYet = true;

                            if (complete != null)
                                complete(CheckInData, ex);
                        });
                    }
                });
            }
            else
            {
                InvokeOnMainThread(() => this.Notices.Clear());

                // update checkin data of previous checkin session if it is still available for editing
                if (_editTimer.Enabled)
                    UpdateCheckIn(this.CheckInData, null);

                CheckIn(sendData, (checkInResult, ex) => {
                    CheckInData = checkInResult;
                    if (checkInResult != null)
                        TypeHelper.GetCardType(checkInResult.CardTypeId, result => CardType = result);
                    _notEditPlateNumberYet = true;
                    if (complete != null)
                        complete(CheckInData, ex);
                });
            }
        }

        private bool HandleCaptureImage(CheckIn data, string cardId, bool shouldCheck)
        {
            _frontImage = Section.FrontInCamera.CaptureImage();
            _backImage = Section.BackInCamera.CaptureImage();

            if (shouldCheck)
            {
                if (CompareImage(_frontImage, _preFrontImage))
                {
                    using (Image tmpFront = (Image)_frontImage.Clone())
                    {
                        ImageUtility.Watermark(tmpFront as Bitmap, cardId + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                        data.FrontImage = tmpFront.ToByteArray(ImageFormat.Jpeg);
                    }
                }
                if (CompareImage(_backImage, _preBackImage))
                {
                    using (Image tmpBack = (Image)_backImage.Clone())
                    {
                        ImageUtility.Watermark(tmpBack as Bitmap, cardId + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                        data.BackImage = tmpBack.ToByteArray(ImageFormat.Jpeg);
                    }
                }
            }
            else
            {
                data.FrontImage = Section.FrontInCamera.CaptureImage(cardId + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                data.BackImage = Section.BackInCamera.CaptureImage(cardId + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }

            if (data.FrontImage == null || data.BackImage == null)
            {
                //HandleError(IconEnums.Error, GetText("camera.capture_null_image"), false, false);
                return false;
            }

            return true;
        }

        private Image _preBackImage = null;
        private Image _preFrontImage = null;
        private Image _frontImage = null;
        private Image _backImage = null;
        private string _preCardId = string.Empty;
        private bool CompareImage(Image capturedImage, Image desImage)
        {
            if (desImage == null) return true;

            float difference = capturedImage.PercentageDifference(desImage); 
            if (difference > 0.05)
                return true;
            return false;
        }

        public void BeginCheckin(Action<CheckIn, Exception> complete)
        {
            try
            {  
                var card = this.CheckedCard;
                if (card == null || string.IsNullOrEmpty(card.Id))
                {
                    HandleError(IconEnums.Warning, GetText("cannot_read_card"), false, true);
                    if (complete != null)
                        complete(null, new Exception(GetText("cannot_read_card")));
                    return;
                }

                if (this.Section == null || this.Section.FrontInCamera == null || this.Section.BackInCamera == null) return;

                CheckIn sendData = new CheckIn();
                sendData.TerminalId = _userPreferenceService.HostSettings.Terminal.Id;
                sendData.CardId = this.CheckedCard.Id;
                sendData.LaneId = Section.Lane.Id;
                sendData.VehicleTypeId = Section.VehicleTypeId;
                //sendData.VehicleSubType = VehicleSubType.Bike_Auto;
                sendData.OperatorId = User.Id;
                //sendData.FrontImage = Section.FrontInCamera.CaptureImage(card.Id + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                //sendData.BackImage = Section.BackInCamera.CaptureImage(card.Id + "  " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                sendData.VehicleNumber = "";
                sendData.AlprVehicleNumber = "";

                HandleCheckIn(sendData, (data, ex) => {
                    if (ex == null)
                    {
                        var tempCheckInData = CheckInData;

                        RecognizePlate(tempCheckInData);

                        _preBackImage = _backImage;
                        _preFrontImage = _frontImage;
                        _preCardId = tempCheckInData.CardId;
                    }

                    if (complete != null)
                        complete(CheckInData, ex);
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                PrintLog<CheckInLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                complete(null, ex);
            }
        }

        private void RecognizePlate(CheckIn tempCheckInData)
        {
            _alprService.RecognizeLicensePlate(_backImage.ToByteArray(ImageFormat.Jpeg), (result, ex1) => {
                if (ex1 != null)
                {
                    HandleError(IconEnums.Warning, GetText("anpr.cannot_connect"), false, true);
                }

                if (_notEditPlateNumberYet && tempCheckInData != null && tempCheckInData == CheckInData)
                {
                    if (result.Equals(_previousRawVehicleNumber) && !string.IsNullOrEmpty(result))
                    {
                        _previousRawVehicleNumber = result;
                        PrintLog<CheckInLaneViewModel>(new Exception("There is something fishy about ANPR"), null, true);
                    }

                    tempCheckInData.AlprVehicleNumber = result;
                    tempCheckInData.VehicleNumber = ExtractVehicleNumber(result);
                    tempCheckInData.PrefixNumberVehicle = ExtractPrefixVehicleNumber(tempCheckInData.AlprVehicleNumber, tempCheckInData.VehicleNumber);
                    ResetAutoCheckInTimer();
                }
            });
        }

        private void SaveImage(CheckIn checkInInfo)
        {
            _storageService.SaveImage(
                new List<string>() { checkInInfo.FrontImagePath, checkInInfo.BackImagePath },
                new List<byte[]>() { checkInInfo.FrontImage, checkInInfo.BackImage },
                (List<Exception> lstExceptions) => {
                    foreach (Exception exception in lstExceptions)
                    {
                        if (exception != null)
                        {
                            PrintLog<CheckInLaneViewModel>(exception);
                            return;
                        }
                    }
                    _server.ReplicateImages(checkInInfo, null);
                });
        }

        public void CheckIn(CheckIn data, Action<CheckIn, Exception> complete)
        {
            if (_editTimer.Enabled)
            {
                _editTimer.Stop();
                ShowCountdown = false;
            }

            _server.CreateCheckIn(data, (result, ex) => {
                if (ex == null)
                {
                    SaveImage(result);
                }
                InvokeOnMainThread(() => {
                    string msg = string.Empty;
                    //this.Notices.Clear();
                    if (ex != null)
                    {
                        if (ex is NotFoundException)
                        {
                            HandleError(IconEnums.Card, GetText("checkin.card_not_found"), false, false);
                        }
                        else
                        {
                            string noticeMsg = RequestExceptionManager.GetExceptionMessage<CheckIn>(ex.Message).Value;
                            HandleError(IconEnums.Error, noticeMsg, true, false);
                        }

                        PrintLog<CheckInLaneViewModel>(ex);
                    }
                    else
                    {
                        CustomerInfo = result.CustomerInfo;
                        HandleError(IconEnums.Check, GetText("checkin.success"), true, false);

                        int roomLeft = result.LimitNumSlots - result.CurrentNumSlots;
                        if (roomLeft < 0)
                        {
                            roomLeft *= -1;
                            string noticeMsg = string.Format(GetText("checkin.out_of_room"), roomLeft);
                            HandleError(IconEnums.Parking, noticeMsg, false, true);
                        }
                        else
                        {
                            string noticeMsg = string.Format(GetText("checkin.left_room"), roomLeft);
                            HandleError(IconEnums.Parking, noticeMsg, false, true);
                        }
                        if (result.VehicleNumberExists && !string.IsNullOrEmpty(data.AlprVehicleNumber))
                        {
                            HandleError(IconEnums.Warning, GetText("checkin.same_vehicle_number"), false, true);
                        }
                    }

                    if (complete != null)
                        complete(result, ex);
                });
            });
        }

        int flag = 0;
        int lastReadTime = 0;

        public override void ReadingCompleted(object sender, CardReaderEventArgs e)
        {
            // thread-safe when doing checkin
            lock(this)
            {
                Interlocked.Increment(ref flag);
                var curTime = Environment.TickCount;
                if (flag > 1 && (curTime - lastReadTime) < 3000)
                    return;
                Interlocked.Exchange(ref flag, 1);
                lastReadTime = curTime;
            }

            base.ReadingCompleted(sender, e);

            BeginCheckin((data, ex) => {
                Interlocked.Exchange(ref flag, 0);
            });
        }

        public void SimulateTapCheckIn(Action<ISection, TestCard, Exception> complete)
        {
            Mvx.Resolve<IWebApiTestingServer>().GetCardCheckin(this.Section, (card, exception) => {
                if (card == null || card.Delay == -1)
                {
                    if (complete != null)
                        complete(Section, card, exception);
                    return;
                }

                if (card.CardId != null)
                {
                    string cardId = card.CardId;
                    this.CheckedCard = new Card(card.CardId.ToString());
                    UpdateCheckIn(this.CheckInData, null);
                    if (cardId != null)
                    {
                        BeginCheckin((result, ex) => {
                            complete(Section, card, ex);
                        });
                    }
                }
                else
                {
                    if (complete != null)
                        complete(Section, card, exception);
                }
            });
        }

        public override void TakingOffCompleted(object sender, CardReaderEventArgs e)
        {
            base.TakingOffCompleted(sender, e);

            var tempCheckinData = CheckInData;
            if (tempCheckinData != null)
                _editTimer.Start(tempCheckinData.CardId);
        }

        /// <summary>
        /// Updates the check in.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="complete">The complete.</param>
        public void UpdateCheckIn(CheckIn data, Action<CheckIn, Exception> complete)
        {
            if (data == null)
            {
                if (complete != null)
                    complete(null, null);
                return;
            }

            _server.UpdateCheckIn(data, (result, ex) => {
                if (ex != null)
                {
                    if (ex is InternalServerErrorException)
                    {
                        HandleError(IconEnums.Error, GetText("checkin.something_wrong_update"), false, true);
                        PrintLog<CheckInLaneViewModel>(ex, _userPreferenceService.HostSettings.LogServerIP);
                    }
                    else
                    {
                        PrintLog<CheckInLaneViewModel>(ex);
                    }
                }

                if (complete != null)
                    complete(result, ex);
            });
        }

        private void ResetAutoCheckInTimer()
        {
            _autoCheckInTimer.Stop();
            _autoCheckInTimer.Start();
        }

        protected override void OnKeyPressed(KeyPressedMessage msg)
        {
            //msg.KeyEventArgs.Handled = false;
            //KeyResponse response = _keyService.HandleLaneKey<CheckIn>(this.Section, msg.KeyEventArgs);

            string output;
            KeyAction action = this.Section.KeyMap.GetAction(msg.KeyEventArgs, out output);

            switch (action)
            {
                case KeyAction.Number:
                    {
                        var tempCheckInData = CheckInData;
                        if (tempCheckInData == null)
                            return;
                        if (_notEditPlateNumberYet)
                        {
                            tempCheckInData.VehicleNumber = string.Empty;
                            _notEditPlateNumberYet = false;
                            ResetAutoCheckInTimer();
                        }
                        tempCheckInData.VehicleNumber += output; //response.Output
                        break;
                    }
                case KeyAction.Delete:
                    {
                        var tempCheckInData = CheckInData;
                        if (tempCheckInData == null)
                            return;
                        int leng = tempCheckInData.VehicleNumber.Length;
                        if (leng > 0)
                            tempCheckInData.VehicleNumber = tempCheckInData.VehicleNumber.Remove(leng - 1);
                        ResetAutoCheckInTimer();
                        break;
                    }
                case KeyAction.Search:
                    {
                        ShowSearchCommand.Execute(null);
                        break;
                    }
                case KeyAction.Logout:
                    {
                        //ShowShiftReportCommand.Execute(null);
                        //ShowConfirmLogout();
                        ConfirmLogoutCommand.Execute(null);
                        break;
                    }
                case KeyAction.ChangeLane:
                    {
                        ChangeLaneDirectionCommand.Execute(null);
                        break;
                    }
                case KeyAction.ShowVehicleType:
                    if (CustomerInfo != null && CustomerInfo.VehicleRegistrationInfo == null)
                        return;
                    ShowChooseVehicleType = true;
                    break;
            }
        }

        private void ResetUIInformation()
        {
            CustomerInfo = null;
            CheckInData = null;
        }

        public override void Close()
        {
            _editTimer.Stop();
            _autoCheckInTimer.Stop();
            base.Close();
        }

        public override void ChooseVehicleType(VehicleType type)
        {
            if (CheckInData != null)
            {
                CheckInData.VehicleType = type;
                ResetAutoCheckInTimer();
            }
        }
    }
}