﻿using Cirrious.MvvmCross.Plugins.Messenger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public class ViewModelServiceLocator : IViewModelServiceLocator
    {
        public IMvxMessenger Messenger { get; private set; }

        public IParameterService Parameter { get; private set; }

        public IRunModeManager ModeManager { get; private set; }

        public ViewModelServiceLocator(
            IMvxMessenger messenger,
            IParameterService parameter,
            IRunModeManager modeManager)
        {
            this.Messenger = messenger;
            this.Parameter = parameter;
            this.ModeManager = modeManager;
        }
    }
}
