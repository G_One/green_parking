﻿using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public class ClaimPromotionSettingsData : BaseSettingsData
    {
        public List<string> TenantNames { get; set; }
    }

    public interface IClaimPromotionSettings : IBaseSettings
    {
        List<string> TenantNames { get; set; }
    }

    public class ClaimPromotionSettings : BaseSettings<ClaimPromotionSettingsData>, IClaimPromotionSettings
    {
        public List<string> TenantNames
        {
            get { return _data.TenantNames; }
            set { _data.TenantNames = value; MarkChanged(); }
        }

        public ClaimPromotionSettings(ArgumentParameter argParams = null)
            : base(argParams)
        {
            
            if (!HasLocal)
            {
                TenantNames = new List<string>();
            }
        }

        public override void ForceSave()
        {
            MarkChanged();
            base.ForceSave();
        }
    }
}
