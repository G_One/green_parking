﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Devices.Dal;
using Squarebit.Devices.Vivotek;
using Cirrious.CrossCore;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public class SystemSettingsData : BaseSettingsData
    {
        public Dictionary<SectionPosition, Section> Sections { get; set; }
        public string OtherTerminalIPs { get; set; }
        public int CameraPort { get; set; }
    }

    public interface ISystemSettings : IBaseSettings
    {
        Dictionary<SectionPosition, Section> Sections { get; set; }
        string OtherTerminalIPs { get; set; }
        int CameraPort { get; set; }
        void ChangeLaneDirection(SectionPosition sectionId, LaneDirection direction);
        void AddSection(Section section);
        void UpdateSection(Section section);
        List<Section> GetAllSections();
    }

    public class SystemSettings : BaseSettings<SystemSettingsData>, ISystemSettings
    {
        public Dictionary<SectionPosition, Section> Sections
        {
            get { return _data.Sections; }
            set { _data.Sections = value; MarkChanged(); }
        }

        public string OtherTerminalIPs
        {
            get { return _data.OtherTerminalIPs; }
            set { _data.OtherTerminalIPs = value; MarkChanged(); }
        }

        public int CameraPort
        {
            get { return _data.CameraPort; }
            set { _data.CameraPort = value; MarkChanged(); }
        }

        public SystemSettings(ArgumentParameter argParams = null)
            : base(argParams)
        {
            if (!HasLocal)
            {
                Sections = new Dictionary<SectionPosition, Section>();
                SectionPosition[] items = (SectionPosition[])Enum.GetValues(typeof(SectionPosition));
                foreach (SectionPosition pos in items)
                {
                    Section section = Mvx.IocConstruct<Section>();
                    section.Id = pos;
                    section.Lane = new Lane();
                    section.KeyMap = new KeyMap(pos);
                    //section.Lane.Direction = section.Direction;
                    section.Lane.Enabled = true;
                    //section.Lane.VehicleType = VehicleType.Bike;
                    section.Lane.Name = null;
                    if (pos == SectionPosition.Lane1 || pos == SectionPosition.Lane2)
                        section.IsConfigured = true;
                    Sections.Add(pos, section);
                }
            }
        }

        //public SystemSettings()
        //    : base()
        //{
        //    if (!HasLocal)
        //    {
        //        Sections = new Dictionary<SectionPosition, Section>();
        //        SectionPosition[] items = (SectionPosition[])Enum.GetValues(typeof(SectionPosition));
        //        foreach (SectionPosition pos in items)
        //        {
        //            Section section = Mvx.IocConstruct<Section>();
        //            section.Id = pos;
        //            section.Lane = new Lane();
        //            //section.Lane.Direction = section.Direction;
        //            section.Lane.Enabled = true;
        //            //section.Lane.VehicleType = VehicleType.Bike;
        //            section.Lane.Name = null;
        //            if (pos == SectionPosition.Lane1 || pos == SectionPosition.Lane2)
        //                section.IsConfigured = true;
        //            Sections.Add(pos, section);
        //        }
        //    }
        //}

        public void ChangeLaneDirection(SectionPosition sectionId, LaneDirection direction)
        {
            Sections[sectionId].Direction = direction;
            MarkChanged();
        }

        public void AddSection(Section section)
        {
            Sections.Add(section.Id, section);
            MarkChanged();
        }

        public void UpdateSection(Section section)
        {
            if (!Sections.ContainsKey(section.Id))
                return;

            Sections[section.Id] = section;
            MarkChanged();
        }

        public List<Section> GetAllSections()
        {
            if (Sections.Count == 0)
            {
                Section sec = new Section();
                Lane lane = new Lane();
                sec.Lane = lane;
                sec.Direction = LaneDirection.In;
                sec.Id = SectionPosition.Lane1;
                Sections.Add(SectionPosition.Lane1, sec);
            }

            return Sections.Where(sec => sec.Value.IsConfigured == true).Select(sec => sec.Value).ToList();
        }

        protected override string GetSettingName()
        {
            if (_argParams.Mode == RunMode.Testing)
                return this.GetType().Name + "Test";
            else
                return base.GetSettingName();
        }

        public override void ForceSave()
        {
            this.Sections.Values.ToList().ForEach(item => item.SaveStates());
            MarkChanged();

            base.ForceSave();
        }
    } 
}
