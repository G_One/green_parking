﻿using Squarebit.Apms.Terminal.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public enum CameraType
    {
        Vivotek,
        Hik,
        Webcam
    }

    public class OptionsSettingsData : BaseSettingsData
    {
		public string MasterPasswordMd5 { get; set; }
        public bool PlateRecognitionEnale { get; set; }
        public bool NoMatchingPlateNoticeEnalbe { get; set; }
        public bool ForceUpdatePlateNumber { get; set; }

        private bool _confirmCheckout = true;
        public bool ConfirmCheckout
        {
            get { return _confirmCheckout; }
            set { _confirmCheckout = value; }
        }

        private int _waitingCheckDuration = 15;
        public int WaitingCheckDuration
        {
            get { return _waitingCheckDuration; }
            set { _waitingCheckDuration = value; }
        }

        private CameraType _cameraType;
        public CameraType CameraType
        {
            get { return _cameraType; }
            set { _cameraType = value; }

        }
        
        private int _displayCheckOutDuration = 15;
        public int DisplayCheckOutDuration
        {
            get { return _displayCheckOutDuration; }
            set { _displayCheckOutDuration = value; }

        }
    }

    public interface IOptionsSettings : IBaseSettings
    {
		string MasterPasswordMd5 { get; set; }
        bool PlateRecognitionEnale { get; set; }
        bool NoMatchingPlateNoticeEnalbe { get; set; }
        bool ForceUpdatePlateNumber { get; set; }
        bool ConfirmCheckout { get; set; }
        int WaitingCheckDuration { get; set; }
        CameraType CameraType { get; set; }
        int DisplayCheckOutDuration { get; set; }
    }

    public class OptionsSettings : BaseSettings<OptionsSettingsData>, IOptionsSettings
    {
		public static string DEFAULT_MASTER_PASSWORD = "admin";

		public string MasterPasswordMd5
		{
			get {
				if (string.IsNullOrEmpty(_data.MasterPasswordMd5))
					return EncryptionUtility.GetMd5Hash(DEFAULT_MASTER_PASSWORD);
				return _data.MasterPasswordMd5; 
			}
			set
			{
				_data.MasterPasswordMd5 = value;
				MarkChanged();
			}
		}

        public bool PlateRecognitionEnale
        {
            get { return _data.PlateRecognitionEnale; }
            set
            {
                _data.PlateRecognitionEnale = value;
                MarkChanged();
            }
        }

        public bool NoMatchingPlateNoticeEnalbe
        {
            get { return _data.NoMatchingPlateNoticeEnalbe; }
            set
            {
                _data.NoMatchingPlateNoticeEnalbe = value;
                MarkChanged();
            }
        }

        public bool ForceUpdatePlateNumber
        {
            get { return _data.ForceUpdatePlateNumber; }
            set
            {
                _data.ForceUpdatePlateNumber = value;
                MarkChanged();
            }
        }

        public bool ConfirmCheckout
        {
            get { return _data.ConfirmCheckout; }
            set
            {
                _data.ConfirmCheckout = value;
                MarkChanged();
            }
        }

        public int WaitingCheckDuration
        {
            get { return _data.WaitingCheckDuration; }
            set
            {
                _data.WaitingCheckDuration = value;
                MarkChanged();
            }
        }

        public CameraType CameraType
        {
            get { return _data.CameraType; }
            set
            {
                _data.CameraType = value;
                MarkChanged();
            }
 		}
 		
        public int DisplayCheckOutDuration
        {
            get { return _data.DisplayCheckOutDuration; }
            set
            {
                _data.DisplayCheckOutDuration = value;
                MarkChanged();
            }
        }

        public OptionsSettings()
            : base()
        {

        }
    }
}
