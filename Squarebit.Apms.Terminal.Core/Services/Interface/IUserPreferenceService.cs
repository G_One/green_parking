﻿using System;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public interface IUserPreferenceService
    {
        bool HasLocal { get; }
        /// <summary>
        /// System settings of client
        /// </summary>
        ISystemSettings SystemSettings { get; }

        IHostSettings HostSettings { get; }

        IOptionsSettings OptionsSettings { get; }

        ITestingSettings TestSettings { get; }

        IClaimPromotionSettings ClaimPromotionSettings { get; }

        void SyncToServer(Action<Exception> complete);
    }
}
