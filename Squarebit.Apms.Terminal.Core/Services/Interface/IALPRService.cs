﻿using System;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public interface IALPRService
    {
        void RecognizeLicensePlate(byte[] image, Action<string, Exception> complete);
    }
}
