﻿using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public interface IParkingFeeService
    {
        float CalculateFee(VehicleTypeEnum type, float billAmount, DateTime checkInTime, DateTime claimTime);
    }
}