﻿using Squarebit.Apms.Terminal.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Services
{
    public class UserServiceLocator : IUserServiceLocator
    {
        private Dictionary<SectionPosition, IUserService> _serviceMap = new Dictionary<SectionPosition, IUserService>();
        private IServer _server;

        public UserServiceLocator(IServer server)
        {
            _server = server;
        }

        public IUserService GetUserService(SectionPosition section)
        {
            if (!_serviceMap.ContainsKey(section))
            {
                _serviceMap[section] = new UserService(_server);
            }
            return _serviceMap[section];
        }
    }
}
