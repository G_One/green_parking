﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core
{
	public class IconEnums
	{
		public static string Error = "appbar_stop";
        public static string Warning = "";
		public static string Close = "appbar_close";
		public static string Duplicate = "appbar_card_2";
		public static string Check = "appbar_check";
		public static string Parking = "appbar_bike_parking";
		public static string Card = "appbar_creditcard";
        public static string Guide = "appbar_warning_circle";
	}
}
