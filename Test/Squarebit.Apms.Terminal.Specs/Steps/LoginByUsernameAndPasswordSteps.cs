﻿using System;
using TechTalk.SpecFlow;

namespace Squarebit.Apms.Terminal.Core.Test
{
	[Binding]
	public class LoginByUsernameAndPasswordSteps
	{
		[Given(@"a user is at login screen of a section of the program")]
		public void GivenAUserIsAtLoginScreenOfASectionOfTheProgram()
		{
			ScenarioContext.Current.Pending();
		}

		[When(@"he inputs his username and password")]
		public void WhenHeInputsHisUsernameAndPassword()
		{
			ScenarioContext.Current.Pending();
		}

		[When(@"his username exists")]
		public void WhenHisUsernameExists()
		{
			ScenarioContext.Current.Pending();
		}

		[When(@"his password matches")]
		public void WhenHisPasswordMatches()
		{
			ScenarioContext.Current.Pending();
		}

		[When(@"his username does not exist")]
		public void WhenHisUsernameDoesNotExist()
		{
			ScenarioContext.Current.Pending();
		}

		[When(@"his password does not match")]
		public void WhenHisPasswordDoesNotMatch()
		{
			ScenarioContext.Current.Pending();
		}

		[Then(@"the login is successful")]
		public void ThenTheLoginIsSuccessful()
		{
			ScenarioContext.Current.Pending();
		}

		[Then(@"he is authenticated for that section")]
		public void ThenHeIsAuthenticatedForThatSection()
		{
			ScenarioContext.Current.Pending();
		}

		[Then(@"the login is failed")]
		public void ThenTheLoginIsFailed()
		{
			ScenarioContext.Current.Pending();
		}

		[Then(@"he is prompted with a message ""(.*)""")]
		public void ThenHeIsPromptedWithAMessage(string p0)
		{
			ScenarioContext.Current.Pending();
		}

		[Then(@"he is not authenticated for that section")]
		public void ThenHeIsNotAuthenticatedForThatSection()
		{
			ScenarioContext.Current.Pending();
		}
	}
}
