﻿Feature: CheckIn
	Để có thể kiểm tra đối chiếu thông tin khi xe ra
	Là nv điều khiển cổng
	Tôi cần lưu thông tin khi xe vào

@checking-in
Scenario: Check-in with a valid card 
	Given a user is at check-in screen of a section of the program
	When he inputs the card Id by tapping the card on a RFID Card Reader
	And the card is valid
	Then the check-in is successful
	And checking-in info is showed 
	And checking-in info is stored
	And he allow the vehicle to enter 

@checking-in
Scenario: Update plate number after valid check-in
	Given a user is at check-in screen of a section of the program
	And a vehicle just checked-in
	And the plate number is incorrect recognition
	When he inputs the number to correct the wrong recognition
	Then checking-in info is updated with new number

@checking-in
Scenario: Update vehicle type after valid check-in
	Given a user is at check-in screen of a section of the program
	And a vehicle just checked-in
	When he inputs the type of vehicle
	Then checking-in info is updated with new vehicle type

@checking-in
Scenario: Check-in with invalid card
	Given a user is at check-in screen of a section of the program
	When he inputs the card Id by tapping the card on a RFID Card Reader
	And the card is not valid 
	Then the check-in is failed
	And a message is displayed to the user