﻿Feature: Login by username and password
	Để có thể thực hiện chức năng trong quyền hạn của tôi
	Là người sử dụng
	Tôi cần đăng nhập bằng tên và mật khẩu

@login
Scenario: Login with correct username and password
	Given a user is at login screen of a section of the program
	When he inputs his username and password
	And his username exists 
	And his password matches
	Then the login is successful
	And he is authenticated for that section

@login
Scenario: Login with incorrect username
	Given a user is at login screen of a section of the program
	When he inputs his username and password
	And his username does not exist
	Then the login is failed
	And he is prompted with a message "Sai tên đăng nhập hoặc mật khẩu"
	And he is not authenticated for that section

@login
Scenario: Login with incorrect password
	Given a user is at login screen of a section of the program
	When he inputs his username and password
	And his username exists
	And his password does not match
	Then the login is failed
	And he is prompted with a message "Sai tên đăng nhập hoặc mật khẩu"
	And he is not authenticated for that section