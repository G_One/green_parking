﻿Feature: CheckOut
	Để đối chiếu thông tin với xe lúc ra nhằm kiểm tra sự hợp lệ của thẻ xe
	Là nv điều khiển cổng
	Tôi cần kiểm tra thông tin xe lúc vào của thẻ xe

Scenario: Review check out with valid card and number recognition is the same
	Given a user is at check-out screen of a section of the program
	When he inputs the card Id by tapping the card on a RFID Card Reader
	And the card is in valid state
	Then the checking-out vehicle is valid
	And checked-in info is showed
	And checking-out info is showed
	And number recognition is the same as checked-in info
	And the screen border is showed with positive color
	And confirm message is showed

Scenario: Confirm checking-out and allow vehicle to exit
	Given a user is at check-out screen of a section of the program
	And  the checking-out vehicle is valid
	When he enter the confirmation key
	Then checking-out info is stored
	And the barrier is open up
	And the vehicle allow to exit

Scenario: Review check out with valid card and number recognition is not the same
	Given a user is at check-out screen of a section of the program
	When he inputs the card Id by tapping the card on a RFID Card Reader
	And the card is in valid state
	Then the checking-out vehicle is invalid
	And checked-in info is showed
	And checking-out info is showed
	And number recognition is NOT the same as checked-in info
	And the screen border is showed with negative color and flashing
	And confirm message is showed

Scenario: Confirm checking-out and allow vehicle to exit
	Given a user is at check-out screen of a section of the program
	And  the checking-out vehicle is invalid
	When he enter the confirmation key
	Then checking-out info is stored
	And the barrier is open up
	And the vehicle allow to exit

Scenario: Check out with invalid card
	Given a user is at check-out screen of a section of the program
	When he inputs the card Id by tapping the card on a RFID Card Reader
	And the card is not in valid state
	Then the check-out is failed
	And a message is displayed to the user