﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow.Generator;
using TechTalk.SpecFlow.Generator.UnitTestProvider;
using TechTalk.SpecFlow.Utils;

namespace CodedUIGeneratorProvider.Generator.SpecFlowPlugin
{
	/// <summary>
	/// The CodedUI generator.
	/// </summary>
	public class CodedUIGeneratorProvider : MsTest2010GeneratorProvider
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CodedUiGeneratorProvider"/> class.
		/// </summary>
		/// <param name="codeDomHelper">
		/// The code dom helper.
		/// </param>
		public CodedUIGeneratorProvider(CodeDomHelper codeDomHelper)
			: base(codeDomHelper)
		{
		}

		/// <summary>
		/// The set test class.
		/// </summary>
		/// <param name="generationContext">
		/// The generation context.
		/// </param>
		/// <param name="featureTitle">
		/// The feature title.
		/// </param>
		/// <param name="featureDescription">
		/// The feature description.
		/// </param>
		public override void SetTestClass(TestClassGenerationContext generationContext, string featureTitle, string featureDescription)
		{
			base.SetTestClass(generationContext, featureTitle, featureDescription);

			foreach (CodeAttributeDeclaration declaration in generationContext.TestClass.CustomAttributes)
			{
				if (declaration.Name == "Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute")
				{
					generationContext.TestClass.CustomAttributes.Remove(declaration);
					break;
				}
			}

			generationContext.TestClass.CustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference("Microsoft.VisualStudio.TestTools.UITesting.CodedUITestAttribute")));
		}
	}
}