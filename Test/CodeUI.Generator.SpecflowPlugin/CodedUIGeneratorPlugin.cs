﻿using TechTalk.SpecFlow.Infrastructure;

// %LocalAppData%\Microsoft\VisualStudio\12.0\Extensions
// https://github.com/techtalk/SpecFlow/wiki/Using-SpecFlow-with-CodedUI-API
// http://blog.jessehouwing.nl/2013/04/creating-custom-unit-test-generator.html
[assembly: GeneratorPlugin(typeof(CodedUIGeneratorProvider.Generator.SpecFlowPlugin.CodedUIGeneratorPlugin))]
namespace CodedUIGeneratorProvider.Generator.SpecFlowPlugin
{
	using System.CodeDom;

	using BoDi;

	using TechTalk.SpecFlow.Generator;
	using TechTalk.SpecFlow.Generator.Configuration;
	using TechTalk.SpecFlow.Generator.Plugins;
	using TechTalk.SpecFlow.Generator.UnitTestProvider;
	using TechTalk.SpecFlow.UnitTestProvider;
	using TechTalk.SpecFlow.Utils;

	/// <summary>
	/// The CodedUI generator plugin.
	/// </summary>
	public class CodedUIGeneratorPlugin : IGeneratorPlugin
	{
		/// <summary>
		/// The register dependencies.
		/// </summary>
		/// <param name="container">
		/// The container.
		/// </param>
		public void RegisterDependencies(ObjectContainer container)
		{
		}

		/// <summary>
		/// The register customizations.
		/// </summary>
		/// <param name="container">
		/// The container.
		/// </param>
		/// <param name="generatorConfiguration">
		/// The generator configuration.
		/// </param>
		public void RegisterCustomizations(ObjectContainer container, SpecFlowProjectConfiguration generatorConfiguration)
		{
			container.RegisterTypeAs<CodedUIGeneratorProvider, IUnitTestGeneratorProvider>();
			container.RegisterTypeAs<MsTest2010RuntimeProvider, IUnitTestRuntimeProvider>();
		}

		/// <summary>
		/// The register configuration defaults.
		/// </summary>
		/// <param name="specFlowConfiguration">
		/// The spec flow configuration.
		/// </param>
		public void RegisterConfigurationDefaults(SpecFlowProjectConfiguration specFlowConfiguration)
		{
		}
	}
}