using Cirrious.CrossCore.Plugins;

namespace Squarebit.Apms.Terminal.Core.Test.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}