﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Devices.Dal;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
	[TestClass]
	public class LoginViewModelTest : MvvmCrossViewModelTestSetup<LoginViewModel>
	{
        private Mock<ISection> _mockSection;
        private CardReaderEventHandler _readCallback;

        private void SendCardId(string cardId)
        {
            Devices.Dal.CardReaderEventArgs e = new Devices.Dal.CardReaderEventArgs();
            e.CardID = cardId;
            _readCallback(null, e);
            //_mockSection.Raise(cardReader => cardReader.ReadingCompleted += null, null, e);
        }

        private int numClose = 0;

        private void OnClose(BaseViewModel vm)
        {
            numClose++;
        }

        [TestInitialize]
		public void Setup()
		{
            var mockUserService = GetMock<IUserService>();
            mockUserService.Setup(userService => userService.Login("ndhoang", "validpassword", It.IsAny<int>(), It.IsAny<Action<Exception>>())).Callback((string username, string password, Action<Exception> complete) => complete(null));
            mockUserService.Setup(userService => userService.Login("valid_card_id", It.IsAny<int>(), It.IsAny<Action<Exception>>())).Callback((string cardId, Action<Exception> complete) => complete(null));
            mockUserService.Setup(userService => userService.Login("somebody", "invalidpassword", It.IsAny<int>(), It.IsAny<Action<Exception>>())).Callback((string username, string password, Action<Exception> complete) => complete(new LoginInvalidException()));
            mockUserService.Setup(userService => userService.Login("invalid_card_id", It.IsAny<int>(), It.IsAny<Action<Exception>>())).Callback((string cardId, Action<Exception> complete) => complete(new LoginInvalidException()));
            mockUserService.Setup(userService => userService.Login("error", "error", It.IsAny<int>(), It.IsAny<Action<Exception>>())).Callback((string username, string password, Action<Exception> complete) => complete(new ServerErrorException()));
            mockUserService.Setup(userService => userService.Login("timeout", "timeout", It.IsAny<int>(), It.IsAny<Action<Exception>>())).Callback((string username, string password, Action<Exception> complete) => complete(new ServerDisconnectException()));

            _mockSection = new Mock<ISection>();
            _mockSection.Setup(section => section.StartCardReader(It.IsAny<CardReaderEventHandler>(), It.IsAny<CardReaderEventHandler>())).Returns(true).Callback((CardReaderEventHandler read, CardReaderEventHandler takeoff) => { _readCallback = read; });
            _mockSection.Setup(section => section.Id).Returns(SectionPosition.Lane1);
            var mockParameterService = GetMock<IParameterService>();
            mockParameterService.Setup(service => service.Retrieve<ISection>(It.IsAny<ParameterKey>())).Returns(_mockSection.Object);
            _viewModel.Init(new ParameterKey());
            _viewModel.OnClose = OnClose;
		}

        [TestMethod]
        public void Login_should_close_window_with_valid_username_password()
        {
            _viewModel.Username = "ndhoang";
            _viewModel.Password = "validpassword";
            _viewModel.LoginCommand.Execute(null);
            Assert.AreEqual<int>(1, numClose);
        }

        [TestMethod]
        public void Login_should_close_window_with_valid_card_id()
        {
            SendCardId("valid_card_id");
            Assert.AreEqual<int>(1, numClose);
        }

        [TestMethod]
        public void Login_should_show_error_with_invalid_username_password()
        {
            _viewModel.Username = "somebody";
            _viewModel.Password = "invalidpassword";
            _viewModel.LoginCommand.Execute(null);
            Assert.AreEqual<string>(LoginViewModel.msgInvalid, _viewModel.ResultMessage);
        }

        [TestMethod]
        public void Login_should_show_error_with_invalid_card_id()
        {
            SendCardId("invalid_card_id");
            Assert.AreEqual<string>(LoginViewModel.msgInvalid, _viewModel.ResultMessage);
        }

        [TestMethod]
        public void Login_should_show_error_when_user_service_got_error_in_process()
        {
            _viewModel.Username = "error";
            _viewModel.Password = "error";
            _viewModel.LoginCommand.Execute(null);
            Assert.AreEqual<string>(LoginViewModel.msgServerError, _viewModel.ResultMessage);
        }

        [TestMethod]
        public void Login_should_show_error_when_user_service_disconnect()
        {
            _viewModel.Username = "timeout";
            _viewModel.Password = "timeout";
            _viewModel.LoginCommand.Execute(null);
            Assert.AreEqual<string>(LoginViewModel.msgServerDisconnect, _viewModel.ResultMessage);
        }

        //[TestMethod]
        //public void Login_should_show_error_when_card_reader_service_is_not_ready()
        //{
        //    SendCardId("notready_card_id");
        //    //Assert.AreEqual<string>("login.card_reader_service_error", _viewModel.ResultMessage);
        //    Assert.AreEqual(0, 1);
        //}
	}
}