﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
    [TestClass]
    public class SearchViewModelTest : MvvmCrossViewModelTestSetup<SearchViewModel>
    {
        Mock<IALPRService> _alprService;
        Mock<IServer> _server;
        ISection _section;
        Mock<IStorageService> _storageService;

        [TestInitialize]
        public override void BaseSetup()
        {
            base.BaseSetup();

            _alprService = GetMock<IALPRService>();
            _storageService = GetMock<IStorageService>();
            _server = GetMock<IServer>();
            _viewModel.SampleCheckIn = new CheckIn();
        }
              
        [TestMethod]
        public void Search_should_return_value_with_only_card_label()
        {
            _server.Setup(f => f.ParkingSessionSearchAdvance(It.IsAny<Action<CheckIn[], Exception>>(),
                DateTime.Now,
                DateTime.Now,
                0,
                null,
                null,
                null,
                VehicleType.None,
                VehicleSubType.None)).Callback(() => {

                    CheckIn[] results = new CheckIn[1];
                    //complete(results, null);
                });

            _viewModel.Search(() => {
                Assert.AreNotEqual(null, _viewModel.CheckIns);
                Assert.AreEqual(1, _viewModel.CheckIns.Length);
            });
        }

        [TestMethod]
        public void Search_should_return_value_with_full_criteria()
        {
            _server.Setup(f => f.ParkingSessionSearchAdvance(It.IsAny<Action<CheckIn[], Exception>>(),
                DateTime.Now,
                DateTime.Now,
                It.IsAny<int>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<VehicleType>(),
                It.IsAny<VehicleSubType>())).Callback((Action<CheckIn[], Exception> complete,
                    DateTime fromTime,
                    DateTime toTime,
                    int limit,
                    string cardId,
                    string cardLabel,
                    string vehicleNumber,
                    VehicleType vehicleType,
                    VehicleSubType vehicleSubType) => {

                    CheckIn[] results = new CheckIn[1];
                    complete(results, null);
                });

            _viewModel.Search(() => {
                Assert.AreNotEqual(null, _viewModel.CheckIns);
                Assert.AreEqual(1, _viewModel.CheckIns.Length);
            });
        }

        [TestMethod]
        public void Search_should_return_value_without_card_id()
        {
            _server.Setup(f => f.ParkingSessionSearchAdvance(It.IsAny<Action<CheckIn[], Exception>>(),
                DateTime.Now,
                DateTime.Now,
                It.IsAny<int>(),
                null,
                It.IsAny<string>(),
                It.IsAny<string>(),
                VehicleType.Bike,
                VehicleSubType.Bike_Auto)).Callback((Action<CheckIn[], Exception> complete,
                    DateTime fromTime,
                    DateTime toTime,
                    int limit,
                    string cardId,
                    string cardLabel,
                    string vehicleNumber,
                    VehicleType vehicleType,
                    VehicleSubType vehicleSubType) => {

                    CheckIn[] results = new CheckIn[1];
                    complete(results, null);
                });

            _viewModel.Search(() => {
                Assert.AreNotEqual(null, _viewModel.CheckIns);
                Assert.AreEqual(1, _viewModel.CheckIns.Length);
            });
        }
    }
}
