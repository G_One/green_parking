﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Devices.Dal;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
	[TestClass]
    public class GeneralConfigViewModelTest : MvvmCrossViewModelTestSetup<GeneralConfigViewModel>
	{
        private Mock<IUserPreferenceService> _mockUserPreferenceService;
        private Mock<IHostSettings> _mockHostSettings;

        protected override void SetupMock()
		{
            _mockUserPreferenceService = GetMock<IUserPreferenceService>();
            _mockHostSettings = new Mock<IHostSettings>();
            _mockUserPreferenceService.Setup(service => service.HostSettings).Returns(_mockHostSettings.Object);
            var mockServer = GetMock<IServer>();
            mockServer.Setup(service => service.CheckHealthServer("192.168.1.216", It.IsAny<Action<Exception>>())).Callback((string ip, Action<Exception> complete) => complete(null));
            mockServer.Setup(service => service.CheckHealthServer("192.168.1.217:8080", It.IsAny<Action<Exception>>())).Callback((string ip, Action<Exception> complete) => complete(new ServerDisconnectException()));
		}

        [TestMethod]
        public void ServerIPConfig_should_show_success_message_if_check_button_clicked_and_success_to_connect_server()
        {
            _viewModel.PrimaryIP = "192.168.1.216";
            _viewModel.SecondaryIP = "192.168.1.216";
            _viewModel.CheckServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgServerSuccess, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(1, _viewModel.ServerMessageLevel);
        }

        [TestMethod]
        public void ServerIPConfig_should_show_fail_message_if_check_button_clicked_and_fail_to_connect_server()
        {
            // Primary server
            _viewModel.PrimaryIP = "192.168.1.217:8080";
            _viewModel.SecondaryIP = "192.168.1.216";
            _viewModel.CheckServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgFailIP1, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ServerMessageLevel);
            // Secondary server
            _viewModel.PrimaryIP = "192.168.1.216";
            _viewModel.SecondaryIP = "192.168.1.217:8080";
            _viewModel.CheckServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgFailIP2, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ServerMessageLevel);
        }

        [TestMethod]
        public void ServerIPConfig_should_show_invalid_ip_message_if_check_button_clicked_and_input_ip_informal()
        {
            // Primary server
            _viewModel.PrimaryIP = "192,168,1,216";
            _viewModel.SecondaryIP = "192.168.1.216";
            _viewModel.CheckServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidIP1, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ServerMessageLevel);
            // Secondary server
            _viewModel.PrimaryIP = "192.168.1.216";
            _viewModel.SecondaryIP = "192,168,1,216";
            _viewModel.CheckServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidIP2, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ServerMessageLevel);
        }

        [TestMethod]
        public void ServerIPConfig_should_show_invalid_ip_message_if_save_button_clicked_and_input_ip_informal()
        {
            _viewModel.PrimaryIP = "192,168,1,216";
            _viewModel.SecondaryIP = "192.168.1.216";
            _viewModel.SaveServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidIP1, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ServerMessageLevel);
            _mockHostSettings.VerifySet(setting => setting.PrimaryServerIP = It.IsAny<string>(), Times.Never);
            _mockHostSettings.VerifySet(setting => setting.SecondaryServerIP = It.IsAny<string>(), Times.Never);
        }

        [TestMethod]
        public void ServerIPConfig_should_save_new_ip_and_show_saved_message_if_save_button_clicked_and_input_ip_formal()
        {
            _viewModel.PrimaryIP = "192.168.1.216:1209";
            _viewModel.SecondaryIP = "192.168.1.211:1234";
            _viewModel.SaveServerConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgSaved, _viewModel.ServerResultMessage);
            Assert.AreEqual<int>(2, _viewModel.ServerMessageLevel);
            _mockHostSettings.VerifySet(setting => setting.PrimaryServerIP = "192.168.1.216:1209", Times.Once);
            _mockHostSettings.VerifySet(setting => setting.SecondaryServerIP = "192.168.1.211:1234", Times.Once);
        }

        [TestMethod]
        public void ServerIPConfig_should_show_pre_config_info_if_saved_config_exist()
        {
            _mockHostSettings.Setup(setting => setting.PrimaryServerIP).Returns("192.168.1.129");
            _mockHostSettings.Setup(setting => setting.SecondaryServerIP).Returns("192.168.1.128");
            _viewModel.Start();
            Assert.AreEqual<string>("192.168.1.129", _viewModel.PrimaryIP);
            Assert.AreEqual<string>("192.168.1.128", _viewModel.SecondaryIP);
        }

        [TestMethod]
        public void LocalConfig_should_show_success_message_if_check_button_clicked_with_valid_terminal_name_and_storage_path()
        {
            _viewModel.TerminalName = "NDHoangPC";
            _viewModel.StoragePath = "D:\\";
            _viewModel.CheckClientConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgClientSuccess, _viewModel.ClientResultMessage);
            Assert.AreEqual<int>(1, _viewModel.ClientMessageLevel);
        }

        [TestMethod]
        public void LocalConfig_should_show_invalid_message_if_check_button_clicked_with_terminal_name_is_empty()
        {
            _viewModel.TerminalName = "";
            _viewModel.StoragePath = "D:\\";
            _viewModel.CheckClientConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidTerminalName, _viewModel.ClientResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ClientMessageLevel);
        }

        [TestMethod]
        public void LocalConfig_should_show_success_message_if_check_button_clicked_with_nonexist_storage_path()
        {
            _viewModel.TerminalName = "NDHoang";
            _viewModel.StoragePath = "C:\\Can\'t touch this";
            _viewModel.CheckClientConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidStoragePath, _viewModel.ClientResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ClientMessageLevel);
        }

        [TestMethod]
        public void LocalConfig_should_show_success_message_if_save_button_clicked_with_valid_terminal_name_and_storage_path()
        {
            Models.Terminal terminal = new Models.Terminal() { TerminalId = "Test", Name = "Test" };
            _mockHostSettings.Setup(setting => setting.Terminal).Returns(terminal);
            _viewModel.Start();
            _viewModel.TerminalName = "NDHoangPC";
            _viewModel.StoragePath = "D:\\";
            _viewModel.SaveClientConfigCommand.Execute(null);
            //Assert.AreEqual<string>(GeneralConfigViewModel.msgSaved, _viewModel.ClientResultMessage);
            //Assert.AreEqual<int>(2, _viewModel.ClientMessageLevel);
            _mockHostSettings.VerifySet(setting => setting.StoragePath = "D:\\", Times.Once);
            _mockHostSettings.VerifySet(setting => setting.Terminal = terminal, Times.Once);
        }

        [TestMethod]
        public void LocalConfig_should_show_invalid_message_if_save_button_clicked_with_terminal_name_is_empty()
        {
            Models.Terminal terminal = new Models.Terminal() { TerminalId = "Test", Name = "Test" };
            _mockHostSettings.Setup(setting => setting.Terminal).Returns(terminal);
            _viewModel.Start();
            _viewModel.TerminalName = "";
            _viewModel.StoragePath = "D:\\";
            _viewModel.SaveClientConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidTerminalName, _viewModel.ClientResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ClientMessageLevel);
            _mockHostSettings.VerifySet(setting => setting.StoragePath = It.IsAny<string>(), Times.Never);
            _mockHostSettings.VerifySet(setting => setting.Terminal = It.IsAny<Models.Terminal>(), Times.Never);
        }

        [TestMethod]
        public void LocalConfig_should_show_invalid_message_if_save_button_clicked_with_nonexist_storage_path()
        {
            Models.Terminal terminal = new Models.Terminal() { TerminalId = "Test", Name = "Test" };
            _mockHostSettings.Setup(setting => setting.Terminal).Returns(terminal);
            _viewModel.Start();
            _viewModel.TerminalName = "NDHoang";
            _viewModel.StoragePath = "D:\\Can\'t touch this";
            _viewModel.SaveClientConfigCommand.Execute(null);
            Assert.AreEqual<string>(GeneralConfigViewModel.msgInvalidStoragePath, _viewModel.ClientResultMessage);
            Assert.AreEqual<int>(3, _viewModel.ClientMessageLevel);
            _mockHostSettings.VerifySet(setting => setting.StoragePath = It.IsAny<string>(), Times.Never);
            _mockHostSettings.VerifySet(setting => setting.Terminal = It.IsAny<Models.Terminal>(), Times.Never);
        }

        [TestMethod]
        public void LocalConfig_should_show_pre_config_info_if_saved_config_exist()
        {
            _mockHostSettings.Setup(setting => setting.StoragePath).Returns("C:\\abc");
            _mockHostSettings.Setup(setting => setting.Terminal).Returns(new Models.Terminal() { TerminalId = "Test", Name = "Test"});
            _viewModel.Start();
            Assert.AreEqual<string>("C:\\abc", _viewModel.StoragePath);
            Assert.AreEqual<string>("Test", _viewModel.TerminalName);
        }
	}
}