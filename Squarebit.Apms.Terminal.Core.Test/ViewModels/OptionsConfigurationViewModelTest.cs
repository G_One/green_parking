﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squarebit.Apms.Terminal.Core.ViewModels;
using FluentAssertions;
using Squarebit.Apms.Terminal.Core.Services;
using Moq;
using Squarebit.Apms.Terminal.Core.Utilities;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
	[TestClass]
	public class OptionsConfigurationViewModelTest : MvvmCrossViewModelTestSetup<OptionsConfigurationViewModel>
	{
		Mock<IUserPreferenceService> _userPref;
		Mock<IOptionsSettings> _optionSettings;
		bool isSaved = false;

		[TestInitialize]
		public override void BaseSetup()
		{
			base.BaseSetup();

			_userPref = GetMock<IUserPreferenceService>();
			_optionSettings = GetMock<IOptionsSettings>();

			_userPref.Setup(f => f.OptionsSettings).Returns(_optionSettings.Object);
			_optionSettings.Setup(f => f.Save()).Callback(() =>
			{
				isSaved = true;
			});
			_optionSettings.Setup(f => f.MasterPasswordMd5).Returns(EncryptionUtility.GetMd5Hash(OptionsSettings.DEFAULT_MASTER_PASSWORD));
		}

		[TestMethod]
		public void Should_show_message_when_input_is_incorrect()
		{

			_viewModel.CurrentPassword = "";
			_viewModel.NewPassword = "";
			_viewModel.NewPasswordAgain = "";
			_viewModel.PasswordSaveCommand.Execute(null);
			_viewModel.ResultMessage.Should().Be("system.password_empty");
			isSaved.Should().BeFalse();

			_viewModel.CurrentPassword = "wrong_pass";
			_viewModel.NewPassword = "new_pass";
			_viewModel.NewPasswordAgain = "new_pass";
			_viewModel.PasswordSaveCommand.Execute(null);
			_viewModel.ResultMessage.Should().Be("system.invalid_master_password");
			isSaved.Should().BeFalse();

			_viewModel.CurrentPassword = OptionsSettings.DEFAULT_MASTER_PASSWORD;
			_viewModel.NewPassword = "new_pass";
			_viewModel.NewPasswordAgain = "new_pass_2";
			_viewModel.PasswordSaveCommand.Execute(null);
			_viewModel.ResultMessage.Should().Be("system.passwords_not_matched");
			isSaved.Should().BeFalse();
		}

		
		[TestMethod]
		public void Should_save_and_show_message_when_input_is_correct()
		{
			_viewModel.CurrentPassword = OptionsSettings.DEFAULT_MASTER_PASSWORD;
			_viewModel.NewPassword = "new_pass";
			_viewModel.NewPasswordAgain = "new_pass";
			_viewModel.PasswordSaveCommand.Execute(null);
			_viewModel.ResultMessage.Should().Be("system.password_changed_success");
			isSaved.Should().BeTrue();
		}
	}
}
