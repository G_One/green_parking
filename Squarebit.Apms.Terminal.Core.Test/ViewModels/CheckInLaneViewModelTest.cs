﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
    [TestClass]
    public class CheckInLaneViewModelTest : MvvmCrossViewModelTestSetup<CheckInLaneViewModel>
    {
        Mock<IALPRService> _alprService;
        Mock<IServer> _server;
        ISection _section;

        [TestInitialize]
        public override void BaseSetup()
        {
            base.BaseSetup();

            _alprService = GetMock<IALPRService>();
            _server = GetMock<IServer>();
        }

        private void SetupDevices()
        {
            _section = new Section();
            _section.Id = SectionPosition.Lane1;
            _section.Lane = new Lane();
            _section.Direction = LaneDirection.In;
            _section.Lane.Enabled = true;
            _section.VehicleTypeId = 2;
            _section.Lane.Name = null;
            _section.IsConfigured = true;
            _viewModel.Section = _section;
            _viewModel.CheckedCard = new Card("cardId");

            Mock<ICamera> fMockCam = new Mock<ICamera>();
            fMockCam.Setup(f => f.CaptureImage()).Returns(new byte[1]);
            Camera mfCam = new Camera();
            mfCam.Position = CameraPosition.Front;
            mfCam.Direction = LaneDirection.In;
            mfCam.RawCamera = fMockCam.Object;
            _section.Cameras.Add(mfCam);

            Mock<ICamera> bMockCam = new Mock<ICamera>();
            bMockCam.Setup(f => f.CaptureImage()).Returns(new byte[1]);
            Camera mbCam = new Camera();
            mbCam.Position = CameraPosition.Back;
            mbCam.Direction = LaneDirection.In;
            mbCam.RawCamera = bMockCam.Object;
            _section.Cameras.Add(mbCam);
           
            _alprService.Setup(f => f.RecognizeLicensePlate(It.IsAny<byte[]>(), It.IsAny<Action<string, Exception>>())).Callback((byte[] image, Action<string, Exception> complete) => {
                complete("54F1-3333", null);
            });
        }

        /// <summary>
        /// A valid card must:
        /// 1/ Not physically damaged.
        /// 2/ Exist in card database.
        /// 3/ Not check in previously.
        /// </summary>
        [TestMethod]
        public void Checkin_should_success_with_valid_card()
        {
            SetupDevices();

            _server.Setup(f => f.CreateCheckIn(It.IsAny<CheckIn>(),
                It.IsAny<Action<CheckIn, Exception>>())).Callback((CheckIn data, Action<CheckIn, Exception> complete) => {
                    complete(new CheckIn(), null);
                });

            _viewModel.BeginCheckin(null);
            Assert.AreNotEqual(null, _viewModel.CheckInData);
        }

        [TestMethod]
        public void Checkin_should_fail_with_wrong_status_card()
        {
            SetupDevices();
            _server.Setup(f => f.CreateCheckIn(It.IsAny<CheckIn>(), It.IsAny<Action<CheckIn, Exception>>())).Callback((CheckIn data, Action<CheckIn, Exception> complete) => {
                complete(null, new Exception("Invalid card status"));
            });
            _viewModel.BeginCheckin(null);
            Assert.AreEqual(null, _viewModel.CheckInData);
        }

        [TestMethod]
        public void Checkin_should_fail_with_damaged_card()
        {
            SetupDevices();

            _server.Setup(f => f.CreateCheckIn(It.IsAny<CheckIn>(), It.IsAny<Action<CheckIn, Exception>>())).Callback((CheckIn data, Action<CheckIn, Exception> complete) => {
                complete(null, new Exception("Invalid card id"));
            });

            _viewModel.BeginCheckin(null);
            Assert.AreEqual(null, _viewModel.CheckInData);
            bool expected = _viewModel.Notices.Count > 0;
            Assert.AreNotEqual(false, expected);
        }

        [TestMethod]
        public void Check_in_should_fail_with_no_capture_front_or_back_image()
        {
            SetupDevices();

            _server.Setup(f => f.CreateCheckIn(It.IsAny<CheckIn>(), It.IsAny<Action<CheckIn, Exception>>())).Callback((CheckIn data, Action<CheckIn, Exception> complete) => {
                complete(null, new Exception("Missing photos"));
            });

            _viewModel.BeginCheckin(null);
            Assert.AreEqual(null, _viewModel.CheckInData);
            bool expected = _viewModel.Notices.Count > 0;
            Assert.AreNotEqual(false, expected);
        }
    }
}