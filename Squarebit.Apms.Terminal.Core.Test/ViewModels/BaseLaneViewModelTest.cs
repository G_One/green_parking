﻿using Cirrious.CrossCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Devices.Dal;
using Squarebit.Devices.Vivotek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
    [TestClass]
    public class BaseLaneViewModelTest : MvvmCrossViewModelTestSetup<BaseLaneViewModel>
    {
       /* Mock<ICardReaderService> _mockCardReaderService;

        Mock<IUserPreferenceService> _mockUserPreferenceService;

        Mock<IRFIDCardReaderService> _mockRFIDCardReaderService;
        Mock<IResourceLocatorService> _resource;

        [TestInitialize]
        public override void BaseSetup()
        {
            base.BaseSetup();
            
            _mockUserPreferenceService = GetMock<IUserPreferenceService>();

            _mockCardReaderService = GetMock<ICardReaderService>();

            _mockRFIDCardReaderService = GetMock<IRFIDCardReaderService>();
            _resource = GetMock<IResourceLocatorService>();
        }

        [TestMethod]
        public void Get_lane_is_not_null()
        {
            _viewModel.SectionId = 1;
            _mockUserPreferenceService.Setup(f => f.Sections[SectionPosition.Lane1]).Returns(new Section { Id = SectionPosition.Lane1 });
            _resource.Setup(f => f.GetCardReader(SectionPosition.Lane1)).Returns(new IdentiveCardReader("a"));
            _viewModel.Start();
            Assert.AreNotEqual(null, _viewModel.Section);
        }

        [TestMethod]
        public void Extract_vehicle_number()
        {
            string rs =_viewModel.ExtractVehicleNumber("aaf1-aaaa");
            Assert.AreEqual("aaaa", rs);
        }      */
    }
}