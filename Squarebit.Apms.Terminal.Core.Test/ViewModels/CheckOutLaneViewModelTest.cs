﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;
using Squarebit.Apms.Terminal.Core.Services;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Devices.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Squarebit.Apms.Terminal.Core.Test.ViewModels
{
    [TestClass]
    public class CheckOutLaneViewModelTest : MvvmCrossViewModelTestSetup<CheckOutLaneViewModel>
    {
       /* Mock<IALPRService> _alprService;
        Mock<IServer> _server;

        [TestInitialize]
        public override void BaseSetup()
        {
            base.BaseSetup();

            _alprService = GetMock<IALPRService>();
            _server = GetMock<IServer>();
        }

        [TestMethod]
        public void Should_respond_upon_tapping_card()
        {
            Assert.IsTrue(false);
        }

        /// <summary>
        /// A valid card must:
        /// 1/ Not physically damaged.
        /// 2/ Exist in card database.
        /// 3/ Checked in previously.
        /// </summary>
        [TestMethod]
        public void Checkout_should_success_with_valid_card()
        {
            GetMock<Camera>().Setup(f => f.CaptureImage()).Returns(new byte[1]);

            //_viewModel.FrontImage = new byte[1];
            //_viewModel.BackImage = new byte[1];
            _viewModel.CheckedCard = new Card("cardId") { Status = CardStatus.Free };
            //_viewModel.FrontCamera = GetMock<Camera>().Object;
            //_viewModel.BackCamera = GetMock<Camera>().Object;

            _alprService.Setup(f => f.RecognizeLicensePlate(It.IsAny<byte[]>(), It.IsAny<Action<string, Exception>>())).Callback((byte[] image, Action<string, Exception> complete) => {
                complete("54F1-3333", null);

            });

            _server.Setup(f => f.CreateCheckOut(It.IsAny<CheckOut>(), It.IsAny<Action<Exception>>())).Callback((CheckOut data, Action<Exception> complete) => {
                complete(null);
            });

            _viewModel.PackCheckOutData();
            Assert.AreEqual("Success", _viewModel.MessageToUser.Message);
        }

        [TestMethod]
        public void Checkout_should_fail_with_non_matching_vehicle_numbers()
        {
            GetMock<Camera>().Setup(f => f.CaptureImage()).Returns(new byte[1]);

            Section sec = new Section();
            sec.Lane = new Lane() { Id = 1 };
            sec.Id = SectionPosition.Lane1;
            sec.Lane.Direction = LaneDirection.In;
            sec.FrontInCamera = GetMock<Camera>().Object;
            sec.FrontOutCamera = GetMock<Camera>().Object;
            sec.BackInCamera = GetMock<Camera>().Object;
            sec.BackOutCamera = GetMock<Camera>().Object;
            _viewModel.Section = sec;
            _viewModel.CheckedCard = new Card("cardId");

            _alprService.Setup(f => f.RecognizeLicensePlate(It.IsAny<byte[]>(), It.IsAny<Action<string, Exception>>())).Callback((byte[] image, Action<string, Exception> complete) => {
                complete("54F1-3333", null);
            });

            _server.Setup(f => f.GetCheckIn(It.IsAny<string>(), It.IsAny<Action<CheckIn, Exception>>())).Callback((string cardId, Action<CheckIn, Exception> complete) => {
                complete(new CheckIn(), null);
            });

            _server.Setup(f => f.CreateCheckOut(It.IsAny<CheckOut>(), It.IsAny<Action<Exception>>())).Callback((CheckOut data, Action<Exception> complete) => {
                complete(new Exception("Non matching number"));
            });

            _viewModel.PackCheckOutData();
            Assert.AreEqual("Non matching number", _viewModel.MessageToUser.Message);
        }

        [TestMethod]
        public void Checkout_should_fail_with_wrong_status_card()
        {
            GetMock<Camera>().Setup(f => f.CaptureImage()).Returns(new byte[1]);

            Section sec = new Section();
            sec.Lane = new Lane() { Id = 1 };
            sec.Id = SectionPosition.Lane1;
            sec.Lane.Direction = LaneDirection.In;
            sec.FrontInCamera = GetMock<Camera>().Object;
            sec.FrontOutCamera = GetMock<Camera>().Object;
            sec.BackInCamera = GetMock<Camera>().Object;
            sec.BackOutCamera = GetMock<Camera>().Object;
            _viewModel.Section = sec;
            _viewModel.CheckedCard = new Card("cardId") { Status = CardStatus.Registered };

            _alprService.Setup(f => f.RecognizeLicensePlate(It.IsAny<byte[]>(), It.IsAny<Action<string, Exception>>())).Callback((byte[] image, Action<string, Exception> complete) => {
                complete("54F1-3333", null);
            });

            _server.Setup(f => f.GetCheckIn(It.IsAny<string>(), It.IsAny<Action<CheckIn, Exception>>())).Callback((string cardId, Action<CheckIn, Exception> complete) => {
                complete(new CheckIn(), null);
            });

            _server.Setup(f => f.CreateCheckOut(It.IsAny<CheckOut>(), It.IsAny<Action<Exception>>())).Callback((CheckOut data, Action<Exception> complete) => {
                complete(new Exception("Already registered"));
            });

            _viewModel.PackCheckOutData();
            Assert.AreEqual("Already registered", _viewModel.MessageToUser.Message);
        }

        [TestMethod]
        public void Checkout_should_fail_with_damaged_card()
        {
            Assert.IsTrue(false);
        }

        [TestMethod]
        public void Checkout_should_fail_with_no_front_back_images()
        {
            GetMock<Camera>().Setup(f => f.CaptureImage()).Returns(new byte[0]);

            Section sec = new Section();
            sec.Lane = new Lane() { Id = 1 };
            sec.Id = SectionPosition.Lane1;
            sec.Lane.Direction = LaneDirection.In;
            sec.FrontInCamera = GetMock<Camera>().Object;
            sec.FrontOutCamera = GetMock<Camera>().Object;
            sec.BackInCamera = GetMock<Camera>().Object;
            sec.BackOutCamera = GetMock<Camera>().Object;
            _viewModel.Section = sec;
            _viewModel.CheckedCard = new Card("cardId") { Status = CardStatus.Registered };

            _alprService.Setup(f => f.RecognizeLicensePlate(It.IsAny<byte[]>(), It.IsAny<Action<string, Exception>>())).Callback((byte[] image, Action<string, Exception> complete) => {
                complete("54F1-3333", null);
            });

            _server.Setup(f => f.GetCheckIn(It.IsAny<string>(), It.IsAny<Action<CheckIn, Exception>>())).Callback((string cardId, Action<CheckIn, Exception> complete) => {
                complete(new CheckIn(), null);
            });

            _server.Setup(f => f.CreateCheckOut(It.IsAny<CheckOut>(), It.IsAny<Action<Exception>>())).Callback((CheckOut data, Action<Exception> complete) => {
                complete(new Exception("Missing images"));
            });

            _viewModel.PackCheckOutData();
            Assert.AreEqual("Missing images", _viewModel.MessageToUser.Message);
        }

        public void CreateCamera()
        {

        }*/
    }
}