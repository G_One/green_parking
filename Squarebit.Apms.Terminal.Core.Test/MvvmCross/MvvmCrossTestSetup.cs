﻿using System;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Platform;
using Cirrious.CrossCore.Platform;
using Cirrious.CrossCore.IoC;
using Cirrious.MvvmCross;
using System.Diagnostics;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Localization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.MvvmCross.Plugins.Messenger;
using Squarebit.Devices.Dal;
using Moq;
using System.Collections.Generic;
using System.Net;
using Squarebit.Apms.Terminal.Core.Models;

namespace Squarebit.Apms.Terminal.Core.Test
{
    [Ignore]
    public class MvvmCrossTestSetup
    {
        private const string baseUrl = "http://192.168.1.216:9000/api";

        private IMvxIoCProvider _ioc;

        protected IMvxIoCProvider Ioc
        {
            get { return _ioc; }
        }

        protected MockMvxViewDispatcher ViewDispatcherMock { get; set; }

        protected Dictionary<Type, Mock> _mockServices = new Dictionary<Type, Mock>();

        private Mock<T> AddMockSingleton<T>() where T : class
        {
            Mock<T> mock = new Mock<T>();
            _mockServices.Add(typeof(T), mock);
            Ioc.RegisterSingleton<T>(mock.Object);
            return mock;
        }

        public Mock<T> GetMock<T>() where T : class
        {
            return _mockServices[typeof(T)] as Mock<T>;
        }

        protected void CleanAndSetup()
        {
            // fake set up of the IoC
            MvxSingleton.ClearAllSingletons();
            _ioc = MvxSimpleIoCContainer.Initialize();
            _ioc.RegisterSingleton(_ioc);
            _ioc.RegisterSingleton<IMvxTrace>(new TestTrace());
            RegisterAdditionalSingletons();
            InitialiseSingletonCache();
            InitialiseMvxSettings();
            MvxTrace.Initialize();
        }

        protected void InitialiseMvxSettings()
        {
            _ioc.RegisterSingleton<IMvxSettings>(new MvxSettings());
        }

        public void InitService()
        {
            AddMockSingleton<IParameterService>();
            AddMockSingleton<ICardReaderService>();
            AddMockSingleton<IUserPreferenceService>();
            AddMockSingleton<IServer>();
            AddMockSingleton<IRFIDCardReaderService>();
            AddMockSingleton<IUserService>();
            AddMockSingleton<IResourceLocatorService>();
            AddMockSingleton<IALPRService>();
            AddMockSingleton<ICamera>();
            AddMockSingleton<IWebClient>();
            AddMockSingleton<IStorageService>();
            AddMockSingleton<IKeyService>();

			AddMockSingleton<IOptionsSettings>();
            
            Mock<IUserServiceLocator> userServiceLocatorMock = AddMockSingleton<IUserServiceLocator>();
            userServiceLocatorMock.Setup(s => s.GetUserService(It.IsAny<Models.SectionPosition>())).Returns(_ioc.GetSingleton<IUserService>());
        }

        protected virtual void RegisterAdditionalSingletons()
        {
            SetupCacheAndTask();

            ViewDispatcherMock = new MockMvxViewDispatcher();
            var dispatcher = new MvxViewDispatcherWrapper(ViewDispatcherMock);

            Ioc.RegisterSingleton(dispatcher);
			Ioc.RegisterSingleton<IMvxMessenger>(Ioc.IoCConstruct<MockMvxMessenger>());
			Ioc.RegisterSingleton<IMvxMainThreadDispatcher>(dispatcher);
            Ioc.RegisterSingleton<IMvxStringToTypeParser>(new MvxStringToTypeParser());
            Ioc.RegisterType<ISection, Section>();

            InitService();
            // Register localization provider
           

            // View model service locator
			// View model service locator
			Ioc.RegisterSingleton<IViewModelServiceLocator>(Ioc.IoCConstruct<ViewModelServiceLocator>());

            SetupMock();
        }

        private static void InitialiseSingletonCache()
        {
            MvxSingletonCache.Initialize();
        }

        private class TestTrace : IMvxTrace
        {
            public void Trace(MvxTraceLevel level, string tag, Func<string> message)
            {
                Debug.WriteLine(tag + ":" + level + ":" + message());
            }

            public void Trace(MvxTraceLevel level, string tag, string message)
            {
                Debug.WriteLine(tag + ": " + level + ": " + message);
            }

            public void Trace(MvxTraceLevel level, string tag, string message, params object[] args)
            {
                Debug.WriteLine(tag + ": " + level + ": " + message, args);
            }
        }

        private void SetupCacheAndTask()
        {
           
        }

        protected virtual void SetupMock()
        {

        }
    }

    [Ignore]
    public class MvvmCrossViewModelTestSetup<TViewModel> : MvvmCrossTestSetup
        where TViewModel : BaseViewModel
    {
        protected TViewModel _viewModel;

        [TestInitialize]
		public virtual void BaseSetup()
        {            
			CleanAndSetup();
            _viewModel = Ioc.IoCConstruct<TViewModel>();
        }

        [TestCleanup]
        public void BaseTearDown()
        {
        }

    }

    [Ignore]
    public class MvvmCrossServiceTestSetup<TService> : MvvmCrossTestSetup
        where TService : class
    {
        protected TService _service;

        [TestInitialize]
        public virtual void BaseSetup()
        {
            CleanAndSetup();
            _service = Ioc.IoCConstruct<TService>();
        }

        [TestCleanup]
        public void BaseTearDown()
        {
        }

    }
}

