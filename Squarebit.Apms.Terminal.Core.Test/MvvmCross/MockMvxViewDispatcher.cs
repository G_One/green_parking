﻿using System;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.Views;
using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;

namespace Squarebit.Apms.Terminal.Core.Test
{
    public class MockMvxViewDispatcher : IMvxViewDispatcher
    {
        public List<MvxPresentationHint> ChangePresentationRequests = new List<MvxPresentationHint>();
        public List<MvxViewModelRequest> ShowRequests = new List<MvxViewModelRequest>();

        public MockMvxViewDispatcher()
        {
        }

        #region IMvxViewDispatcher implementation

        public bool ShowViewModel(MvxViewModelRequest request)
        {
            ShowRequests.Add(request);
            return true;
        }

        public bool ChangePresentation(MvxPresentationHint hint)
        {
            ChangePresentationRequests.Add(hint);
            return true;
        }


        #endregion

        #region IMvxMainThreadDispatcher implementation

        public bool RequestMainThreadAction(Action action)
        {
            action();
            return true;
        }

        #endregion
    }

    public class MvxViewDispatcherWrapper : MvxMainThreadDispatcher, IMvxViewDispatcher
    {
        private readonly IMvxViewDispatcher _decorated;

        public MvxViewDispatcherWrapper(IMvxViewDispatcher decorated)
        {
            _decorated = decorated;
        }

        public bool ChangePresentation(MvxPresentationHint hint)
        {
            return _decorated.ChangePresentation(hint);
        }

        public bool RequestMainThreadAction(Action action)
        {
            return _decorated.RequestMainThreadAction(action);
        }

        public bool ShowViewModel(MvxViewModelRequest request)
        {
            return _decorated.ShowViewModel(request);
        }
    }
}