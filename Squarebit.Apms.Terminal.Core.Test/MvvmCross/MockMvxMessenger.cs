﻿using System;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace Squarebit.Apms.Terminal.Core.Test
{
	class MockMvxMessenger : IMvxMessenger
	{
		#region IMvxMessenger implementation
		public MvxSubscriptionToken Subscribe<TMessage>(Action<TMessage> deliveryAction, MvxReference reference = MvxReference.Weak, string tag = null) where TMessage : MvxMessage
		{
			return null;
		}
		public MvxSubscriptionToken SubscribeOnMainThread<TMessage>(Action<TMessage> deliveryAction, MvxReference reference = MvxReference.Weak, string tag = null) where TMessage : MvxMessage
		{
			return null;
		}
		public MvxSubscriptionToken SubscribeOnThreadPoolThread<TMessage>(Action<TMessage> deliveryAction, MvxReference reference = MvxReference.Weak, string tag = null) where TMessage : MvxMessage
		{
			return null;
		}
		public void Unsubscribe<TMessage>(MvxSubscriptionToken mvxSubscriptionId) where TMessage : MvxMessage
		{

		}
		public bool HasSubscriptionsFor<TMessage>() where TMessage : MvxMessage
		{
			return false;
		}
		public int CountSubscriptionsFor<TMessage>() where TMessage : MvxMessage
		{
			return 0;
		}
		public bool HasSubscriptionsForTag<TMessage>(string tag) where TMessage : MvxMessage
		{
			return false;
		}
		public int CountSubscriptionsForTag<TMessage>(string tag) where TMessage : MvxMessage
		{
			return 0;
		}
		public System.Collections.Generic.IList<string> GetSubscriptionTagsFor<TMessage>() where TMessage : MvxMessage
		{
			return null;
		}
		public void Publish<TMessage>(TMessage message) where TMessage : MvxMessage
		{
			return;
		}
		public void Publish(MvxMessage message)
		{
			return;
		}
		public void Publish(MvxMessage message, Type messageType)
		{
			return;
		}
		public void RequestPurge(Type messageType)
		{
			return;
		}
		public void RequestPurgeAll()
		{
			return;
		}
		#endregion
	}
}

