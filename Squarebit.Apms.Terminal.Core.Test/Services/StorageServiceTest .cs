﻿using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.MvvmCross.ViewModels;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;
using System.Net;

namespace Squarebit.Apms.Terminal.Core.Test.Services
{
    [TestClass]
    public class StorageServiceTest : MvvmCrossServiceTestSetup<StorageService>
    {
        private byte[] _data = new byte[] { 1, 2, 3, 4 };
        private byte[] _loadData = null;
        private Mock<IWebClient> _mockDownloadService;
        private bool _saved = false;
        
        [TestInitialize]
        public void Setup()
        {
            _mockDownloadService = GetMock<IWebClient>();
            _mockDownloadService.Setup(client => client.DownloadData(It.IsAny<string>(), It.IsAny<Action<byte[], Exception>>()))
                .Callback((string path, Action<byte[], Exception> complete) => complete(_data, null));
            _mockDownloadService.Setup(client => client.DownloadData(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Action<byte[], Exception>>()))
                .Callback((string host, string path, Action<byte[], Exception> complete) => complete(_data, null));
        }

        private void OnComplete(byte[] data, Exception ex)
        {
            _loadData = data;
        }

        private void OnSaved(Exception ex)
        {
            _saved = true;
        }

        [TestMethod]
        public void StorageService_should_build_path_according_to_input_parameters()
        {
            string pathCheckinFront = _service.BuildFilePath("abcd", true, true, new DateTime(2014, 7, 1, 14, 4, 10));
            string pathCheckinBack = _service.BuildFilePath("abcd", false, true, new DateTime(2014, 7, 1, 14, 28, 21));
            string pathCheckoutFront = _service.BuildFilePath("abcd", true, false, new DateTime(2014, 7, 1, 14, 43, 32));
            string pathCheckoutBack = _service.BuildFilePath("abcd", false, false, new DateTime(2014, 7, 1, 14, 55, 59));

            Assert.AreEqual("images/20140701/in/1400/0410_abcd_front.jpg", pathCheckinFront);
            Assert.AreEqual("images/20140701/in/1415/2821_abcd_back.jpg", pathCheckinBack);
            Assert.AreEqual("images/20140701/out/1430/4332_abcd_front.jpg", pathCheckoutFront);
            Assert.AreEqual("images/20140701/out/1445/5559_abcd_back.jpg", pathCheckoutBack);
        }

        [TestMethod]
        public void StorageService_should_generate_a_valid_path_and_save_file_success()
        {
            string expectPath = "images/20140701/in/1400/0410_abcd_front.jpg";
            string imagePath = _service.BuildFilePath("abcd", true, true, new DateTime(2014, 7, 1, 14, 4, 10));
            string savedPath = _service.SaveImage(imagePath, _data, OnSaved);
            while (!_saved) ;
            Assert.AreEqual(expectPath, savedPath);
            Assert.IsTrue(System.IO.File.Exists(expectPath));
            System.IO.File.Delete(expectPath);
        }

        [TestMethod]
        public void StorageService_should_load_from_disk_if_file_exists_on_local()
        {
            string expectPath = "images/20140701/in/1400/0410_abcd_front.jpg";
            string imagePath = _service.BuildFilePath("abcd", true, true, new DateTime(2014, 7, 1, 14, 4, 10));
            string savedPath = _service.SaveImage(imagePath, _data, OnSaved);
            while (!_saved) ;
            _service.LoadImage(expectPath, "192.168.1.216", OnComplete);
            while (_loadData == null) ;
            CollectionAssert.AreEqual(_data, _loadData);
            _mockDownloadService.Verify(client => client.DownloadData(It.IsAny<string>(), It.IsAny<Action<byte[], Exception>>()), Times.Never);
            System.IO.File.Delete(expectPath);
        }

        [TestMethod]
        public void StorageService_should_load_from_host_if_file_not_exists_on_local()
        {
            string expectPath = "images/20140701/in/1400/0410_abcd_front.jpg";
            _service.LoadImage(expectPath, "192.168.1.216", OnComplete);
            while (_loadData == null) ;
            CollectionAssert.AreEqual(_data, _loadData);
            _mockDownloadService.Verify(client => client.DownloadData(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Action<byte[], Exception>>()), Times.Once);
        }

        [TestMethod]
        public void StorageService_should_load_from_server_if_file_not_exists_on_local_and_no_terminal_lives()
        {
            string expectPath = "images/20140701/in/1400/0410_abcd_front.jpg";
            string host = null;
            _service.LoadImage(expectPath, host, OnComplete);
            while (_loadData == null) ;
            CollectionAssert.AreEqual(_data, _loadData);
            _mockDownloadService.Verify(client => client.DownloadData(It.IsAny<string>(), It.IsAny<Action<byte[], Exception>>()), Times.Once);
        }
    }
}
