﻿using Squarebit.Apms.Terminal.Core.Services;
using Cirrious.MvvmCross.ViewModels;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Squarebit.Apms.Terminal.Core.ViewModels;
using Moq;
using Squarebit.Apms.Terminal.Core.Models;

namespace Squarebit.Apms.Terminal.Core.Test.Services
{
    [TestClass]
    public class UserServiceTest : MvvmCrossServiceTestSetup<UserService>
    {
        private Exception _resultException = null;

        [TestInitialize]
        public void Setup()
        {
            var mockServer = GetMock<IServer>();
            mockServer.Setup(server => server.Login("ndhoang", "validpassword", 1, It.IsAny<Action<ApmsUser, Exception>>()))
                .Callback((string username, string password, Action<ApmsUser, Exception> complete) => complete(new ApmsUser(), null));
            mockServer.Setup(server => server.Login("valid_card_id", 1, It.IsAny<Action<ApmsUser, Exception>>()))
                .Callback((string cardId, Action<ApmsUser, Exception> complete) => complete(new ApmsUser(), null));
            mockServer.Setup(server => server.Login("somebody", "invalidpassword", 1, It.IsAny<Action<ApmsUser, Exception>>()))
                .Callback((string username, string password, Action<ApmsUser, Exception> complete) => complete(null, new LoginInvalidException()));
            mockServer.Setup(server => server.Login("invalid_card_id", 1, It.IsAny<Action<ApmsUser, Exception>>()))
                .Callback((string cardId, Action<ApmsUser, Exception> complete) => complete(null, new LoginInvalidException()));
            mockServer.Setup(server => server.Login("error", 1, It.IsAny<Action<ApmsUser, Exception>>()))
                .Callback((string cardId, Action<ApmsUser, Exception> complete) => complete(null, new ServerErrorException()));
            mockServer.Setup(server => server.Login("disconnect", 1, It.IsAny<Action<ApmsUser, Exception>>()))
                .Callback((string cardId, Action<ApmsUser, Exception> complete) => complete(null, new ServerDisconnectException()));
        }

        private void OnComplete(Exception ex)
        {
            _resultException = ex;
        }

        private void OnLogoutComplete(UserShift shift, Exception ex)
        {
            _resultException = ex;
        }

        [TestMethod]
        public void UserService_should_contain_current_user_and_callback_with_no_exception_if_username_password_is_valid()
        {
            _service.Login("ndhoang", "validpassword", 1, OnComplete);
            Assert.IsNotNull(_service.CurrentUser);
            Assert.IsTrue(_service.IsLogin);
            Assert.IsNull(_resultException);
        }

        [TestMethod]
        public void UserService_should_contain_current_user_and_callback_with_no_exception_if_card_id_is_valid()
        {
            _service.Login("valid_card_id", 1, OnComplete);
            Assert.IsNotNull(_service.CurrentUser);
            Assert.IsTrue(_service.IsLogin);
            Assert.IsNull(_resultException);
        }

        [TestMethod]
        public void UserService_should_callback_with_exception_if_username_password_is_invalid()
        {
            _service.Login("somebody", "invalidpassword", 1, OnComplete);
            Assert.IsNull(_service.CurrentUser);
            Assert.IsFalse(_service.IsLogin);
            Assert.IsInstanceOfType(_resultException, typeof(LoginInvalidException));
        }

        [TestMethod]
        public void UserService_should_callback_with_exception_if_card_id_is_invalid()
        {
            _service.Login("invalid_card_id", 1, OnComplete);
            Assert.IsNull(_service.CurrentUser);
            Assert.IsFalse(_service.IsLogin);
            Assert.IsInstanceOfType(_resultException, typeof(LoginInvalidException));
        }

        [TestMethod]
        public void UserService_should_remove_current_user_and_callback_with_no_exception_after_logout()
        {
            _service.Login("valid_card_id", 1, OnComplete);
            _service.Logout(1, OnLogoutComplete);
            Assert.IsNull(_service.CurrentUser);
            Assert.IsFalse(_service.IsLogin);
            Assert.IsNull(_resultException);
        }

        [TestMethod]
        public void UserService_should_callback_with_error_if_server_got_error_in_process()
        {
            _service.Login("error", 1, OnComplete);
            Assert.IsNull(_service.CurrentUser);
            Assert.IsFalse(_service.IsLogin);
            Assert.IsInstanceOfType(_resultException, typeof(ServerErrorException));
        }

        [TestMethod]
        public void UserService_should_callback_with_error_if_server_disconnect()
        {
            _service.Login("disconnect", 1, OnComplete);
            Assert.IsNull(_service.CurrentUser);
            Assert.IsFalse(_service.IsLogin);
            Assert.IsInstanceOfType(_resultException, typeof(ServerDisconnectException));
        }
    }
}
