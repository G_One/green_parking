﻿using System;
using System.Drawing;
using System.Windows.Controls;

/// <summary>
/// 
/// </summary>
namespace Squarebit.Devices.Dal
{
    public class ZoomFactor
    {
        public float Factor { get; set; }
        public float ZoomX { get; set; }
        public float ZoomY { get; set; }
        public bool ZoomEnabled { get; set; }

        public ZoomFactor()
        {
            Factor = 100;
        }
    }

	/// <summary>
	/// Describes an event relating to new image frames. 
	/// </summary>
	public class FrameEventArgs
	{
		/// <summary>
		/// Gets or set the image frame.
		/// </summary>
		public Bitmap Frame { get; set; }
	}

    public class ZoomEventArgs
    {
        public ZoomFactor ZoomFactor { get; set; }
    }

	/// <summary>
	/// 
	/// </summary>
	/// <param name="sender">The sender.</param>
	/// <param name="e">The <see cref="FrameEventArgs"/> instance containing the event data.</param>
	public delegate void FrameEventHandler(object sender, FrameEventArgs e);
    public delegate void ZoomEventHandler(object sender, ZoomEventArgs e);

	/// <summary>
	/// Describes a capture device.
	/// </summary>
	public interface ICamera
	{
        int DeviceId { get; set; }

        ZoomFactor ZoomFactor { get; set; }

		/// <summary>
		/// Occurs when a new frame is received.
		/// </summary>
		event FrameEventHandler OnFrameReceived;

        event ZoomEventHandler OnZoomReceived;

        void FrameReceived(FrameEventArgs arg);

		/// <summary>
		/// Starts capturing and sending image data.
		/// </summary>
		void Start();

		/// <summary>
		/// Pauses the device temporarily.
		/// </summary>
		void Pause();

        /// <summary>
        /// Continue capturing device
        /// </summary>
        void Continue();
        
		/// <summary>
		/// Stops the device and closes it. After calling this function, the capture devices
		/// cannot be started again.
		/// </summary>
		void Stop();

        void Dispose();

        /// <summary>
        /// Capture current frame image
        /// </summary>
        /// <returns></returns>
        byte[] CaptureImage(string waterMark = "");

        System.Drawing.Image CaptureImage();

        /// <summary>
        /// Setups the specified ip.
        /// </summary>
        /// <param name="ip">The ip.</param>
        void Setup(string ip);
        void Load(string ip, ZoomFactor zoomFactor);

        /// <summary>
        /// Saves the state of the zoom.
        /// </summary>
        void SaveZoomState();

        void ChangeIPAddress(string ip);

        /// <summary>
        /// Gets or sets the container.
        /// </summary>
        /// <value>
        /// The container.
        /// </value>
        UserControl Container { get; set; }
	}
}
