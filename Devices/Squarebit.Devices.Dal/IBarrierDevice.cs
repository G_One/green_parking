﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Squarebit.Devices.Dal
{
    public interface IBarrierDevice
    {
		string DeviceName { get; }
        string DevicePort { get; set; }

		bool Open();
    }

	public interface IBarrierDeviceManager
	{
		string[] GetAllDeviceNames();

		IBarrierDevice GetDevice(string deviceName, string devicePort);
	}
}
